"""humanresource URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.core.urlresolvers import reverse_lazy
# from apps.institution import views
# from apps.schedule import views
# from apps.person import views
# from apps.contract import views
# from apps.accounts import views
from django.contrib.auth import login, logout
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include('apps.security.urls')),
    url(r'^evaluation/', include('apps.evaluation.urls', namespace='evaluation')),
    url(r'^accounting_finance/', include('apps.accounting_finance.urls', namespace='accounting_finance')),
    url(r'^catalogo/', include('apps.settings.urls', namespace='catalogo')),
    url(r'^institution/', include('apps.institution.urls', namespace='institution')),
    url(r'^accounts/', include('apps.accounts.urls', namespace='accounts')),
    url(r'^roles/', include('apps.roles.urls', namespace='roles')),
    url(r'^partidas/', include('apps.partidas.urls', namespace='partidas')),
    url(r'^schedule/', include('apps.schedule.urls', namespace='schedule')),
    url(r'^person/', include('apps.person.urls', namespace='person')),
    url(r'^contract/', include('apps.contract.urls', namespace='contract')),
    url(r'^employee/', include('apps.employee.urls', namespace='employee')),
    url(r'^vacacion/', include('apps.vacacion.urls', namespace='vacacion')),
    url(r'^cambios/', include('apps.cambios.urls', namespace='cambios')),
    url(r'^curriculum/', include('apps.curriculum.urls', namespace='curriculum')),
    url(r'^personnelactions/', include('apps.personnel_actions.urls', namespace='personnelactions')),
    url(r'^economicdata/', include('apps.economicdata.urls', namespace='economicdata')),
    url(r'^permissions/', include('apps.permissions.urls', namespace='permissions')),
    url(r'^login/$', login, {'template_name': 'app/login.html'}, name='security_login'),
    url(r'^logout/$', logout, {'next_page': reverse_lazy('security_main')}, name='security_logout'),

]
# urlpatterns += staticfiles_urlpatterns()

# urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = 'Municipio de Loja'