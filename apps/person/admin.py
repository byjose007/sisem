# -*- coding: utf-8 -*-
from django.contrib import admin
from apps.person.models import Person

# Register your models here.

class PersonAdmin(admin.ModelAdmin):
	list_display = ('name','lastname','cedula')
	search_fields = ('name','lastname','cedula')


	


admin.site.register(Person, PersonAdmin)


