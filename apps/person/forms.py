from django import forms
from django.forms import ModelForm
from .models import Person
from apps.settings.models import Catalog, Item


# class PersonForm(ModelForm):
#	class Meta:
#		model = Person
#		fields = '__all__'
#		exclude = ('id',)

#		self.fields['institutional_unit'].queryset = InstitutionalUnit.objects.filter(institution=institution)


class PersonForm(ModelForm):
    def clean(self):
        cleaned_data = super(PersonForm, self).clean()

        name = cleaned_data.get("name")
        if name == None:
            self.add_error('name', 'Nombre debe contener al menos 3 caracteres!')
        elif len(name) < 3:
            self.add_error('name', 'Nombre debe contener al menos 3 caracteres!')

        cedula = cleaned_data.get("cedula")
        if cedula == None:
            self.add_error('cedula', 'Cédula debe contener 10 o 13 caracteres!')
        elif len(cedula) < 10 and len(cedula) <= 13:
            self.add_error('cedula', 'Cédula debe contener 10 o 13 caracteres!')

        lastname = cleaned_data.get("lastname")
        if lastname == None:
            self.add_error('lastname', 'Apellido debe contener al menos 3 caracteres!')
        elif len(lastname) < 3:
            self.add_error('lastname', 'Apellido debe contener al menos 3 caracteres!')

        return cleaned_data

    # def __init__(self, *args, **kwargs):

    #	catalog = Catalog.objects.get(pk=2)
    #	super (PersonForm, self ). __init__(*args, **kwargs)
    #	self.fields['sex'].queryset = forms.ModelChoiceField(queryset = Item.objects.filter(catalog=2))



    class Meta:
        model = Person
        fields = '__all__'
        exclude = ('id',)
        widgets = {
            'birthdate': forms.DateInput(attrs={'class': 'datepicker', 'placeholder':'día/mes/año: ( 29/03/2017 )' }),
        }

