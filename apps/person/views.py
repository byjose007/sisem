# -*- coding: utf-8 -*-
from django.db import transaction, IntegrityError, OperationalError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators  import  login_required
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from apps.person.models import Person

from apps.institution.models import Institucion, JefaturaCoordinacion, Directivo, Direccion
# from apps.institution.models import Institution, InstitutionalUnit, Department
from apps.curriculum.models import CurriculumVitae, FormalInstruction, OfficesOrsubactivities,Language,Training,WorkExperience,PerformanceEvaluation,PersonalAchievement,AffirmativeAction,PersonalReferences
from .forms import PersonForm
from apps.employee.models import Employee
from apps.curriculum.models import CurriculumVitae, AffirmativeAction
from django.db.models import Q
from django.contrib import messages
from apps.accounts.models import UserProfile
# from apps.institution.models import Institution
# Create your views here.
#-----------Persona-----------
@login_required()
def person_list(request, template_name='person/person_list.html'):
    person = Person.objects.all()
    data = {}
    query = request.GET.get('q', '')
    if query == '':
        person = Person.objects.all()
        paginator = Paginator(person, 10) # Show 25 contract per page
        page = request.GET.get('page')

        try:
            persons = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            persons = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            persons = paginator.page(paginator.num_pages)

    else:

        if query:
            qset = (
                Q(name__icontains=query) |
                Q(lastname__icontains=query) |
                Q(cedula__icontains=query)
            )
            person = Person.objects.filter(qset).distinct()
            paginator = Paginator(person, 10) # Show 25 contract per page
            page = request.GET.get('page')

            try:    
                persons = paginator.page(page)
            except PageNotAnInteger:
            # If page is not an integer, deliver first page.
                persons = paginator.page(1)
            except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
                persons = paginator.page(paginator.num_pages)
        else:
            persons = []

    data['object_list'] = persons
    data['query'] = query
    return render(request, template_name, data)


@login_required()
@transaction.atomic
def person_create(request, template_name='person/person_form.html'):
    user_profile_autent = UserProfile.objects.get(user=request.user) #Obtengo el perfil del cedula donde esta autenticado
    institution = user_profile_autent.institution_default   #Obtengo la institucion por defecto del cedula autenticado
    form = PersonForm(request.POST or None)
    sp1 = transaction.savepoint()
    try:
        if form.is_valid():
            form.save()
            #messages.add_message(request, messages.INFO, 'Persona Agregada Correctamente.')
            person = get_object_or_404(Person, cedula=request.POST.get('cedula'))
            employee, created = Employee.objects.get_or_create(person=person,institution=institution)        
            curriculum_vitae, created = CurriculumVitae.objects.get_or_create(person=person)
            curriculum = CurriculumVitae.objects.get(person=person)
            affirmativeaction = AffirmativeAction.objects.create(curriculum_vitae=curriculum, hero=False, former_fighter=False, ecuadorian_migrant=False)
            transaction.savepoint_commit(sp1)
            messages.success(request, 'Buen Trabajo. Datos ingresados correctamente.')
            return redirect('employee:employee_edit' ,pk=employee.pk)
            # return redirect('person:person_list')
        

    except Exception as e:
        messages.error(request, 'Tuvimos un problema. Los Datos no han sido almacenados.')
        print (e)
        #messages.add_message(request, messages.ERROR, 'Los Datos no han sido almacenados')
        transaction.savepoint_rollback(sp1)
        return redirect('person:person_list')
    finally:
        Final="Todas las operaciones ejecutadas con exito"
    
    return render(request, template_name, {'form':form})


@login_required()
def person_update(request, pk, template_name='person/person_form.html'):
    person = get_object_or_404(Person, pk=pk)
    form = PersonForm(request.POST or None, instance=person)
    if form.is_valid():
        form.save()
        messages.info(request, 'Buen Trabajo. Datos actualizados correctamente.')
        return redirect('person:person_list')
    return render(request, template_name, {'form':form})

@login_required()
def person_delete(request, pk):
    try:
        person = get_object_or_404(Person, pk=pk)    
        person.delete()
        messages.success(request, 'Bien. Los datos han sido eliminados correctamente.')
    
    except Exception as inst:
        
        messages.error(request, 'Error. Los datos no se han podido eliminar. Verificar que la persona no tenga información asociada como Currículo o Contratos')

    return redirect('person:person_list')
    




class PersonDetail(DetailView):
    model = Person

