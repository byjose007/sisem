from django.conf.urls import url

from .views import (
    person_create,
    person_list,
    person_update,
    person_delete,
    PersonDetail,
)


urlpatterns = [
    url(r'^$', person_list, name='person_list'),
    url(r'^detail/(?P<pk>\d+)$', PersonDetail.as_view(), name='person_detail'),
    url(r'^new$',person_create, name='person_new'),
    url(r'^edit/(?P<pk>\d+)$', person_update, name='person_edit'),
    url(r'^delete/(?P<pk>\d+)$', person_delete, name='person_delete'),
]

