# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from apps.settings.models import Catalog
from apps.settings.models import Item

from django.db import models


# Create your models here.
class Person(models.Model):
    # -------------Tipo de Documento---------------
    pasaporte = 'pasaporte'
    ci = 'cedula'
    document_type_list = (
        (ci, 'Cédula'),
        (pasaporte, 'Pasaporte'),
    )

    # catalog = Catalog.objects.get(pk=1)
    # items = Item.objects.filter(catalog='SEXO')
    # GENERO_CHOICES = tuple([(item.name,item.name) for item in items])



    document_type = models.CharField(verbose_name='Tipo de Documento', max_length=9, choices=document_type_list,
                                     default=ci, blank=True, null=True)
    cedula = models.CharField(verbose_name='# Documento de Identificación', unique=True, max_length=13)
    name = models.CharField(verbose_name='Nombres', max_length=100)
    lastname = models.CharField(verbose_name='Apellidos', max_length=100)
    photo = models.ImageField(upload_to='albums/images/', verbose_name='Fotografía', blank=True, null=True)
    sex = models.ForeignKey(Item, on_delete=models.PROTECT, limit_choices_to={'catalog__code': 'SEXO'}, max_length=10,
                            verbose_name='Sexo', blank=True, null=True)
    country = models.ForeignKey(Item, on_delete=models.PROTECT, limit_choices_to={'catalog__code': 'PAIS', },
                                max_length=25, verbose_name='Pais', blank=True, null=True, related_name='catalog_pais')
    province_of_residence = models.CharField(verbose_name='Provincia de residencia', max_length=50, blank=True,
                                             null=True)
    canton_of_residence = models.CharField(verbose_name='Cantón de residencia', max_length=50, blank=True, null=True)
    parroquia_of_residence = models.CharField(verbose_name='Parroquia de residencia', max_length=50, blank=True,
                                              null=True)
    address = models.TextField(verbose_name='Dirección', max_length=300, blank=True, null=True)
    reference_address = models.TextField(verbose_name='Referencia', max_length=300, blank=True, null=True)
    province_of_birth = models.CharField(verbose_name='Provincia de nacimiento', max_length=50, blank=True, null=True)
    canton_of_birth = models.CharField(verbose_name='Cantón de nacimiento', max_length=50, blank=True, null=True)
    parroquia_of_birth = models.CharField(verbose_name='Parroquia de nacimiento', max_length=50, blank=True, null=True)
    place_of_birth = models.CharField(verbose_name='Lugar de nacimiento', max_length=100, blank=True, null=True)
    birthdate = models.DateField(verbose_name='Fecha de nacimiento', blank=True, null=True)
    civil_status = models.ForeignKey(Item, on_delete=models.PROTECT, limit_choices_to={'catalog__code': 'ESTADO CIVIL'},
                                     max_length=25, verbose_name='Estado civil', blank=True, null=True,
                                     related_name='catalog_civil_status')
    avatar = models.BooleanField(default=False, blank=True)
    phone = models.CharField(verbose_name='Teléfono', max_length=50, blank=True, null=True)
    cell_phone = models.CharField(verbose_name='Celular', max_length=10, blank=True, null=True)
    email = models.EmailField(verbose_name='Correo electrónico', max_length=100, blank=True, null=True)
    blood_type = models.ForeignKey(Item, on_delete=models.PROTECT, limit_choices_to={'catalog__code': 'TIPO DE SANGRE'},
                                   verbose_name='Tipo de sangre', blank=True, null=True,
                                   related_name='catalog_blood_type')

    disability_has = models.BooleanField(verbose_name='Posee alguna discapacidad', default=False)
    disability = models.ForeignKey(Item, on_delete=models.PROTECT, limit_choices_to={'catalog__code': 'DISCAPACIDAD'},
                                   verbose_name='Discapacidad', blank=True, null=True,
                                   related_name='catalog_disability')
    percentage_of_disability = models.IntegerField(verbose_name='Porcentaje de discapacidad', blank=True, null=True)
    is_has_catastrophic_illness = models.BooleanField(verbose_name='Posee alguna enfermedad catastrófica',
                                                      default=False)
    catastrophic_illness = models.CharField(verbose_name='Enfermedad catastrófica', max_length=200, blank=True,
                                            null=True)
    substitute = models.BooleanField(verbose_name='Es sustituto', default=False)
    family_identity_card = models.CharField(verbose_name='Cédula del familiar', max_length=13, blank=True, null=True)
    family_name = models.CharField(verbose_name='Nombre del familiar', max_length=100, blank=True, null=True)
    family_type_of = models.ForeignKey(Item, on_delete=models.PROTECT,
                                       limit_choices_to={'catalog__code': 'DISCAPACIDAD'},
                                       verbose_name='Tipo de discapacidad del familiar', blank=True, null=True,
                                       related_name='catalog_family_type_of')
    porcentage_of_family_disability = models.IntegerField(verbose_name='Porcentaje de discapacidad del familiar',
                                                          blank=True, null=True)
    relationship = models.ForeignKey(Item, on_delete=models.PROTECT, limit_choices_to={'catalog__code': 'PARENTESCO'},
                                     verbose_name='Parentesco', blank=True, null=True,
                                     related_name='catalog_relationship')

    class Meta:
        # ordering = ['lastname', 'name', 'cedula']
        verbose_name = 'Persona'
        verbose_name_plural = 'Personas'

    def __str__(self):
        return u'{0}-{1}-{2}'.format(self.name,self.lastname, self.cedula)

    def __unicode__(self):
        return u'{0}-{1}-{2}'.format(self.name,self.lastname,  self.cedula)
