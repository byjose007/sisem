from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType

from apps.roles.forms import GrupoForm, PermisoForm


@login_required()
def roles_list2(request, template_name='roles/rol_list.html'):
    roles = Group.objects.all()
    context = {
        "roles": roles,
        "accion": 'listar',
    }

    return render(request, template_name, context)



@login_required()
def crear_rol(request, template_name='roles/rol_form.html'):

    permisosAll = Permission.objects.all()
    list_permisos = []
    setApps = []
    permisosForm = []
    titulo = 'Nuevo Rol'

    form = GrupoForm(request.POST or None)



    # for app in permisosAll:
    #     setApps.append(app.content_type.app_label)
    #
    # setApps = list(set(setApps))
    # for app in setApps:
    #     permisos = Permission.objects.filter(content_type__app_label=app)
    #     list_permisos.append({'app': app, 'permisos': permisos})
    #
    # if request.method == 'POST':
    #     grupo = request.POST.get('name')
    #     new_group, created = Group.objects.get_or_create(name=grupo)
    #     if(new_group):
    #
    #         for lista in list_permisos:
    #             print(lista)
    #             for permiso in lista['permisos']:
    #                 print(permiso.id)
    #                 # appName = request.POST.get(lista['app']) acceder al titulo
    #                 if request.POST.get(str(permiso.id)):
    #                     ct = ContentType.objects.get(pk=permiso.content_type.id)
    #                     permisosForm.append(permiso.id)
    #                     new_group.permissions.add(Permission.objects.get
    #                                               (codename=permiso.codename,
    #                                                name=permiso.name,
    #                                                content_type=ct))
    #         messages.success(request, 'Buen Trabajo. Datos ingresados correctamente.')
    #         return redirect('roles:rol_list')
    #     else:
    #         messages.success(request, 'Los campos son obligatorios.')
    if request.method == 'POST':

        if form.is_valid():

            form.save()
            # url = reverse_lazy('security_main')
            return redirect('roles:rol_list')
    else:
        form = GrupoForm()

    # return render(request, template_name, context)

    return render(request, template_name, {'form': form, 'titulo':titulo})

@login_required()
def rolDetalle(request, pk, template_name='roles/rol_detail.html'):
    # roles = Group.objects.all()
    # grupo = get_object_or_404(Group, pk=pk)
    grupo = Group.objects.filter(pk=pk)
    permisos = Permission.permissions.all()

    # per = Permission.objects.filter(content_type=)


    context = {
        # "permisos": grupo.permissions,
        "grupo": grupo
    }
    print(grupo[0].name)


    per = Group.permissions_set
    for per in grupo:
        print(per.permissions.content_type)
        # print(grupo[per].permissions.content_type)


    return render(request, template_name, context)


@login_required()
def crear_permiso(request, template_name='roles/permiso_form.html'):
    form = PermisoForm(request.POST or None)
    titulo = 'Nuevo Permiso'

    if form.is_valid():
        form.save()
        messages.success(request, 'Buen Trabajo. Datos ingresados correctamente.')
        return redirect('roles:rol_list')
    return render(request, template_name, {'form': form, 'titulo':titulo})


@login_required()
def roles_list(request):
    redirect('http://192.168.1.26/admin/auth/group/')
