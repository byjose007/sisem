from django.conf.urls import url

from .views import *


urlpatterns = [
    url(r'^$', roles_list, name='rol_list'),
    url(r'^detail/(?P<pk>\d+)$', rolDetalle, name='rol_detail'),
    url(r'^nuevo$',crear_rol, name='nuevo_rol'),
    # url(r'^edit/(?P<pk>\d+)$', rol_update, name='rol_edit'),
    # url(r'^delete/(?P<pk>\d+)$', rol_delete, name='rol_delete'),
]

