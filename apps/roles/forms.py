import os

from django import forms
from django.contrib import admin
from django.contrib.admin.widgets import FilteredSelectMultiple
from django.contrib.auth.models import User, Group, Permission
from django.forms import ModelForm, CheckboxSelectMultiple, ModelMultipleChoiceField

from humanresource import settings


class GrupoForm(ModelForm):

    # def __init__(self, *args, **kwargs):
    #     super(GrupoForm, self).__init__(*args, **kwargs)
    #
    #     self.fields["permissions"].widget = CheckboxSelectMultiple()
    #     self.fields["permissions"].queryset = Permission.objects.all()
    # permissions = forms.ModelMultipleChoiceField(queryset=Permission.objects.all(),
    #                                           widget=FilteredSelectMultiple("Permissions", is_stacked=False),
    #                                           required=False)

    class Meta:
        model = Group
        fields = '__all__'
        # css = {'all': ('/static/admin/css/widgets.css',), }
        # js = ('/admin/jsi18n',)




class PersonAdmin(admin.ModelAdmin):
    # exclude = ['age']
    form = GrupoForm




class PermisoForm(ModelForm):
    class Meta:
        model = Permission
        fields = '__all__'