from django import forms
from django.forms import ModelForm
from .models import Catalog, Item


class CatalogForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super(CatalogForm, self).__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super(CatalogForm, self).clean()
        catalogoNuevo = cleaned_data.get("name")
        catalogoExistente = Catalog.objects.filter(name=catalogoNuevo).values('name')
        if catalogoExistente:
            self.add_error('name',
                           'El nombre de catalogo ya existe. Por favor ingrese otro nombre de catalogo')
        return cleaned_data

    class Meta:
        model = Catalog
        fields = '__all__'
        exclude = []


class ItemForm(ModelForm):
    class Meta:
        model = Item
        #fields = ['code', 'name', 'description', 'parent_item']
        fields = '__all__'
        exclude = ['catalog']
