from django.conf.urls import url

from apps.settings import views

urlpatterns = [

    url(r'^$', views.catalog_index, name='catalog_index'),
    url(r'^catalog/create/$', views.catalog_create, name='catalog_create'),
    url(r'^catalog/edit/(?P<pk>\d+)/$', views.catalog_edit, name='catalog_edit'),
    url(r'^catalog/detail/(?P<pk>\d+)/$', views.CatalogDetail.as_view(), name='catalog_detail'),
    url(r'^item/all/$', views.item_all, name='item_all'),
    url(r'^(?P<catalog_pk>\d+)/item/$', views.item_index, name='item_index'),
    url(r'^(?P<catalog_pk>\d+)/item/create/$', views.item_create, name='item_create'),
    url(r'^(?P<catalog_pk>\d+)/item/edit/(?P<item_pk>\d+)/$', views.item_edit, name='item_edit'),
    # url(r'^(?P<catalog_pk>\d+)/item/delete/(?P<item_pk>\d+)$', views.item_delete, name='item_delete'),
    url(r'^(?P<catalog_pk>\d+)/item/detail/(?P<pk>\d+)$', views.ItemDetail.as_view(), name='item_detail'),
    url(r'^borrar/(?P<pk>\d+)$', views.catalog_delete, name='catalog_delete'),
    url(r'^(?P<catalog_pk>\d+)/item/delete/(?P<item_pk>\d+)$', views.item_delete, name='item_delete'),
]
