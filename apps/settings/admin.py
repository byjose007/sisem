from django.contrib import admin
from apps.settings.models import Catalog, Item

# Register your models here.

admin.site.register(Catalog)
admin.site.register(Item)
