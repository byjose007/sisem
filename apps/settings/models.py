from django.db import models

# Create your models here.

STATES_ACTIVE_INACTIVE_CHOICES = (
        ('ACTIVE', 'Activo'),
        ('INACTIVE', 'Inactivo'),
    )

class Catalog(models.Model):

    code = models.CharField(verbose_name='Código* :', max_length=90)
    name = models.CharField(verbose_name='Nombre* :', max_length=90)
    description = models.TextField(verbose_name='Descripción', blank=True, null=True)
    state = models.CharField(verbose_name='Estado', max_length=10,
        choices=STATES_ACTIVE_INACTIVE_CHOICES,
        default='ACTIVE') #Estado de la evaluación

    class Meta:
        verbose_name = "catalogo"
        verbose_name_plural = "catalogos"
        ordering = ['name']

    def __str__(self):
        return '%s' % (self.name)


class Item(models.Model):

    name = models.CharField(verbose_name='Nombre: *', max_length=100, blank=False)
    description = models.TextField(verbose_name='Descripción: *', blank=False, null=True)
    catalog = models.ForeignKey(Catalog, blank=False, verbose_name='Catálogo')


    class Meta:
        verbose_name = "detalle"
        verbose_name_plural = "detalles"
        ordering = ['name']

    def __str__(self):
        return '%s' % (self.name)
