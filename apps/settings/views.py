import json
from django.shortcuts import render, redirect, get_object_or_404, render_to_response
from django.contrib.auth.decorators import login_required
from django.template.loader import get_template
from django.template import Context, RequestContext
from django.http import HttpResponse, HttpResponseBadRequest, Http404, JsonResponse, QueryDict
from .models import Catalog, Item
from .forms import CatalogForm, ItemForm
from django.http import JsonResponse
from django.contrib import messages
from django.db.models.deletion import ProtectedError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic.detail import DetailView


# Create your views here.

def catalog_index(request):
    print('intro catalog_index')
    catalog_list = Catalog.objects.all().order_by('-id')
    print(len(list(catalog_list)))
    paginator = Paginator(catalog_list, 100)  # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        page_obj = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        page_obj = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        page_obj = paginator.page(paginator.num_pages)
    context = {'page_obj': page_obj}
    return render(request, 'catalog_list.html', context)


def bank_index(request):
    bank_list = Catalog.objects.filter(code='BANK')
    context = {'bank_list': bank_list}
    return render(request, 'bank_list.html', context)


"""def item_index(request, catalog_pk, catalog_code=None):
    print(type(catalog_code))
    print(catalog_code)
    catalog = get_object_or_404(Catalog, pk=catalog_pk)
    item_list = Item.objects.filter(catalog__pk=catalog_pk)
    context = {'catalog': catalog, 'item_list': item_list}
    return render(request, 'item_list.html', context)"""


def catalog_create(request, template_name='catalog.html'):
    form = CatalogForm(request.POST or None)
    titulo = 'Crear CAtalogo'
    if form.is_valid():
        form.save()
        messages.success(request, 'Datos ingresados correctamente')
        return redirect('settings:catalog_index')
    return render(request, template_name, {'form': form, 'titulo': titulo})


class CatalogDetail(DetailView):
    model = Catalog
    template_name = 'catalog_detail.html'


def catalog_edit(request, pk):
    print('intro catalog_edit')
    catalog = get_object_or_404(Catalog, pk=pk)
    if 'save' in request.POST:
        form = CatalogForm(instance=catalog, data=request.POST)
        if form.is_valid():
            print('FORM IS VALID')
            form.save()
            messages.success(request, 'El nuevo catálogo ha sido actualizado exitosamente')
            return redirect('catalogo:catalog_index')
        else:
            print('FORM IS INVALID')
            print(form)
            context = RequestContext(request, {'form': form, 'create': False})
            return render_to_response('catalog.html', context)

    if 'cancel' in request.POST:
        return redirect('catalogo:catalog_index')

    form = CatalogForm(instance=catalog)
    context = {'form': form, 'create': False}
    return render(request, 'catalog.html', context)


def item_index(request, catalog_pk):
    print('intro item_index')
    catalog = get_object_or_404(Catalog, pk=catalog_pk)
    catalog_code = catalog.code
    print(type(catalog_code))
    print(catalog_code)
    item_list = Item.objects.filter(catalog__pk=catalog_pk)
    # item_list = Item.objects.all()
    paginator = Paginator(item_list, 10)  # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        page_obj = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        page_obj = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        page_obj = paginator.page(paginator.num_pages)
    context = {'catalog': catalog, 'page_obj': page_obj}
    return render(request, 'item_list.html', context)


def catalog_delete(request, pk):
    catalog = get_object_or_404(Catalog, pk=pk)
    catalog.delete()
    return redirect('catalogo:catalog_index')
    # return render(request, template_name)


def item_all(request):
    print('intro item_all')
    catalog = get_object_or_404(Catalog, pk=2)
    item_list = Item.objects.all()
    paginator = Paginator(item_list, 10)  # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        page_obj = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        page_obj = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        page_obj = paginator.page(paginator.num_pages)
    context = {'catalog': catalog, 'page_obj': page_obj}
    return render(request, 'item_list.html', context)


def item_create(request, catalog_pk):
    if 'save' in request.POST:
        form = ItemForm(data=request.POST)
        if form.is_valid():
            item = form.save(commit=False)
            item.catalog = Catalog.objects.get(id=catalog_pk)
            item.save()
            messages.success(request, 'El ítem ha sido creado exitosamente')
            return redirect('catalogo:item_index', catalog_pk=catalog_pk)
        else:
            context = RequestContext(request, {'form': form, 'create': True})
            # return render_to_response('item.html', context)
            return render_to_response('item.html', context)


    form = ItemForm()
    # item_list = Item.objects.all()
    # context = {'form': form, 'create': True, 'item_list': item_list}
    # return render(request, 'cities.html', context)
    context = {'form': form, 'create': True}
    return render(request, 'item.html', context)


def item_edit(request, catalog_pk, item_pk):
    item = get_object_or_404(Item, pk=item_pk)
    if 'save' in request.POST:
        form = ItemForm(instance=item, data=request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'El ítem ha sido actualizado exitosamente')
            return redirect('catalogo:item_index', catalog_pk=catalog_pk)
        else:
            context = RequestContext(request, {'form': form, 'create': False})
            return render('item.html', context)

    if 'cancel' in request.POST:
        return redirect('catalogo:item_index', catalog_pk=catalog_pk)

    form = ItemForm(instance=item)
    context = {'form': form, 'create': False}
    return render(request, 'item.html', context)


def item_edit_aux(request, catalog_pk, item_pk):
    item = get_object_or_404(Item, pk=item_pk)
    if 'save' in request.POST:
        form = ItemForm(instance=item, data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('catalogo:item_index', catalog_pk=catalog_pk)
        else:
            context = RequestContext(request, {'form': form, 'create': False})
            return render('cities.html', context)

    if 'cancel' in request.POST:
        return redirect('catalogo:item_index', catalog_pk=catalog_pk)

    # form = ItemForm(instance=item)
    item_list = Item.objects.all()
    context = {'item': item, 'item_list': item_list, 'create': False}
    return render(request, 'cities.html', context)


def item_create_aux(request, catalog_pk):
    if 'save' in request.POST:
        form = ItemForm(data=request.POST)
        if form.is_valid():
            item = form.save(commit=False)
            item.catalog = Catalog.objects.get(id=catalog_pk)
            item.save()
            return redirect('catalogo:item_index', catalog_pk=catalog_pk)
        else:
            context = RequestContext(request, {'form': form, 'create': True})
            return render_to_response('item.html', context)

    if 'cancel' in request.POST:
        return redirect('catalogo:item_index', catalog_pk=catalog_pk)

    form = ItemForm()
    context = {'form': form, 'create': True}
    return render(request, 'item.html', context)


def item_delete(request, catalog_pk, item_pk):
    item = Item.objects.get(pk=item_pk)
    try:
        item.delete()
        messages.success(request, "El ítem seleccionado ha sido eliminado correctamente")
    except ProtectedError:
        messages.error(request,
                       "El ítem seleccionado está siendo utilizado en otras funcionalidades, verifique su estado para continuar")

    return redirect('catalogo:item_index', catalog_pk=catalog_pk)


class ItemDetail(DetailView):
    model = Item
    template_name = 'item_detail.html'

    def get_context_data(self, **kwargs):
        print('>>> get_context_data')
        context = super(ItemDetail, self).get_context_data(**kwargs)
        catalog_pk = self.kwargs['catalog_pk']
        catalog = get_object_or_404(Catalog, pk=catalog_pk)
        context['catalog'] = catalog
        return context
