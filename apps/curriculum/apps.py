from django.apps import AppConfig


class CurriculumConfig(AppConfig):
    name = 'apps.curriculum'
