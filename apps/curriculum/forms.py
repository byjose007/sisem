# -*- coding: utf-8 -*-
from django import forms
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from .models import CurriculumVitae
from .models import FormalInstruction
from .models import OfficesOrsubactivities
from .models import Language
from .models import Training
from .models import WorkExperience
from .models import PerformanceEvaluation
from .models import PersonalAchievement
from .models import AffirmativeAction
from .models import PersonalReferences


class CurriculumVitaeForm(ModelForm):
    class Meta:
        model = CurriculumVitae
        fields = '__all__'
        exclude = ('id',)


class FormalInstructionForm(ModelForm):

    def clean(self):

        cleaned_data = super(FormalInstructionForm, self).clean()
        level_of_instruction = cleaned_data.get("level_of_instruction")
        obtained_title = cleaned_data.get("obtained_title")
        educational_institution = cleaned_data.get("educational_institution")
        area_studies = cleaned_data.get("area_studies")
        studying_time = cleaned_data.get("studying_time")
        date_of_graduation = cleaned_data.get("date_of_graduation")

        if level_of_instruction == None:
            self.add_error('level_of_instruction', 'Campo Obligatorio!')

        if obtained_title == None:
            self.add_error('obtained_title', 'Campo Obligatorio!')
        elif len(obtained_title) < 3:
            self.add_error('obtained_title', 'El campo debe tener mínimo 3 caracteres')

        if educational_institution == None:
            self.add_error('educational_institution', 'Campo Obligatorio!')
        elif len(educational_institution) < 3:
            self.add_error('educational_institution', 'El campo debe tener mínimo 3 caracteres')

        if area_studies == None:
            self.add_error('area_studies', 'Campo Obligatorio!')

        if studying_time == None:
            self.add_error('studying_time', 'Campo Obligatorio!')

        if date_of_graduation == None:
            self.add_error('date_of_graduation', 'Campo Obligatorio!')

        return cleaned_data

    class Meta:
        model = FormalInstruction
        fields = '__all__'
        exclude = ('id', 'curriculum_vitae',)


class OfficesOrsubactivitiesForm(ModelForm):

    def clean(self):

        cleaned_data = super(OfficesOrsubactivitiesForm, self).clean()
        job = cleaned_data.get("job")
        description = cleaned_data.get("description")

        if job == None:
            self.add_error('job', 'Campo Obligatorio!')

        if description == None:
            self.add_error('description', 'Campo Obligatorio!')
        elif len(description) < 5:
            self.add_error('description', 'El campo debe tener mínimo 5 caracteres')

        return cleaned_data

    class Meta:
        model = OfficesOrsubactivities
        fields = '__all__'
        exclude = ('id', 'curriculum_vitae',)


class LanguageForm(ModelForm):

    def clean(self):

        cleaned_data = super(LanguageForm, self).clean()
        language = cleaned_data.get("language")
        spoken_level = cleaned_data.get("spoken_level")
        written = cleaned_data.get("written")

        if language == None:
            self.add_error('language', 'Campo Obligatorio!')

        if spoken_level == None:
            self.add_error('spoken_level', 'Campo Obligatorio!')

        if written == None:
            self.add_error('written', 'Campo Obligatorio!')

        return cleaned_data

    class Meta:
        model = Language
        fields = '__all__'
        exclude = ('id', 'curriculum_vitae',)


class TrainingForm(ModelForm):
    def clean(self):
        cleaned_data = super(TrainingForm, self).clean()
        type_certificate = cleaned_data.get("type_certificate")
        date_from = cleaned_data.get("date_from")
        event_type = cleaned_data.get("event_type")
        area_studies = cleaned_data.get("area_studies")
        date_up = cleaned_data.get("date_up")
        total_hours = cleaned_data.get("total_hours")

        if type_certificate == None:
            self.add_error('type_certificate', 'Campo Obligatorio!')

        if date_from == None:
            self.add_error('date_from', 'Campo Obligatorio!')

        if event_type == None:
            self.add_error('event_type', 'Campo Obligatorio!')

        if area_studies == None:
            self.add_error('area_studies', 'Campo Obligatorio!')

        if date_up == None:
            self.add_error('date_up', 'Campo Obligatorio!')

        if total_hours == None:
            self.add_error('total_hours', 'Campo Obligatorio!')

        return cleaned_data

    class Meta:
        model = Training
        fields = '__all__'
        exclude = ('id', 'curriculum_vitae',)


class WorkExperienceForm(ModelForm):

    def clean(self):
        cleaned_data = super(WorkExperienceForm, self).clean()
        work_area = cleaned_data.get("work_area")
        date_since = cleaned_data.get("date_since")

        if work_area == None:
            self.add_error('work_area', 'Campo Obligatorio!')

        if date_since == None:
            self.add_error('date_since', 'Campo Obligatorio!')

        return cleaned_data

    class Meta:
        model = WorkExperience
        fields = '__all__'
        exclude = ('id', 'curriculum_vitae',)


class PerformanceEvaluationForm(ModelForm):
    class Meta:
        model = PerformanceEvaluation
        fields = '__all__'
        exclude = ('id', 'curriculum_vitae',)


class PersonalAchievementForm(ModelForm):
    def clean(self):
        cleaned_data = super(PersonalAchievementForm, self).clean()
        kind_of_achievement = cleaned_data.get("kind_of_achievement")

        if kind_of_achievement == None:
            self.add_error('kind_of_achievement', 'Campo Obligatorio!')

        return cleaned_data

    class Meta:
        model = PersonalAchievement
        fields = '__all__'
        exclude = ('id', 'curriculum_vitae',)


class AffirmativeActionForm(ModelForm):
    class Meta:
        model = AffirmativeAction
        fields = '__all__'
        exclude = ('id', 'curriculum_vitae',)


class PersonalReferencesForm(ModelForm):
    class Meta:
        model = PersonalReferences
        fields = '__all__'
        exclude = ('id', 'curriculum_vitae',)
