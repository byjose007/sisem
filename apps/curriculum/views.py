from django.db import transaction, IntegrityError, OperationalError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators  import  login_required
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse_lazy, reverse
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
	CreateView,
	UpdateView,
	DeleteView
)
from .models import CurriculumVitae, FormalInstruction, OfficesOrsubactivities, Language, Training, WorkExperience, PerformanceEvaluation, PersonalAchievement, AffirmativeAction, PersonalReferences
from .forms import CurriculumVitaeForm, FormalInstructionForm, OfficesOrsubactivitiesForm, LanguageForm, TrainingForm, WorkExperienceForm, PerformanceEvaluationForm, PersonalAchievementForm, AffirmativeActionForm, PersonalReferencesForm
from apps.person.models import Person
from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4, cm
from django.http import HttpResponse
from django.http.response import HttpResponseRedirect
from reportlab.lib.colors import pink, black, red, blue, green
from django.contrib import messages

# Create your views here.
mensaje_actualizar = 'Buen Trabajo. Datos actualizados correctamente.'
mensaje_eliminar = 'Buen Trabajo. Datos eliminados correctamente.'
mensaje_crear = 'Buen Trabajo. Datos ingresados correctamente.'
mensaje_error = 'Error. No se ha ejecutado la acción'
mensaje_sucess = 'Buen Trabajo. Se han ejecutado todas las acciones correctamente.'
  #-------CurriculumVitae----------
@login_required()
def curriculum_vitae_list(request, template_name='curriculum/curriculum_vitae_list.html'):
    #curriculum_vitae = CurriculumVitae.objects.all()
    data = {}
    query = request.GET.get('q', '')
    if query == '':
        curriculum_vitae = CurriculumVitae.objects.all()
        paginator = Paginator(curriculum_vitae, 10) # Show 25 contract per page
        page = request.GET.get('page')
        try:
            curriculum_vitae = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            curriculum_vitae = paginator.page(1)
        except EmptyPage:
            # If page is out of rang
            curriculum_vitae = paginator.page(paginator.num_pages)
    else:

        if query:
            qset = (
                Q(name__icontains=query) |
                Q(lastname__icontains=query) |
                Q(cedula__icontains=query)
            )
            curriculum_vitae = CurriculumVitae.objects.filter(qset).distinct()
            paginator = Paginator(curriculum_vitae, 10) # Show 25 contract per page
            page = request.GET.get('page')

            try:    
                curriculum_vitae = paginator.page(page)
            except PageNotAnInteger:
            # If page is not an integer, deliver first page.
                curriculum_vitae = paginator.page(1)
            except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
                curriculum_vitae = paginator.page(paginator.num_pages)
        else:
            curriculum_vitae = []

    data['object_list'] = curriculum_vitae
    data['query'] = query
    
    return render(request, template_name, data)
    
    
@login_required()
def curriculum_vitae_create(request, template_name='curriculum/form.html'):
    form = CurriculumVitaeForm(request.POST or None)
    titulo = "Currículo Vitae"
    funcion = "Crear"
    if form.is_valid():
        form.save()
        return redirect('curriculum:curriculum_vitae_list')
    return render(request, template_name, {'form':form, 'titulo':titulo, 'funcion':funcion})

@login_required()
def curriculum_vitae_update(request, pk, template_name='curriculum/form.html'):
    curriculum_vitae = get_object_or_404(CurriculumVitae, pk=pk)
    form = CurriculumVitaeForm(request.POST or None, instance=curriculum_vitae)
    if form.is_valid():
        form.save()
        return redirect('curriculum:curriculum_vitae_list')
    return render(request, template_name, {'form':form, 'titulo':titulo})

@login_required()
def curriculum_vitae_delete(request, pk, template_name='curriculum/confirm_delete.html'):
    curriculum_vitae = get_object_or_404(CurriculumVitae, pk=pk)    
    if request.method=='POST':
        curriculum_vitae.delete()
        return redirect('curriculum:curriculum_vitae_list')
    return render(request, template_name, {'object':curriculum_vitae})

@login_required()
def curriculum_vitae_detail(request,pk,template_name='curriculum/curriculum_detail_admin.html'):
    #curriculo_vitae = CurriculumVitae.objects.get(id=pk)
    #offices_or_subactivities = OfficesOrsubactivities.objects.filter(curriculum_vitae=pk)
    #persona = Person.objects.get(id=curriculum.)
    curriculum_vitae = get_object_or_404(CurriculumVitae, pk=pk)    
    formal_instruction = FormalInstruction.objects.filter(curriculum_vitae=pk)
    offices_or_subactivities = OfficesOrsubactivities.objects.filter(curriculum_vitae=pk)
    language = Language.objects.filter(curriculum_vitae=pk)
    training = Training.objects.filter(curriculum_vitae=pk)
    work_experience = WorkExperience.objects.filter(curriculum_vitae=pk)
    performance_evaluation = PerformanceEvaluation.objects.filter(curriculum_vitae=pk)
    personal_achievement = PersonalAchievement.objects.filter(curriculum_vitae=pk)
    affirmative_action = AffirmativeAction.objects.filter(curriculum_vitae=pk)
    personal_references = PersonalReferences.objects.filter(curriculum_vitae=pk)
    
    titulo = "Curriculo Vitae"
    funcion = "Detalle"
    
    data = {
        "titulo":titulo,
        "curriculum_vitae":curriculum_vitae,
        "formal_instruction":formal_instruction,
        "offices_or_subactivities":offices_or_subactivities,
        "language":language,
        "training":training,
        "work_experience":work_experience,
        "performance_evaluation":performance_evaluation,
        "personal_achievement":personal_achievement,
        "affirmative_action":affirmative_action,
        "personal_references":personal_references,
    }
    return render(request, template_name,data)

@login_required()
def curriculum_vitae_detail_user(request,pk,template_name='curriculum/curriculum_detail.html'):
    #curriculo_vitae = CurriculumVitae.objects.get(id=pk)
    #offices_or_subactivities = OfficesOrsubactivities.objects.filter(curriculum_vitae=pk)
    #persona = Person.objects.get(id=curriculum.)
    # curriculum_vitae = get_object_or_404(CurriculumVitae, pk=pk)
    #TODO   obtener id persona de la tabla empleado (pk es de empleado)
    curriculum_vitae = get_object_or_404(CurriculumVitae, person_id=pk)
    formal_instruction = FormalInstruction.objects.filter(curriculum_vitae=pk)
    offices_or_subactivities = OfficesOrsubactivities.objects.filter(curriculum_vitae=pk)
    language = Language.objects.filter(curriculum_vitae=pk)
    training = Training.objects.filter(curriculum_vitae=pk)
    work_experience = WorkExperience.objects.filter(curriculum_vitae=pk)
    performance_evaluation = PerformanceEvaluation.objects.filter(curriculum_vitae=pk)
    personal_achievement = PersonalAchievement.objects.filter(curriculum_vitae=pk)
    affirmative_action = AffirmativeAction.objects.filter(curriculum_vitae=pk)
    personal_references = PersonalReferences.objects.filter(curriculum_vitae=pk)
    
    titulo = "Curriculo Vitae"
    funcion = "Detalle"
    
    data = {
        "titulo":titulo,
        "curriculum_vitae":curriculum_vitae,
        "formal_instruction":formal_instruction,
        "offices_or_subactivities":offices_or_subactivities,
        "language":language,
        "training":training,
        "work_experience":work_experience,
        "performance_evaluation":performance_evaluation,
        "personal_achievement":personal_achievement,
        "affirmative_action":affirmative_action,
        "personal_references":personal_references,
    }
    return render(request, template_name,data)

class CurriculumVitaeDetail(DetailView):
	model = CurriculumVitae



  #-------FormalInstruction----------
def formal_instruction_list(request, template_name='curriculum/formal_instruction_list.html'):
    formal_instruction = FormalInstruction.objects.all()
    data = {}
    data['object_list'] = formal_instruction
    return render(request, 'curriculum/formal_instruction_list.html', data)

def formal_instruction_list_employee(request, pk, template_name='curriculum/formal_instruction_list.html'):
    formal_instruction = FormalInstruction.objects.filter(curriculum_vitae=pk)
    curriculum = CurriculumVitae.objects.get(id=pk)
    titulo = "Instrucción Formal"
    data = {
        "curriculum": curriculum,
        "titulo": titulo,
        

    }
    data['object_list'] = formal_instruction
    return render(request, 'curriculum/formal_instruction_list.html', data)


def formal_instruction_create(request, pk,template_name='curriculum/form.html'):
    try:
        curriculum = get_object_or_404(CurriculumVitae, pk=pk)
        form = FormalInstructionForm(request.POST or None)
        titulo = "Instrucción Formal"
        funcion = 1 #Crear
        funcion_return = 1
        if form.is_valid():
            formal_instruction = form.save(commit=False)
            formal_instruction.curriculum_vitae = curriculum
            formal_instruction = form.save()
            messages.success(request, mensaje_crear)           
            return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess

    return render(request, template_name, {'form':form, 'titulo':titulo,'curriculum':curriculum,'funcion':funcion,'funcion_return':funcion_return})
    
    
def formal_instruction_update(request, pk, template_name='curriculum/form.html'):
    try:    
        formal_instruction = get_object_or_404(FormalInstruction, pk=pk)
        curriculum = get_object_or_404(CurriculumVitae, pk=formal_instruction.curriculum_vitae.id)
        form = FormalInstructionForm(request.POST or None, instance=formal_instruction)
        titulo = "Instrucción Formal"
        funcion = 2 #Editar
        funcion_return = 1
        if form.is_valid():
            form.save()
            messages.success(request, mensaje_actualizar)           
            return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': formal_instruction.curriculum_vitae.id }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess
    return render(request, template_name, {'form':form, 'titulo':titulo,'curriculum':curriculum,'funcion':funcion,'funcion_return':funcion_return})

def formal_instruction_delete(request, pk, template_name='curriculum/confirm_delete.html'):
    try:
        formal_instruction = get_object_or_404(FormalInstruction, pk=pk) 
        funcion = 3 #Eliminar
        funcion_return = 1
        formal_instruction.delete()
        messages.success(request, mensaje_eliminar)           
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': formal_instruction.curriculum_vitae.id }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess

    return render(request, template_name, {'object':formal_instruction,'funcion_return':funcion_return})

class FormalInstructionDetail(DetailView):
	model = FormalInstruction


  #-------OfficesOrsubactivities----------
def offices_or_subactivities_list(request, template_name='curriculum/offices_or_subactivities_list.html'):
    offices_or_subactivities = OfficesOrsubactivities.objects.all()
    data = {}
    data['object_list'] = offices_or_subactivities
    return render(request, 'curriculum/offices_or_subactivities_list.html', data)

def offices_or_subactivities_list_employee(request,pk, template_name='curriculum/offices_or_subactivities_list.html'):
    offices_or_subactivities = OfficesOrsubactivities.objects.filter(curriculum_vitae=pk)
    curriculum = CurriculumVitae.objects.get(id=pk)
    titulo = "Oficios o Subactividades"
    data = {
        "curriculum": curriculum,
        "titulo": titulo,
    }
    data['object_list'] = offices_or_subactivities
    return render(request, 'curriculum/offices_or_subactivities_list.html', data)

def offices_or_subactivities_create(request, pk,template_name='curriculum/form.html'):
    try:
        curriculum = get_object_or_404(CurriculumVitae, pk=pk)
        form = OfficesOrsubactivitiesForm(request.POST or None)
        titulo = "Oficios o Subactividades"
        funcion = 1
        funcion_return = 2
       
        if form.is_valid():
            formal_instruction = form.save(commit=False)
            formal_instruction.curriculum_vitae = curriculum
            formal_instruction = form.save()
            messages.success(request, mensaje_crear)           
            return HttpResponseRedirect(reverse('curriculum:officesorsubactivities_list_employee', kwargs={ 'pk': pk }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return redirect('person:person_list')
    finally:
        Final=mensaje_sucess
    return render(request, template_name, {'form':form, 'titulo':titulo,'curriculum':curriculum,'funcion':funcion,'funcion_return':funcion_return})

def offices_or_subactivities_update(request, pk, template_name='curriculum/form.html'):
    try:
        offices_or_subactivities = get_object_or_404(OfficesOrsubactivities, pk=pk)
        curriculum = get_object_or_404(CurriculumVitae, pk=offices_or_subactivities.curriculum_vitae.id)
        form = OfficesOrsubactivitiesForm(request.POST or None, instance=offices_or_subactivities)
        titulo = "Oficios o Subactividades"
        funcion = 2
        funcion_return = 2
        if form.is_valid():
            form.save()
            messages.success(request, mensaje_actualizar)           
            return HttpResponseRedirect(reverse('curriculum:officesorsubactivities_list_employee', kwargs={ 'pk': offices_or_subactivities.curriculum_vitae.id }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return redirect('person:person_list')
    finally:
        Final=mensaje_sucess
    return render(request, template_name, {'form':form, 'titulo':titulo,'curriculum':curriculum,'funcion':funcion,'funcion_return':funcion_return})

def offices_or_subactivities_delete(request, pk, template_name='curriculum/confirm_delete.html'):
    try:    
        offices_or_subactivities = get_object_or_404(OfficesOrsubactivities, pk=pk)
        funcion = 3
        funcion_return = 2
        offices_or_subactivities.delete()
        messages.success(request, mensaje_eliminar)           
        return HttpResponseRedirect(reverse('curriculum:officesorsubactivities_list_employee', kwargs={ 'pk': offices_or_subactivities.curriculum_vitae.id }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess    

    return render(request, template_name, {'object':offices_or_subactivities,'funcion_return':funcion_return})

class OfficesOrsubactivitiesDetail(DetailView):
	model = OfficesOrsubactivities



  #-------Language----------
def language_list(request, template_name='curriculum/language_list.html'):
    language = Language.objects.all()
    data = {}
    data['object_list'] = language
    return render(request, 'curriculum/language_list.html', data)

def language_list_employee(request, pk, template_name='curriculum/language_list.html'):
    language = Language.objects.filter(curriculum_vitae=pk)
    curriculum = CurriculumVitae.objects.get(id=pk)
    titulo = "Idiomas"
    data = {
        "curriculum": curriculum,
        "titulo": titulo,
    }
    data['object_list'] = language
    return render(request, 'curriculum/language_list.html', data)

def language_create(request, pk,template_name='curriculum/form.html'):
    try:
        curriculum = get_object_or_404(CurriculumVitae, pk=pk)
        form = LanguageForm(request.POST or None)
        titulo = "Idiomas"
        funcion = 1
        funcion_return = 3
        if form.is_valid():
            formal_instruction = form.save(commit=False)
            formal_instruction.curriculum_vitae = curriculum
            formal_instruction = form.save()
            messages.success(request, mensaje_crear)           
            return HttpResponseRedirect(reverse('curriculum:language_list_employee', kwargs={ 'pk': pk }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess 
    return render(request, template_name, {'form':form, 'titulo':titulo,'curriculum':curriculum,'funcion':funcion,'funcion_return':funcion_return})

def language_update(request, pk, template_name='curriculum/form.html'):
    try:        
        language = get_object_or_404(Language, pk=pk)
        curriculum = get_object_or_404(CurriculumVitae, pk=language.curriculum_vitae.id)
        form = LanguageForm(request.POST or None, instance=language)
        titulo = "Idiomas"
        funcion = 2
        funcion_return = 3
        if form.is_valid():
            form.save()
            messages.success(request, mensaje_actualizar)           
            return HttpResponseRedirect(reverse('curriculum:language_list_employee', kwargs={ 'pk': curriculum.id }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess 
    return render(request, template_name, {'form':form, 'titulo':titulo,'curriculum':curriculum,'funcion':funcion,'funcion_return':funcion_return})

def language_delete(request, pk, template_name='curriculum/confirm_delete.html'):
    try:
        language = get_object_or_404(Language, pk=pk)
        funcion = 2
        funcion_return = 3
        language.delete()
        messages.success(request, mensaje_eliminar)
        return HttpResponseRedirect(reverse('curriculum:language_list_employee', kwargs={ 'pk': language.curriculum_vitae.id }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess 

    return render(request, template_name, {'object':language,'funcion':funcion,'funcion_return':funcion_return})

class LanguageDetail(DetailView):
	model = Language


  #-------Training----------
def training_list(request, template_name='curriculum/training_list.html'):
    training = Training.objects.all()
    data = {}
    data['object_list'] = training
    return render(request, 'curriculum/training_list.html', data)

def training_list_employee(request, pk, template_name='curriculum/training_list.html'):
    training = Training.objects.filter(curriculum_vitae=pk)
    curriculum = CurriculumVitae.objects.get(id=pk)
    titulo = "Capacitación"
    funcion = "Crear"
    data = {
        "curriculum": curriculum,
        "titulo": titulo,
    }
    data['object_list'] = training
    return render(request, 'curriculum/training_list.html', data)

def training_create(request, pk,template_name='curriculum/form.html'):
    curriculum = get_object_or_404(CurriculumVitae, pk=pk)
    form = TrainingForm(request.POST or None)
    titulo = "Capacitación"
    funcion = 1
    funcion_return = 4
    if form.is_valid():
        formal_instruction = form.save(commit=False)
        formal_instruction.curriculum_vitae = curriculum
        formal_instruction = form.save()
        return HttpResponseRedirect(reverse('curriculum:training_list_employee', kwargs={ 'pk': pk }))
    return render(request, template_name, {'form':form, 'titulo':titulo,'curriculum':curriculum,'funcion':funcion,'funcion_return':funcion_return})

def training_update(request, pk, template_name='curriculum/form.html'):
    training = get_object_or_404(Training, pk=pk)
    curriculum = get_object_or_404(CurriculumVitae, pk=training.curriculum_vitae.id)
    form = TrainingForm(request.POST or None, instance=training)
    titulo = "Capacitación"
    funcion = 2
    funcion_return = 4
    if form.is_valid():
        form.save()
        return HttpResponseRedirect(reverse('curriculum:training_list_employee', kwargs={ 'pk': curriculum.id }))
    return render(request, template_name, {'form':form, 'titulo':titulo,'curriculum':curriculum,'funcion':funcion,'funcion_return':funcion_return})

def training_delete(request, pk, template_name='curriculum/confirm_delete.html'):
    training = get_object_or_404(Training, pk=pk) 
    funcion = 3  
    funcion_return = 4
    if request.method=='POST':
        training.delete()
        return HttpResponseRedirect(reverse('curriculum:training_list_employee', kwargs={ 'pk': training.curriculum_vitae.id }))
    return render(request, template_name, {'object':training,'funcion':funcion,'funcion_return':funcion_return})

class TrainingDetail(DetailView):
	model = Training


  #-------WorkExperience----------
def work_experience_list(request, template_name='curriculum/work_experience_list.html'):
    work_experience = WorkExperience.objects.all()
    data = {}
    data['object_list'] = work_experience
    return render(request, 'curriculum/work_experience_list.html', data)

def work_experience_list_employee(request, pk, template_name='curriculum/work_experience_list.html'):
    work_experience = WorkExperience.objects.filter(curriculum_vitae=pk)
    curriculum = CurriculumVitae.objects.get(id=pk)
    titulo = "Experiencia Laboral"

    data = {
        "curriculum": curriculum,
        "titulo": titulo,
    }
    data['object_list'] = work_experience
    return render(request, 'curriculum/work_experience_list.html', data)

def work_experience_create(request, pk,template_name='curriculum/form.html'):
    try:
        curriculum = get_object_or_404(CurriculumVitae, pk=pk)
        form = WorkExperienceForm(request.POST or None)
        titulo = "Experiencia Laboral"
        funcion = 1
        funcion_return = 5
        if form.is_valid():
            formal_instruction = form.save(commit=False)
            formal_instruction.curriculum_vitae = curriculum
            formal_instruction = form.save()
            messages.success(request, mensaje_crear)           
            return HttpResponseRedirect(reverse('curriculum:workexperience_list_employee', kwargs={ 'pk': pk }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess 
    

    return render(request, template_name, {'form':form, 'titulo':titulo,'curriculum':curriculum,'funcion':funcion,'funcion_return':funcion_return})

def work_experience_update(request, pk, template_name='curriculum/form.html'):
    try:
        work_experience = get_object_or_404(WorkExperience, pk=pk)
        curriculum = get_object_or_404(CurriculumVitae, pk=work_experience.curriculum_vitae.id)
        form = WorkExperienceForm(request.POST or None, instance=work_experience)
        titulo = "Experiencia Laboral"
        funcion = 2
        funcion_return = 5
        if form.is_valid():
            form.save()
            messages.success(request, mensaje_actualizar)           
            return HttpResponseRedirect(reverse('curriculum:workexperience_list_employee', kwargs={ 'pk': curriculum.id }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess

    return render(request, template_name, {'form':form, 'titulo':titulo,'curriculum':curriculum,'funcion':funcion,'funcion_return':funcion_return})

def work_experience_delete(request, pk, template_name='curriculum/confirm_delete.html'):
    try:
        work_experience = get_object_or_404(WorkExperience, pk=pk)
        funcion = 3
        funcion_return = 5
        work_experience.delete()
        messages.success(request, mensaje_eliminar)           
        return HttpResponseRedirect(reverse('curriculum:workexperience_list_employee', kwargs={ 'pk': work_experience.curriculum_vitae.id }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess

    return render(request, template_name, {'object':work_experience})

class WorkExperienceDetail(DetailView):
	model = WorkExperience



  #-------PerformanceEvaluation----------
def performance_evaluation_list(request, template_name='curriculum/performance_evaluation_list.html'):
    performance_evaluation = PerformanceEvaluation.objects.all()
    data = {}
    data['object_list'] = performance_evaluation
    return render(request, 'curriculum/performance_evaluation_list.html', data)

def performance_evaluation_list_employee(request, pk, template_name='curriculum/performance_evaluation_list.html'):
    performance_evaluation = PerformanceEvaluation.objects.filter(curriculum_vitae=pk)
    curriculum = CurriculumVitae.objects.get(id=pk)
    titulo = "Evaluación de Desempeño"

    data = {
        "curriculum": curriculum,
        "titulo": titulo,
    }
    data['object_list'] = performance_evaluation
    return render(request, 'curriculum/performance_evaluation_list.html', data)


def performance_evaluation_create(request,pk, template_name='curriculum/form.html'):
    try:        
        curriculum = get_object_or_404(CurriculumVitae, pk=pk)
        form = PerformanceEvaluationForm(request.POST or None)
        titulo = "Evaluación de Desempeño"
        funcion = 1
        funcion_return = 6
        if form.is_valid():
            formal_instruction = form.save(commit=False)
            formal_instruction.curriculum_vitae = curriculum
            formal_instruction = form.save()
            messages.success(request, mensaje_crear)
            return HttpResponseRedirect(reverse('curriculum:performanceevaluation_list_employee', kwargs={ 'pk': pk }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess

    return render(request, template_name, {'form':form, 'titulo':titulo,'curriculum':curriculum,'funcion':funcion,'funcion_return':funcion_return})

def performance_evaluation_update(request, pk, template_name='curriculum/form.html'):
    try:    
        performance_evaluation = get_object_or_404(PerformanceEvaluation, pk=pk)
        curriculum = get_object_or_404(CurriculumVitae, pk=performance_evaluation.curriculum_vitae.id)
        form = PerformanceEvaluationForm(request.POST or None, instance=performance_evaluation)
        titulo = "Evaluación de Desempeño"
        funcion = 2
        funcion_return = 6
        if form.is_valid():
            form.save()
            messages.success(request, mensaje_actualizar)
            return HttpResponseRedirect(reverse('curriculum:performanceevaluation_list_employee', kwargs={ 'pk': curriculum.id }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess

    return render(request, template_name, {'form':form, 'titulo':titulo,'curriculum':curriculum,'funcion':funcion,'funcion_return':funcion_return})

def performance_evaluation_delete(request, pk, template_name='curriculum/confirm_delete.html'):
    try:    
        performance_evaluation = get_object_or_404(PerformanceEvaluation, pk=pk)
        funcion = 3
        funcion_return = 6
        performance_evaluation.delete()
        messages.success(request, mensaje_eliminar)
        return HttpResponseRedirect(reverse('curriculum:performanceevaluation_list_employee', kwargs={ 'pk': performance_evaluation.curriculum_vitae.id }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess

    return render(request, template_name, {'object':performance_evaluation,'funcion':funcion,'funcion_return':funcion_return})

class PerformanceEvaluationDetail(DetailView):
	model = PerformanceEvaluation




  #-------PersonalAchievement----------
def personal_achievement_list(request, template_name='curriculum/personal_achievement_list.html'):
    personal_achievement = PersonalAchievement.objects.all()
    data = {}
    data['object_list'] = personal_achievement
    return render(request, 'curriculum/personal_achievement_list.html', data)

def personal_achievement_list_employee(request, pk, template_name='curriculum/personal_achievement_list.html'):
    personal_achievement = PersonalAchievement.objects.filter(curriculum_vitae=pk)
    curriculum = CurriculumVitae.objects.get(id=pk)
    titulo = "Logros Personales"
    data = {
        "curriculum": curriculum,
        "titulo": titulo,
    }
    data['object_list'] = personal_achievement
    return render(request, 'curriculum/personal_achievement_list.html', data)

def personal_achievement_create(request,pk, template_name='curriculum/form.html'):
    try:
        curriculum = get_object_or_404(CurriculumVitae, pk=pk)
        form = PersonalAchievementForm(request.POST or None)
        titulo = "Logros Personales"
        funcion = 1
        funcion_return = 7
        if form.is_valid():
            formal_instruction = form.save(commit=False)
            formal_instruction.curriculum_vitae = curriculum
            formal_instruction = form.save()
            messages.success(request, mensaje_crear)
            return HttpResponseRedirect(reverse('curriculum:personalachievement_list_employee', kwargs={ 'pk': pk }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess
    return render(request, template_name, {'form':form, 'titulo':titulo,'curriculum':curriculum,'funcion':funcion,'funcion_return':funcion_return})

def personal_achievement_update(request, pk, template_name='curriculum/form.html'):
    try:
        personal_achievement = get_object_or_404(PersonalAchievement, pk=pk)
        curriculum = get_object_or_404(CurriculumVitae, pk=personal_achievement.curriculum_vitae.id)
        form = PersonalAchievementForm(request.POST or None, instance=personal_achievement)
        titulo = "Logros Personales"
        funcion = 2
        funcion_return = 7
        if form.is_valid():
            form.save()
            messages.success(request, mensaje_actualizar)
            return HttpResponseRedirect(reverse('curriculum:personalachievement_list_employee', kwargs={ 'pk': curriculum.id }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess
    
    return render(request, template_name, {'form':form, 'titulo':titulo,'curriculum':curriculum,'funcion':funcion,'funcion_return':funcion_return})

def personal_achievement_delete(request, pk, template_name='curriculum/confirm_delete.html'):
    try:
        personal_achievement = get_object_or_404(PersonalAchievement, pk=pk)  
        funcion = 3  
        funcion_return = 7
        personal_achievement.delete()
        messages.success(request, mensaje_eliminar)
        return HttpResponseRedirect(reverse('curriculum:personalachievement_list_employee', kwargs={ 'pk': personal_achievement.curriculum_vitae.id }))

    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess
    return render(request, template_name, {'object':personal_achievement,'funcion':funcion,'funcion_return':funcion_return})

class PersonalAchievementDetail(DetailView):
	model = PersonalAchievement




  #-------AffirmativeAction----------
def affirmative_action_list(request, template_name='curriculum/affirmative_action_list.html'):
    affirmative_action = AffirmativeAction.objects.all()
    data = {}
    data['object_list'] = affirmative_action
    return render(request, 'curriculum/affirmative_action_list.html', data)

def affirmative_action_list_employee(request, pk, template_name='curriculum/affirmative_action_list.html'):
    affirmative_action = AffirmativeAction.objects.filter(curriculum_vitae=pk)
    curriculum = CurriculumVitae.objects.get(id=pk)
    titulo = "Acciones Afirmativas"   
    data = {
        "curriculum": curriculum,
        "titulo": titulo,
    }
    data['object_list'] = affirmative_action
    return render(request, 'curriculum/affirmative_action_list.html', data)


def affirmative_action_create(request, pk,template_name='curriculum/form.html'):
    try:        
        curriculum = get_object_or_404(CurriculumVitae, pk=pk)
        form = AffirmativeActionForm(request.POST or None)
        titulo = "Acciones Afirmativas"
        funcion = 1
        funcion_return = 8
        if form.is_valid():
            affirmative_action = form.save(commit=False)
            affirmative_action.curriculum_vitae = curriculum
            affirmative_action = form.save()
            messages.success(request, mensaje_crear)
            return HttpResponseRedirect(reverse('curriculum:affirmativeaction_list_employee', kwargs={ 'pk': pk }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess
    return render(request, template_name, {'form':form, 'titulo':titulo,'curriculum':curriculum,'funcion':funcion,'funcion_return':funcion_return})

def affirmative_action_update(request, pk, template_name='curriculum/form.html'):
    try:
        affirmative_action = get_object_or_404(AffirmativeAction, pk=pk)
        curriculum = get_object_or_404(CurriculumVitae, pk=affirmative_action.curriculum_vitae.id)
        form = AffirmativeActionForm(request.POST or None, instance=affirmative_action)
        titulo = "Acciones Afirmativas"
        funcion = 2
        funcion_return = 8
        if form.is_valid():
            form.save()
            messages.success(request, mensaje_actualizar)
            return HttpResponseRedirect(reverse('curriculum:affirmativeaction_list_employee', kwargs={ 'pk': curriculum.id }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess
    return render(request, template_name, {'form':form, 'titulo':titulo,'curriculum':curriculum,'funcion':funcion,'funcion_return':funcion_return})

def affirmative_action_delete(request, pk, template_name='curriculum/confirm_delete.html'):
    try:    
        affirmative_action = get_object_or_404(AffirmativeAction, pk=pk) 
        funcion = "Eliminar" 
        funcion = 3  
        funcion_return = 8
        affirmative_action.delete()
        messages.success(request, mensaje_eliminar)
        return HttpResponseRedirect(reverse('curriculum:affirmativeaction_list_employee', kwargs={ 'pk': affirmative_action.curriculum_vitae.id }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess
    return render(request, template_name, {'object':affirmative_action,'funcion':funcion,'funcion_return':funcion_return})

class AffirmativeActionDetail(DetailView):
	model = AffirmativeAction




  #-------PersonalReferences----------
def personal_references_list(request, template_name='curriculum/personal_references_list.html'):
    personal_references = PersonalReferences.objects.all()
    data = {}
    data['object_list'] = personal_references
    return render(request, 'curriculum/personal_references_list.html', data)

def personal_references_list_employee(request, pk, template_name='curriculum/personal_references_list.html'):
    personal_references = PersonalReferences.objects.filter(curriculum_vitae=pk)
    curriculum = CurriculumVitae.objects.get(id=pk)
    titulo = "Referencias Personales"
    data = {
        "curriculum": curriculum,
        "titulo": titulo,
    }
    data['object_list'] = personal_references
    return render(request, 'curriculum/personal_references_list.html', data)


def personal_references_create(request, pk,template_name='curriculum/form.html'):
    try:    
        curriculum = get_object_or_404(CurriculumVitae, pk=pk)
        form = PersonalReferencesForm(request.POST or None)
        titulo = "Referencias Personales"
        funcion = 1
        funcion_return = 9
        if form.is_valid():
            formal_instruction = form.save(commit=False)
            formal_instruction.curriculum_vitae = curriculum
            formal_instruction = form.save()
            messages.success(request, mensaje_crear)
            return HttpResponseRedirect(reverse('curriculum:personalreferences_list_employee', kwargs={ 'pk': pk }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess
    return render(request, template_name, {'form':form, 'titulo':titulo,'curriculum':curriculum,'funcion':funcion,'funcion_return':funcion_return})

def personal_references_update(request, pk, template_name='curriculum/form.html'):
    try:
        personal_references = get_object_or_404(PersonalReferences, pk=pk)
        curriculum = get_object_or_404(CurriculumVitae, pk=personal_references.curriculum_vitae.id)
        form = PersonalReferencesForm(request.POST or None, instance=personal_references)
        titulo = "Referencias Personales"
        funcion = 2
        funcion_return = 9
        context = {
            "curriculum": curriculum,
            #"persona": persona,
        }
        if form.is_valid():
            form.save()
            messages.success(request, mensaje_actualizar)
            return HttpResponseRedirect(reverse('curriculum:personalreferences_list_employee', kwargs={ 'pk': curriculum.id }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess
    return render(request, template_name, {'form':form, 'titulo':titulo,'curriculum':curriculum,'funcion':funcion,'funcion_return':funcion_return})

def personal_references_delete(request, pk, template_name='curriculum/confirm_delete.html'):
    try:   
        personal_references = get_object_or_404(PersonalReferences, pk=pk)    
        funcion = 3
        funcion_return = 9
        personal_references.delete()
        messages.success(request, mensaje_eliminar)
        return HttpResponseRedirect(reverse('curriculum:personalreferences_list_employee', kwargs={ 'pk': personal_references.curriculum_vitae.id }))
    except Exception as e:
        messages.error(request, mensaje_error)
        return HttpResponseRedirect(reverse('curriculum:formalinstruction_list_employee', kwargs={ 'pk': pk }))
    finally:
        Final=mensaje_sucess
    return render(request, template_name, {'object':personal_references,'funcion':funcion,'funcion_return':funcion_return})

class PersonalReferencesDetail(DetailView):
	model = PersonalReferences



def menu_curriculo(request, pk):
    curriculum = CurriculumVitae.objects.get(id=pk)
    #persona = Person.objects.get(id=curriculum.)
    context = {
        "curriculum": curriculum,
        #"persona": persona,
    }
    return render(request, "curriculum/curriculum_menu.html", context)
    

def report(request,pk):
    curriculum_vitae = get_object_or_404(CurriculumVitae, pk=pk)    
    formal_instruction = FormalInstruction.objects.filter(curriculum_vitae=pk)
    offices_or_subactivities = OfficesOrsubactivities.objects.filter(curriculum_vitae=pk)
    language = Language.objects.filter(curriculum_vitae=pk)
    training = Training.objects.filter(curriculum_vitae=pk)
    work_experience = WorkExperience.objects.filter(curriculum_vitae=pk)
    performance_evaluation = PerformanceEvaluation.objects.filter(curriculum_vitae=pk)
    personal_achievement = PersonalAchievement.objects.filter(curriculum_vitae=pk)
    affirmative_action = AffirmativeAction.objects.filter(curriculum_vitae=pk)
    personal_references = PersonalReferences.objects.filter(curriculum_vitae=pk)

    #Create the HttpResponse header with PDF
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename='+curriculum_vitae.person.name+'_'+curriculum_vitae.person.lastname+'.pdf'
    buffer = BytesIO()
    c = canvas.Canvas(buffer,pagesize=A4)
    #Header 
    c.setLineWidth(.3)
    c.setFont('Helvetica',64)
    c.drawString(80,500,'Currículo Vitae')
    c.setFont('Helvetica', 18)
    c.drawString(200,450,curriculum_vitae.person.name+" "+curriculum_vitae.person.lastname)
    c.showPage()
    c.setFont('Helvetica',10)
    c.drawString(300,810,'Currículo Vitae - '+curriculum_vitae.person.name+" "+curriculum_vitae.person.lastname)
    
    high = 765
    
    
    #-------------------------------------Datos Personales------------------
    if high < 40:
        c.showPage()
        c.setFont('Helvetica',10)
        c.drawString(300,810,'Currículo Vitae - '+curriculum_vitae.person.name+" "+curriculum_vitae.person.lastname)
        high = 755
    else:
        c.setFont('Helvetica-Bold', 16)
        c.setFillColor(green)    
        c.drawString(30,high,'Datos Personales')
        c.setFillColor(black)    
        c.setFont('Helvetica', 12)
        c.line(30,high-3,560,high-3)
        high = high - 20
        c.setFont('Helvetica-Bold', 12)
        c.drawString(35,high,"Nombre:")
        c.setFont('Helvetica', 12)
        c.drawString(180,high,curriculum_vitae.person.name+" "+curriculum_vitae.person.lastname)
        high = high - 15
        c.setFont('Helvetica-Bold', 12)
        c.drawString(35,high,"Dirección:")
        c.setFont('Helvetica', 12)
        c.drawString(180,high,curriculum_vitae.person.address)
        high = high - 15
        c.setFont('Helvetica-Bold', 12)
        c.drawString(35,high,"Teléfono (s):")
        c.setFont('Helvetica', 12)
        c.drawString(180,high,curriculum_vitae.person.phone)
        high = high - 15
        c.setFont('Helvetica-Bold', 12)
        c.drawString(35,high,"Cédula de identidad:")
        c.setFont('Helvetica', 12)
        c.drawString(180,high,curriculum_vitae.person.cedula)
        high = high - 15
        c.setFont('Helvetica-Bold', 12)
        c.drawString(35,high,"Correo electrónico:")
        c.setFont('Helvetica', 12)
        c.drawString(180,high,curriculum_vitae.person.email)
        high = high - 15
        c.setFont('Helvetica-Bold', 12)
        c.drawString(35,high,"Provincia / Cantón / Parróquia:")
        c.setFont('Helvetica', 12)
        c.drawString(220,high,curriculum_vitae.person.province_of_residence +' / '+ curriculum_vitae.person.canton_of_residence + ' / '+ curriculum_vitae.person.parroquia_of_residence)
        high = high - 15
        high = high - 15

    #-------------------------------------Instruccion Formal------------------
    if len(formal_instruction)>0:
        if high < 40:
            c.showPage()
            c.setFont('Helvetica',10)
            c.drawString(300,810,'Currículo Vitae - '+curriculum_vitae.person.name+" "+curriculum_vitae.person.lastname)
            high = 755
        else:
            c.setFillColor(green)
            c.setFont('Helvetica-Bold', 16)  
            c.drawString(30,high,'Instrucción Formal')
            c.line(30,high-3,560,high-3)
            c.setFillColor(black)
            high = high - 20
            for instruction in formal_instruction:
                if high < 40:
                    c.showPage()
                    high = 755
                else:
                    c.setFont('Helvetica-Bold', 12)
                    c.drawString(40,high,"Nivel de Instrucción: ")
                    c.setFont('Helvetica', 12)
                    c.drawString(180,high,instruction.level_of_instruction.name)
                    high = high - 15
                    c.setFont('Helvetica-Bold', 12)
                    c.drawString(48,high,"Institución Educativa: ")
                    c.setFont('Helvetica', 12)
                    c.drawString(180,high,instruction.educational_institution)
                    high = high - 15
                    c.setFont('Helvetica-Bold', 12)
                    c.drawString(48,high,"Area de Estudios: ")
                    c.setFont('Helvetica', 12)
                    c.drawString(180,high,instruction.area_studies.name)
                    high = high - 15
                    c.setFont('Helvetica-Bold', 12)
                    c.drawString(48,high,"Tiempo de Estudio: ")
                    c.setFont('Helvetica', 12)
                    c.drawString(180,high,str(instruction.studying_time))
                    high = high - 15
                    c.setFont('Helvetica-Bold', 12)
                    c.drawString(48,high,"Año de Egresamiento o Graduación: ")
                    c.setFont('Helvetica', 12)
                    c.drawString(300,high,instruction.date_of_graduation.name)
                    #añador año de egresamiento
                    
                    high = high - 15

                    c.setFont('Helvetica-Bold', 12)
                    c.drawString(48,high,"Nro. registro SENESCYT: ")
                    c.setFont('Helvetica', 12)
                    c.drawString(190,high,instruction.senecyt_registration_number)    
                    high = high - 25

    #-------------------------------------Oficios y Subactividades------------------
    if len(offices_or_subactivities)>0:
        if high < 40:
            c.showPage()
            c.setFont('Helvetica',10)
            c.drawString(300,810,'Currículo Vitae - '+curriculum_vitae.person.name+" "+curriculum_vitae.person.lastname)
            high = 755
        else:
            c.setFillColor(green)
            c.setFont('Helvetica-Bold', 16)
            c.drawString(30,high,'Oficios / Subactividades')
            c.line(30,high-3,560,high-3)
            c.setFillColor(black)
            high = high - 20
            for offices in offices_or_subactivities:
                if high < 40:
                    c.showPage()
                    high = 755
                else:
                    c.setFont('Helvetica-Bold', 12)
                    c.drawString(40,high,"Oficio: ")
                    c.setFont('Helvetica', 12)
                    c.drawString(120,high,offices.job.name)
                    high = high - 15
                    c.setFont('Helvetica-Bold', 12)
                    c.drawString(40,high,"Descripción: ")
                    c.setFont('Helvetica', 12)
                    c.drawString(120,high,offices.description)
                    high = high - 30
    #--------------------------Idiomas--------------------------
    if len(language)>0:
        if high < 40:
            c.showPage()
            c.setFont('Helvetica',10)
            c.drawString(300,810,'Currículo Vitae - '+curriculum_vitae.person.name+" "+curriculum_vitae.person.lastname)
            high = 755
        else:
            c.setFillColor(green)
            c.setFont('Helvetica-Bold', 16)
            c.drawString(30,high,'Idiomas')
            c.setFillColor(black)
            c.line(30,high-3,560,high-3)
            high = high - 20
            c.setFont('Helvetica-Bold', 12)
            c.drawString(40,high,'Idioma')
            c.drawString(230,high,'Nivel Hablado')
            c.drawString(430,high,'Nivel Escrito')
            high = high - 15
            for languages in language:
                if high < 40:
                    c.showPage()
                    high = 755
                else:
                    c.setFont('Helvetica', 12)
                    c.drawString(45,high,languages.language.name)
                    print (languages)        
                    c.drawString(232,high,languages.spoken_level.name)
                    
                    c.drawString(432,high,languages.written.name)
                    high = high - 15

        high = high - 15


    #-------------------------------------Capacitación------------------
    if len(training)>0:
        if high < 40:
            c.showPage()
            c.setFont('Helvetica',10)
            c.drawString(300,810,'Currículo Vitae - '+curriculum_vitae.person.name+" "+curriculum_vitae.person.lastname)
            high = 755
        else:
            c.setFillColor(green)
            c.setFont('Helvetica-Bold', 16)
            c.drawString(30,high,'Capacitación')
            c.line(30,high-3,560,high-3)
            c.setFillColor(black)
            high = high - 20
            for trainings in training:
                if high < 40:
                    c.showPage()
                    high = 755
                else:
                    c.setFont('Helvetica', 12)
                    c.drawString(40,high,trainings.event_type.name)
                    c.drawString(180,high,trainings.name_of_event)
                    high = high - 15
                    c.drawString(40,high,'('+str(trainings.total_hours)+' horas)')    
                    #c.drawString(40,high,'('+str(trainings.total_hours)    
                    c.drawString(180,high,trainings.institution)
                    
                    
                    high = high - 15
                    c.drawString(180,high,trainings.area_studies.name)
                    high = high - 15                             
                    c.drawString(180,high,trainings.type_certificate.name)
                    high = high - 15    
                    c.setFont('Helvetica-Bold', 12)                         
                    c.drawString(180,high,"Desde: ")
                    c.setFont('Helvetica', 12)
                    #c.drawString(180,high,trainings.date_from)    
                    c.drawString(230,high,'2016-05-15')    
                   
                    c.setFont('Helvetica-Bold', 12)
                    c.drawString(350,high,"Hasta: ")
                    c.setFont('Helvetica', 12)
                    c.drawString(400,high,'2016-05-15') 
                    #c.drawString(180,high,trainings.date_up)    
                    high = high - 25
             
        high = high - 15

    #-------------------------------------Acciones Afirmartivas------------------
    if len(affirmative_action)>0:
        if high < 40:
            c.showPage()
            c.setFont('Helvetica',10)
            c.drawString(300,810,'Currículo Vitae - '+curriculum_vitae.person.name+" "+curriculum_vitae.person.lastname)
            high = 755
        else:
            c.setFillColor(green)
            c.setFont('Helvetica-Bold', 16)
            c.drawString(30,high,'Acciones Afirmativas')
            c.line(30,high-3,560,high-3)
            c.setFillColor(black)
            high = high - 20
            for affirmative_actiones in affirmative_action:
                if high < 40:
                    c.showPage()
                    high = 755
                else:
                    c.setFont('Helvetica-Bold', 12)
                    c.drawString(40,high,'Heore o Heroína') 
                    c.setFont('Helvetica', 12) 
                    if affirmative_actiones.hero:
                        c.drawString(180,high,"Si")
                    else:
                        c.drawString(180,high,"No") 
                    high = high - 15
                    c.setFont('Helvetica-Bold', 12)
                    c.drawString(40,high,'Ex-combatiente')    
                    c.setFont('Helvetica', 12)
                    if affirmative_actiones.former_fighter:
                        c.drawString(180,high,"Si")
                    else:
                        c.drawString(180,high,"No")
                    
                    high = high - 15
                    c.setFont('Helvetica-Bold', 12)
                    c.drawString(40,high,'Migrante Ecuatoriano')    
                    c.setFont('Helvetica', 12)
                    if affirmative_actiones.ecuadorian_migrant:
                        c.drawString(180,high,"Si")
                    else:
                        c.drawString(180,high,"No")
                    high = high - 15

                    
    #-------------------------------------Logros------------------
    if len(personal_achievement)>0:
        high = high - 15
        if high < 40:
            c.showPage()
            c.setFont('Helvetica',10)
            c.drawString(300,810,'Currículo Vitae - '+curriculum_vitae.person.name+" "+curriculum_vitae.person.lastname)
            high = 755
        else:
            c.setFillColor(green)
            c.setFont('Helvetica-Bold', 16)
            c.drawString(30,high,'Logros')
            c.line(30,high-3,560,high-3)
            c.setFillColor(black)
            high = high - 20
            for personal_achievements in personal_achievement:
                if high < 40:
                    c.showPage()
                    high = 755
                else:
                    c.setFont('Helvetica-Bold', 12)
                    c.drawString(40,high,'Tipo de logro: ')
                    
                    c.drawString(120,high,personal_achievements.kind_of_achievement.name)
                    high = high - 15
                    c.setFont('Helvetica', 12)

                    c.setFont('Helvetica-Bold', 12)   
                    c.drawString(45,high,'Descripción')
                    high = high - 15
                    c.drawString(120,high,personal_achievements.description) 
                    high = high - 25
                    

        high = high - 15

#--------------------------referencias personales------------------------------

    if len(personal_references)>0:
        if high < 40:
            c.showPage()
            c.setFont('Helvetica',10)
            c.drawString(300,810,'Currículo Vitae - '+curriculum_vitae.person.name+" "+curriculum_vitae.person.lastname)
            high = 755
        else:
            c.setFillColor(green)
            c.setFont('Helvetica-Bold', 16)
            c.drawString(30,high,'Referencias Personales')

            c.line(30,high-3,560,high-3)
            c.setFillColor(black)
            c.setFont('Helvetica-Bold', 12)
            high = high - 20
            for personal_reference in personal_references:
                if high < 40:
                    c.showPage()
                    high = 755
                else:
                    
                    
                    c.drawString(40,high,personal_reference.name+' '+personal_reference.lastname)    
                    high = high - 15
                    c.drawString(40,high,personal_reference.email)    
                    high = high - 15
                    c.drawString(40,high,personal_reference.phone)    
                    high = high - 25
                
    c.save()
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)
    return response