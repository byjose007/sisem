from django.contrib import admin
from .models import CurriculumVitae
from .models import FormalInstruction
from .models import OfficesOrsubactivities
from .models import Language
from .models import Training
from .models import WorkExperience
from .models import PerformanceEvaluation
from .models import PersonalAchievement
from .models import AffirmativeAction
from .models import PersonalReferences

# Register your models here.


class FormalInstructionAdmin(admin.ModelAdmin):
	list_display = ('level_of_instruction', 'area_studies', 'educational_institution')
	search_fields = ('level_of_instruction', 'area_studies', 'educational_institution')

class OfficesOrsubactivitiesAdmin(admin.ModelAdmin):
	list_display = ('job', 'description')
	search_fields = ('job', 'description')

class LanguageAdmin(admin.ModelAdmin):
	list_display = ('language','spoken_level','written')
	search_fields = ('language','spoken_level','written')

# class TrainingAdmin(admin.ModelAdmin):
# 	list_display = ('name_of_event', 'institution', 'area_studies', 'total_hours')
# 	search_fields = ('name_of_event', 'institution', 'area_studies', 'total_hours')
#
# class WorkExperienceAdmin(admin.ModelAdmin):
# 	list_display = ('job', 'work_area','institution')
# 	search_fields = ('job', 'work_area','institution')

class PerformanceEvaluationAdmin(admin.ModelAdmin):
	list_display = ('institution', 'qualification_obtained', 'date_since', 'date_until', 'observation')
	search_fields = ('institution', 'qualification_obtained', 'date_since', 'date_until', 'observation')

class PersonalAchievementAdmin(admin.ModelAdmin):
	list_display = ('kind_of_achievement','description')
	search_fields = ('kind_of_achievement','description')

class AffirmativeActionAdmin(admin.ModelAdmin):
	list_display = ('hero', 'former_fighter', 'ecuadorian_migrant'	)
	search_fields = ('hero', 'former_fighter', 'ecuadorian_migrant'	)

class PersonalReferencesAdmin(admin.ModelAdmin):
	list_display = ('name', 'lastname', 'phone', 'email'	)
	search_fields = ('name', 'lastname', 'phone', 'email'	)

	


admin.site.register(CurriculumVitae)
admin.site.register(FormalInstruction,FormalInstructionAdmin)
admin.site.register(OfficesOrsubactivities,OfficesOrsubactivitiesAdmin)
admin.site.register(Language,LanguageAdmin)
admin.site.register(Training)
admin.site.register(WorkExperience)
admin.site.register(PerformanceEvaluation,PerformanceEvaluationAdmin)
admin.site.register(PersonalAchievement,PersonalAchievementAdmin)
admin.site.register(AffirmativeAction,AffirmativeActionAdmin)
admin.site.register(PersonalReferences,PersonalReferencesAdmin)
