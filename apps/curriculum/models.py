from __future__ import unicode_literals
from django.contrib.postgres.fields import ArrayField

from django.db import models
from apps.person.models import Person

from apps.settings.models import Item


class CurriculumVitae(models.Model):
    person = models.ForeignKey(Person, verbose_name='Empleado', on_delete=models.PROTECT)

    class Meta:
        ordering = ['person']
        verbose_name = 'Curriculo'
        verbose_name_plural = 'Curriculo'

    def __unicode__(self):
        return u'{0}'.format(self.person)

    def __str__(self):
        return u'{0}'.format(self.person)


class FormalInstruction(models.Model):
    curriculum_vitae = models.ForeignKey('CurriculumVitae', verbose_name='Empleado')
    level_of_instruction = models.ForeignKey(Item, on_delete=models.PROTECT,
                                             limit_choices_to={'catalog__code': 'NIVELESTUDIOS'},
                                             verbose_name='Nivel de instrución', blank=True, null=True,
                                             related_name='catalog_level_of_instruction')
    obtained_title = models.CharField(verbose_name='Título Obtenido', max_length=100, blank=True, null=True)
    educational_institution = models.CharField(verbose_name='Institución educativa', max_length=100, blank=True,
                                               null=True)
    area_studies = models.CharField(verbose_name='Área de estudios', blank=True, null=True, max_length=100)
    studying_time = models.IntegerField(verbose_name='Tiempo de estudio en años', blank=True, null=True)
    date_of_graduation = models.CharField(verbose_name='Año de egresamiento o graduación', blank=True, null=True,max_length=100)
    senecyt_registration_number = models.IntegerField(blank=True, null=True, verbose_name='Nro. registro SENESCYT')
    number_acta = models.IntegerField(blank=True, null=True, verbose_name='Nro. de Acta')
    number_refrendacion = models.IntegerField( blank=True, null=True, verbose_name='Nro. de refrendación')

    class Meta:
        ordering = ['level_of_instruction']
        verbose_name = 'Instrucción Formal'
        verbose_name_plural = 'Instrucción Formal'

    def __unicode__(self):
        return u'{0},{1},{2}'.format(self.level_of_instruction, self.area_studies, self.educational_institution)

    def __str__(self):
        return u'{0},{1},{2}'.format(self.level_of_instruction, self.area_studies, self.educational_institution)


class OfficesOrsubactivities(models.Model):
    curriculum_vitae = models.ForeignKey('CurriculumVitae', verbose_name='Empleado')
    job = models.CharField(verbose_name='Oficio', blank=True, null=True,max_length=100)
    description = models.TextField(verbose_name='Descripción',blank=True, null=True)

    class Meta:
        ordering = ['job']
        verbose_name = 'Oficio o subactividad'
        verbose_name_plural = 'Oficios o subactividades'

    def __unicode__(self):
        return u'{0},{1}'.format(self.job, self.description)

    def __str__(self):
        return u'{0},{1}'.format(self.job, self.description)


class Language(models.Model):
    curriculum_vitae = models.ForeignKey('CurriculumVitae', verbose_name='Empleado')
    language = models.CharField(verbose_name='Idioma', blank=True, null=True, max_length=30)
    spoken_level = models.CharField(blank=True, null=True, max_length=30)
    written = models.CharField(verbose_name='Nivel Escrito', blank=True, null=True, max_length=30)

    class Meta:
        ordering = ['language']
        verbose_name = 'Idioma'
        verbose_name_plural = 'Idiomas'

    def __unicode__(self):
        return u'{0},{1},{2}'.format(self.language, self.spoken_level, self.written)

    def __str__(self):
        return u'{0},{1},{2}'.format(self.language, self.spoken_level, self.written)


class Training(models.Model):
    curriculum_vitae = models.ForeignKey('CurriculumVitae', verbose_name='Empleado')
    documento_pdf = models.FileField(upload_to='documents/curriculums/capacitaciones/',verbose_name='Capacitaciones', blank=True, null=True)

    class Meta:
        verbose_name = 'Capacitación'
        verbose_name_plural = 'Capacitaciones'

    def __unicode__(self):
        return u'{0}'.format(self.curriculum_vitae)

    def __str__(self):
        return u'{0}'.format(self.curriculum_vitae)


class WorkExperience(models.Model):
    curriculum_vitae = models.ForeignKey('CurriculumVitae', verbose_name='Empleado')
    documento_pdf=  models.FileField(upload_to='documents/curriculums/expericiencia/', verbose_name='Experiencia', blank=True, null=True)


    class Meta:
        # ordering = ['job', 'institution', 'work_area']
        verbose_name = 'Experiencia laboral'
        verbose_name_plural = 'Experiencias laborales'

    def __unicode__(self):
        return u'{0}'.format(self.curriculum_vitae)

    def __str__(self):
        return u'{0}'.format(self.curriculum_vitae)


class PerformanceEvaluation(models.Model):
    curriculum_vitae = models.ForeignKey('CurriculumVitae', verbose_name='Empleado')
    institution = models.ForeignKey('WorkExperience', verbose_name='Institución')
    qualification_obtained = models.CharField(max_length=100, verbose_name='Calificación Obtenida')
    date_since = models.DateField(max_length=100, blank=True, null=True, verbose_name='Fecha desde')
    date_until = models.DateField(max_length=100, blank=True, null=True, verbose_name='Fecha hasta')
    observation = models.TextField(max_length=100, verbose_name='Observación')

    class Meta:
        ordering = ['institution', 'qualification_obtained']
        verbose_name = 'Evaluación personal'
        verbose_name_plural = 'Evaluaciones personales'

    def __unicode__(self):
        return u'{0},{1},{2},{3},{4}'.format(self.institution, self.qualification_obtained, self.date_since,
                                             self.date_until, self.observation)

    def __str__(self):
        return u'{0},{1},{2},{3},{4}'.format(self.institution, self.qualification_obtained, self.date_since,
                                             self.date_until, self.observation)


class PersonalAchievement(models.Model):
    curriculum_vitae = models.ForeignKey('CurriculumVitae', verbose_name='Empleado')
    kind_of_achievement = models.ForeignKey(Item, on_delete=models.PROTECT,
                                            limit_choices_to={'catalog__code': 'TIPO LOGRO'},
                                            verbose_name='Tipo de logro', blank=True, null=True,
                                            related_name='catalog_kind_of_achievement')
    description = models.TextField(verbose_name='Descripción')

    class Meta:
        ordering = ['kind_of_achievement', 'description']
        verbose_name = 'Logro personale'
        verbose_name_plural = 'Logros personales'

    def __unicode__(self):
        return u'{0},{1}'.format(self.kind_of_achievement, self.description)

    def __str__(self):
        return u'{0},{1}'.format(self.kind_of_achievement, self.description)


class AffirmativeAction(models.Model):
    curriculum_vitae = models.OneToOneField(CurriculumVitae, verbose_name='Empleado')
    hero = models.NullBooleanField(default=False, verbose_name='Heroe o Heroína')
    former_fighter = models.NullBooleanField(default=False, verbose_name='Ex-combatiente')
    ecuadorian_migrant = models.NullBooleanField(default=False, verbose_name='Migrante Ecuatoriano')

    class Meta:
        verbose_name = 'Accion Afirmativa'
        verbose_name_plural = 'Acciones Afirmativas'

    def __unicode__(self):
        return u'{0},{1},{2}'.format(self.hero, self.former_fighter, self.ecuadorian_migrant)

    def __str__(self):
        return u'{0},{1},{2}'.format(self.hero, self.former_fighter, self.ecuadorian_migrant)


class PersonalReferences(models.Model):
    curriculum_vitae = models.ForeignKey('CurriculumVitae', verbose_name='Empleado')
    name = models.CharField(verbose_name='Nombres', max_length=100)
    lastname = models.CharField(verbose_name='Apellidos', max_length=100)
    phone = models.CharField(verbose_name='Teléfono', max_length=100)
    email = models.EmailField('e-mail', blank=True, null=True)

    class Meta:
        ordering = ['name', 'lastname']
        verbose_name = 'Referencia Personal'
        verbose_name_plural = 'Referencias personales'

    def __unicode__(self):
        return u'{0},{1},{2}'.format(self.name, self.lastname, self.phone, self.email)

    def __str__(self):
        return u'{0},{1},{2}'.format(self.name, self.lastname, self.phone, self.email)
