# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.forms.widgets import CheckboxInput

# Administrar Partida
from apps.employee.models import Employee


class Area_Programa(models.Model):
    code = models.CharField(verbose_name="Código*", max_length=255)
    name = models.CharField(verbose_name="Nombre*", max_length=255)
    version = models.IntegerField(blank=True, null=True, verbose_name='version')

    class Meta:
        ordering = ['code']
        verbose_name = 'Area-Programa'
        verbose_name_plural = 'Areas-Programas'

    def __str__(self):
        return u'{0} - {1}  '.format(self.code,self.name)

    def __unicode__(self):
        return u'{0} - {1}'.format(self.code,self.name)


class Subprograma(models.Model):
    code = models.CharField(verbose_name="Código*", max_length=255)
    name = models.CharField(verbose_name="Nombre*", max_length=255)
    version = models.IntegerField(blank=True, null=True, verbose_name='version')
    programa = models.ForeignKey(Area_Programa, verbose_name='Area/Programa*', on_delete=models.PROTECT)

    class Meta:
        ordering = ['code']
        verbose_name = 'Subprograma'
        verbose_name_plural = 'Subprogramas'

    def __str__(self):
        return u'{0} - {1}'.format(self.code, self.name)

    def __unicode__(self):
        return u'{0}- {1}'.format(self.code, self.name)


class Proyecto(models.Model):
    code = models.CharField(verbose_name="Código*", max_length=255)
    name = models.CharField(verbose_name="Nombre*", max_length=255)
    version = models.IntegerField(blank=True, null=True, verbose_name='Version')
    subprograma = models.ForeignKey(Subprograma, verbose_name='Subprograma*', on_delete=models.PROTECT)

    class Meta:
        ordering = ['code']
        verbose_name = 'Proyecto'
        verbose_name_plural = 'Proyecto'

    def __str__(self):
        return u'{0} - {1}'.format(self.code,self.name)

    def __unicode__(self):
        return u'{0} - {1}'.format(self.code,self.name)


class Actividad(models.Model):
    code = models.CharField(verbose_name="Código*", max_length=255)
    name = models.CharField(verbose_name="Nombre*", max_length=255)
    version = models.IntegerField(blank=True, null=True, verbose_name='version')
    proyecto = models.ForeignKey(Proyecto, verbose_name='Proyecto*', on_delete=models.PROTECT)

    class Meta:
        ordering = ['name']
        verbose_name = 'Acividad'
        verbose_name_plural = 'Actividades'

    def __str__(self):
        return u'{0} - {1}'.format(self.code, self.name)

    def __unicode__(self):
        return u'{0} - {1}'.format(self.code, self.name)


# Administrar Catalogo

class Catalogo(models.Model):
    code = models.CharField(verbose_name="Código*", max_length=255)
    description = models.TextField(verbose_name="Descripción*", max_length=255)
    name = models.CharField(verbose_name="Nombre*", max_length=80)
    version = models.IntegerField(blank=True, null=True, verbose_name='version')

    class Meta:
        ordering = ['name']
        verbose_name = 'Catalogo'
        verbose_name_plural = 'Catalogos'

    def __str__(self):
        return u'{0} - {1} '.format(self.name, self.code)

    def __unicode__(self):
        return u'{0} - {1} '.format(self.name, self.code)


class Item(models.Model):
    code = models.CharField(verbose_name="Código*", max_length=255)
    description = models.TextField(verbose_name="Descripción*", max_length=255)
    name = models.CharField(verbose_name="Nombre*", max_length=80)
    version = models.IntegerField(blank=True, null=True, verbose_name='version')
    catalogo = models.ForeignKey(Catalogo, blank=False, verbose_name='Catálogo')

    class Meta:
        ordering = ['name']
        verbose_name = 'Item'
        verbose_name_plural = 'Items'

    def __str__(self):
        return u'{0}  '.format(self.name)

    def __unicode__(self):
        return u'{0}  '.format(self.name)


class Partida(models.Model):
    active = models.BooleanField(verbose_name='Estado', default='True')
    clonedto = models.BigIntegerField(verbose_name='Clonada', blank=True, null=True)
    code = models.CharField(verbose_name="Código*", max_length=255)
    creationdate = models.DateTimeField(verbose_name='Fecha crear', auto_now_add=True, blank=True, null=True)

    creationuser = models.ForeignKey(User, on_delete=models.PROTECT, max_length=10,
                                     verbose_name='Usuario que crea partida', blank=True, null=True,
                                     related_name='creationuser1')

    deletedate = models.DateTimeField(verbose_name='Fecha eliminar', auto_now_add=False, blank=True, null=True)
    deleteuser = models.ForeignKey(User, on_delete=models.PROTECT, max_length=10,
                                   verbose_name='Usuario que elimina partida', blank=True, null=True,
                                   related_name='creationuser2')
    modificationdate = models.DateTimeField(verbose_name='Fecha modificar', auto_now_add=False, blank=True, null=True)
    modificationuser = models.ForeignKey(User, on_delete=models.PROTECT, max_length=10,
                                         verbose_name='Usuario que edita partida', blank=True, null=True,
                                         related_name='creationuser3')
    observation = models.TextField(verbose_name="Observaciones", blank=True, null=True)
    remuneration = models.DecimalField(verbose_name='Remuneración ', max_digits=19, decimal_places=2,
                                       blank=False, null=False)
    thisiscloned = models.BooleanField(verbose_name='es clonada?', default='False')
    version = models.IntegerField(blank=True, null=True, verbose_name='version')
    group_itm = models.ForeignKey(Item, on_delete=models.PROTECT, limit_choices_to={'catalogo__code': 'CAT_GRUPOS'},
                                  max_length=10,
                                  verbose_name='Grupos de Partidas', blank=True, null=True)
    category_itm = models.ForeignKey(Item, on_delete=models.PROTECT,
                                     limit_choices_to={'catalogo__code': 'CAT_CATEGORIAS'},
                                     verbose_name='Categorias de Partidas', blank=True, null=True,
                                     related_name='category_itm')
    charge_itm = models.ForeignKey(Item, on_delete=models.PROTECT,
                                   limit_choices_to={'catalogo__code': 'CAT_CARGOS'},
                                   verbose_name='Cargos de Partidas', blank=True, null=True,
                                   related_name='charge_itm')
    currentemployee = models.ForeignKey(Employee, on_delete=models.PROTECT,
                                        verbose_name='Empleados', blank=True, null=True,
                                        related_name='currentemployee')
    grade_itm = models.ForeignKey(Item, on_delete=models.PROTECT,
                                  limit_choices_to={'catalogo__code': 'CAT_GRADOS'},
                                  verbose_name='Grados de Partidas', blank=True, null=True,
                                  related_name='grade_itm')
    parent = models.ForeignKey(Item, on_delete=models.PROTECT,
                               limit_choices_to={'catalogo__code': 'parent'},
                               verbose_name='Parent', blank=True, null=True,
                               related_name='parent')
    actividad = models.ForeignKey(Actividad, on_delete=models.PROTECT,
                                verbose_name='Actividad', blank=True, null=True,
                                related_name='activid')
    regime_itm = models.ForeignKey(Item, on_delete=models.PROTECT,
                                   limit_choices_to={'catalogo__code': 'CAT_REGIMENES'},
                                   verbose_name='Regimen de Partidas', blank=True, null=True,
                                   related_name='regime_itm')
    spending_itm = models.ForeignKey(Item, on_delete=models.PROTECT,
                                     limit_choices_to={'catalogo__code': 'CAT_GASTOS'},
                                     verbose_name='Gastos de Partidas', blank=True, null=True,
                                     related_name='spending_itm')
    status_itm = models.ForeignKey(Item, on_delete=models.PROTECT,
                                   limit_choices_to={'catalogo__code': 'CAT_ESTADOS'},
                                   verbose_name='Estados de Partidas', blank=True, null=True,
                                   related_name='status_itm')

    class Meta:
        ordering = ['currentemployee__person__name']
        verbose_name = 'Partida'
        verbose_name_plural = 'Partidas'

    def __str__(self):
        return u'{0} '.format(self.code)

    def __unicode__(self):
        return u'{0} '.format(self.code)


class Historial(models.Model):
    reason = models.TextField(verbose_name="Razon")
    registerdate = models.DateTimeField(verbose_name='Fecha Registro', auto_now_add=True)
    version = models.IntegerField(blank=True, null=True, verbose_name='version')
    partida = models.ForeignKey(Partida, on_delete=models.PROTECT, max_length=25,
                                verbose_name='Partida', blank=True, null=True, related_name='partida')
    estado = models.ForeignKey(Item, on_delete=models.PROTECT,
                               limit_choices_to={'catalogo__code': 'CAT_ESTADOS'},
                               verbose_name='Estados de Partidas', blank=True, null=True,
                               related_name='estadoPartida')
    user = models.ForeignKey(User, on_delete=models.PROTECT, max_length=10,
                             verbose_name='Usuario que crea la partida', blank=True, null=True,
                             related_name='cedula')

    class Meta:
        ordering = ['pk']
        verbose_name = 'Historial'
        verbose_name_plural = 'Historiales'

    def __str__(self):
        return u'{0}  '.format(self.name)

    def __unicode__(self):
        return u'{0}  '.format(self.name)


class EmpleadoHistorial(models.Model):
    fechaRegistro=models.DateTimeField(verbose_name='Fecha registro',blank=True, null=True)
    empleado= models.ForeignKey(Employee, on_delete=models.PROTECT, max_length=25,
                                verbose_name='Empleado', blank=True, null=True, related_name='empleado')
    partida = models.ForeignKey(Partida, on_delete=models.PROTECT, max_length=25,
                                verbose_name='Partida', blank=True, null=True, related_name='partidaEmpleado')
    fechaSalida = models.DateTimeField(verbose_name='Fecha Salida',blank=True, null=True)

    class Meta:
        ordering = ['pk']
        verbose_name = 'Historial Empleado'
        verbose_name_plural = 'Historiales por Empleados'

    def __str__(self):
        return u'{0}  '.format(self.empleado)

    def __unicode__(self):
        return u'{0}  '.format(self.empleado)