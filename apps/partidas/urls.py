from django.conf.urls import url

from .views import *

urlpatterns = [
    url(r'^api_organigrama/partidas/$', api_organigrama, name='api_partidas'),
    # Partidas

    url(r'^$', listar_programa, name='listar_programa'),
    url(r'^nuevo$', crear_programa, name='crear_programa'),
    url(r'^editar/(?P<pk>\d+)$', modificar_programa, name='modificar_programa'),
    url(r'^(?P<programa_pk>\d+)/subprograma/$', listar_subprograma, name='listar_subprograma'),
    url(r'^(?P<programa_pk>\d+)/subprograma/create/$', crear_subprograma, name='crear_subprograma'),
    url(r'^(?P<programa_pk>\d+)/subprograma/edit/(?P<subprograma_pk>\d+)/$', editar_subprograma,
        name='editar_subprograma'),
    url(r'^(?P<subprograma_pk>\d+)/proyecto/$', listar_proyecto, name='listar_proyecto'),
    url(r'^(?P<subprograma_pk>\d+)/proyecto/create/$', crear_proyecto, name='crear_proyecto'),
    url(r'^(?P<subprograma_pk>\d+)/proyecto/edit/(?P<proyecto_pk>\d+)/$', editar_proyecto, name='editar_proyecto'),
    url(r'^(?P<proyecto_pk>\d+)/actividad/$', listar_actividad, name='listar_actividad'),
    url(r'^(?P<proyecto_pk>\d+)/actividad/create/$', crear_actividad, name='crear_actividad'),
    url(r'^(?P<proyecto_pk>\d+)/actividad/edit/(?P<actividad_pk>\d+)/$', editar_actividad, name='editar_actividad'),
    # Catalogo
    url(r'^catalogo$', listar_catalogo, name='listar_catalogo'),
    url(r'^catalogo/nuevo/$', crear_catalogo, name='crear_catalogo'),
    url(r'^catalogo/editar/(?P<pk>\d+)$', modificar_catalogo, name='modificar_catalogo'),
    url(r'^catalogo/ver/(?P<pk>\d+)$', ver_catalogo, name='ver_catalogo'),
    url(r'^(?P<catalogo_pk>\d+)/item/$', listar_item, name='listar_item'),
    url(r'^(?P<catalogo_pk>\d+)/item/create/$', crear_item, name='crear_item'),
    url(r'^(?P<catalogo_pk>\d+)/item/edit/(?P<item_pk>\d+)/$', editar_item, name='editar_item'),
    url(r'^(?P<catalogo_pk>\d+)/item/delete/(?P<item_pk>\d+)$', eliminar_item, name='eliminar_item'),
    # Partidas Presupuestarias
    url(r'^partida/(?P<clone>\d+)$', listar_partida, name='listar_partida'),

    url(r'^partida/nuevo$', crear_partida, name='crear_partida'),
    url(r'^partida/historial/(?P<pk>\d+)$', historial_partida, name='historial_partida'),
    url(r'^partida/editar/(?P<pk>\d+)$', editar_partida, name='editar_partida'),
    url(r'^partida/liberar/(?P<pk>\d+)/(?P<empleado>\d+)$', liberar_partida, name='liberar_partida'),
    url(r'^partida/empleado/(?P<pk>\d+)$', historial_empleado, name='historial_empleado'),
    url(r'^estado/partida/(?P<pk>\d+)$', estado_partida, name='estado_partida'),
    url(r'^partida/asignar/(?P<pk>\d+)/(?P<cedula>\d+)$', asignar_partida, name='asignar_partida'),
    url(r'^partida/duplicar/(?P<pk>\d+)$', duplicar_partida, name='duplicar_partida'),
    url(r'^partida/clon/(?P<pk>\d+)$', clonar_partida, name='clonar_partida'),
    url(r'^partida/prueba$', prueba, name='prueba'),

    #     REPORTES
    url(r'^reporte_personas_pdf/$', ReportePartidasPDF.as_view(), name="reporte_personas_pdf"),
    url(r'^report_pdf/$', report_pdf, name='report_pdf'),
]
