# -*- coding: utf-8 -*-
# from dal import autocomplete
from django import forms
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout

from apps.employee.models import Employee, DatosInstitucionales
from apps.institution.models import Institucion
from apps.partidas.models import Subprograma, Proyecto, Actividad, Catalogo, Item, Partida, Historial
from apps.person.models import Person
from .models import Area_Programa
from django.contrib import messages


class Area_Programa_Form(ModelForm):
    def clean(self):
        # Sobrecargar clean devuelve un diccionario con los campos
        cleaned_data = super(Area_Programa_Form, self).clean()
        area_programa = cleaned_data.get("name")

        if area_programa == None:
            self.add_error('name', 'Nombre de la Area debe contener al menos 3 caracteres!')

        elif len(area_programa) < 3:
            self.add_error('name', 'Nombre de la Area debe contener al menos 3 caracteres!')

        return cleaned_data

    class Meta:
        model = Area_Programa
        fields = '__all__'
        exclude = ('version',)


class Area_Programa_EditarForm(ModelForm):
    def clean(self):
        # Sobrecargar clean devuelve un diccionario con los campos
        cleaned_data = super(Area_Programa_EditarForm, self).clean()
        institucion = cleaned_data.get("code")

    class Meta:
        model = Area_Programa
        fields = '__all__'
        exclude = ('version',)


def isfloat(value):
    try:
        float(value)
        return False
    except:
        return True


class SubprogramaForm(ModelForm):
    class Meta:
        model = Subprograma
        # fields = ['code', 'name', 'description', 'parent_item']
        fields = '__all__'
        exclude = ["programa", 'version']


class ProyectoForm(ModelForm):
    class Meta:
        model = Proyecto
        fields = '__all__'
        exclude = ["subprograma", 'version']


class ActividadForm(ModelForm):
    class Meta:
        model = Actividad
        fields = '__all__'
        exclude = ["proyecto", 'version']


class CatalogoForm(ModelForm):
    class Meta:
        model = Catalogo
        fields = '__all__'
        exclude = ['version']


class CatalogoEditForm(ModelForm):
    class Meta:
        model = Catalogo
        fields = '__all__'
        exclude = ['version']
        widgets = {
            'code': forms.TextInput(attrs={'readonly': 'readonly'})
        }


class CatalogoVerForm(ModelForm):
    class Meta:
        model = Catalogo
        fields = '__all__'
        exclude = ['version']
        widgets = {
            'code': forms.TextInput(attrs={'readonly': 'readonly'}),
            'name': forms.TextInput(attrs={'readonly': 'readonly'}),
            'description': forms.TextInput(attrs={'readonly': 'readonly'})
        }


class ItemForm(ModelForm):
    class Meta:
        model = Item
        fields = '__all__'
        exclude = ['version', 'catalogo']


class ItemEditForm(ModelForm):
    class Meta:
        model = Item
        fields = '__all__'
        exclude = ['version', 'catalogo']
        widgets = {
            'code': forms.TextInput(attrs={'readonly': 'readonly'})
        }


class PartidaForm(forms.Form):
    programas = forms.ModelChoiceField(queryset=Area_Programa.objects.all())
    subprogramas = forms.ModelChoiceField(queryset=Subprograma.objects.all())
    proyectos = forms.ModelChoiceField(queryset=Proyecto.objects.all())
    actividades = forms.ModelChoiceField(queryset=Actividad.objects.all())
    estados = forms.ModelChoiceField(queryset=Item.objects.filter(catalogo__code='CAT_ESTADOS'))
    gastos = forms.ModelChoiceField(queryset=Item.objects.filter(catalogo__code='CAT_GASTOS'))
    regimen = forms.ModelChoiceField(queryset=Item.objects.filter(catalogo__code='CAT_REGIMENES'))
    cargos = forms.ModelChoiceField(queryset=Item.objects.filter(catalogo__code='CAT_CARGOS'))
    grupos = forms.ModelChoiceField(queryset=Item.objects.filter(catalogo__code='CAT_GRUPOS'))
    grados = forms.ModelChoiceField(queryset=Item.objects.filter(catalogo__code='CAT_GRADOS'))
    categorias = forms.ModelChoiceField(queryset=Item.objects.filter(catalogo__code='CAT_CATEGORIAS'))


class PartidaCrearForm(ModelForm):
    class Meta:
        model = Partida
        fields = '__all__'
        exclude = ['id', 'currentemployee', 'clonedto', 'active', 'creationuser', 'deleteuser',
                   'deletedate', 'creationdate', 'modificationdate', 'modificationuser', 'thisiscloned',
                   'version', 'status_itm', 'parent']

class PartidaDuplicarForm(ModelForm):
    class Meta:
        model = Partida
        fields = '__all__'
        exclude = ['id','code', 'actividad','regime_itm','spending_itm','currentemployee', 'clonedto', 'active', 'creationuser', 'deleteuser',
                   'deletedate', 'creationdate', 'modificationdate', 'modificationuser', 'thisiscloned',
                   'version', 'status_itm', 'parent','observation']

class PartidaClonarForm(ModelForm):
    class Meta:
        model = Partida
        fields = '__all__'
        exclude = ['id','code', 'actividad','remuneration','group_itm','category_itm','charge_itm','grade_itm','regime_itm','spending_itm','currentemployee', 'clonedto', 'active', 'creationuser', 'deleteuser',
                   'deletedate', 'creationdate', 'modificationdate', 'modificationuser', 'thisiscloned',
                   'version', 'status_itm', 'parent','observation']


class HistorialForm(ModelForm):
    class Meta:
        model = Historial
        fields = '__all__'
        exclude = ['pk','version']


# class PartidaEditarForm(ModelForm):
#     class Meta:
#         model = Partida
#         fields = '__all__'
#         exclude = ['id', 'currentemployee', 'clonedto', 'active', 'creationuser', 'deleteuser',
#                    'deletedate', 'creationdate', 'modificationdate', 'modificationuser', 'thisiscloned',
#                    'version','status_itm']
#
class PartidaEditarForm(forms.Form):
    programa = forms.ModelChoiceField(queryset=Area_Programa.objects.all(), disabled=True, label='Programa')
    subprograma = forms.ModelChoiceField(queryset=Subprograma.objects, required=False, disabled=True,
                                         label='Subprograma')
    proyecto = forms.ModelChoiceField(queryset=Proyecto.objects, required=False, disabled=True, label='Proyecto')
    actividad = forms.ModelChoiceField(queryset=Actividad.objects, required=False, disabled=True, label='Actividad')
    gasto = forms.ModelChoiceField(queryset=Item.objects.filter(catalogo__code='CAT_GASTOS'), required=False,
                                   disabled=False, label='Gasto')
    regimen = forms.ModelChoiceField(queryset=Item.objects.filter(catalogo__code='CAT_REGIMENES'), required=False,
                                     disabled=False, label='Regimen')
    codigo = forms.CharField(required=False, label='codigo')

    def __init__(self, *args, data=None, **kwargs):
        if data:
            super(PartidaEditarForm, self).__init__(*args, **kwargs)

            self.fields['programa'].initial = data['programa']
            self.fields['subprograma'].initial = data['subprograma']
            self.fields['proyecto'].initial = data['proyecto']
            self.fields['actividad'].initial = data['actividad']
            if data['gasto']:
                self.fields['gasto'].initial = data['gasto']
            if data['regimen']:
                self.fields['regimen'].initial = data['regimen']

                # actividad2 = Actividad.objects.get(pk=self.fields['actividad'])
                # proyecto2 = Proyecto.objects.get(pk=self.fields['proyecto'])
                # subprograma2 = Subprograma.objects.get(pk=self.fields['subprograma'])
                # programa2 = Area_Programa.objects.get(pk=self.fields['programa'])

                # self.fields['codigo'].initial = programa2.code+'.'+ subprograma2.code+'.'+ proyecto2.code+'.'+ actividad2.code
        else:
            super(PartidaEditarForm, self).__init__(*args, **kwargs)


class PartidaAsignarForm(ModelForm):
    class Meta:
        model = Partida
        fields = '__all__'
        exclude = ['id', 'clonedto', 'active', 'modificationuser', 'creationuser', 'deleteuser',
                   'deletedate', 'creationdate', 'modificationdate', 'thisiscloned',
                   'version', 'status_itm', 'code', 'observation', 'remuneration', 'group_itm', 'category_itm'
            , 'charge_itm', 'grade_itm', "subprograma", 'spending_itm', 'regime_itm', 'parent', 'actividad']

        # def __init__(self, *args, **kwargs):
        #     # print(kwargs['cedula'])
        #     cedula = kwargs.pop('cedula')
        #     empleado_actual = Employee.objects.filter(person__cedula=cedula)
        #     super(PartidaAsignarForm, self).__init__(*args, **kwargs)
        #     self.fields['currentemployee'].queryset = empleado_actual
        #     if (empleado_actual):
        #         self.fields['currentemployee'].initial = empleado_actual[0].id


class FilterPartidaForm(ModelForm):
    code = forms.CharField(required=False)
    remuneration = forms.CharField(required=False)

    class Meta:
        model = Partida
        fields = '__all__'


class HistorialForm(ModelForm):
    class Meta:
        model = Historial
        fields = '__all__'
        exclude = ['version', 'registerdate', 'partida', 'estado', 'user']
