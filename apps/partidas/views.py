import datetime

from django.contrib import messages
from django.contrib.auth.models import User
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models.deletion import ProtectedError
from django.db.models.query_utils import Q
from django.http import HttpResponseRedirect
from django.http.response import JsonResponse, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404, render_to_response
from humanresource.html_to_pdf import render_to_pdf

# Create your views here.
from django.template.context import RequestContext
from reportlab.graphics.shapes import String
from reportlab.lib.units import cm
from reportlab.platypus.tables import TableStyle, Table

from apps import partidas
from apps.employee.models import Employee
from apps.partidas.apps import PartidasConfig
from apps.partidas.forms import Area_Programa_Form, Area_Programa_EditarForm, SubprogramaForm, ProyectoForm, \
    ActividadForm, CatalogoForm, CatalogoEditForm, CatalogoVerForm, ItemForm, ItemEditForm, PartidaForm, \
    PartidaCrearForm, PartidaEditarForm, PartidaAsignarForm, FilterPartidaForm, HistorialForm, PartidaDuplicarForm, \
    PartidaClonarForm
from apps.person.models import Person
from apps.vacacion.forms import VacacionForm
from apps.partidas.models import Area_Programa, Subprograma, Proyecto, Actividad, Catalogo, Item, Partida, Historial, \
    EmpleadoHistorial
from apps.institution.models import Directivo, Direccion, Institucion, JefaturaCoordinacion

# REPORTES
from django.conf import settings
from io import BytesIO
from reportlab.pdfgen import canvas
from django.views.generic import View
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4, cm
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Paragraph, Table, TableStyle
from reportlab.lib.enums import TA_JUSTIFY, TA_LEFT, TA_CENTER
from reportlab.lib import colors


# INICIO PROGRAMA
def listar_programa(request, template_name='partidas/programa_list.html'):
    programa = Area_Programa.objects.all()
    data = {}
    data['object_list'] = programa

    programas = Area_Programa.objects.all()
    data = {'programas': programas}
    return render(request, template_name, data)


def crear_programa(request, template_name='partidas/programa_form.html'):
    form = Area_Programa_Form(request.POST or None)
    titulo = 'Nueva Area/Programa'

    if form.is_valid():
        form.save()
        messages.success(request, 'Buen Trabajo. Datos ingresados correctamente.')
        return redirect('partidas:listar_programa')
    return render(request, template_name, {'form': form, 'titulo': titulo})


def modificar_programa(request, pk, template_name='partidas/programa_form.html'):
    partida = get_object_or_404(Area_Programa, pk=pk)
    titulo = 'Editar Area/Programa'
    form = Area_Programa_EditarForm(request.POST or None, instance=partida)
    if form.is_valid():
        form.save()
        messages.success(request, 'Buen Trabajo. Datos ingresados correctamente.')
        return redirect('partidas:listar_programa')
    return render(request, template_name, {'form': form, 'titulo': titulo})


# FIN PROGRAMA
#  INICIO Subprograma
def listar_subprograma(request, programa_pk, template_name='partidas/subprograma_list.html'):
    programa = get_object_or_404(Area_Programa, pk=programa_pk)
    subprogramas_list = Subprograma.objects.filter(programa__pk=programa_pk)
    areas = Area_Programa.objects.all()

    if request.POST:
        id = request.POST['id']
        area_id = request.POST['area']
        Subprograma.objects.filter(pk=id).update(programa=area_id)

    # item_list = Item.objects.all()
    # paginator = Paginator(subprogramas_list, 10)  # Show 25 contacts per page
    # page = request.GET.get('page')
    # try:
    #     page_obj = paginator.page(page)
    # except PageNotAnInteger:
    #     # If page is not an integer, deliver first page.
    #     page_obj = paginator.page(1)
    # except EmptyPage:
    #     # If page is out of range (e.g. 9999), deliver last page of results.
    #     page_obj = paginator.page(paginator.num_pages)
    context = {'programa': programa, 'subprogramas_list': subprogramas_list, 'areas': areas}
    return render(request, template_name, context)


def crear_subprograma(request, programa_pk, template_name='partidas/subprograma_form.html'):
    if 'save' in request.POST:
        form = SubprogramaForm(data=request.POST)
        if form.is_valid():
            subprograma = form.save(commit=False)
            subprograma.area = Area_Programa.objects.get(id=programa_pk)
            subprograma.save()
            messages.success(request, 'El subprograma ha sido creado exitosamente.')
            return redirect('partidas:listar_subprograma', programa_pk=programa_pk)
        else:
            context = RequestContext(request, {'form': form, 'create': True})
            # return render_to_response('item.html', context)
            return render_to_response(template_name, context)
    form = SubprogramaForm()
    context = {'form': form, 'create': True}
    return render(request, template_name, context)


def editar_subprograma(request, programa_pk, subprograma_pk, template_name='partidas/subprograma_form.html'):
    item = get_object_or_404(Subprograma, pk=subprograma_pk)
    if 'save' in request.POST:
        form = SubprogramaForm(instance=item, data=request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'El subprograma ha sido actualizado exitosamente.')
            return redirect('partidas:listar_subprograma', programa_pk=programa_pk)
        else:
            context = RequestContext(request, {'form': form, 'create': False})
            return render('partidas:subprograma', context)

    if 'cancel' in request.POST:
        return redirect('partidas:listar_subprograma', programa_pk=programa_pk)

    form = SubprogramaForm(instance=item)
    context = {'form': form, 'create': False}
    return render(request, template_name, context)


# FIN SUBPROGRAMA
# INICIO PROYECTOS
def listar_proyecto(request, subprograma_pk, template_name='partidas/proyecto_list.html'):
    subprograma = get_object_or_404(Subprograma, pk=subprograma_pk)
    proyectos_list = Proyecto.objects.filter(subprograma__pk=subprograma_pk)
    subprogramas = Subprograma.objects.all()

    if request.POST:
        id = request.POST['id']
        subprograma_id = request.POST['subprograma']
        Proyecto.objects.filter(pk=id).update(subprograma=subprograma_id)

    paginator = Paginator(proyectos_list, 10)
    page = request.GET.get('page')
    try:
        page_obj = paginator.page(page)
    except PageNotAnInteger:
        page_obj = paginator.page(1)
    except EmptyPage:
        page_obj = paginator.page(paginator.num_pages)
    context = {'subprograma': subprograma, 'page_obj': page_obj, 'subprogramas': subprogramas}
    return render(request, template_name, context)


def crear_proyecto(request, subprograma_pk, template_name='partidas/proyecto_form.html'):
    if 'save' in request.POST:
        form = ProyectoForm(data=request.POST)
        if form.is_valid():
            proyecto = form.save(commit=False)
            proyecto.program = Subprograma.objects.get(id=subprograma_pk)
            proyecto.save()
            messages.success(request, 'El proyecto ha sido creado exitosamente.')
            return redirect('partidas:listar_proyecto', subprograma_pk=subprograma_pk)
        else:
            context = RequestContext(request, {'form': form, 'create': True})
            # return render_to_response('item.html', context)
            return render_to_response(template_name, context)
    form = ProyectoForm()
    context = {'form': form, 'create': True}
    return render(request, template_name, context)


def editar_proyecto(request, subprograma_pk, proyecto_pk, template_name='partidas/proyecto_form.html'):
    item = get_object_or_404(Proyecto, pk=proyecto_pk)
    if 'save' in request.POST:
        form = ProyectoForm(instance=item, data=request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'El proyecto ha sido actualizado exitosamente.')
            return redirect('partidas:listar_proyecto', subprograma_pk=subprograma_pk)
        else:
            context = RequestContext(request, {'form': form, 'create': False})
            return render('partidas:proyecto', context)

    if 'cancel' in request.POST:
        return redirect('partidas:listar_proyecto', subprograma_pk=subprograma_pk)

    form = ProyectoForm(instance=item)
    context = {'form': form, 'create': False}
    return render(request, template_name, context)


# FIN PROYECTOS
# INICIO ACTIVIDADES

def listar_actividad(request, proyecto_pk, template_name='partidas/actividad_list.html'):
    proyecto = get_object_or_404(Proyecto, pk=proyecto_pk)
    actividad_list = Actividad.objects.filter(proyecto__pk=proyecto_pk)
    proyectos = Proyecto.objects.all()

    if request.POST:
        id = request.POST['id']
        proyecto_id = request.POST['proyecto']
        Actividad.objects.filter(pk=id).update(proyecto=proyecto_id)

    paginator = Paginator(actividad_list, 10)
    page = request.GET.get('page')
    try:
        page_obj = paginator.page(page)
    except PageNotAnInteger:
        page_obj = paginator.page(1)
    except EmptyPage:
        page_obj = paginator.page(paginator.num_pages)
    context = {'proyecto': proyecto, 'page_obj': page_obj, 'proyectos': proyectos}
    return render(request, template_name, context)


def crear_actividad(request, proyecto_pk, template_name='partidas/actividad_form.html'):
    if 'save' in request.POST:
        form = ActividadForm(data=request.POST)
        if form.is_valid():
            actividad = form.save(commit=False)
            actividad.subprogram = Proyecto.objects.get(id=proyecto_pk)
            actividad.save()
            messages.success(request, 'La actividad ha sido creada exitosamente.')
            return redirect('partidas:listar_actividad', proyecto_pk=proyecto_pk)
        else:
            context = RequestContext(request, {'form': form, 'create': True})
            # return render_to_response('item.html', context)
            return render_to_response(template_name, context)
    form = ActividadForm()
    context = {'form': form, 'create': True}
    return render(request, template_name, context)


def editar_actividad(request, proyecto_pk, actividad_pk, template_name='partidas/actividad_form.html'):
    item = get_object_or_404(Actividad, pk=actividad_pk)
    if 'save' in request.POST:
        form = ActividadForm(instance=item, data=request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'La actividad ha sido actualizado exitosamente.')
            return redirect('partidas:listar_actividad', proyecto_pk=proyecto_pk)
        else:
            context = RequestContext(request, {'form': form, 'create': False})
            return render('partidas:actividad', context)

    if 'cancel' in request.POST:
        return redirect('partidas:listar_actividad', proyecto_pk=proyecto_pk)

    form = ActividadForm(instance=item)
    context = {'form': form, 'create': False}
    return render(request, template_name, context)
    # FIN ACTIVIDADES


# INICIO CATALOGO
def listar_catalogo(request, template_name='partidas/catalogo_list.html'):
    catalogo = Catalogo.objects.all()
    data = {}
    data['object_list'] = catalogo

    catalogos = Catalogo.objects.all()
    data = {'catalogos': catalogos}
    return render(request, template_name, data)


def crear_catalogo(request, template_name='partidas/catalogo_form.html'):
    form = CatalogoForm(request.POST or None)
    titulo = 'Nuevo Catalogo'

    if form.is_valid():
        form.save()
        messages.success(request, 'Buen Trabajo. Datos ingresados correctamente.')
        return redirect('partidas:listar_catalogo')
    return render(request, template_name, {'form': form, 'titulo': titulo, 'accion': 'crear'})


def modificar_catalogo(request, pk, template_name='partidas/catalogo_form.html'):
    catalogo = get_object_or_404(Catalogo, pk=pk)
    titulo = 'Editar Catálogo'
    form = CatalogoEditForm(request.POST or None, instance=catalogo)
    if form.is_valid():
        form.save()
        messages.success(request, 'Buen Trabajo. Datos ingresados correctamente.')
        return redirect('partidas:listar_catalogo')
    return render(request, template_name, {'form': form, 'titulo': titulo, 'accion': 'crear'})


def ver_catalogo(request, pk, template_name='partidas/catalogo_form.html'):
    catalogo = get_object_or_404(Catalogo, pk=pk)
    titulo = 'Ver Catálogo'
    form = CatalogoVerForm(request.POST or None, instance=catalogo)
    if form.is_valid():
        form.save()
        messages.success(request, 'Buen Trabajo. Datos ingresados correctamente.')
        return redirect('partidas:listar_catalogo')
    return render(request, template_name, {'form': form, 'titulo': titulo, 'accion': 'ver'})


# INICIO ITEMS

def listar_item(request, catalogo_pk, template_name='partidas/item_list.html'):
    catalogo = get_object_or_404(Catalogo, pk=catalogo_pk)
    item_list = Item.objects.filter(catalogo__pk=catalogo_pk)
    catalogos = Catalogo.objects.all()

    if request.POST:
        id = request.POST['id']
        catalogo_id = request.POST['catalogo']
        Item.objects.filter(pk=id).update(catalogo=catalogo_id)

    # item_list = Item.objects.all()
    paginator = Paginator(item_list, 100000)  # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        page_obj = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        page_obj = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        page_obj = paginator.page(paginator.num_pages)
    context = {'catalogo': catalogo, 'page_obj': page_obj, 'catalogos': catalogos}
    return render(request, template_name, context)


def crear_item(request, catalogo_pk, template_name='partidas/item_form.html'):
    if 'save' in request.POST:
        form = ItemForm(data=request.POST)
        if form.is_valid():
            item = form.save(commit=False)
            item.catalogo = Catalogo.objects.get(id=catalogo_pk)
            item.save()
            messages.success(request, 'El item ha sido creado exitosamente.')
            return redirect('partidas:listar_item', catalogo_pk=catalogo_pk)
        else:
            context = RequestContext(request, {'form': form, 'create': True})
            return render_to_response(template_name, context)
    form = ItemForm()
    context = {'form': form, 'create': True}
    return render(request, template_name, context)


def editar_item(request, catalogo_pk, item_pk, template_name='partidas/item_form.html'):
    item = get_object_or_404(Item, pk=item_pk)
    if 'save' in request.POST:
        form = ItemEditForm(instance=item, data=request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'El item ha sido actualizado exitosamente.')
            return redirect('partidas:listar_item', catalogo_pk=catalogo_pk)
        else:
            context = RequestContext(request, {'form': form, 'create': False})
            return render('partidas:item', context)

    if 'cancel' in request.POST:
        return redirect('partidas:listar_item', catalogo_pk=catalogo_pk)

    form = ItemEditForm(instance=item)
    context = {'form': form, 'create': False}
    return render(request, template_name, context)


def eliminar_item(request, catalogo_pk, item_pk):
    item = Item.objects.get(pk=item_pk)
    try:
        item.delete()
        messages.success(request, "El ítem seleccionado ha sido eliminado correctamente.")
    except ProtectedError:
        messages.error(request,
                       "El ítem seleccionado está siendo utilizado en otras funcionalidades, verifique su estado para continuar.")

    return redirect('partidas:listar_item', catalogo_pk=catalogo_pk)


# INICIO PARTIDAS

def listar_partida(request, clone, template_name='partidas/partida_list.html'):
    if request.POST:
        programa_post = request.POST.get('programa')
        subprograma_post = request.POST.get('subprograma')
        proyecto_post = request.POST.get('proyecto')
        actividad_post = request.POST.get('actividad')
        cedula_post = request.POST.get('cedula')
        estado = request.POST.get('status_itm')
        gasto_post = request.POST.get('spending_itm')
        regimen_post = request.POST.get('regime_itm')
        cargo = request.POST.get('charge_itm')
        grupo_post = request.POST.get('group_itm')
        categoria_post = request.POST.get('category_itm')
        grado = request.POST.get('grade_itm')
        codigo = request.POST.get('codigo')
        remuneracion = request.POST.get('remuneracion')
        observacion = request.POST.get('observacion')

        data = {
            'cedula': cedula_post,
            'programa_pk': programa_post,
            'subprograma_pk': subprograma_post,
            'proyecto_pk': proyecto_post,
            'actividad_pk': actividad_post,
            'remuneracion': remuneracion,
            'observacion': observacion,
            'codigo': codigo,
            'gasto': gasto_post,
            'regimen': regimen_post,
            'grupo': grupo_post,
            'categoria': categoria_post,
        }

        actividad = Actividad.objects.filter(pk=actividad_post)
        proyecto = Proyecto.objects.filter(pk=proyecto_post)
        subprograma = Subprograma.objects.filter(pk=subprograma_post)
        programa = Area_Programa.objects.filter(pk=programa_post)
        cedula = Employee.objects.filter(person__cedula=cedula_post)

        if estado:
            estado = Item.objects.filter(pk=estado)
        if gasto_post:
            gasto = Item.objects.filter(pk=gasto_post)
        if regimen_post:
            regimen = Item.objects.filter(pk=regimen_post)
        if cargo:
            cargo = Item.objects.filter(pk=cargo)
        if grupo_post:
            grupo = Item.objects.filter(pk=grupo_post)
        if categoria_post:
            categoria = Item.objects.filter(pk=categoria_post)
        if grado:
            grado = Item.objects.filter(pk=grado)

        formPartidas = FilterPartidaForm(request.POST)
        qset = (Q())
        if programa:
            qsetProg = Q(actividad__proyecto__subprograma__programa__pk=programa[0].pk)
            qset = (qsetProg)

            if programa and subprograma:
                qsetSubp = Q(actividad__proyecto__subprograma__pk=subprograma[0].pk)
                qset = (qsetProg & qsetSubp)
            if subprograma and programa and proyecto:
                qsetProy = Q(actividad__proyecto__pk=proyecto[0].pk)
                qset = (qsetProg & qsetSubp & qsetProy)
            if subprograma and programa and proyecto and actividad:
                qsetAct = Q(actividad__pk=actividad[0].pk)
                qset = (qsetProg & qsetSubp & qsetProy & qsetAct)

        if codigo:
            partidas = Q(code=codigo)
            qset = (qset & partidas)
        if remuneracion:
            partidas = Q(remuneration=remuneracion)
            qset = (qset & partidas)
        if cedula:
            qsetCed = Q(currentemployee__person__cedula=cedula[0].person.cedula)
            qset = (qset & qsetCed)

        if observacion:
            qsetObs = Q(observation=observacion)
            qset = (qset & qsetObs)
        if estado:
            qsetEst = Q(status_itm=estado)
            qset = (qset & qsetEst)
            # partidas = Partida.objects.filter(qset)
        if gasto_post:
            qsetGas = Q(spending_itm=gasto)
            qset = (qset & qsetGas)
            # partidas = Partida.objects.filter(qset)
        if regimen_post:
            qsetReg = Q(regime_itm=regimen)
            qset = (qset & qsetReg)
            # partidas = Partida.objects.filter(qset)
        if cargo:
            qsetCar = Q(charge_itm=cargo)
            qset = (qset & qsetCar)
            # partidas = Partida.objects.filter(qset)
        if grupo_post:
            qsetGru = Q(group_itm=grupo)
            qset = (qset & qsetGru)
            # partidas = Partida.objects.filter(qset)
        if categoria_post:
            qsetCat = Q(category_itm=categoria)
            qset = (qset & qsetCat)
            # partidas = Partida.objects.filter(qset)
        if grado:
            qsetGra = Q(grade_itm=grado)
            qset = (qset & qsetGra)
            # partidas = Partida.objects.filter(qset)

        if clone == '1':
            partidas = Partida.objects.filter(qset & Q(clonedto=1))
        else:
            partidas = Partida.objects.filter(qset & Q(clonedto=None))

    else:
        data = {}
        formPartidas = FilterPartidaForm()
        if clone == '1':
            partidas = Partida.objects.filter(clonedto=1).order_by('-pk')[:30]
        else:
            partidas = Partida.objects.filter(clonedto=None).order_by('-pk')[:30]

    catalogos = Catalogo.objects.all()
    data['catalogos'] = catalogos
    data['partidaform'] = formPartidas
    data['partidas'] = partidas
    data['clone'] = clone

    if request.POST.get('reporte'):
        data['user'] = request.user
        data['total_registros'] = partidas.count()
        if request.POST.get('programa'):
            data['programa'] = Area_Programa.objects.get(pk=int(programa_post))
        if request.POST.get('subprograma'):
            data['subprograma'] = Subprograma.objects.get(pk=int(subprograma_post))
        if request.POST.get('proyecto'):
            data['proyecto'] = Proyecto.objects.get(pk=int(proyecto_post))
        if request.POST.get('actividad'):
            data['actividad'] = Actividad.objects.get(pk=int(actividad_post))
        if request.POST.get('spending_itm'):
            data['gasto'] = Item.objects.get(pk=int(gasto_post))
        if request.POST.get('regime_itm'):
            data['regimen'] = Item.objects.get(pk=int(regimen_post))
        if request.POST.get('group_itm'):
            data['grupo'] = Item.objects.get(pk=int(grupo_post))
        if request.POST.get('category_itm'):
            data['categoria'] = Item.objects.get(pk=int(categoria_post))

        return render_to_pdf('partidas/reportes.html', data)

    if request.POST.get('liberarTodo'):
        libre = Item.objects.get(code='EST_LIBRE')
        partidas = Partida.objects.filter(qset & Q(clonedto=None)).exclude(status_itm=libre)
        partidas_liberadas = partidas

        motivo_txt = "Partida Liberada por grupo"
        for partida in partidas_liberadas:
            Historial.objects.create(reason=motivo_txt, registerdate=datetime.datetime.now(),
                                     estado=libre, partida=partida, user=request.user)
            EmpleadoHistorial.objects.filter(partida=partida.pk, empleado=partida.currentemployee).update(
                fechaSalida=datetime.datetime.now())
        partidas.update(status_itm=libre, currentemployee=None)

        return render(request, template_name, data)

    if clone == '1':
        return render(request, 'partidas/partida_clon.html', data)
    else:
        return render(request, template_name, data)


def prueba(request, template_name='partidas/prueba.html'):
    partida = Catalogo.objects.all()
    areaform = PartidaForm()
    data = {}
    data['object_list'] = partida

    catalogos = Catalogo.objects.all()
    data = {'catalogos': catalogos, 'areaform': areaform}
    return render(request, template_name, data)


# API COMBOS
def api_organigrama(request):
    # Estado del empleado
    estado_empledo = Catalogo.objects.get(code='CAT_ESTADOS')
    estados = Item.objects.filter(catalogo_id=estado_empledo).values('name', 'pk')
    # Organigrama
    programas = Area_Programa.objects.all().values('name', 'pk', 'code')
    subprogramas = Subprograma.objects.all().values('name', 'pk', 'programa__id', 'code')
    proyectos = Proyecto.objects.all().values('name', 'pk', 'subprograma__id', 'code')
    actividades = Actividad.objects.all().values('name', 'pk', 'proyecto__id', 'code')
    gastos = Item.objects.filter(catalogo__code='CAT_GASTOS').values('name', 'pk', 'code')
    regimenes = Item.objects.filter(catalogo__code='CAT_REGIMENES').values('name', 'pk', 'code')
    organigrama = {
        'programas': list(programas),
        'subprogramas': list(subprogramas),
        'proyectos': list(proyectos),
        'actividades': list(actividades),
        'gastos': list(gastos),
        'regimenes': list(regimenes)
    }
    return JsonResponse(organigrama, safe=False)


def crear_partida(request, template_name='partidas/partida_form.html'):
    if request.POST:

        partidaform = PartidaCrearForm(request.POST)
        if partidaform.is_valid():
            estado = Item.objects.get(code='EST_LIBRE', catalogo__code='CAT_ESTADOS')
            partida = partidaform.save(commit=False)
            partida.status_itm = estado
            partida.active = True
            partida.creationuser = User.objects.get(username=request.user.username)
            partida.actividad_id = int(request.POST.get('actividad'))
            partida.code = request.POST.get('code')
            # partida.creationdate = datetime.datetime.now()
            if request.POST.get('gasto'):
                partida.spending_itm_id = int(request.POST.get('gasto'))

            if request.POST.get('regimen'):
                partida.regime_itm_id = int(request.POST.get('regimen'))
            partida.save()

            Historial.objects.create(reason='Creacion Partida', registerdate=datetime.datetime.now(),
                                     estado=partida.status_itm,
                                     partida=partida, user=request.user)

            return redirect('partidas:listar_partida', clone=0)
    else:
        partidaform = PartidaCrearForm()

    data = {'partidaform': partidaform}

    return render(request, template_name, data)


def editar_partida(request, pk, template_name='partidas/partida_editar.html'):
    if request.POST:

        partida = get_object_or_404(Partida, pk=pk)
        formPartidas = PartidaCrearForm(request.POST or None, instance=partida)

        if formPartidas.is_valid():
            partidas = formPartidas.save(commit=False)
            # partidas.pk = partida.pk
            partida.modificationuser = User.objects.get(username=request.user.username)
            partida.modificationdate = datetime.datetime.now()
            partidas.save()
            return redirect('partidas:listar_partida', clone=0)
    else:
        partida = get_object_or_404(Partida, pk=pk)
        actividad = Actividad.objects.get(pk=partida.actividad_id)
        proyecto = Proyecto.objects.get(pk=actividad.proyecto.pk)
        subprograma = Subprograma.objects.get(pk=proyecto.subprograma.pk)
        programa = Area_Programa.objects.get(pk=subprograma.programa.pk)
        gasto = Item.objects.filter(catalogo__code='CAT_GASTOS').values('name', 'pk', 'code')
        regimen = Item.objects.filter(catalogo__code='CAT_REGIMENES').values('name', 'pk', 'code')

        codigo = programa.code + '.' + subprograma.code + '.' + proyecto.code + '.' + actividad.code
        formPartidas = PartidaCrearForm(instance=partida)

        data = {'formPartidas': formPartidas, 'programa_pk': programa.pk,
                'subprograma_pk': subprograma.pk, 'proyecto_pk': proyecto.pk,
                'actividad_pk': actividad.pk}

        if partida.spending_itm:
            data['gasto_pk'] = int(partida.spending_itm.pk)

        if partida.regime_itm:
            data['regimen_pk'] = int(partida.regime_itm.pk)

    return render(request, template_name, data)


def asignar_partida(request, pk, cedula, template_name='partidas/partida_empleado.html'):
    partida = get_object_or_404(Partida, pk=pk)
    empleados = Employee.objects.filter(person__cedula=cedula)
    mensaje = ''
    employee = ""
    formAsignar = PartidaAsignarForm()
    if Partida.objects.filter(currentemployee__person__cedula=cedula, clonedto=None):
        mensaje = 'El empleado seleccionado ya tiene asignado una partida presupuestaria'
    # elif empleados[0].person.cedula != cedula:
    #     mensaje = 'La cédula ingresada es incorrecta o no esta registrada'

    else:
        if empleados:
            employee = empleados[0]
        elif cedula != '0':
            if cedula != empleados:
                mensaje = 'La cédula ingresada es incorrecta o no esta registrada'




        if request.POST:

            razon_txt = request.POST.get('razon')
            estado = Item.objects.get(code='EST_OCUPADA', catalogo__code='CAT_ESTADOS')
            Historial.objects.create(reason=razon_txt, registerdate=datetime.datetime.now(),
                                     estado=estado, partida=partida, user=request.user)

            EmpleadoHistorial.objects.create(fechaRegistro=datetime.datetime.now(), partida=partida, empleado=employee)

            formAsignar = PartidaAsignarForm(request.POST or None, instance=partida)


        if formAsignar.is_valid():
            estado = Item.objects.get(code='EST_OCUPADA', catalogo__code='CAT_ESTADOS')
            part = formAsignar.save(commit=False)
            part.status_itm = estado
            part.save()
            # messages.success(request, 'Se ha registrado correctamente')
            return redirect('partidas:listar_partida', clone=0)

    return render(request, template_name,
                  {'formAsignar': formAsignar, 'pk': pk, 'empleado': employee, 'mensaje': mensaje})


def historial_partida(request, pk, template_name='partidas/partida_historialEstado.html'):
    partida = get_object_or_404(Partida, pk=pk)
    data = {}
    historiales = Historial.objects.filter(partida=partida)
    data['historiales'] = historiales
    data['partida'] = partida
    data['partida'] = pk
    return render(request, template_name, data, )


def historial_empleado(request, pk, template_name='partidas/partidaHistorialEmpleado.html'):
    partida = get_object_or_404(Partida, pk=pk)
    data = {}
    historialesEmpleado = EmpleadoHistorial.objects.filter(partida=partida)
    data['historialesEmpleado'] = historialesEmpleado
    # data['partida'] = partida
    data['partida'] = pk

    return render(request, template_name, data)


def estado_partida(request, pk, template_name='partidas/partida_cambiarEstado.html'):
    estado_empledo = Catalogo.objects.get(code='CAT_ESTADOS')
    estados = Item.objects.filter(catalogo_id=estado_empledo)
    partida = get_object_or_404(Partida, pk=pk)
    if request.POST:
        estado = Item.objects.get(id=int(request.POST.get('estadoPartida')))

        Partida.objects.filter(pk=pk).update(status_itm=estado)

        motivo_txt = request.POST.get('motivo')
        partida_ref = get_object_or_404(Partida, pk=pk)

        Historial.objects.create(reason=motivo_txt, registerdate=datetime.datetime.now(),
                                 estado=partida_ref.status_itm, partida=partida_ref, user=request.user)

        return redirect('partidas:listar_partida', clone=0)

    data = {'estados': estados, 'partida': pk}

    return render(request, template_name, data)


def liberar_partida(request, pk, empleado, template_name='partidas/partida_modalLiberar.html'):
    partida = get_object_or_404(Partida, pk=pk)
    empleado = get_object_or_404(Employee, pk=empleado)

    estado = Item.objects.get(code='EST_LIBRE', catalogo__code='CAT_ESTADOS')
    if request.POST:
        motivo_txt = request.POST.get('motivo')
        Partida.objects.filter(pk=pk, currentemployee=empleado).update(currentemployee="", status_itm=estado)
        Historial.objects.create(reason=motivo_txt, registerdate=datetime.datetime.now(),
                                 estado=estado, partida=partida, user=request.user)
        EmpleadoHistorial.objects.filter(partida=pk, empleado=empleado).update(fechaSalida=datetime.datetime.now())

        return redirect('partidas:listar_partida', clone=0)
    data = {}
    data = {'partida': pk, 'empleado': empleado}
    return render(request, template_name, data)


def duplicar_partida(request, pk, template_name='partidas/partida_modalDuplicar.html'):
    partida = get_object_or_404(Partida, pk=pk)
    titulo = 'Duplicar Partida Presupuestaria'
    form = PartidaDuplicarForm(request.POST or None, instance=partida)
    data = {}
    data['partida'] = partida
    data['partida'] = pk
    data['titulo'] = titulo
    data['form'] = form

    if request.POST:
        form = PartidaDuplicarForm(request.POST)
        if form.is_valid():
            nueva_partida = form.save(commit=False)
            if partida.status_itm.name == 'OCUPADA':
                catalogo = Catalogo.objects.get(code='CAT_ESTADOS')
                estado = Item.objects.get(catalogo=catalogo, name='LIBRE')
                nueva_partida.status_itm_id = estado.pk

            else:
                nueva_partida.status_itm_id = partida.status_itm.pk

            nueva_partida.code = partida.code
            nueva_partida.actividad = partida.actividad
            nueva_partida.regime_itm = partida.regime_itm
            nueva_partida.spending_itm = partida.spending_itm
            nueva_partida.observation = partida.observation
            nueva_partida.save()
            Historial.objects.create(reason='Partida Duplicada', registerdate=datetime.datetime.now(),
                                     estado=nueva_partida.status_itm,
                                     partida=nueva_partida, user=request.user)
            messages.success(request, 'La partida ha sido duplicada satisfactoriamente')
            return redirect('partidas:listar_partida', clone=0)
    return render(request, template_name, data)


def clonar_partida(request, pk, template_name='partidas/partida_modalClonar.html'):
    partida = get_object_or_404(Partida, pk=pk)
    titulo = 'Clonar Partida Presupuestaria'
    form = PartidaClonarForm(request.POST or None, instance=partida)
    data = {}
    data['partida'] = partida
    data['partida'] = pk
    data['titulo'] = titulo
    data['form'] = form

    if request.POST:
        form = PartidaClonarForm(request.POST)
        if form.is_valid():
            nueva_partida = form.save(commit=False)

            nueva_partida.status_itm_id = partida.status_itm.pk
            nueva_partida.code = partida.code
            nueva_partida.currentemployee = partida.currentemployee
            nueva_partida.group_itm = partida.group_itm
            nueva_partida.category_itm = partida.category_itm
            nueva_partida.charge_itm = partida.charge_itm
            nueva_partida.grade_itm = partida.grade_itm
            nueva_partida.remuneration = partida.remuneration
            nueva_partida.actividad = partida.actividad
            nueva_partida.regime_itm = partida.regime_itm
            nueva_partida.spending_itm = partida.spending_itm
            nueva_partida.observation = partida.observation
            nueva_partida.clonedto = 1
            nueva_partida.save()

            Historial.objects.create(reason='Partida Clonada', registerdate=datetime.datetime.now(),
                                     estado=nueva_partida.status_itm,
                                     partida=nueva_partida, user=request.user)
            EmpleadoHistorial.objects.create(fechaRegistro=datetime.datetime.now(), partida=nueva_partida,
                                             empleado=partida.currentemployee)
            # Partida.objects.filter(pk=pk).update(clonedto=1)
            messages.success(request, 'La partida ha sido clonada satisfactoriamente')
            return redirect('partidas:listar_partida', clone=0)
    return render(request, template_name, data)


class ReportePartidasPDF(View):
    def cabecera(self, pdf):
        fecha = datetime.datetime.now()

        # Utilizamos el archivo logo.png que está guardado en la carpeta media/imagenes
        archivo_imagen = settings.MEDIA_ROOT + '/loja.jpg'
        # Definimos el tamaño de la imagen a cargar y las coordenadas correspondientes
        pdf.drawImage(archivo_imagen, 10, 740, 120, 100, preserveAspectRatio=True)
        # Establecemos el tamaño de letra en 20 y el tipo de letra Helvetica
        pdf.setFont("Helvetica", 20)
        # Dibujamos una cadena en la ubicación X,Y especificada
        pdf.drawString(230, 790, u"MUNICIPIO DE LOJA")
        pdf.setFont("Helvetica", 14)
        pdf.drawString(220, 770, u"Reporte de partidas presupuestarias", )
        pdf.drawString(120, 740, u"Usuario:" + " ")
        pdf.drawString(120, 710, u"Fecha:" + str(fecha.date()))

    def tabla(self, pdf, y):
        # Creamos una tupla de encabezados para nuestra tabla
        encabezados = ('Cédula', 'Nombres', 'Nivel Directivo', 'Dirección Ins.', 'Cargo', 'Remuner.', 'Partida')
        # Creamos una lista de tuplas que van a contener a las personas
        detallesPartida = [(partida.currentemployee, partida.currentemployee, partida.charge_itm, partida.remuneration)
                           for partida in Partida.objects.all()]

        partidas = Partida.objects.all()
        for partida in partidas:
            print(partida.currentemployee.person.cedula)

        # Establecemos el tamaño de cada una de las columnas de la tabla
        detalle_orden = Table([encabezados] + detallesPartida,
                              colWidths=[2 * cm, 3 * cm, 3 * cm, 3 * cm, 3 * cm, 2 * cm, 3 * cm])
        # Aplicamos estilos a las celdas de la tabla
        detalle_orden.setStyle(TableStyle(
            [
                # La primera fila(encabezados) va a estar centrada
                ('ALIGN', (0, 0), (3, 0), 'CENTER'),
                # Los bordes de todas las celdas serán de color negro y con un grosor de 1
                ('GRID', (0, 0), (-1, -1), 1, colors.black),
                # El tamaño de las letras de cada una de las celdas será de 8
                ('FONTSIZE', (0, 0), (-1, -1), 8),
            ]
        ))
        # Establecemos el tamaño de la hoja que ocupará la tabla
        detalle_orden.wrapOn(pdf, 800, 600)
        # Definimos la coordenada donde se dibujará la tabla
        detalle_orden.drawOn(pdf, 30, y)

    def get(self, request, *args, **kwargs):
        # Indicamos el tipo de contenido a devolver, en este caso un pdf
        response = HttpResponse(content_type='application/pdf')
        # La clase io.BytesIO permite tratar un array de bytes como un fichero binario, se utiliza como almacenamiento temporal
        buffer = BytesIO()
        # Canvas nos permite hacer el reporte con coordenadas X y Y
        pdf = canvas.Canvas(buffer)
        # Llamo al método cabecera donde están definidos los datos que aparecen en la cabecera del reporte.
        self.cabecera(pdf)
        y = 600
        self.tabla(pdf, y)
        # Con show page hacemos un corte de página para pasar a la siguiente
        pdf.showPage()
        pdf.save()
        pdf = buffer.getvalue()
        buffer.close()
        response.write(pdf)
        return response


def report_pdf(request, context_dict={}):
    # pasar parametros a imprimir
    return render_to_pdf('partidas/reportes.html', context_dict)
    # return render_to_pdf('partidas/reportes.html', {'title': 'Reportes prueba'})
