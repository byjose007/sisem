import datetime

from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from django.urls import reverse, reverse_lazy
from django.views.decorators.csrf import csrf_exempt

from apps.cambios.forms import CambiosForm
from apps.cambios.models import Cambios
from apps.employee.models import Employee


def cambios_list(request, template_name='cambios/cambios_list.html'):
    cambios = Cambios.objects.all()
    data = {}
    data['object_list'] = cambios
    return render(request, template_name, data)


def cambios_create(request, cedula, template_name='cambios/cambios_form.html'):
    usuario_actual = request.user.first_name + ' ' + request.user.last_name + ' - ' + request.user.username
    form = CambiosForm(request.POST or None, initial={"registro_por": usuario_actual}, cedula=cedula)
    if form.is_valid():
        form.save()
        return redirect('cambios:cambios_list')
    return render(request, template_name, {'form': form, 'create': True})


def cambios_update(request, pk, template_name='cambios/cambios_form.html'):
    cambios = get_object_or_404(Cambios, pk=pk)
    cedula = cambios.empleado.person.cedula
    form = CambiosForm(request.POST or None, instance=cambios, cedula=cedula)

    if form.is_valid():
        form.save()
        return redirect('cambios:cambios_list')
    return render(request, template_name, {'form': form})
