from django import forms
from django.conf import settings
from django.forms import ModelForm, TextInput, ModelChoiceField
# from bootstrap_datepicker.widgets import DatePicker

from apps.employee.models import Employee
from apps.cambios.models import Cambios


class CambiosForm(ModelForm):
    class Meta:
        model = Cambios
        fields = '__all__'
        exclude = ('id',)

    def __init__(self, *args, **kwargs):
        cedula = kwargs.pop('cedula')
        empleado_actual = Employee.objects.filter(person__cedula=cedula)
        super(CambiosForm, self).__init__(*args, **kwargs)
        self.fields['fecha_emision'].widget = TextInput(attrs={'type': 'date'})
        self.fields['fecha_vigente'].widget = TextInput(attrs={'type': 'date'})
        self.fields['registro_por'].widget = TextInput(attrs={'readonly': 'readonly'})
        self.fields['empleado'].queryset = empleado_actual
        if (empleado_actual):
            self.fields['empleado'].initial = empleado_actual[0].pk
