from django.apps import AppConfig


class CambiosConfig(AppConfig):
    name = 'apps.cambios'
