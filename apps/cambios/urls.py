from django.conf.urls import url

from .views import *

urlpatterns = [
    url(r'^$', cambios_list, name='cambios_list'),
    url(r'^new/(?P<cedula>\d+)$', cambios_create, name='cambios_new'),
    url(r'^editar/(?P<pk>\d+)$', cambios_update, name='cambios_edit'),

]
