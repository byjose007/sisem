from django.db import models

from apps.employee.models import Employee
from apps.settings.models import Item


class Cambios(models.Model):
    empleado = models.ForeignKey(Employee, verbose_name='Empleado', on_delete=models.PROTECT)
    estado = models.ForeignKey(Item, on_delete=models.PROTECT,
                                               limit_choices_to={'catalog__code': 'ESTADOACCIONES'},
                                               max_length=25, verbose_name='Estado', blank=True, null=True,
                                               related_name='catalog_estados')
    numero = models.IntegerField(blank=True, null=True, verbose_name='Número')
    fecha_emision = models.DateField(verbose_name='Fecha Emisión', blank=True, null=True)
    fecha_vigente = models.DateField(verbose_name='Fecha Vigente', blank=True, null=True)
    accion = models.ForeignKey(Item, on_delete=models.PROTECT, limit_choices_to={'catalog__code': 'ACCIONES'},
                               max_length=10,verbose_name='Acción', blank=True, null=True)
    cambios_administrativos = models.ForeignKey(Item, on_delete=models.PROTECT,
                                                limit_choices_to={'catalog__code': 'CAMBIOSADMINISTRATIVOS'},
                                                max_length=25, verbose_name='Tipo de Acción', blank=True, null=True,
                                                related_name='catalog_cambios')
    unidad_actual = models.CharField(verbose_name='Unidad Actual', max_length=100)
    dep_actual = models.CharField(verbose_name='Departamento Actual', max_length=100)
    dep_propuesto = models.CharField(verbose_name='Departamento propuesto', max_length=100)
    puesto_actual = models.CharField(verbose_name='Puesto Actual', max_length=100)
    puesto_propuesto = models.CharField(verbose_name='Puesto propuesto', max_length=100)
    grupo_ocupacional = models.CharField(verbose_name='Grupo Ocupacional', max_length=100)
    grupo_ocupacional_propuesto = models.CharField(verbose_name='Grupo Ocupacional propuesto', max_length=100)
    nivel_grado = models.CharField(verbose_name='Nivel o Grado', max_length=100)
    nivel_grado_propuesto = models.CharField(verbose_name='Nivel o Grado propuesto', max_length=100)
    sueldo_actual = models.CharField(verbose_name='Sueldo Actual', max_length=100)
    sueldo_propuesto = models.CharField(verbose_name='Sueldo propuesto', max_length=100)
    lugar_actual = models.CharField(verbose_name='Lugar Actual', max_length=100)
    lugar_propuesto = models.CharField(verbose_name='Lugar propuesto', max_length=100)
    doc_accion = models.CharField(verbose_name='Documento que genera la acción', max_length=100)
    explicacion = models.TextField(max_length=100, verbose_name='explicación')
    registro_por = models.CharField(verbose_name='Registrado por', max_length=100)

    class Meta:
        # ordering = ['empleado']
        verbose_name = 'Cambio Administrativo'
        verbose_name_plural = 'Cambios Administrativos'

    def __str__(self):
        return u'{0}'.format(self.empleado)

    def __unicode__(self):
        return u'{0}'.format(self.empleado)
