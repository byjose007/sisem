from django import forms
from django.contrib.auth.models import User, Group

from django.contrib.auth.forms import AuthenticationForm
from django.forms.widgets import PasswordInput, TextInput

from apps.institution.models import Institucion


class CustomAuthForm(AuthenticationForm):
    username = forms.CharField(label='', widget=forms.TextInput(attrs={'placeholder': 'Usuario'}))
    password = forms.CharField(label='', widget=forms.PasswordInput(attrs={'placeholder': 'Contraseña'}))

class RegistroUserForm(forms.Form):
    cedula = forms.CharField(max_length=10, label='Cédula')
    username = forms.CharField(min_length=5, label='Usuario')
    first_name = forms.CharField(min_length=5, label='Nombre')
    last_name = forms.CharField(min_length=3,label='Apellido')
    email = forms.EmailField(label='Correo')
    rol = forms.ModelChoiceField(queryset=Group.objects.all(), label='Rol')
    password = forms.CharField(min_length=4, widget=forms.PasswordInput(),label='Contraseña')
    password2 = forms.CharField(widget=forms.PasswordInput(),label='Repita Contraseña')



    def clean_cedula (self):
        cedula = self.cleaned_data["cedula"]
        if cedula == None:
            self.add_error('cedula', 'el número de cédula es obligatorio!')
        elif len(cedula) < 10 and len(cedula) <= 13:
            self.add_error('cedula', 'Cédula debe contener 10 o 13 caracteres!')
        return  cedula
    
    def clean_username(self):
        """Comprueba que no exista un username igual en la db"""
        username = self.cleaned_data['username']
        if User.objects.filter(username=username):
            raise forms.ValidationError('Nombre de usuario ya registrado.')
        return username

    def clean_email(self):
        """Comprueba que no exista un email igual en la db"""
        email = self.cleaned_data['email']
        if User.objects.filter(email=email):
            raise forms.ValidationError('Ya existe un email igual en la base de datos.')
        return email

    def clean_password2(self):
        """Comprueba que password y password2 sean iguales."""
        password = self.cleaned_data['password']
        password2 = self.cleaned_data['password2']
        if password != password2:
            raise forms.ValidationError('Las contraseñas no coinciden.')
        return password2

class UpdatePasswordForm(forms.Form):

    password = forms.CharField(min_length=4, widget=forms.PasswordInput(),label='Contraseña')
    password2 = forms.CharField(widget=forms.PasswordInput(),label='Confirmar Contraseña')
    
    # def clean_password2(self):
    #     """Comprueba que password y password2 sean iguales."""
    #     password = self.cleaned_data['password']
    #     password2 = self.cleaned_data['password2']
    #     if password != password2:
    #         raise forms.ValidationError('Las contraseñas no coinciden.')
    #     return password2