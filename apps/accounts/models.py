
from django.db import models
from django.conf import settings
from apps.person.models import Person
from apps.institution.models import Institucion
from django.contrib.auth.models import User, Group


# Create your models here.


class UserProfile(models.Model):
    user = models.OneToOneField(User, verbose_name='Usuario')
    photo = models.ImageField(upload_to='profiles', blank=True, null=True, verbose_name='Foto')
    # institution = models.ManyToManyField(Institution, verbose_name='Institución')
    institution_default = models.ForeignKey(Institucion, verbose_name='Institución por defecto',blank=True, null=True)
    # cedula = models.CharField(max_length=10,blank=False, null=False, verbose_name='Cédula')
    # rol = models.ForeignKey(Group,verbose_name='Rol')
    
    class Meta:
    	
    	verbose_name = 'Perfil de Usuario'
    	verbose_name_plural = 'Perfiles de Usuario'

    def __str__(self):
    	return u'{0}'.format(self.user)

    def __unicode__(self):
    	return u'{0}'.format(self.user)

