from django.conf.urls import url

from apps.accounts.views import api_user, api_roles
from . import views
# from rest_framework import routers
#
#
# router = routers.DefaultRouter()
# rest_base_url = "api/"

urlpatterns = [
    url(r'^user/new$', views.registro_usuario_view, name='user_new'),
    url(r'^user/$', views.user_list, name='user_list'),
    url(r'^user/(?P<pk>\d+)$', views.user_detail, name='user_detail'),
    url(r'^user/update_default_institution/(?P<pk>\d+)$', views.user_profile_update_institution, name='user_config_update'),
    url(r'^user/profile/(?P<pk>\d+)$', views.user_profile, name='user_profile'),
    url(r'^user/list/', views.user_list_user, name='user_list_user'),
    url(r'^user/delete/(?P<pk>\d+)$', views.user_delete, name='user_delete'),


#    ----------------------Api-----------------------------------------

    # API --- VUE
    url(r'^api_user/(?P<cedula>\d+)$', api_user, name='api_user'),
    url(r'^api_roles/$', api_roles, name='api_roles'),
    # url('^api/persona/(?P<cedula>[0-9]+)$', views.buscarPersonalViewSet.as_view()),



]

