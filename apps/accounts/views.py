import json

from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Q
from django.shortcuts import render
from django.shortcuts import redirect
from django.contrib.auth.models import User
from django.contrib.auth.models import Permission
from django.contrib.auth.models import Group
from django.shortcuts import get_object_or_404
from django.utils.encoding import force_text
from django.views.decorators.csrf import csrf_exempt

from apps.employee.models import Employee
from apps.person.models import Person
from .models import UserProfile
from .models import Institucion
from django.core.urlresolvers import reverse_lazy, reverse
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse

# from rest_framework import generics

# Create your views here.
from .forms import RegistroUserForm, UpdatePasswordForm


def registro_usuario_view(request, template_name='accounts/user_form.html'):
    if request.method == 'POST':

        # form = RegistroUserForm(request.POST, request.FILES)
        usuario = request.POST.get('usuario')
        email = request.POST.get('email')
        password = request.POST.get('password')
        nombre = request.POST.get('nombres')
        apellidos = request.POST.get('apellidos')
        institucion = request.POST.get('institucion')
        foto = request.POST.get('foto')
        rol_id = request.POST.get('rol')

        # Add User
        user = User.objects.create_user(username=usuario,email=email, password=password, first_name=nombre, last_name=apellidos)
        user.save()
        # Add grupo
        grupo = Group.objects.get(pk=rol_id)
        user.groups.add(grupo)
        # Add perfil
        perfil = UserProfile(user_id=user.id, photo=foto, institution_default_id=institucion)
        perfil.save()
        mensaje = 'El usuario se ha creado correctamente'
        userList = getUsers(request)
        return render(request, 'accounts/user_list.html', {'usuarios': userList,'mensaje':mensaje})
    else:

        form = RegistroUserForm()


    context = {
        'form': form,

    }
    return render(request, template_name, context)


def user_list(request, template_name='accounts/user_list.html'):
    userList = getUsers(request)
    return render(request, template_name, {'usuarios': userList})


# Obtener todos los usuarios con sus roles
def getUsers(request):
    users = User.objects.order_by('-date_joined')
    permisos = request.user.get_group_permissions()
    # print(request.user.groups.all())
    userList = []
    for usuario in users:
        roles = usuario.groups.all()
        # print(roles)
        userList.append(
            {
                'user': usuario,
                'roles': roles
            }
        )
    return  userList


def user_detail(request, pk, template_name='accounts/user_detail.html'):
    usuario = get_object_or_404(User, pk=pk)
    grupos = usuario.groups.all()
    permisos = usuario.get_all_permissions()

    print(grupos)

    context = {
        "usuario": usuario,
        "grupos": grupos,
        "permisos": permisos,
    }
    return render(request, template_name, context)


def user_profile(request, pk, template_name='accounts/user_profile.html'):
    usuario = get_object_or_404(User, pk=pk)

    perfil = UserProfile.objects.get(user=usuario)
    grupos = usuario.groups.all()
    permisos = usuario.get_all_permissions()
    empleado = Employee.objects.get(person__cedula=usuario.username)
    context = {
        "usuario": usuario,
        "perfil": perfil,
        "grupos": grupos,
        "permisos": permisos,
        "empleado": empleado
    }


    if request.method == 'POST':
        usuario.set_password(request.POST.get('password'))
        usuario.save()
        context['mensaje'] = 'La contraseña se guardo correctamente!'
        return render(request, template_name, context)
        # return redirect('accounts:user_profile', pk)

    return render(request, template_name, context)


def user_profile_update_institution(request, pk, template_name='accounts/user_profile.html'):
    print(pk)
    usuario = get_object_or_404(User, username=request.user)
    user_profile = UserProfile.objects.get(user=request.user)
    institution = get_object_or_404(Institucion, pk=pk)
    user_profile.institution_default = institution
    user_profile.save()
    institution = user_profile.institution_default
    request.session['institution_id'] = institution.id
    url = reverse('accounts:user_config')
    return HttpResponseRedirect(url)

def user_delete(request,pk, template_name='accounts/user_list.html'):
    userRemove = User.objects.get(pk=pk)
    userRemove.delete()
    mensaje = 'El usuario se ha eliminado correctamente'
    if(userRemove == request.user):
        url = reverse_lazy('security_main')
        return redirect(url)
    else:
        userList = getUsers(request)
        return render(request, template_name, {'usuarios': userList, 'mensaje': mensaje})


    # -------------------------------Usuarios-------------


def user_list_user(request, template_name='accounts/user_list_user.html'):
    user_profile = UserProfile.objects.get(user=request.user)

    institution = user_profile.institution_default
    user = UserProfile.objects.filter(institution_default=institution)
    # user = User.objects.all()
    data = {}
    data['object_list'] = user
    print(user_profile.user.username)
    print(user_profile.user.first_name)
    return render(request, template_name, data)


# -------------------------------Api buscar empleado-------------
@csrf_exempt
def api_user(request, cedula):
    empleado = Employee.objects.filter(person__cedula=cedula).values('institution','person__cedula','person__name','person__lastname', 'person__email', 'person__photo')
    # if empleado.count() >= 1:
    existeUSer = User.objects.filter(username=cedula)
    if empleado.__len__() <= 0:
        mensaje = 'El empleado con número de cédula '+ cedula + ' no existe!'
        return JsonResponse({'mensaje':mensaje})
    elif existeUSer:
        mensaje = 'El empleado con número de cédula ' + cedula + ' ya es un usuario del sistema!'
        return JsonResponse({'mensaje': mensaje})
    else:
        datos_empleado = list(empleado)
        # datosEmpleado = serializers.serialize('json', list(empleado), cls=LazyEncoder)
        return JsonResponse(datos_empleado, safe=False)

    # else:
    #     return HttpResponse('No existe un empleado con éste número de cédula')



@csrf_exempt
def api_roles(request):
    rolesAll = Group.objects.all().values('name', 'pk')
    roles = list(rolesAll)
    return JsonResponse(roles, safe=False)

