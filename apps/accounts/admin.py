
from django.contrib import admin
from .models import UserProfile


# Register your models here.

class UserProfileAdmin(admin.ModelAdmin):
	list_display = ('user','institution_default')
	search_fields = ('user','institution_default')





admin.site.register(UserProfile, UserProfileAdmin)