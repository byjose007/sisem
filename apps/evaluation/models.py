from django.db import models
from django.core.validators import MinValueValidator
from django.core.validators import MaxValueValidator
import datetime
from datetime import date
from decimal import Decimal
from django.db.models import Q
from apps.settings.models import Catalog, Item
from apps.institution.models import Institucion, JefaturaCoordinacion, Directivo, Direccion
# from apps.institution.models import Institution, InstitutionalUnit
from apps.contract.models import Contract, LaborSystem, TypeOfContract
from apps.person.models import Person
from apps.employee.models import Employee
from django.core.validators import RegexValidator
from .validators import valid_extension
import os.path

# Create your models here.

YEAR_CHOICES = []
for r in range(1990, (datetime.datetime.now().year + 1)):
    YEAR_CHOICES.append((r, r))

STATES_YES_NO_CHOICES = (
    ('YES', 'Sí'),
    ('NOT', 'No'),
)

STATES_PERFOM_EVAL_CHOICES = (
    ('GENERATED', 'Generada'),
    ('INITIATED', 'Iniciada'),
    ('FINALIZED', 'Finalizada'),
)

STATES_INST_UNIT_EVAL_CHOICES = (
    ('GENERATED', 'Generada'),
    ('INITIATED', 'Iniciada'),
    ('FINALIZED', 'Finalizada'),
)

STATES_PERSON_EVAL_CHOICES = (
    ('GENERATED', 'Generada'),
    ('INITIATED', 'Iniciada'),
    ('FINALIZED', 'Finalizada'),
    ('CANCELED', 'Anulada'),
)

LEVEL_KNOWLEDGE_CHOICES = (
    ('OUTSTANDING', 'Sobresaliente'),
    ('VERYGOOD', 'Muy bueno'),
    ('GOOD', 'Bueno'),
    ('REGULAR', 'Regular'),
    ('INSUFFICIENT', 'Insuficiente'),
)

RELEVANCE_LEVEL_CHOICES = (
    ('HIGH', 'Alta'),
    ('HALF', 'Media'),
    ('SHORT', 'Baja'),
)

DEVELOPMENT_LEVEL_CHOICES = (
    ('HIGHLY_DEVELOPED', 'Altamente desarrollada'),
    ('DEVELOPED', 'Desarrollada'),
    ('MODERATELY_DEVELOPED', 'Medianamente desarrollada'),
    ('LITTLE_DEVELOPED', 'Poco desarrollada'),
    ('NO_DEVELOPED', 'No desarrollada'),
)


# evaluacion de rendimiento
class PerformanceEvaluation(models.Model):
    year = models.IntegerField(verbose_name='Año',
                               validators=[
                                   MinValueValidator(1990),
                                   MaxValueValidator(3000)], choices=YEAR_CHOICES, default=datetime.datetime.now().year,
                               error_messages={
                                   'unique': "El año para la Evaluación de desempeño ya ha sido registrado."})  # Anio de evaluacion
    start_date = models.DateField(verbose_name='Fecha de inicio')  # Fecha de inicio
    end_date = models.DateField(verbose_name='Fecha de fin')  # Fecha de finalizacion
    state = models.CharField(verbose_name='Estado', max_length=10,
                             choices=STATES_PERFOM_EVAL_CHOICES,
                             default='GENERATED')  # Estado de la evaluación
    quantity_evaluated = models.IntegerField(verbose_name='Cantidad de evaluados',
                                             default=0)  # Cantidad de personas evaluadas
    percentage_evaluation = models.DecimalField(verbose_name='Porcentage final', max_digits=4, decimal_places=2,
                                                default=Decimal('0.00'))  # Porcentage final de la evaluacion
    activity_factor = models.DecimalField(verbose_name='Factor evaluación actividades', max_digits=4, decimal_places=2,
                                          blank=True, null=True)  # Factor evaluacion de gestion del puesto
    advance_goals_percentage = models.DecimalField(verbose_name='Porcentage por adelantar metas', max_digits=4,
                                                   decimal_places=2)  # Porcentage de aumento por adelanto de objetivos
    knowledge_factor = models.DecimalField(verbose_name='Factor evaluación de conocimientos', max_digits=4,
                                           decimal_places=2)  # Factor evaluacion de porcentage de conocimientos
    technical_competence_factor = models.DecimalField(verbose_name='Factor para evaluación de competencias técnicas',
                                                      max_digits=4,
                                                      decimal_places=2)  # Factor evaluacion de competencias técnicas
    universal_competence_factor = models.DecimalField(verbose_name='Factor evaluación de cimpetencias universales',
                                                      max_digits=4,
                                                      decimal_places=2)  # Factor evaluacion de comptencias universales
    teamwork_factor = models.DecimalField(verbose_name='Factor de evaluación de trabajo en equipo', max_digits=4,
                                          decimal_places=2)  # Factor evaluacion de trabajo en equipo
    reduction_complaints_percentage = models.DecimalField(verbose_name='Porcentage de reducción por quejas',
                                                          max_digits=4,
                                                          decimal_places=2)  # Porcentage de reduccion por quejas
    institution = models.ForeignKey(Institucion, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return '{}  {}'.format(self.year, self.state)

    # class Meta:
    #        unique_together = ('user', 'post')

    class Meta:
        unique_together = (('year', 'institution'))

        # def unique_error_message(self, model_class, unique_check):
        # 	print('>>>>> INTRO CHECK')
        # 	return 'My custom error message'
        # if model_class == type(self) and unique_check == ('year', 'institution'):
        # 	print('>>>>> 1')
        # 	return 'My custom error message'
        # else:
        # 	print('>>>>> 2')
        # 	return super(PerformanceEvaluation, self).unique_error_message(model_class, unique_check)

        # def unique_error_message(self, model_class, unique_check):
        # 	if model_class == type(self) and unique_check == ('year', 'institution'):
        # 		return 'Your custom error message.'
        # 	else:
        # 		return super(PerformanceEvaluation, self).unique_error_message(model_class, unique_check)


class CatalogTechnicalCompetence(models.Model):
    # skill = models.TextField(verbose_name='Destreza')#Destreza tecnica
    skill = models.ForeignKey(Item, on_delete=models.PROTECT, limit_choices_to={'catalog__code': 'DESTREZATECNICA'},
                              verbose_name='Destreza técnica')
    relevance = models.CharField(verbose_name='Relevancia', max_length=15,
                                 choices=RELEVANCE_LEVEL_CHOICES, )  # Relevancia de la destreza
    observable_behavior = models.TextField(
        verbose_name='Comportamiento observable')  # Comportamiento observable frete a la competencia tecnica
    performance_evaluation = models.ForeignKey(PerformanceEvaluation, on_delete=models.CASCADE,
                                               verbose_name='Evaluación de desempeño')  # Referencia a la evaluacion de desempeño que pertenece

    def __str__(self):
        return '%s - %s ' % (self.skill.name, self.get_relevance_display())


class CatalogUniversalCompetence(models.Model):
    skill = models.ForeignKey(Item, on_delete=models.PROTECT, limit_choices_to={'catalog__code': 'DESTREZAUNIVERSAL'},
                              verbose_name='Destreza universal')
    relevance = models.CharField(verbose_name='Relevancia', max_length=15,
                                 choices=RELEVANCE_LEVEL_CHOICES, )  # Relevancia de la destreza
    observable_behavior = models.TextField(
        verbose_name='Comportamiento observable')  # Comportamiento observable frete a la competencia tecnica
    performance_evaluation = models.ForeignKey(PerformanceEvaluation, on_delete=models.CASCADE,
                                               verbose_name='Evaluación de desempeño')  # Referencia a la evaluacion de desempeño que pertenece

    def __str__(self):
        return '%s - %s' % (self.skill.name, self.get_relevance_display())


class CatalogTeamwork(models.Model):
    ability = models.ForeignKey(Item, on_delete=models.PROTECT, limit_choices_to={'catalog__code': 'TRABAJOEQUIPO'},
                                verbose_name='Habilidad')
    relevance = models.CharField(verbose_name='Relevancia', max_length=15,
                                 choices=RELEVANCE_LEVEL_CHOICES, )  # Relevancia de la destreza
    observable_behavior = models.TextField(
        verbose_name='Comportamiento observable')  # Comportamiento observable frete a la competencia tecnica
    performance_evaluation = models.ForeignKey(PerformanceEvaluation, on_delete=models.CASCADE,
                                               verbose_name='Evaluación de desempeño')  # Referencia a la evaluacion de desempeño que pertenece

    def __str__(self):
        return '%s - %s' % (self.ability.name, self.get_relevance_display())


class TypeContractsEvaluate(models.Model):
    type_contract = models.ForeignKey(TypeOfContract, on_delete=models.PROTECT,
                                      verbose_name='Tipo de contrato')  # Referencia al tipo de contrato
    performance_evaluation = models.ForeignKey(PerformanceEvaluation, on_delete=models.CASCADE,
                                               verbose_name='Evaluación de desempeño')  # Referencia a la evaluacion de desempeño que pertenece


class InstitutionalUnitEvaluation(models.Model):
    start_date = models.DateField(verbose_name='Fecha de inicio')  # Fecha de inicio
    end_date = models.DateField(verbose_name='Fecha de fin')  # Fecha de finalizacion
    temporary_activation = models.BooleanField(verbose_name='Activación temporal', default=False)
    state = models.CharField(verbose_name='Estado', max_length=10,
                             choices=STATES_INST_UNIT_EVAL_CHOICES,
                             default='GENERATED')  # Estado de la evaluación
    quantity_evaluated = models.IntegerField(verbose_name='Cantidad de evaluados',
                                             default=Decimal('0.00'))  # Cantidad de personas evaluadas
    percentage_evaluation = models.DecimalField(verbose_name='Porcentage final', max_digits=4, decimal_places=2,
                                                default=Decimal('0.00'))  # Porcentage final de la evaluacion
    institutional_unit = models.ForeignKey(Directivo, on_delete=models.CASCADE,
                                           verbose_name='Unidad institucional')
    performance_evaluation = models.ForeignKey(PerformanceEvaluation, on_delete=models.CASCADE,
                                               verbose_name='Evaluación de desempeño')  # Referencia a la evaluacion de desempeño a la que pertence

    def __str__(self):
        return '{} {}'.format(self.institutional_unit.institutional_unit, self.performance_evaluation.year)

        # def clean(self):
        # 	print('intro >>> clean')
        # 	print(self.end_date)
        # 	print(self.performance_evaluation)
        # 	inst_unit_eval = InstitutionalUnitEvaluation.objects.get(
        # 		Q(performance_evaluation__pk=self.performance_evaluation.pk) & Q(institutional_unit__pk = self.institutional_unit.pk)
        # 	)
        # 	print(inst_unit_eval)
        # 	if self.status == 'draft' and self.pub_date is not None:
        # 		raise ValidationError(_('Draft entries may not have a publication date.'))


class PersonalEvaluation(models.Model):
    def get_self_institutional_unit(self):
        print('unidad institucional >>> %s' % (self.institutional_unit_evaluation.institutional_unit))
        return self.institutional_unit_evaluation.institutional_unit

    def _path_signed_document(self):
        # file will be uploaded to MEDIA_ROOT/evaluations/<year>/<cedula>/
        return 'evaluations/{0}/{1}/'.format(self.institutional_unit_evaluation.performance_evaluation.year,
                                             self.evaluated.person.cedula)

    path = property(_path_signed_document)
    start_date = models.DateField(verbose_name='Fecha de inicio',
                                  default=date.today)  # Fecha de inicio de la evaluacion
    end_date = models.DateField(verbose_name='Fecha de fin', blank=True, null=True)  # Fecha de fin de la evaluacion
    period_from = models.DateField(verbose_name='Período desde')  # Periodo desde de la evaluacion
    period_to = models.DateField(verbose_name='Período hasta')  # Periodo hasta de la evaluacion
    state = models.CharField(verbose_name='Estado', max_length=10,
                             choices=STATES_PERSON_EVAL_CHOICES,
                             default='GENERATED')  # Estado de la evaluacion
    job_management_indicators = models.DecimalField(verbose_name='Porcentaje total de actividades', max_digits=5,
                                                    decimal_places=2)  # Resultado referente a la gestion de trabajos
    knowledge = models.DecimalField(verbose_name='Porcentaje total de conocimientos', max_digits=5,
                                    decimal_places=2)  # Resultado referente a los conocimientos
    technical_competence = models.DecimalField(verbose_name='Porcentaje de competencias técnicas', max_digits=5,
                                               decimal_places=2)  # Resultado referente a las competencias tecnicas
    universal_competence = models.DecimalField(verbose_name='Porcentaje de competencias universales', max_digits=5,
                                               decimal_places=2)  # Resultado referente a las competencias universales
    teamwork = models.DecimalField(verbose_name='Porcentaje de trabajo en equipo', max_digits=5,
                                   decimal_places=2)  # Resultado referente con respecto al trabajo en equipo
    evaluation_citizens = models.DecimalField(verbose_name='Porcentaje de reducción por quejas', max_digits=5,
                                              decimal_places=2)  # Resultado referente a la evaluacion de ciudadanos
    final_evaluation_percentage = models.DecimalField(verbose_name='Porcentaje final de la evaluación', max_digits=5,
                                                      decimal_places=2)  # Resultado final del porcentaje final de la evaluacion
    final_evaluation_result = models.CharField(verbose_name='Resultado final de la evaluación',
                                               max_length=100)  # Resultado final en en palabras de la evaluacion
    evaluated = models.ForeignKey(Employee, related_name='person_evaluated', on_delete=models.CASCADE,
                                  verbose_name='Evaluado')
    contract = models.ForeignKey(Contract, on_delete=models.PROTECT)
    immediate_superior = models.ForeignKey(Employee, related_name='person_immediate_superior', on_delete=models.CASCADE,
                                           verbose_name='Jefe inmediato superior')  # Referencia al jefe inmediato duperior
    institutional_unit_director = models.ForeignKey(Employee, related_name='person_institutional_unit_director',
                                                    on_delete=models.CASCADE,
                                                    verbose_name='Director de la unidad institucional')  # Referencia al jefe la unidad institucional
    institutional_unit_evaluation = models.ForeignKey(InstitutionalUnitEvaluation, on_delete=models.CASCADE, blank=True,
                                                      null=True,
                                                      verbose_name='Unidad institucional a evaluar')  # Referencia a la unidad institucional a evaluar

    # signed_evaluation = models.FileField(upload_to= path)



    def get_evaluation_percentage(self):
        if self.final_evaluation_percentage > Decimal('0.00'):
            return 'TOTAL EVALUACIÓN DEL DESEMPEÑO: %s' % (self.final_evaluation_percentage)
        else:
            return 'PROCESO INCORRECTO'


class ExtensionValidator(RegexValidator):
    def __init__(self, extensions, message=None):
        if not hasattr(extensions, '__iter__'):
            extensions = [extensions]
        regex = '\.(%s)$' % '|'.join(extensions)
        if message is None:
            message = 'File type not supported. Accepted types are: %s.' % ', '.join(extensions)
        super(ExtensionValidator, self).__init__(regex, message)

    def __call__(self, value):
        super(ExtensionValidator, self).__call__(value.name)


def generate_path(instance, filename):
    print(instance)
    folder = "modelo_" + str(instance.personal_evaluation.evaluated.person.cedula)
    return os.path.join("modelo", folder, filename)


class Report(models.Model):
    def path_signed_document(self):
        print('intro >>> path_signed_document')
        print('>>> %s' % (self.personal_evaluation.institutional_unit_evaluation.performance_evaluation.year))
        return 'evaluations/{0}/{1}/'.format(
            self.personal_evaluation.institutional_unit_evaluation.performance_evaluation.year,
            self.personal_evaluation.evaluated.person.cedula)

    description = models.CharField(max_length=255, blank=True, verbose_name='Descripción')
    # document = models.FileField(upload_to= 'documents/%Y/%m/%d/', validators=[valid_extension])
    document = models.FileField(upload_to=generate_path, validators=[valid_extension], verbose_name='Documento')
    uploaded_at = models.DateTimeField(auto_now_add=True)
    # personal_evaluation = models.ForeignKey(
    # 		PersonalEvaluation,
    # 		on_delete=models.CASCADE,
    # 		verbose_name='Evaluación personal'
    # 	)#Hace referencia a la evaluacion personal a la que pertence
    personal_evaluation = models.OneToOneField(
        PersonalEvaluation,
        on_delete=models.CASCADE,
        verbose_name='Evaluación personal'
    )  # Hace referencia a la evaluacion personal a la que pertence

    def save(self, *args, **kwargs):
        if self.pk is not None:
            print('>>> execute update')
            orig = Report.objects.get(pk=self.pk)
            orig_path = str(orig.document).split("/")
            print('>>> old.document: %s - new.document: %s' % (orig_path[2], self.document))
            if (orig_path[2] != self.document):
                print('>>> diferent name')
                if os.path.isfile(orig.document.path):
                    os.remove(orig.document.path)
        else:
            print('>>> execute create')
        super(Report, self).save(*args, **kwargs)


class ActivityEvaluation(models.Model):
    number_activities = models.IntegerField(verbose_name='Número de actividades',
                                            validators=[
                                                MinValueValidator(0),
                                                MaxValueValidator(50)], default=0)  # Total de actividades ingresadas
    factor = models.DecimalField(verbose_name='Factor', max_digits=4,
                                 decimal_places=2)  # Factor de porcentaje para el parametro a evaluar
    applies_advance_goals = models.CharField(verbose_name='Aplica porcentage de avance', max_length=3,
                                             choices=STATES_YES_NO_CHOICES,
                                             default='NOT')  # Si aplica porcentage de aumento por avance de metas
    percentage_increase = models.DecimalField(verbose_name='Porcentage de aumento', max_digits=4, decimal_places=2,
                                              default=Decimal('0.00'))  # Valor de aumento por avance en metas
    percentage_final = models.DecimalField(verbose_name='Porcentage final', max_digits=4, decimal_places=2,
                                           default=Decimal(
                                               '0.00'))  # Porcentage final de la evaluacion de las actividades
    personal_evaluation = models.ForeignKey(PersonalEvaluation, on_delete=models.CASCADE,
                                            verbose_name='Evaluación personal')  # Hace referencia a la evaluacion personal a la que pertence


class Activity(models.Model):
    description = models.TextField(verbose_name='Descripción')  # Descripcion de la actividad
    indicator = models.TextField(verbose_name='Indicador')  # Indicador que hace referencia a las actividades realizadas
    period_goal = models.DecimalField(verbose_name='Metas del período', max_digits=4,
                                      decimal_places=2)  # Metas del periodo
    complete_goal = models.DecimalField(verbose_name='Metas cumplidas', max_digits=4,
                                        decimal_places=2)  # Metas completadas
    percentage_fulfillment = models.DecimalField(verbose_name='Porcentaje de cumplimiento', max_digits=5,
                                                 decimal_places=2)  # Porcentaje de cumplimiento
    level_fulfillment = models.DecimalField(verbose_name='Nivel de cumplimiento', max_digits=4,
                                            decimal_places=2)  # Nivel de cumplimiento
    activity_evaluation = models.ForeignKey(ActivityEvaluation, on_delete=models.CASCADE,
                                            verbose_name='Evaluación de actividades')  # Hace referencia a la evaluacion de actividades a la que pertence


class KnowledgeEvaluation(models.Model):  # Evaluacion de conocimiento
    number_knowledge = models.IntegerField(verbose_name='Número de conocimientos',
                                           validators=[
                                               MinValueValidator(0),
                                               MaxValueValidator(50)], default=0)  # Numero de conocimientos ingresados
    factor = models.DecimalField(verbose_name='Factor', max_digits=4,
                                 decimal_places=2)  # Factor de porcentaje para el parametro a evaluar
    percentage_final = models.DecimalField(verbose_name='Porcentaje final', max_digits=4, decimal_places=2,
                                           default=Decimal('0.00'))  # Porcentaje final de la evaluacíon de concimientos
    personal_evaluation = models.ForeignKey(PersonalEvaluation, on_delete=models.CASCADE,
                                            verbose_name='Evaluación personal')  # Hace referenecia a la evaluacion personal a la que pertenece


class Knowledge(models.Model):  # Conocimiento
    knowledge = models.TextField(verbose_name='Conocimiento')  # Descripcion del conocimiento
    level_knowledge = models.CharField(verbose_name='Nivel de conocimiento', max_length=15,
                                       choices=LEVEL_KNOWLEDGE_CHOICES,
                                       default='GOOD')  # Nivel de conocimiento
    knowledge_evaluation = models.ForeignKey(KnowledgeEvaluation, on_delete=models.CASCADE,
                                             verbose_name='Evaluación de conocimiento')  # Hace referencia a la evaluación del concimiento al que pertenece


class TechnicalCompetenceEvaluation(models.Model):  # Evaluacion competencia tecnica
    number_competence = models.IntegerField(verbose_name='Número de competencias',
                                            validators=[
                                                MinValueValidator(0),
                                                MaxValueValidator(50)],
                                            default=0)  # Número total de competencias ingresadas
    factor = models.DecimalField(verbose_name='Factor', max_digits=4,
                                 decimal_places=2)  # Factor que representa el porcentaje del parametro a evaluar
    percentage_final = models.DecimalField(verbose_name='Porcentaje final', max_digits=4, decimal_places=2,
                                           default=Decimal(
                                               '0.00'))  # Porcentaje final de la evaluacion de competencias tecnicas
    personal_evaluation = models.ForeignKey(PersonalEvaluation, on_delete=models.CASCADE,
                                            verbose_name='Evaluación personal')  ##Hace referencia a la evaluacion personal a la que pertenece


class TechnicalCompetence(models.Model):  # Competencia tecnica
    HIGHLY_DEVELOPED = 5
    DEVELOPED = 4
    MODERATELY_DEVELOPED = 3
    LITTLE_DEVELOPED = 2
    NO_DEVELOPED = 1
    LEVEL_TECHNICAL_DEVELOPMENT_CHOICES = (
        (HIGHLY_DEVELOPED, 'Altamente desarrollada'),
        (DEVELOPED, 'Desarrollada'),
        (MODERATELY_DEVELOPED, 'Medianamente desarrollada'),
        (LITTLE_DEVELOPED, 'Poco desarrollada'),
        (NO_DEVELOPED, 'No desarrollada'),
    )
    # skill = models.TextField(verbose_name='Destreza', blank=True, null=True)#Destreza tecnica
    # relevance = models.CharField(verbose_name='Relevancia', max_length=15,
    # choices=RELEVANCE_LEVEL_CHOICES,)#Relevancia de la destreza
    # observable_behavior = models.TextField(verbose_name='Comportamiento observable')#Comportamiento observable referente a la destreza
    development_level = models.IntegerField(verbose_name='Nivel de desarrollo',
                                            choices=LEVEL_TECHNICAL_DEVELOPMENT_CHOICES)  # Nivel de desarrollo
    catalog_technical_competence = models.ForeignKey(CatalogTechnicalCompetence, on_delete=models.PROTECT)
    technical_competence_evaluation = models.ForeignKey(TechnicalCompetenceEvaluation, on_delete=models.CASCADE,
                                                        verbose_name='Evaluación competencia técnica')  # Hace referencia a la evaluacion tecnica a la que pertenece

    def item_attribute(self):
        item = Item.objects.get(pk=int(self.skill))
        return item.name


class UniversalCompetenceEvaluation(models.Model):  # Evaluacion competencia universal
    number_competence = models.IntegerField(verbose_name='Número de competencias',
                                            validators=[
                                                MinValueValidator(0),
                                                MaxValueValidator(50)],
                                            default=0)  # Indica el número de competencias universales ingresadas
    factor = models.DecimalField(verbose_name='Factor', max_digits=4,
                                 decimal_places=2, )  # Factor que representa el porcentaje del parametro a evaluar
    percentage_final = models.DecimalField(verbose_name='Porcentaje final', max_digits=4, decimal_places=2,
                                           default=Decimal(
                                               '0.00'))  # Porcentaje final de la evaluacion de las competencias universales
    personal_evaluation = models.ForeignKey(PersonalEvaluation, on_delete=models.CASCADE,
                                            verbose_name='Evaluación personal')  # Hace referencia a la evaluacion personal a ala que pertence


class UniversalCompetence(models.Model):
    ALWAYS = 5
    FREQUENTLY = 4
    EVER = 3
    RARELY = 2
    NEVER = 1
    FREQUENCY_APPLICATION_CHOICES = (
        (ALWAYS, 'Siempre'),
        (FREQUENTLY, 'Frecuentemente'),
        (EVER, 'Alguna vez'),
        (RARELY, 'Rara vez'),
        (NEVER, 'Nunca'),
    )
    # skill = models.TextField(verbose_name='Destreza')#Destreza tecnica
    # relevance = models.CharField(verbose_name='Relevancia', max_length=15,
    # 	choices=RELEVANCE_LEVEL_CHOICES,)#Relevancia de la destreza
    # observable_behavior = models.TextField(verbose_name='Comportamiento observable')#Comportamiento observable referente a la destreza
    frequency_application = models.IntegerField(verbose_name='Frecuencia de aplicación',
                                                choices=FREQUENCY_APPLICATION_CHOICES)  # Nivel de desarrollo
    catalog_universal_competence = models.ForeignKey(CatalogUniversalCompetence, on_delete=models.PROTECT)
    universal_competence_evaluation = models.ForeignKey(UniversalCompetenceEvaluation, on_delete=models.CASCADE)

    def item_attribute(self):
        item = Item.objects.get(pk=int(self.skill))
        return item.name


class TeamworkEvaluation(models.Model):
    number_variables = models.IntegerField(
        validators=[
            MinValueValidator(0),
            MaxValueValidator(50)], default=0)
    factor = models.DecimalField(max_digits=4, decimal_places=2)
    percentage_final = models.DecimalField(max_digits=4, decimal_places=2, default=Decimal('0.00'))
    personal_evaluation = models.ForeignKey(PersonalEvaluation, on_delete=models.CASCADE)


class Teamwork(models.Model):
    ALWAYS = 5
    FREQUENTLY = 4
    EVER = 3
    RARELY = 2
    NEVER = 1
    FREQUENCY_APPLICATION_CHOICES = (
        (ALWAYS, 'Siempre'),
        (FREQUENTLY, 'Frecuentemente'),
        (EVER, 'Alguna vez'),
        (RARELY, 'Rara vez'),
        (NEVER, 'Nunca'),
    )
    # ability = models.TextField(verbose_name='Habilidad')#Destreza tecnica
    # relevance = models.CharField(verbose_name='Relevancia', max_length=15,
    # choices=RELEVANCE_LEVEL_CHOICES,)#Relevancia de la destreza
    # observable_behavior = models.TextField(verbose_name='Comportamiento observable')
    frequency_application = models.IntegerField(verbose_name='Frecuencia de aplicación',
                                                choices=FREQUENCY_APPLICATION_CHOICES)  # Nivel de desarrollo
    catalog_teamwork = models.ForeignKey(CatalogTeamwork, on_delete=models.PROTECT)
    teamwork_evaluation = models.ForeignKey(TeamworkEvaluation, on_delete=models.CASCADE)

    def item_attribute(self):
        item = Item.objects.get(pk=int(self.ability))
        return item.name


class ChiefObservation(models.Model):
    description = models.TextField(verbose_name='Descripción')
    personal_evaluation = models.ForeignKey(PersonalEvaluation, on_delete=models.CASCADE)


class ComplaintEvaluation(models.Model):
    number_complaint = models.IntegerField(
        validators=[
            MinValueValidator(0),
            MaxValueValidator(50)], default=0, verbose_name='Número de quejas')  # Número total de quejas
    percentage_reduction = models.DecimalField(max_digits=4, decimal_places=2,
                                               verbose_name='Porcentaje de reducción')  # Porcentaje de reducción para cada queja
    percentage_final = models.DecimalField(max_digits=4, decimal_places=2, default=Decimal('0.00'),
                                           verbose_name='Porcentaje final')  # Porcentaje total de reducción
    personal_evaluation = models.ForeignKey(PersonalEvaluation, on_delete=models.CASCADE,
                                            verbose_name='Evaluación de personal')  # Evaluacion de personal a la que pertenece


class Complaint(models.Model):
    person_making_complaint = models.CharField(max_length=100,
                                               verbose_name='Persona realiza la denuncia')  # Indica el nombre de la persona que realiza la denuncia
    identification_making_complaint = models.CharField(max_length=100, blank=True, null=True,
                                                       verbose_name='Identificación')  # Identificacion de la persona que realiza la denuncia
    description = models.TextField(verbose_name='Descripción')  # Descripcion acerca de la denuncia
    form_number = models.CharField(max_length=10,
                                   verbose_name='Número de formulario')  # Numero de formulario con el que se genero la denuncia
    discount_applies = models.CharField(verbose_name='Aplica descuento', max_length=3,
                                        choices=STATES_YES_NO_CHOICES,
                                        default='NOT')  # Indica si aplica descuento a no para la queja
    percentage_reduction = models.DecimalField(max_digits=4, decimal_places=2, default=Decimal('0.00'),
                                               verbose_name='Porcentaje de reducción')  # Valor del porcentaje de reduccion
    complaint_evaluation = models.ForeignKey(ComplaintEvaluation, on_delete=models.CASCADE,
                                             verbose_name='Evaluación de quejas')  # Indica la evaluacion de quejas a la que pertenece
