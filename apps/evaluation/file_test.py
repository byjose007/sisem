# Sample platypus document
# From the FAQ at reportlab.org/oss/rl-toolkit/faq/#1.1

from reportlab.platypus import (SimpleDocTemplate, PageBreak, Image, Spacer,
    Paragraph, Table, TableStyle)
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.rl_config import defaultPageSize
from reportlab.lib.units import inch
from reportlab.lib.enums import TA_LEFT, TA_RIGHT, TA_CENTER, TA_JUSTIFY
from reportlab.lib.pagesizes import A4, letter, landscape
from reportlab.platypus import (BaseDocTemplate, Frame, Paragraph, 
    NextPageTemplate, PageBreak, PageTemplate)
from reportlab.lib import colors
import os.path

PAGE_HEIGHT=defaultPageSize[1]
PAGE_WIDTH=defaultPageSize[0]
styles = getSampleStyleSheet()
Title = "Hello world"
pageinfo = "platypus example"

def myFirstPage(canvas, doc):
    canvas.saveState()
    canvas.setFont('Times-Bold',16)
    canvas.drawCentredString(PAGE_WIDTH/2.0, PAGE_HEIGHT-108, Title)
    canvas.setFont('Times-Roman',9)
    canvas.drawString(inch, 0.75 * inch,"First Page / %s" % pageinfo)
    canvas.restoreState()
    
def myLaterPages(canvas, doc):
    canvas.saveState()
    canvas.setFont('Times-Roman', 9)
    canvas.drawString(inch, 0.75 * inch,"Page %d %s" % (doc.page, pageinfo))
    canvas.restoreState()
    
def go_aux():
    doc = SimpleDocTemplate("phello.pdf")
    Story = [Spacer(1,2*inch)]
    style = styles["Normal"]
    for i in range(100):
        bogustext = ("Paragraph number %s. " % i) *20
        p = Paragraph(bogustext, style)
        Story.append(p)
        Story.append(Spacer(1,0.2*inch))
    doc.build(Story, onFirstPage=myFirstPage, onLaterPages=myLaterPages)

def go_tmp(buff):
    doc = SimpleDocTemplate(buff,
                            pagesize=letter,
                            rightMargin=40,
                            leftMargin=40,
                            topMargin=60,
                            bottomMargin=18,
                            )
    Story = [Spacer(1,2*inch)]
    style = styles["Normal"]
    for i in range(100):
        bogustext = ("Paragraph number %s. " % i) *20
        p = Paragraph(bogustext, style)
        Story.append(p)
        Story.append(Spacer(1,0.2*inch))
    doc.build(Story, onFirstPage=myFirstPage, onLaterPages=myLaterPages)
    
# if __name__ == "__main__":
#     go()

def encabezado(canvas,doc):
    canvas.saveState()
    canvas.setFont('Times-Roman',9)
    canvas.drawString(inch, A4[1]-50, "Ejemplo de DocTemplate y PageTemplate")
    canvas.restoreState()
    
def pie(canvas,doc):
    canvas.saveState()
    canvas.setFont('Times-Roman',9)
    canvas.drawString(inch, 0.75 * inch, "Page %d" % doc.page)
    canvas.restoreState()

def go_ant(buff):
    #Creamos la hoja de Estilo
    estilo=getSampleStyleSheet()

    #Iniciamos el platypus story
    story=[]

    #Añadimos al story los flowables. Hay que tener en cuenta que se inicia
    #con el primer pageTemplate "UnaColumna"
    story.append(Paragraph("Esto es el texto del Frame normal del pagetemplate" +\
                           " de una columna"* 500, estilo['Normal']))
                            
    story.append(NextPageTemplate('DosColumnas'))  # Cambio de PageTemplate
    story.append(PageBreak())  # Inicio en otra hoja
    story.append(Paragraph("Esto es el texto del Frame que pertenece al" +\
                           " pagetemplate de dos columnas" * 500, estilo['Normal']))
                    
    story.append(NextPageTemplate('UnaColumna'))
    story.append(PageBreak())
    story.append(Paragraph("Regresamos al texto del Frame normal del" +\
                            " pagetemplate de dos columnas"*100, estilo['Normal']))

    #NIVEL 3: CREAMOS LOS FRAMES, para luego asignarlos a un pagetemplate.
    #===========================
    #Frame (x1, y1, ancho, alto, leftPadding=6, bottomPadding=6, rightPadding=6,
    # topPadding=6, id=None, showBoundary=0)

    #1. Frame que contendrá a toda el contenido de una hoja
    frameN = Frame(inch, inch, 451, 697, id='normal')

    #2. Frame de columnas
    frame1 = Frame(inch, inch, 220, 697, id='col1')
    frame2 = Frame(inch + 230, inch, 220, 697, id='col2')

    #NIVEL 4: CREAMOS LOS PAGETEMPLATE, le asignamos los frames y los canvas
    #=================================
    #PageTemplate(id=None,frames=[],onPage=_doNothing,onPageEnd=_doNothing)
    PTUnaColumna = PageTemplate(id='UnaColumna', frames=frameN, onPage=pie)
    PTDosColumnas =  PageTemplate(id='DosColumnas', frames=[frame1,frame2],
                            onPage=encabezado, onPageEnd=pie)

    #NIVEL 5: CREAMOS EL DOCTEMPLATE, a partir del BaseDocTemplate
    #===============================
    doc = BaseDocTemplate(buff, pageTemplates=[PTUnaColumna, PTDosColumnas], 
            pagesize=A4)

    #Construimos el PDF
    doc.build(story)

def go_not(buff):
    # doc = SimpleDocTemplate(buff,
    #                         pagesize=letter,
    #                         rightMargin=40,
    #                         leftMargin=40,
    #                         topMargin=60,
    #                         bottomMargin=18,
    #                         )
    doc = SimpleDocTemplate(buff, pagesize = A4)
    story=[]

    #Ejemplo 01
    t = Table([
            ['', 'Ventas', 'Compras'], 
            ['Enero', 1000, 2000], 
            ['Febrero', 3000, 100.5],
            ['Marzo', 2000, 1000],
            ['Abril', 1500, 1500]
        ], colWidths=80, rowHeights=30)
    t.setStyle([
            ('TEXTCOLOR', (0, 1), (0, -1), colors.blue),
            ('TEXTCOLOR', (1, 1), (2, -1), colors.green),
            ('BACKGROUND',(1,1),(-1,-1),colors.cyan),
            ('BOX',(1,1),(-1,-1),1.25,colors.yellow),
            ('INNERGRID',(1,1),(-1,-1),1,colors.red),
            ('VALIGN', (0,0), (-1, -1), 'MIDDLE'),
        ])
    story.append(t)
    story.append(Spacer(0,15))

    #Ejemplo02
    datos = (
            ('Nombre ciclo', 'Núm. Alumnos', 'Núm aprobados'),
            ('Desarrollo Aplic. Informáticas', 15, 5),
            ('Admin. Sist. Informáticos', 40, 25),
            ('Explotación Sist. Informáticos', 50, 20)
        )
    tabla = Table(data = datos,
                  style = [
                           ('GRID',(0,0),(-1,-1),0.5,colors.grey),
                           ('BOX',(0,0),(-1,-1),2,colors.black),
                           ('BACKGROUND', (0, 0), (-1, 0), colors.pink),
                           ]
                  )
    story.append(tabla)
    story.append(Spacer(0,15))

    #Ejemplo 03
    estiloTabla = TableStyle([
         ('LINEABOVE', (0,0), (-1, 0), 2, colors.green),
         ('ALIGN', (1,1), (-1, -1), 'RIGHT'),
         ('ROWBACKGROUNDS', (0,0), (-1, -1), (colors.yellow, None)),
         
        ])
    estiloTabla.add('BACKGROUND', (0,0), (-1,0), colors.Color(0, 0.7, 0.7))
    t = Table([
            ['', 'Ventas', 'Compras'], 
            ['Enero', 1000, 2000], 
            ['Febrero', 3000, 100.5],
            ['Marzo', 2000, 1000],
            ['Abril', 1500, 1500]
        ], style = estiloTabla)

    #Ejemplo 04: CELDAS COMPLEJAS
    estilo = getSampleStyleSheet()
    # I = Image('tuxTemplario.png', width=100, height=100)
    image_path = path_relative_to_file('/static/img/logo-iml.png')
    I = Image(image_path, width=100, height=100)

    P1 = Paragraph('''El tux templario''', estilo["BodyText"])
    P2 = Paragraph('''Viva Linux''', estilo["BodyText"])

    t = Table(
        data=[
            ['A',   'B', 'C',     P1, 'D'],
            ['00', '01', '02', [I,P2], '04'],
            ['10', '11', '12', '13', '14'],
            ['20', '21', '22',  '23', '24'],
            ['30', '31', '32',  '33', '34']
        ],
        style=[
            ('GRID',(1,1),(-2,-2),1,colors.green),
            ('BOX',(0,0),(1,-1),2,colors.red),
            ('LINEABOVE',(1,2),(-2,2),1,colors.blue),
            ('LINEBEFORE',(2,1),(2,-2),1,colors.pink),
            ('BACKGROUND', (0, 0), (0, 1), colors.pink),
            ('BACKGROUND', (1, 1), (1, 2), colors.lavender),
            ('BACKGROUND', (2, 2), (2, 3), colors.orange),
            ('BOX',(0,0),(-1,-1),2,colors.black),
            ('GRID',(0,0),(-1,-1),0.5,colors.black),
            ('VALIGN',(3,0),(3,0),'BOTTOM'),
            ('BACKGROUND',(3,0),(3,0),colors.limegreen),
            ('BACKGROUND',(3,1),(3,1),colors.khaki),
            ('ALIGN',(3,1),(3,1),'CENTER'),
            ('BACKGROUND',(3,2),(3,2),colors.beige),
            ('ALIGN',(3,2),(3,2),'LEFT'),
        ]
    )
    story.append(t)

    #Ejemplo 05: Combinación de celdas

    t=Table(
        data=[
            ['Arriba\nIzquierda', '', '02', '03', '04'],
            ['', '', '12', '13', '14'],
            ['20', '21', '22', 'Abajo\nDerecha', ''],
            ['30', '31', '32', '', '']
        ],
        style=[
            ('GRID',(0,0),(-1,-1),0.5,colors.grey),
            ('BACKGROUND',(0,0),(1,1),colors.palegreen),
            ('SPAN',(0,0),(1,1)),
            ('BACKGROUND',(-2,-2),(-1,-1), colors.pink),
            ('SPAN',(-2,-2),(-1,-1)),
        ])
    story.append(Spacer(0, 10))
    story.append(t)

    doc.build(story)

def go(buff):
    # doc = SimpleDocTemplate(buff,
    #                         pagesize=letter,
    #                         rightMargin=40,
    #                         leftMargin=40,
    #                         topMargin=60,
    #                         bottomMargin=18,
    #                         )
    # Helvetica-Bold
    # Courier-Bold
    # Times-Bold  
    doc = SimpleDocTemplate(buff, pagesize = A4)
    story=[]
    image_path = path_relative_to_file('/static/img/logo-mrl.png')
    estilo=getSampleStyleSheet()
    estilo.add(ParagraphStyle(name="TitleMain", alignment=TA_CENTER, fontName='Times-Bold', fontSize=8))
    estilo.add(ParagraphStyle(name="TitleSmall", alignment=TA_CENTER, fontName='Times-Bold', fontSize=7))
    estilo.add(ParagraphStyle(name="DefaultNormal", alignment=TA_LEFT, fontName='Times', fontSize=8))
    estilo.add(ParagraphStyle(name="DefaultBold", alignment=TA_LEFT, fontName='Times-Bold', fontSize=8))
    estilo.add(ParagraphStyle(name="DefaultNormalColor", alignment=TA_CENTER, fontName='Times', fontSize=8, textColor=colors.Color(0.000, 0.000, 0.545)))
    estilo.add(ParagraphStyle(name="DefaultBoldColor", alignment=TA_CENTER, fontName='Times-Bold', fontSize=8, textColor=colors.Color(0.000, 0.000, 0.545)))
    estilo.add(ParagraphStyle(name="Result", alignment=TA_RIGHT, fontName='Times-Bold', fontSize=8, textColor=colors.Color(0.502, 0.000, 0.000)))

    title_color =  colors.Color(0.412, 0.412, 0.412)
    subtitle_color = colors.Color(0.753, 0.753, 0.753)
    result_color = colors.Color(0.941, 0.902, 0.549)


    tbl_data = [
        [Image(image_path, width=100, height=25), Paragraph("FORMULARIO MRL-EVAL-01 - MODIFICADO", estilo['TitleMain'])],
    ]
    tbl = Table(tbl_data, colWidths=[150, 300])
    tbl.setStyle([
        ('TEXTCOLOR', (0, 1), (0, -1), colors.blue),
        ('TEXTCOLOR', (1, 1), (2, -1), colors.green),
        # ('BACKGROUND',(1,1),(-1,-1),colors.cyan),
        # ('BOX',(1,1),(-1,-1),1.25,colors.yellow),
        # ('INNERGRID',(1,1),(-1,-1),1,colors.red),
        ('VALIGN', (0,0), (-1, -1), 'MIDDLE'),
    ])
    story.append(tbl)

    #Datos servidor
    datos = (
            (Paragraph( 'FORMULARIO PARA LA EVALUACIÓN DEL DESEMPEÑO POR COMPETENCIAS PARA USO DEL JEFE INMEDIATO', estilo['TitleMain']), '', '', ''),
            (Paragraph('DATOS DEL SERVIDOR:', estilo['DefaultBold']), '', '', ''),
            (Paragraph('Apellidos y Nombres del Servidor (Evaluado):', estilo['DefaultBold']), '', '', ''),
            (Paragraph('Denominación del Puesto que Desempeña:', estilo['DefaultBold']), '', '', ''),
            (Paragraph('Título o profesión:', estilo['DefaultBold']), '', '', ''),
            (Paragraph('Apellidos o nombre del fefe departamental:', estilo['DefaultBold']), '', '', ''),
            (Paragraph('Apellidos o nombre del jefe inmediato o superior inmediato (Evaluador):', estilo['DefaultBold']), '', '', ''),
            (Paragraph('Período de evaluación (dd/mm/aaaa): Desde:', estilo['DefaultBold']), '', Paragraph('Hasta:',estilo['DefaultBold']), '')

        )
    tabla = Table(data = datos,
                    colWidths=[250,100,100,100],
                    style=[
                            ('GRID',(0,0), (-1,-1), 0.9, colors.grey),
                            ('BOX',(0,0), (-1,-1), 2, colors.black),
                            # ('LINEABOVE', (0,0), (-1,0), 2, colors.black),
                            # ('LINEABOVE', (0,1), (-1,-1), 0.25, colors.black),
                            # ('LINEBELOW', (0,7), (-1,-1), 2, colors.black),
                            ('SPAN',(0,0),(-1,0)), # Union todas columnas fila 0
                            ('SPAN',(0,1),(-1,1)), # Union todas columnas fila 1
                            ('SPAN',(1,2),(-1,2)), # Union columnas 1-todas fila 2
                            ('SPAN',(1,3),(-1,3)), # Union columnas 1-todas fila 3
                            ('SPAN',(1,4),(-1,4)), # Union columnas 1-todas fila 4
                            ('SPAN',(1,5),(-1,5)), # Union columnas 1-todas fila 5
                            ('SPAN',(1,6),(-1,6)), # Union columnas 1-todas fila 6
                            ('BACKGROUND',(0,0), (-1,0), colors.Color(0.412, 0.412, 0.412)),
                            ('BACKGROUND',(0,1),(-1,1), colors.Color(0.753, 0.753, 0.753)),
                            ('BACKGROUND',(0,2),(0,-1), colors.Color(0.753, 0.753, 0.753)),
                            ('BACKGROUND',(2,7),(2,7), colors.Color(0.753, 0.753, 0.753)),
                        ]
                  )
    story.append(tabla)
    story.append(Spacer(0,15))

    #Datos evaluacion
    job_evaluation_activities = Paragraph('EVALUACIÓN DE ACTIVIDADES DEL PUESTO', estilo['TitleMain'])
    indicator = Paragraph('INDICADORES DE GESTIÓN DEL PUESTO:', estilo['DefaultBold'])
    activities_number = Paragraph('# Actividades:', estilo['DefaultNormalColor'])
    factor = Paragraph('Factor:', estilo['DefaultNormalColor'])
    description_activities = Paragraph("Descripción de actividades", estilo['DefaultBold'])
    indicator = Paragraph("Indicador", estilo['DefaultBold'])
    goal = Paragraph("Meta del período evaluado", estilo['DefaultBold'])
    completed = Paragraph("Cumplidos", estilo['DefaultBold'])
    comply_percentage = Paragraph("% de cumplimiento", estilo['DefaultBold'])
    comply_level = Paragraph("Nivel de cumplimiento", estilo['DefaultBold'])
    advance_objectives = Paragraph("¿A más del cumplimeinto de la totalidad de las metas y objetivos se adelantó y cumplió con objetivos y metas provistas para el siguiente período de evaluación?", estilo['DefaultBold'])
    increase_applies = Paragraph("APLICA EL + 4%", estilo['DefaultBold'])
    increase_porcentage = Paragraph("% DE AUMENTO", estilo['DefaultBold'])
    activities_total = Paragraph("Total actividades escenciales:", estilo['Result'])
    knowledge = Paragraph('CONOCIMIENTOS', estilo['DefaultBold'])
    knowledge_number = Paragraph('# de Conocimientos:', estilo['DefaultNormalColor'])
    knowledge_level = Paragraph('Nivel de conocimiento', estilo['DefaultBold'])
    knowledge_total = Paragraph("Total de conocimientos:", estilo['Result'])
    technical_competence = Paragraph("COMPETENCIAS TÉCNICAS DEL PUESTO", estilo['DefaultBold'])
    competence_number = Paragraph("# Competencias", estilo['DefaultNormalColor'])
    skill = Paragraph("DESTREZAS", estilo['DefaultBold'])
    relevance = Paragraph("Relevancia", estilo['DefaultBold'])
    observable_behavior = Paragraph("Comportamiento observable", estilo['DefaultBold'])
    development_level = Paragraph("Nivel de desarrollo", estilo['DefaultBold'])
    total_technical_competence = Paragraph("Total Competencias Técnicas del Puesto :", estilo['Result'])
    universal_competence = Paragraph("COMPETENCIAS UNIVERSALES", estilo['DefaultBold'])
    required_fields = Paragraph("ESTOS CAMPOS DEBEN SER LLENADOS OBLIGATORIAMENTE:", estilo['TitleMain'])
    frequency_application = Paragraph("Frecuencia de aplicación", estilo['DefaultBold'])
    total_universal_competence = Paragraph("Total competencias universales", estilo['Result'])
    teamwork = Paragraph("TRABAJO EN EQUIPO, INICIATIVA Y LIDERAZGO", estilo['DefaultBold'])
    description = Paragraph("DESCRIPCIÓN", estilo['DefaultBold'])
    field_leadership = Paragraph("LLENAR EL CAMPO DE LIDERAZGO, SOLO PARA QUIENES TENGAN SERVIDORES SUBORDINADOS BAJO SU RESPONSABILIDAD DE GESTIÓN", estilo['TitleSmall'])    
    total_teamwork = Paragraph("Total Trabajo en Equipo, Iniciativa y Liderazgo:", estilo['Result'])
    observations = Paragraph("OBSERVACIONES DEL JEFE INMEDIATO (EN CASO DE QUE LAS TENGA)", estilo['DefaultBold'])
    complaints = Paragraph("QUEJAS DEL CIUDADANO (PARA USO DE LAS UARHS) INFORMACIÓN PROVENIENTE DEL FORMULARIO EVAL-02", estilo['DefaultBold'])
    name_complainant = Paragraph("Nombre de la persona que realiza la queja", estilo['DefaultBold'])
    form_number = Paragraph("No. de formulario", estilo['DefaultBold'])
    applies_discount = Paragraph("Aplica descuento a la evaluación del desempeño", estilo['DefaultBold'])
    reduction_percentage = Paragraph("% de reducción", estilo['DefaultBold'])
    total_complaints = Paragraph("Total", estilo['Result'])
    evaluation_result = Paragraph("RESULTADO DE LA EVALUACIÓN", estilo['TitleMain'])
    evaluation_factors = Paragraph("FACTORES DE EVALUACIÓN", estilo['DefaultBold'])
    score = Paragraph("CALIFICACIÓN ALCANZADA", estilo['DefaultBold'])
    subtotal_job_management_indicators = Paragraph("Indicadores de Gestión del puesto", estilo['DefaultNormalColor'])
    subtotal_knowledge = Paragraph("Conocimientos", estilo['DefaultNormalColor'])
    subtotal_technical_competence = Paragraph("Competencias técnicas del puesto", estilo['DefaultNormalColor'])
    subtotal_universal_competence = Paragraph("Competencias universales", estilo['DefaultNormalColor'])
    subtotal_teamwork = Paragraph("Trabajo en equipo, iniciativa y liderazgo", estilo['DefaultNormalColor'])
    subtotal_citizen_evaluation = Paragraph("Evaluación del ciudadano (-)", estilo['DefaultNormalColor'])
    evaluator_official = Paragraph("FUNCIONARIO (A) EVALUADOR (A)", estilo['TitleMain'])
    evaluation_date = Paragraph("Fecha (dd/mm/aaaa)", estilo['DefaultBoldColor'])
    certificate = Paragraph("CERTIFICO: Que he evaluado al (a la) servidor (a) acorde al procedimiento de la norma de Evaluación del Desempeño",
        estilo['DefaultNormalColor'])
    signature = Paragraph("FIRMA", estilo['DefaultBoldColor'])
    top_boss = Paragraph("Evaluador o Jefe inmediato", estilo['DefaultNormalColor'])
    inst_unit_boss = Paragraph("Jefe de la unidad", estilo['DefaultNormalColor'])
    lines = Paragraph("__________________________________", estilo['DefaultNormalColor'])

    datos = (
            (job_evaluation_activities, '', '', '', '', ''),
            (indicator, '', activities_number, '', factor, ''),
            (description_activities, indicator, goal, completed, comply_percentage, comply_level),
            ('a', 'b', 'c','d', 'e', 'f'),
            ('1', '2', '3','4', '5', '6'),
            (advance_objectives, '', '',increase_applies, '', increase_porcentage),
            ('', '', '', '', '', '0'),
            (activities_total, '', '','', '', '0%'),
            (knowledge, knowledge_number, '',factor, '', knowledge_level),
            ('ABCDEX', '', '', '', '', ''),
            ('123456', '', '', '', '', ''),
            (knowledge_total, '', '', '', '', '0%'),
            (technical_competence, competence_number, '0', factor, '0', ''),
            (skill, relevance, observable_behavior, '', '', development_level),
            ('A', 'B', 'C', 'D', 'E', 'F'),
            ('G', 'H', 'I', 'J', 'K', 'L'),
            ('M', 'N', 'O', 'P', 'Q', 'R'),
            ('S', 'T', 'U', 'V', 'W', 'X'),
            (total_technical_competence, '', '', '', '', '0%'),
            (universal_competence, competence_number, '0', factor, '0%', ''),
            (required_fields, '', '', '', '', ''),
            (skill, relevance, observable_behavior, '', '', development_level),
            ('A', 'B', 'C', 'D', 'E', 'F'),
            ('G', 'H', 'I', 'J', 'K', 'L'),
            ('M', 'N', 'O', 'P', 'Q', 'R'),
            ('S', 'T', 'U', 'V', 'W', 'X'),
            (total_universal_competence, '', '', '', '', '0%'),
            (teamwork, '', '', factor, '', '0%'),
            (description, relevance, observable_behavior, '', '', frequency_application),
            ('A', 'B', 'C', 'D', 'E', 'F'),
            ('G', 'H', 'I', 'J', 'K', 'L'),
            (field_leadership, '', '', '', '', ''),
            ('A', 'B', 'C', 'D', 'E', 'F'),
            (total_teamwork, '', '', '', '', '0%'),
            (observations, '', '', '', '', ''),
            ('A', 'B', 'C', 'D', 'E', 'F'),
            (complaints, '', '', '', '', ''),
            (name_complainant, description, '', form_number, applies_discount, reduction_percentage),
            ('A', 'B', 'C', 'D', 'E', 'F'),
            (total_complaints, '', '', '', '', '0%'),
            (evaluation_result, '', '', '', '', ''),
            (evaluation_factors, '', '', '', '', score),
            (subtotal_job_management_indicators, '', '', '', '', ''),
            (subtotal_knowledge, '', '', '', '', ''),
            (subtotal_technical_competence, '', '', '', '', ''),
            (subtotal_universal_competence, '', '', '', '', ''),
            (subtotal_teamwork, '', '', '', '', ''),
            (subtotal_citizen_evaluation, '', '', '', '', ''),
            ('PROCESO INCORRECTO', '', '', '', '', ''),
            ('PROCESO INCORRECTO', '', '', '', '', ''),
            (evaluator_official, '', '', '', '', ''),
            (evaluation_date, '', '', '', '', ''),
            (certificate, '', '', '', '', ''),
            ('', '', '', '', '', ''),
            (lines, '', '', lines, '', ''),
            (signature, '', '', signature, '', ''),
            (top_boss, '', '', inst_unit_boss, '', ''),
            ('', '', '', '', '', ''),

        )

    tabla = Table(data = datos,
                colWidths=[125,125,75,75,75,75],
                style=[
                        ('GRID',(0,0), (-1,-1), 0.9, colors.grey),
                        ('GRID',(0,53),(-1,56), 3,colors.white),
                        ('BOX',(0,0), (-1,-1), 2, colors.black),
                        # ('LINEABOVE', (0,0), (-1,0), 2, colors.black),
                        # ('LINEABOVE', (0,1), (-1,-1), 0.25, colors.black),
                        # ('LINEBELOW', (0,7), (-1,-1), 2, colors.black),
                        ('SPAN',(0,0),(-1,0)), 
                        ('SPAN',(0,1),(1,1)), 
                        ('SPAN',(0,5),(2,6)),
                        ('SPAN',(3,5),(4,5)),
                        ('SPAN',(3,6),(4,6)),
                        ('SPAN',(0,7),(4,7)),
                        ('SPAN',(0,9),(4,9)),
                        ('SPAN',(0,10),(4,10)),
                        ('SPAN',(0,11),(4,11)),
                        ('SPAN',(2,13),(4,13)),
                        ('SPAN',(2,14),(4,14)),
                        ('SPAN',(2,15),(4,15)),
                        ('SPAN',(2,16),(4,16)),
                        ('SPAN',(2,17),(4,17)),
                        ('SPAN',(0,18),(4,18)),
                        ('SPAN',(0,20),(-1,20)),
                        ('SPAN',(2,21),(4,21)),
                        ('SPAN',(0,26),(4,26)),
                        ('SPAN',(0,27),(2,27)),
                        ('SPAN',(2,28),(4,28)),
                        ('SPAN',(0,31),(-1,31)),
                        ('SPAN',(0,33),(4,33)),
                        ('SPAN',(0,34),(-1,34)),
                        ('SPAN',(0,36),(-1,36)), # Titulo de quejas o denuncias
                        ('SPAN',(1,37),(2,37)), # Union de columnas para el campo Descripcion de denuncias
                        ('SPAN',(0,39),(4,39)), # Resultado de las quejas o denuncias
                        ('SPAN',(0,40),(-1,40)), # Titulo del resultado de la evaluacion
                        ('SPAN',(0,41),(4,41)), # Titulo de factores de evaluacion
                        ('SPAN',(0,42),(4,42)), # Subtotal indicadores de gestion  
                        ('SPAN',(0,43),(4,43)), # Subtotal conocimientos
                        ('SPAN',(0,44),(4,44)), # Subtotal competencias tecnicas
                        ('SPAN',(0,45),(4,45)), # Subtotal competencias universales
                        ('SPAN',(0,46),(4,46)), # Subtotal trabajo en equipo
                        ('SPAN',(0,47),(4,47)), # Subtotal evaluacion del ciudadano
                        ('SPAN',(0,48),(-1,48)), # Porcentage final de la evaluacion
                        ('SPAN',(0,49),(-1,49)), # Resultado final de la evaluacion
                        ('SPAN',(0,50),(-1,50)), # Titulo funcionario evaluador
                        ('SPAN',(0,51),(2,51)), # Titulo fecha evaluacion
                        ('SPAN',(3,51),(-1,51)), # Campo valor fecha evaluacion
                        ('SPAN',(0,52),(-1,52)), # Titulo certifico
                        ('SPAN',(0,53),(2,53)), # Linea en blanco firma jefe inmediato
                        ('SPAN',(3,53),(-1,53)), # Linea en blanco firma jefe de la unidad
                        ('SPAN',(0,54),(2,54)), # Linea firma jefe inmediato
                        ('SPAN',(3,54),(-1,54)), # Linea firma jefe de la unidad
                        ('SPAN',(0,55),(2,55)), # Titulo firma jefe inmediato
                        ('SPAN',(3,55),(-1,55)), # Titulo firma jefe unidad
                        ('SPAN',(0,56),(2,56)), # Titulo evaluador jefe inmediato
                        ('SPAN',(3,56),(-1,56)), # Titulo evaluador jefe unidad
                        ('SPAN',(0,57),(-1,57)), # Linea final vacia
                        ('BACKGROUND',(0,0), (-1,0), title_color),
                        ('BACKGROUND',(0,1),(-1,2), subtitle_color),
                        ('BACKGROUND',(0,5),(-1,6), subtitle_color),
                        ('BACKGROUND',(0,7),(-1,7), result_color),
                        ('BACKGROUND',(0,7),(-1,7), result_color),
                        ('BACKGROUND',(3,6),(4,6), colors.white),
                        ('BACKGROUND',(0,8),(-1,8), subtitle_color),
                        ('BACKGROUND',(0,11),(-1,11), result_color),
                        ('BACKGROUND',(0,12),(-1,13), subtitle_color),
                        ('BACKGROUND',(0,18),(-1,18), result_color), # Resultado competencias tecnicas
                        ('BACKGROUND',(0,19),(-1,21), subtitle_color), # Titulo competencias univerasles 
                        ('BACKGROUND',(0,26),(-1,26), result_color), # Resultado competencias universales
                        ('BACKGROUND',(0,27),(-1,28), subtitle_color), # Titulo trabajo en equipo
                        ('BACKGROUND',(0,31),(-1,31), subtitle_color), # Nota llenar campo liderazgo - solo si tiene trabajadores a cargo
                        ('BACKGROUND',(0,33),(-1,33), result_color), # Resultado trabajo en equipo
                        ('BACKGROUND',(0,34),(-1,34), subtitle_color), # Titulo observaciones
                        ('BACKGROUND',(0,36),(-1,36), title_color), # Titulo quejas o denuncias
                        ('BACKGROUND',(0,37),(-1,37), subtitle_color), # Titulo de los campos de las quejas o denuncias
                        ('BACKGROUND',(0,39),(-1,39), result_color), # Resultado de quejas o denuncias
                        ('BACKGROUND',(0,40),(-1,40), title_color), # Titulo resultado de evaluacion
                        ('BACKGROUND',(0,41),(-1,41), subtitle_color), # Titulo factores de evaluacion
                        ('BACKGROUND',(0,42),(-1,47), result_color), # Subtotal indicadores de gestion del puesto, conocimientos, competencias tecnicas, competencias universales, trabajo en equipo y evaluacion cidadana
                        ('BACKGROUND',(0,48),(-1,52), subtitle_color), # Porcentaje, resultado, titulo funcionario evaluador, fecha, certifico y firmas
                        ('BACKGROUND',(0,57),(-1,57), subtitle_color), # Fila final vacia



                    ]
                  )
    story.append(tabla)
    story.append(Spacer(0,15))


    #Ejemplo 01
    t = Table([
            ['', 'Ventas', 'Compras'], 
            ['Enero', 1000, 2000], 
            ['Febrero', 3000, 100.5],
            ['Marzo', 2000, 1000],
            ['Abril', 1500, 1500]
        ], colWidths=80, rowHeights=30)
    t.setStyle([
            ('TEXTCOLOR', (0, 1), (0, -1), colors.blue),
            ('TEXTCOLOR', (1, 1), (2, -1), colors.green),
            ('BACKGROUND',(1,1),(-1,-1),colors.cyan),
            ('BOX',(1,1),(-1,-1),1.25,colors.yellow),
            ('INNERGRID',(1,1),(-1,-1),1,colors.red),
            ('VALIGN', (0,0), (-1, -1), 'MIDDLE'),
        ])
    story.append(t)
    story.append(Spacer(0,15))

    #Ejemplo02
    datos = (
            ('Nombre ciclo', 'Núm. Alumnos', 'Núm aprobados'),
            ('Desarrollo Aplic. Informáticas', 15, 5),
            ('Admin. Sist. Informáticos', 40, 25),
            ('Explotación Sist. Informáticos', 50, 20)
        )
    tabla = Table(data = datos,
                  style = [
                           ('GRID',(0,0),(-1,-1),0.5,colors.grey),
                           ('BOX',(0,0),(-1,-1),2,colors.black),
                           ('BACKGROUND', (0, 0), (-1, 0), colors.pink),
                           ]
                  )
    story.append(tabla)
    story.append(Spacer(0,15))

    #Ejemplo 03
    estiloTabla = TableStyle([
         ('LINEABOVE', (0,0), (-1, 0), 2, colors.green),
         ('ALIGN', (1,1), (-1, -1), 'RIGHT'),
         ('ROWBACKGROUNDS', (0,0), (-1, -1), (colors.yellow, None)),
         
        ])
    estiloTabla.add('BACKGROUND', (0,0), (-1,0), colors.Color(0, 0.7, 0.7))
    t = Table([
            ['', 'Ventas', 'Compras'], 
            ['Enero', 1000, 2000], 
            ['Febrero', 3000, 100.5],
            ['Marzo', 2000, 1000],
            ['Abril', 1500, 1500]
        ], style = estiloTabla)

    #Ejemplo 04: CELDAS COMPLEJAS
    estilo = getSampleStyleSheet()
    # I = Image('tuxTemplario.png', width=100, height=100)
    image_path = path_relative_to_file('/static/img/logo-iml.png')
    I = Image(image_path, width=100, height=100)

    P1 = Paragraph('''El tux templario''', estilo["BodyText"])
    P2 = Paragraph('''Viva Linux''', estilo["BodyText"])

    t = Table(
        data=[
            ['A',   'B', 'C',     P1, 'D'],
            ['00', '01', '02', [I,P2], '04'],
            ['10', '11', '12', '13', '14'],
            ['20', '21', '22',  '23', '24'],
            ['30', '31', '32',  '33', '34']
        ],
        style=[
            ('GRID',(1,1),(-2,-2),1,colors.green),
            ('BOX',(0,0),(1,-1),2,colors.red),
            ('LINEABOVE',(1,2),(-2,2),1,colors.blue),
            ('LINEBEFORE',(2,1),(2,-2),1,colors.pink),
            ('BACKGROUND', (0, 0), (0, 1), colors.pink),
            ('BACKGROUND', (1, 1), (1, 2), colors.lavender),
            ('BACKGROUND', (2, 2), (2, 3), colors.orange),
            ('BOX',(0,0),(-1,-1),2,colors.black),
            ('GRID',(0,0),(-1,-1),0.5,colors.black),
            ('VALIGN',(3,0),(3,0),'BOTTOM'),
            ('BACKGROUND',(3,0),(3,0),colors.limegreen),
            ('BACKGROUND',(3,1),(3,1),colors.khaki),
            ('ALIGN',(3,1),(3,1),'CENTER'),
            ('BACKGROUND',(3,2),(3,2),colors.beige),
            ('ALIGN',(3,2),(3,2),'LEFT'),
        ]
    )
    story.append(t)

    #Ejemplo 05: Combinación de celdas

    t=Table(
        data=[
            ['Arriba\nIzquierda', '', '02', '03', '04'],
            ['', '', '12', '13', '14'],
            ['20', '21', '22', 'Abajo\nDerecha', ''],
            ['30', '31', '32', '', '']
        ],
        style=[
            ('GRID',(0,0),(-1,-1),0.5,colors.grey),
            ('BACKGROUND',(0,0),(1,1),colors.palegreen),
            ('SPAN',(0,0),(1,1)),
            ('BACKGROUND',(-2,-2),(-1,-1), colors.pink),
            ('SPAN',(-2,-2),(-1,-1)),
        ])
    story.append(Spacer(0, 10))
    story.append(t)

    doc.build(story)

def path_relative_to_file(relative_path):
    print('>>> intro path_relative_to_file')
    project_path = os.path.abspath(os.path.dirname(__name__))
    return (project_path + relative_path)