from django.contrib import admin
from apps.evaluation.models import (PerformanceEvaluation, CatalogTechnicalCompetence, PersonalEvaluation, ActivityEvaluation, Activity,
	KnowledgeEvaluation, Knowledge, TechnicalCompetenceEvaluation, TechnicalCompetence, UniversalCompetenceEvaluation,
	UniversalCompetence, TeamworkEvaluation, Teamwork, ComplaintEvaluation, Complaint, ChiefObservation)

# Register your models here.

class PerformanceEvaluationAdmin(admin.ModelAdmin):
    list_display = ('year', 'state')
    list_filter = ('year', 'state', 'start_date', 'end_date')
    ordering = ('-year',)
    search_fields = ['state']

class PersonalEvaluationAdmin(admin.ModelAdmin):
	list_display = ('period_from', 'period_to', 'state')
	ordering = ('period_from',)
	search_fields= ['state']

admin.site.register(ActivityEvaluation)
admin.site.register(Activity)
admin.site.register(PerformanceEvaluation, PerformanceEvaluationAdmin)
admin.site.register(CatalogTechnicalCompetence)
admin.site.register(PersonalEvaluation, PersonalEvaluationAdmin)
admin.site.register(KnowledgeEvaluation)
admin.site.register(Knowledge)
admin.site.register(TechnicalCompetenceEvaluation)
admin.site.register(TechnicalCompetence)
admin.site.register(UniversalCompetenceEvaluation)
admin.site.register(UniversalCompetence)
admin.site.register(TeamworkEvaluation)
admin.site.register(Teamwork)
admin.site.register(ComplaintEvaluation)
admin.site.register(Complaint)
admin.site.register(ChiefObservation)