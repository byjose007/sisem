from django import template

register = template.Library()

@register.filter(name='addclass')
def addclass(value, arg):
    return value.as_widget(attrs={'class': arg})

@register.filter(name='setstate')
def setstate(obj, value):
	state = "Activo"
	if(value=='INACTIVE'):
		state = "Inactivo"
	return state