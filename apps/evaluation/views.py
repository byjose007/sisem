from django.core.files.storage import FileSystemStorage
from django.shortcuts import render, redirect, get_object_or_404, render_to_response
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth.models import User
from django.template.loader import get_template
from django.template import loader, Context, RequestContext
from django.http import HttpResponse, HttpResponseBadRequest , Http404, JsonResponse, QueryDict 
from django.http.response import HttpResponseRedirect
from django.views.generic import ListView # this is the generic view
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy, reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core import serializers
import json
import os.path
import time
import datetime
from django.db.models import Q, Sum
from django.db import connection
from datetime import date
from django.utils.timezone import now
from decimal import *
from functools import reduce
from django.core.exceptions import ValidationError
from .models import (PerformanceEvaluation, CatalogTechnicalCompetence,  CatalogUniversalCompetence, CatalogTeamwork, TypeContractsEvaluate,
    InstitutionalUnitEvaluation, PersonalEvaluation, Report, ActivityEvaluation, Activity, KnowledgeEvaluation, Knowledge, 
    TechnicalCompetenceEvaluation, TechnicalCompetence, UniversalCompetenceEvaluation, UniversalCompetence, TeamworkEvaluation, Teamwork,
    ComplaintEvaluation, Complaint,ChiefObservation)
from .forms import (PerformanceEvaluationForm, InstitutionalUnitEvaluationForm, PersonalEvaluationForm, ReportForm,
    CatalogTechnicalCompetenceForm, CatalogUniversalCompetenceForm, CatalogTeamworkForm, InstitutionalUnitEvaluationForm, 
    ActivityForm, KnowledgeForm, TechnicalCompetenceForm, UniversalCompetenceForm, TeamworkForm, ChiefObservationForm, ComplaintForm)
from apps.person.models import Person
from apps.employee.models import Employee

from apps.institution.models import Directivo, Direccion, JefaturaCoordinacion, Institucion
from apps.contract.models import LaborSystem, TypeOfContract, Contract
from apps.curriculum.models import CurriculumVitae, FormalInstruction
from apps.settings.models import Catalog, Item
from apps.directorinstitutionalunit.models import HistoryDirectorInstitutionalUnit
from reportlab.pdfgen import canvas
from django.core.exceptions import ObjectDoesNotExist
from reportlab.pdfgen import canvas
from reportlab.platypus import (SimpleDocTemplate, PageBreak, Image, Spacer,
    Paragraph, Table, TableStyle)
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.rl_config import defaultPageSize
from reportlab.lib.units import inch
from reportlab.lib.enums import TA_LEFT, TA_RIGHT, TA_CENTER, TA_JUSTIFY
from reportlab.lib.pagesizes import A4, letter, landscape
from reportlab.platypus import (BaseDocTemplate, Frame, Paragraph, 
    NextPageTemplate, PageBreak, PageTemplate)
from reportlab.lib import colors
from io import BytesIO
from .file_test import go

PAGE_WIDTH = A4[0]
PAGE_HEIGHT = A4[1]

styles = getSampleStyleSheet()


def set_if_not_none(mapping, key, value):
    print(value)
    if not (value is None or value is ""):
        print('intro >>>')
        mapping[key] = value

class PerformanceEvaluationList(ListView):
    STATE_CHOICES = [
        ('ACTIVE', 'Activo'),
        ('INACTIVE', 'Inactivo'),
    ]
    choices = {
        '': '----------',
        'GENERATED': 'Generada',
        'INITIATED': 'Iniciada', 
        'FINALIZED': 'Finalizada'
        }
    model = PerformanceEvaluation
    template_name = 'performance_evaluation/perform_eval_list.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(PerformanceEvaluationList, self).get_context_data(**kwargs)
        context['list_states'] = self.choices
        state = self.request.GET.get('state')
        year = self.request.GET.get('year')
        # The params tpsort by
        print('get_context_data')
        set_if_not_none(context, 'state', state)
        set_if_not_none(context, 'year', year)
        #print("{} {}".format(">>>", len(params)))
        return context

    def get_queryset(self):
         # A bunch of gets
        institution_id = self.request.session['institution_id']
        state = self.request.GET.get('state')
        year = self.request.GET.get('year')
        # The params tpsort by
        params = {}
        print('get_queryset')
        set_if_not_none(params, 'state', state)
        set_if_not_none(params, 'year', year)

        #print("{} {}".format(">>>", len(params)))
        queryset = PerformanceEvaluation.objects.filter(institution__id=institution_id).order_by('-year')      
        #if self.request.GET.get("state"):
            #selection = self.request.GET.get("state")
            #queryset = PerformanceEvaluation.objects.filter(state=selection)
        if (len(params) > 0):
            #queryset = PerformanceEvaluation.objects.filter(**params)
            query = Q()
            query &= Q(institution__id=institution_id)
            if not (state is None or state is ""):
                query &= Q(state__exact=state)
            if not (year is None or year is ""):
                query &= Q(year__exact=year)
            queryset =  PerformanceEvaluation.objects.filter(query).order_by('-year') 
        return queryset

class PerformanceEvaluationDetail(DetailView):
    model = PerformanceEvaluation
    template_name = 'performance_evaluation/perform_eval_detail.html'

class PerformanceEvaluationCreation(CreateView):
    form_class = PerformanceEvaluationForm
    success_url = reverse_lazy('evaluation:performance_evaluation_all')
    template_name = 'performance_evaluation/perform_eval_form.html'
    # fields = '__all__'

    # def dispatch(self, *args, **kwargs):
    #     print('intro >>> dispatch')
    #     institution_id = self.request.session['institution_id']
    #     self.institution = get_object_or_404(Institution, pk=institution_id)
    #     return super(PerformanceEvaluationCreation, self).dispatch(*args, **kwargs)

    # def get_form_kwargs(self, **kwargs):
    #     print('intro >>> get_form_kwargs')
    #     print('>>>>> PARAM -> institution: %s' % (self.institution))
    #     form_kwargs = super(PerformanceEvaluationCreation, self).get_form_kwargs(**kwargs)
    #     form_kwargs["institution"] = self.institution
    #     return form_kwargs

    def get_initial(self):
        print('intro >>>>> get_initial')
        institution_id = self.request.session['institution_id']
        institution = get_object_or_404(Institucion, pk=institution_id)
        return {
            'institution':institution,
        }

    def post(self, request, *args, **kwargs):
        print('intro >>> post')
        form = PerformanceEvaluationForm(data=self.request.POST)
        # form = PerformanceEvaluationForm(data=self.request.POST, institution = self.institution)
        if form.is_valid():
            print('form_valid')
            self.object = form.save(commit=False)
            # if self.validate_performance_evaluation():
            #     self.object.save()
            #     messages.success(self.request, 'La evaluación de desempeño ha sido creada correctamente.')
            #     url = reverse('evaluation:performance_evaluation_all', kwargs={})
            #     return HttpResponseRedirect(url)
            # else:
            #     print('form_invalid')
            #     print(form)
            #     messages.error(self.request, 'Ya se encuentra registrada una evalación para el año de evaluación ingresado.')
            #     context = RequestContext(self.request, {'form': form})
            #     return render_to_response(self.template_name, context)
            self.object.save()
            messages.success(self.request, 'La evaluación de desempeño ha sido creada correctamente.')
            url = reverse('evaluation:performance_evaluation_all', kwargs={})
            return HttpResponseRedirect(url)

        else:
            print('form_invalid')
            context = RequestContext(self.request, {'form': form})
            return render_to_response(self.template_name, context)

    # def validate_performance_evaluation(self):
    #     self.valid = False        
    #     try:
    #         perform_eval = PerformanceEvaluation.objects.get( 
    #             Q(year=self.object.year),
    #             Q(institution__pk=self.object.institution.id)
    #         )
    #     except ObjectDoesNotExist:
    #         self.valid = True
    #     return self.valid

class PerformanceEvaluationUpdate(UpdateView):
    model = PerformanceEvaluation
    form_class = PerformanceEvaluationForm
    success_url = reverse_lazy('evaluation:performance_evaluation_all')
    template_name = 'performance_evaluation/perform_eval_form.html'
    #fields = '__all__'
    # success_message = 'La evaluación de desempeño ha sido actualizada correctamente.'

    def get_object(self):
        print('>>> get_object')
        self.object = get_object_or_404(PerformanceEvaluation, pk=self.kwargs.get('pk')) 
        return self.object

    # def dispatch(self, *args, **kwargs):
    #     print('intro >>> dispatch')
    #     institution_id = self.request.session['institution_id']
    #     self.institution = get_object_or_404(Institution, pk=institution_id)
    #     return super(PerformanceEvaluationUpdate, self).dispatch(*args, **kwargs)

    # def get_form_kwargs(self, **kwargs):
    #     print('intro >>> get_form_kwargs')
    #     print('>>>>> PARAM -> institution: %s' % (self.institution))
    #     form_kwargs = super(PerformanceEvaluationUpdate, self).get_form_kwargs(**kwargs)
    #     form_kwargs["institution"] = self.institution
    #     return form_kwargs

    def get_context_data(self, **kwargs):
        print('>>> get_context_data')
        context = super(PerformanceEvaluationUpdate, self).get_context_data(**kwargs)
        return context

    def post(self, request, *args, **kwargs):
        print('>>> post')
        self.object = self.get_object()
        form = PerformanceEvaluationForm(instance=self.object, data=self.request.POST)
        if form.is_valid():
            print('form_valid')
            self.object = form.save(commit=False)
            self.object.save()
            messages.success(self.request, 'La evaluación de desempeño ha sido actualizada correctamente.')
            url = reverse('evaluation:performance_evaluation_all', kwargs={})
            return HttpResponseRedirect(url)
        else:
            print('form_invalid')
            context = RequestContext(self.request, { 'form': form })
            return render_to_response(self.template_name, context)

class PerformanceEvaluationDelete(DeleteView):
    model = PerformanceEvaluation
    success_url = reverse_lazy('evaluation:performance_evaluation_all')
    template_name = 'performance_evaluation/perform_eval_delete.html'

def performance_evaluation_delete(request, pk):
    print('intro performance_evaluation_delete')
    perform_eval = PerformanceEvaluation.objects.get(pk=pk)
    if perform_eval.state == 'GENERATED':
        perform_eval.delete()
        messages.success(request, 'La Evaluación de desempeño ha sido eliminada correctamente.')
    else:
        messages.error(request, 'La Evaluación de desempeño no se pudo eliminar, debido a que se encuentra iniciada o finalizada.')
    url = reverse('evaluation:performance_evaluation_all', kwargs={})
    return HttpResponseRedirect(url)

class CatalogTCList(ListView):
    model = CatalogTechnicalCompetence
    template_name = 'performance_evaluation/catalog_tc_list.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        print('>>> get_context_data')
        context = super(CatalogTCList, self).get_context_data(**kwargs)
        performance_evaluation_pk  = self.kwargs['perform_eval_pk']
        performance_evaluation = get_object_or_404(PerformanceEvaluation, pk=performance_evaluation_pk)
        context['performance_evaluation'] = performance_evaluation
        return context

    def get_queryset(self):
        print('>>> get_queryset')
        performance_evaluation_pk  = self.kwargs['perform_eval_pk']
        queryset =  CatalogTechnicalCompetence.objects.filter(performance_evaluation__pk=performance_evaluation_pk)
        return queryset

class CatalogTCDetail(DetailView):
    model = CatalogTechnicalCompetence
    template_name = 'performance_evaluation/catalog_tc_detail.html'

    def dispatch(self, *args, **kwargs):
        self.performance_evaluation = get_object_or_404(PerformanceEvaluation, pk=kwargs['perform_eval_pk'])
        return super(CatalogTCDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CatalogTCDetail, self).get_context_data(**kwargs)
        context['performance_evaluation'] = self.performance_evaluation
        return context


class CatalogTCCreation(CreateView):
    model = CatalogTechnicalCompetence
    success_url = reverse_lazy('evaluation:catalog_tc_all')
    template_name = 'performance_evaluation/catalog_tc_form.html'
    form_class = CatalogTechnicalCompetenceForm

    # def get_form(self, form_class):
    #     print('intro >>> get_form')
    #     form = super(CatalogTCCreation, self).get_form(form_class)
    #     form.fields["performance_evaluation"].initial = self.performance_evaluation
    #     return form

    def dispatch(self, *args, **kwargs):
        print('intro >>> dispatch')
        self.performance_evaluation = get_object_or_404(PerformanceEvaluation, pk=kwargs['perform_eval_pk'])
        return super(CatalogTCCreation, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        print('intro >>> get_context_data')
        context = super(CatalogTCCreation, self).get_context_data(**kwargs)
        context['performance_evaluation'] = self.performance_evaluation
        return context

    def form_valid(self, form):
        print('intro >>> form_valid')
        skill = self.request.POST['skill']
        relevance = self.request.POST['relevance']
        print('>>>>> skill: %s , relevance: %s, performance_evaluation: %s' %(skill, relevance, self.performance_evaluation.pk))
        catalog = CatalogTechnicalCompetence.objects.filter(
            skill=skill, relevance=relevance, performance_evaluation= self.performance_evaluation)
        if catalog:
            messages.error(self.request, 'Ya se encuentra registrada una destreza técnica con la relevancia seleccionada.')
            context = RequestContext(self.request, {'form': form, 'performance_evaluation': self.performance_evaluation})
            return render_to_response(self.template_name, context)
        else:
            self.object = form.save(commit=False)
            self.object.performance_evaluation = self.performance_evaluation
            self.object.save()
            messages.success(self.request, 'La competencia técnica ha sido creada correctamente.')
            url = reverse('evaluation:catalog_tc_all', kwargs={ 'perform_eval_pk': self.performance_evaluation.pk })
            return HttpResponseRedirect(url)

class CatalogTCUpdate(UpdateView):
    model = CatalogTechnicalCompetence
    success_url = reverse_lazy('evaluation:catalog_tc_all')
    template_name = 'performance_evaluation/catalog_tc_form.html'
    form_class = CatalogTechnicalCompetenceForm

    def get_object(self):
        print('>>> get_object')
        self.object = get_object_or_404(CatalogTechnicalCompetence, pk=self.kwargs.get('pk')) 
        return self.object

    def dispatch(self, *args, **kwargs):
        print('intro >>> dispatch')
        self.performance_evaluation = get_object_or_404(PerformanceEvaluation, pk=kwargs['perform_eval_pk'])
        return super(CatalogTCUpdate, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        print('>>> get_context_data')
        context = super(CatalogTCUpdate, self).get_context_data(**kwargs)
        context['performance_evaluation'] = self.performance_evaluation
        return context

    def post(self, request, *args, **kwargs):
        print('>>> post')
        self.object = self.get_object()
        form = CatalogTechnicalCompetenceForm(instance=self.object, data=self.request.POST)
        if form.is_valid():
            print('form_valid')
            skill = self.request.POST['skill']
            relevance = self.request.POST['relevance']
            print('>>>>>id: %s, skill: %s , relevance: %s, performance_evaluation: %s'
                % (self.object.id, skill, relevance, self.performance_evaluation.pk))
            catalog = CatalogTechnicalCompetence.objects.filter(
            skill=skill, relevance=relevance, performance_evaluation= self.performance_evaluation).exclude(id=self.object.id)
            if catalog:
                messages.error(self.request, 'Ya se encuentra registrada una destreza técnica con la relevancia seleccionada.')
                context = RequestContext(self.request, {'form': form, 'performance_evaluation': self.performance_evaluation})
                return render_to_response(self.template_name, context)
            else:
                self.object = form.save(commit=False)
                self.object.performance_evaluation = self.performance_evaluation
                self.object.save()
                #url = reverse('accounting_finance:accounts_plan_all')
                #return HttpResponseRedirect('%s?accounting_period=%s' % (url, self.accounting_period.pk))
                messages.success(self.request, 'La competencia técnica ha sido actualizada correctamente.')
                url = reverse('evaluation:catalog_tc_all', kwargs={ 'perform_eval_pk': self.performance_evaluation.pk })
                return HttpResponseRedirect(url)
        else:
            #return self.render_to_response(self.get_context_data(form=form))
            print('form_invalid')
            print(form)
            context = RequestContext(self.request, {'form': form, 'performance_evaluation': self.performance_evaluation})
            return render_to_response(self.template_name, context)

class CatalogTCDelete(DeleteView):
    model = CatalogTechnicalCompetence
    success_url = reverse_lazy('evaluation:catalog_tc_all')
    template_name = 'performance_evaluation/evaluation_delete.html'

def catalog_tc_delete(request):
    print('intro catalog_tc_delete')
    if request.method == 'DELETE':
        cat_tc_id = int(QueryDict(request.body).get('cat_tc_id'))
        print(cat_tc_id)
        cat_tc = CatalogTechnicalCompetence.objects.get(pk=cat_tc_id)
        perform_eval = cat_tc.performance_evaluation
        if perform_eval.state == 'GENERATED':
            cat_tc.delete()
            messages.success(request, 'La competencia técnica ha sido eliminada correctamente.')
        else:
            messages.error(request, 'La competencia universal no se puedo eliminar, debido a que la evaluación de desempeño se encuentra iniciada o finalizada.')
        
        response_data = {}
        response_data['msg'] = 'Ok'
        return HttpResponse(json.dumps(response_data), content_type="application/json")

def catalog_uc_delete(request, pk):
    print('intro catalog_uc_delete')
    #perform_eval = PerformanceEvaluation.objects.get(pk=pk)
    cata_univ_comp = CatalogUniversalCompetence.objects.get(pk=pk)
    perform_eval = cata_univ_comp.performance_evaluation
    if perform_eval.state == 'GENERATED':
        cata_univ_comp.delete()
        messages.success(request, 'La competencia universal ha sido eliminada correctamente.')
    else:
        messages.error(request, 'La competencia universal no se puedo eliminar, debido a que la evaluación de desempeño se encuentra iniciada o finalizada.')
    url = reverse('evaluation:catalog_uc_all', kwargs={'perform_eval_pk':perform_eval.pk})

    return HttpResponseRedirect(url)

class CatalogUCList(ListView):
    model = CatalogUniversalCompetence
    template_name = 'performance_evaluation/catalog_uc_list.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        print('>>> get_context_data')
        context = super(CatalogUCList, self).get_context_data(**kwargs)
        performance_evaluation_pk  = self.kwargs['perform_eval_pk']
        performance_evaluation = get_object_or_404(PerformanceEvaluation, pk=performance_evaluation_pk)
        context['performance_evaluation'] = performance_evaluation
        return context

    def get_queryset(self):
        print('>>> get_queryset')
        performance_evaluation_pk  = self.kwargs['perform_eval_pk']
        queryset =  CatalogUniversalCompetence.objects.filter(performance_evaluation__pk=performance_evaluation_pk)
        return queryset

class CatalogUCDetail(DetailView):
    model = CatalogUniversalCompetence
    template_name = 'performance_evaluation/catalog_uc_detail.html'

    def dispatch(self, *args, **kwargs):
        self.performance_evaluation = get_object_or_404(PerformanceEvaluation, pk=kwargs['perform_eval_pk'])
        return super(CatalogUCDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CatalogUCDetail, self).get_context_data(**kwargs)
        context['performance_evaluation'] = self.performance_evaluation
        return context

class CatalogUCCreation(CreateView):
    model = CatalogUniversalCompetence
    success_url = reverse_lazy('evaluation:catalog_uc_all')
    template_name = 'performance_evaluation/catalog_uc_form.html'
    form_class = CatalogUniversalCompetenceForm

    def dispatch(self, *args, **kwargs):
        self.performance_evaluation = get_object_or_404(PerformanceEvaluation, pk=kwargs['perform_eval_pk'])
        return super(CatalogUCCreation, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CatalogUCCreation, self).get_context_data(**kwargs)
        context['performance_evaluation'] = self.performance_evaluation
        return context

    def form_valid(self, form):
        print('intro >>> form_valid')
        skill = self.request.POST['skill']
        relevance = self.request.POST['relevance']
        print('>>>>> skill: %s , relevance: %s, performance_evaluation: %s' %(skill, relevance, self.performance_evaluation.pk))
        catalog = CatalogUniversalCompetence.objects.filter(
            skill=skill, relevance=relevance, performance_evaluation= self.performance_evaluation)
        if catalog:
            messages.error(self.request, 'Ya se encuentra registrada una destreza técnica con la relevancia seleccionada.')
            context = RequestContext(self.request, {'form': form, 'performance_evaluation': self.performance_evaluation})
            return render_to_response(self.template_name, context)
        else:
            self.object = form.save(commit=False)
            self.object.performance_evaluation = self.performance_evaluation
            self.object.save()
            messages.success(self.request, 'La competencia universal ha sido creada correctamente.')
            #return HttpResponseRedirect(self.get_success_url())
            url = reverse('evaluation:catalog_uc_all', kwargs={ 'perform_eval_pk': self.performance_evaluation.pk })
            return HttpResponseRedirect(url)

class CatalogUCUpdate(UpdateView):
    model = CatalogUniversalCompetence
    success_url = reverse_lazy('evaluation:catalog_uc_all')
    template_name = 'performance_evaluation/catalog_uc_form.html'
    form_class = CatalogUniversalCompetenceForm

    def get_object(self):
        print('>>> get_object')
        self.object = get_object_or_404(CatalogUniversalCompetence, pk=self.kwargs.get('pk')) 
        return self.object

    def dispatch(self, *args, **kwargs):
        print('intro >>> dispatch')
        self.performance_evaluation = get_object_or_404(PerformanceEvaluation, pk=kwargs['perform_eval_pk'])
        return super(CatalogUCUpdate, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        print('>>> get_context_data')
        context = super(CatalogUCUpdate, self).get_context_data(**kwargs)
        context['performance_evaluation'] = self.performance_evaluation
        return context

    def post(self, request, *args, **kwargs):
        print('>>> post')
        self.object = self.get_object()
        form = CatalogUniversalCompetenceForm(instance=self.object, data=self.request.POST)
        if form.is_valid():
            print('form_valid')
            skill = self.request.POST['skill']
            relevance = self.request.POST['relevance']
            print('>>>>>id: %s, skill: %s , relevance: %s, performance_evaluation: %s'
                % (self.object.id, skill, relevance, self.performance_evaluation.pk))
            catalog = CatalogUniversalCompetence.objects.filter(
                skill=skill, relevance=relevance, performance_evaluation=self.performance_evaluation).exclude(id=self.object.id)
            if catalog:
                messages.error(self.request, 'Ya se encuentra registrada una destreza técnica con la relevancia seleccionada.')
                context = RequestContext(self.request, {'form': form, 'performance_evaluation': self.performance_evaluation})
                return render_to_response(self.template_name, context)
            else:
                self.object = form.save(commit=False)
                self.object.performance_evaluation = self.performance_evaluation
                self.object.save()
                messages.success(self.request, 'La competencia universal ha sido actualizada correctamente.')
                url = reverse('evaluation:catalog_uc_all', kwargs={ 'perform_eval_pk': self.performance_evaluation.pk })
                return HttpResponseRedirect(url)
        else:
            print('form_invalid')
            context = RequestContext(self.request, {'form': form, 'performance_evaluation': self.performance_evaluation})
            return render_to_response(self.template_name, context)

class CatalogUCDelete(DeleteView):
    model = CatalogUniversalCompetence
    success_url = reverse_lazy('evaluation:catalog_tc_all')
    template_name = 'performance_evaluation/evaluation_delete.html'

# def catalog_uc_delete(request):
#     print('intro catalog_uc_delete')
#     if request.method == 'DELETE':
#         cat_uc_id = int(QueryDict(request.body).get('cat_uc_id'))
#         print(cat_uc_id)
#         cat_uc = CatalogUniversalCompetence.objects.get(pk=cat_uc_id)
#         cat_uc.delete()
#         response_data = {}
#         response_data['msg'] = 'La competencia universal ha sido borrara del catálogo.'
#         return HttpResponse(
#             json.dumps(response_data),
#             content_type="application/json"
#         )

def catalog_uc_delete(request, pk):
    print('intro catalog_uc_delete')
    #perform_eval = PerformanceEvaluation.objects.get(pk=pk)
    cata_univ_comp = CatalogUniversalCompetence.objects.get(pk=pk)
    perform_eval = cata_univ_comp.performance_evaluation
    if perform_eval.state == 'GENERATED':
        cata_univ_comp.delete()
        messages.success(request, 'La competencia universal ha sido eliminada correctamente.')
    else:
        messages.error(request, 'La competencia universal no se puedo eliminar, debido a que la evaluación de desempeño se encuentra iniciada o finalizada.')
    url = reverse('evaluation:catalog_uc_all', kwargs={'perform_eval_pk':perform_eval.pk})

    return HttpResponseRedirect(url)

class CatalogTWList(ListView):
    model = CatalogTeamwork
    template_name = 'performance_evaluation/catalog_tw_list.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        print('>>> get_context_data')
        context = super(CatalogTWList, self).get_context_data(**kwargs)
        performance_evaluation_pk  = self.kwargs['perform_eval_pk']
        performance_evaluation = get_object_or_404(PerformanceEvaluation, pk=performance_evaluation_pk)
        context['performance_evaluation'] = performance_evaluation
        return context

    def get_queryset(self):
        print('>>> get_queryset')
        performance_evaluation_pk  = self.kwargs['perform_eval_pk']
        queryset =  CatalogTeamwork.objects.filter(performance_evaluation__pk=performance_evaluation_pk)
        return queryset

class CatalogTWDetail(DetailView):
    model = CatalogTeamwork
    template_name = 'performance_evaluation/catalog_tw_detail.html'

    def dispatch(self, *args, **kwargs):
        self.performance_evaluation = get_object_or_404(PerformanceEvaluation, pk=kwargs['perform_eval_pk'])
        return super(CatalogTWDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CatalogTWDetail, self).get_context_data(**kwargs)
        context['performance_evaluation'] = self.performance_evaluation
        return context

class CatalogTWCreation(CreateView):
    model = CatalogTeamwork
    success_url = reverse_lazy('evaluation:catalog_tw_all')
    template_name = 'performance_evaluation/catalog_tw_form.html'
    form_class = CatalogTeamworkForm

    def dispatch(self, *args, **kwargs):
        self.performance_evaluation = get_object_or_404(PerformanceEvaluation, pk=kwargs['perform_eval_pk'])
        return super(CatalogTWCreation, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CatalogTWCreation, self).get_context_data(**kwargs)
        context['performance_evaluation'] = self.performance_evaluation
        return context

    def form_valid(self, form):
        print('intro >>> form_valid')
        ability = self.request.POST['ability']
        relevance = self.request.POST['relevance']
        print('>>>>> ability: %s , relevance: %s, performance_evaluation: %s' %(ability, relevance, self.performance_evaluation.pk))
        catalog = CatalogTeamwork.objects.filter(
            ability=ability, relevance=relevance, performance_evaluation= self.performance_evaluation)
        if catalog:
            messages.error(self.request, 'Ya se encuentra registrada una destreza técnica con la relevancia seleccionada.')
            context = RequestContext(self.request, {'form': form, 'performance_evaluation': self.performance_evaluation})
            return render_to_response(self.template_name, context)
        else:
            self.object = form.save(commit=False)
            self.object.performance_evaluation = self.performance_evaluation
            self.object.save()
            messages.success(self.request, 'La aptitud de trabajo en equipo ha sido creada correctamente.')
            url = reverse('evaluation:catalog_tw_all', kwargs={ 'perform_eval_pk': self.performance_evaluation.pk })
            return HttpResponseRedirect(url)

class CatalogTWUpdate(UpdateView):
    model = CatalogTeamwork
    success_url = reverse_lazy('evaluation:catalog_tw_all')
    template_name = 'performance_evaluation/catalog_tw_form.html'
    form_class = CatalogTeamworkForm

    def dispatch(self, *args, **kwargs):
        self.performance_evaluation = get_object_or_404(PerformanceEvaluation, pk=kwargs['perform_eval_pk'])
        return super(CatalogTWUpdate, self).dispatch(*args, **kwargs)

    def get_object(self):
        self.object = get_object_or_404(CatalogTeamwork, pk=self.kwargs.get('pk')) 
        return self.object

    def get_context_data(self, **kwargs):
        context = super(CatalogTWUpdate, self).get_context_data(**kwargs)
        context['performance_evaluation'] = self.performance_evaluation
        return context

    def form_valid(self, form):
        print('intro >>> form_valid')
        self.object = self.get_object()
        ability = self.request.POST['ability']
        relevance = self.request.POST['relevance']
        print('>>>>> ability: %s , relevance: %s, performance_evaluation: %s' %(ability, relevance, self.performance_evaluation.pk))
        catalog = CatalogTeamwork.objects.filter(
            ability=ability, relevance=relevance, performance_evaluation= self.performance_evaluation).exclude(id=self.object.id)
        if catalog:
            messages.error(self.request, 'Ya se encuentra registrada una destreza técnica con la relevancia seleccionada.')
            context = RequestContext(self.request, {'form': form, 'performance_evaluation': self.performance_evaluation})
            return render_to_response(self.template_name, context)
        else:
            self.object = form.save(commit=False)
            self.object.performance_evaluation = self.performance_evaluation
            self.object.save()
            messages.success(self.request, 'La aptitud de trabajo en equipo ha sido creada correctamente.')
            url = reverse('evaluation:catalog_tw_all', kwargs={ 'perform_eval_pk': self.performance_evaluation.pk })
            return HttpResponseRedirect(url)

class CatalogTWDelete(DeleteView):
    model = CatalogTeamwork
    success_url = reverse_lazy('evaluation:catalog_tw_all')
    template_name = 'performance_evaluation/catalog_tw_delete.html'

def catalog_tw_delete(request, pk):
    print('intro catalog_tw_delete')
    cata_teamw = CatalogTeamwork.objects.get(pk=pk)
    perform_eval = cata_teamw.performance_evaluation
    if perform_eval.state == 'GENERATED':
        cata_teamw.delete()
        messages.success(request, 'La aptitud de trabajo en equipo ha sido eliminada correctamente.')
    else:
        messages.error(request, 'La aptitud de trabajo en equipo no se puedo eliminar, debido a que la evaluación de desempeño se encuentra iniciada o finalizada.')
    url = reverse('evaluation:catalog_tw_all', kwargs={'perform_eval_pk':perform_eval.pk})
    return HttpResponseRedirect(url)

def selection_contracts(request, perform_eval_pk):
    print('>>> selection_contracts')
    performance_evaluation = PerformanceEvaluation.objects.get(pk=perform_eval_pk)
    list_type_contracts_evaluate = TypeContractsEvaluate.objects.filter(performance_evaluation__pk=perform_eval_pk) 
    list_type_contracts = TypeOfContract.objects.all()
    if request.method == 'GET':
        if list_type_contracts_evaluate:
            id_list = get_id_list(list_type_contracts_evaluate)
            print(id_list)
            list_type_contracts = TypeOfContract.objects.all().exclude(id__in=id_list)    
        context = {'performance_evaluation': performance_evaluation ,'list_type_contracts': list_type_contracts, 'list_type_contracts_evaluate': list_type_contracts_evaluate}
        return render(request, 'performance_evaluation/selection_contracts.html', context)
    if request.method == 'POST':
        selected_values = request.POST.getlist('to')
        selected_values = [int(i) for i in selected_values]
        final_list = selected_values
        deleted_list = []
        if list_type_contracts_evaluate:
            print('it contains elements')
            id_list = get_id_list(list_type_contracts_evaluate)
            print(id_list)
            deleted_list = [item for item in id_list if item not in selected_values]
            final_list = [item for item in selected_values if item not in id_list]
        print(selected_values)
        print(deleted_list)
        print(final_list)
        if deleted_list:
            for var in deleted_list:
                type_contracts_evaluate = TypeContractsEvaluate.objects.filter(
                    Q(type_contract__pk=var),
                    Q(performance_evaluation__pk=perform_eval_pk)
                    )
                type_contracts_evaluate.delete()
        if final_list:
            for var in final_list:
                type_contract = TypeOfContract.objects.get(pk=var)
                tce = TypeContractsEvaluate(type_contract = type_contract, performance_evaluation = performance_evaluation)
                tce.save()
        messages.success(request, 'La lista de contratos a evaluar ha sido actualizada correcatamente.')
        url = reverse('evaluation:selection_contracts', kwargs={ 'perform_eval_pk': performance_evaluation.pk })
        return HttpResponseRedirect(url)

def get_id_list(list):
    id_list = []
    for var in list:
        id_list.append(var.type_contract.id)
    return id_list

def inst_unit_eval_all(request, perform_eval_pk):
    print('intro >>> inst_unit_eval_index')
    performance_evaluation = PerformanceEvaluation.objects.get(pk=perform_eval_pk)
    ins_unit_eval_list = InstitutionalUnitEvaluation.objects.filter(performance_evaluation__pk=perform_eval_pk)
    paginator = Paginator(ins_unit_eval_list, 10)
    page = request.GET.get('page')
    try:
        objects = paginator.page(page)
    except PageNotAnInteger:
        objects = paginator.page(1)
    except EmptyPage:
        objects = paginator.page(paginator.num_pages)
    context = {'objects': objects, 'performance_evaluation': performance_evaluation} 
    return render(request, 'performance_evaluation/inst_unit_eval_list.html', context)

class InstUnitEvalDetail(DetailView):
    model = InstitutionalUnitEvaluation
    template_name = 'performance_evaluation/inst_unit_eval_detail.html'

    def dispatch(self, *args, **kwargs):
        self.performance_evaluation = get_object_or_404(PerformanceEvaluation, pk=kwargs['perform_eval_pk'])
        return super(CatalogTWDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CatalogTWDetail, self).get_context_data(**kwargs)
        context['performance_evaluation'] = self.performance_evaluation
        return context

def inst_unit_eval_detail(request, perform_eval_pk, pk):
    inst_unit_eval = get_object_or_404(InstitutionalUnitEvaluation, pk=pk)
    perform_eval = PerformanceEvaluation.objects.get(pk=perform_eval_pk)
    context = {'object': inst_unit_eval, 'perform_eval': perform_eval}
    return render(request, 'performance_evaluation/inst_unit_eval_detail.html', context)

def inst_unit_eval_create(request, perform_eval_pk):
    print('intro >>> ins_unit_eval_create')
    perform_eval = PerformanceEvaluation.objects.get(pk=perform_eval_pk)
    if request.method=='POST':
        form = InstitutionalUnitEvaluationForm(data=request.POST)
        if form.is_valid():
            inst_unit_eval = form.save(commit=False)
            inst_unit_eval.performance_evaluation = perform_eval
            result = check_data_create(inst_unit_eval) 
            if result: 
                for m in result:
                    messages.error(request, m)
                context = RequestContext(request, {'form': form, 'perform_eval': perform_eval})
                return render_to_response(
                    'performance_evaluation/inst_unit_eval_form.html', context)
            else:
                inst_unit_eval.save()
                messages.success(request, 'La evaluación para la Unidad Instucional ha sido creada correcatamente.')
                return redirect('evaluation:inst_unit_eval_all', perform_eval_pk=perform_eval_pk)
        else:
            context = RequestContext(request, {'form': form, 'perform_eval': perform_eval})
            return render_to_response(
                'performance_evaluation/inst_unit_eval_form.html', context)

    form = InstitutionalUnitEvaluationForm()
    #form.fields["performance_evaluation"].initial = perform_eval
    context = {'form': form, 'perform_eval': perform_eval}
    return render(request, 'performance_evaluation/inst_unit_eval_form.html', context)

def check_data_create(inst_unit_eval):
    print('intro >>> check_data')
    messages_list = []
    result = InstitutionalUnitEvaluation.objects.filter(
        Q(performance_evaluation__pk=inst_unit_eval.performance_evaluation.pk) &
        Q(institutional_unit__pk = inst_unit_eval.institutional_unit.pk)
    )
    if result:
        messages_list.append('Ya existe la evaluación para el departamento seleccionado.')
    return messages_list

def inst_unit_eval_edit(request, perform_eval_pk, pk):
    inst_unit_eval = get_object_or_404(InstitutionalUnitEvaluation, pk=pk)
    perform_eval = PerformanceEvaluation.objects.get(pk=perform_eval_pk)
    if request.method == 'POST':
        form = InstitutionalUnitEvaluationForm(instance=inst_unit_eval, data=request.POST)
        if form.is_valid():
            inst_unit_eval = form.save(commit=False)
            inst_unit_eval.performance_evaluation = perform_eval
            errors = check_data_edit(inst_unit_eval) 
            if errors: 
                for e in errors:
                    messages.error(request, e)
                context = RequestContext(request, {'form': form, 'perform_eval': perform_eval})
                return render_to_response(
                    'performance_evaluation/inst_unit_eval_form.html', context)
            else:
                inst_unit_eval.save()
                messages.success(request, 'La evaluación para la Unidad Instucional ha sido actualizada correcatamente.')
                return redirect('evaluation:inst_unit_eval_all', perform_eval_pk=perform_eval_pk)
        else:
            context = RequestContext(request, {'form': form, 'perform_eval': perform_eval})
            return render('settings/cities.html', context)
    form = InstitutionalUnitEvaluationForm(instance=inst_unit_eval)
    context = {'form': form, 'perform_eval': perform_eval}
    return render(request, 'performance_evaluation/inst_unit_eval_form.html', context)

def inst_unit_eval_delete(request, pk):
    print('intro inst_unit_eval_delete')
    inst_unit_eval = InstitutionalUnitEvaluation.objects.get(pk=pk)
    perform_eval = inst_unit_eval.performance_evaluation    
    if inst_unit_eval.state == 'GENERATED':
        inst_unit_eval.delete()    
        messages.success(request, 'La evaluación de desempeño para la unidad institucional ha sido eliminada correctamente')
    else:
        messages.error(request, 'La evaluación de desempeño para la unidad institucional no se ha podido eliminar, debido a que se encuentra iniciada o finalizada.')
    url = reverse('evaluation:inst_unit_eval_all', kwargs={'perform_eval_pk':perform_eval.pk})
    return HttpResponseRedirect(url)

def check_data_edit(inst_unit_eval):
    print('intro >>> check_data')
    messages_list = []
    result = InstitutionalUnitEvaluation.objects.filter(
        Q(performance_evaluation__pk=inst_unit_eval.performance_evaluation.pk) &
        Q(institutional_unit__pk = inst_unit_eval.institutional_unit.pk)
    ).exclude(pk=inst_unit_eval.pk)
    if result:
        messages_list.append('Ya existe la evaluación para el departamento seleccionado.')
    return messages_list

def initiate_performance_eval(request, perform_eval_pk):
    print('>>> initiate_performance_eval')
    #performance_eval_id = request.GET.get('perform_eval_pk')
    print(perform_eval_pk)
    performance_eval = PerformanceEvaluation.objects.get(pk = perform_eval_pk)
    data_validation = validate_initiate_performance_eval(perform_eval_pk)
    if data_validation:
        for n in data_validation:
            messages.error(request, n)
    else:
        performance_eval.state = 'INITIATED'
        performance_eval.save()
        messages.success(request, "La Evaluación de desempeño ha sido iniciada correctamente.")    
    url = reverse('evaluation:performance_evaluation_all', kwargs={})
    return HttpResponseRedirect(url)

def validate_initiate_performance_eval(perform_eval_pk):
    print('>>> validate_initiate_performance_eval')
    messages_list = []
    
    amount_perform_eval_active = PerformanceEvaluation.objects.filter(state = 'INITIATED').count()
    if amount_perform_eval_active > 0:
        messages_list.append('Se encuentra activa una evaluación de desempeño, por favor finalice la evaluación para continuar.')
        return messages_list

    amount_ctc = CatalogTechnicalCompetence.objects.filter(performance_evaluation__pk = perform_eval_pk).count()
    if amount_ctc <= 0:
        messages_list.append('No se encuentran registradas las competencias técnicas a evaluar')
        return messages_list
    
    amount_cuc = CatalogUniversalCompetence.objects.filter(performance_evaluation__pk = perform_eval_pk).count()
    if amount_cuc <= 0:
        messages_list.append('No se encuentran registradas las competencias universales a evaluar')
        return messages_list

    amount_ctw = CatalogTeamwork.objects.filter(performance_evaluation__pk = perform_eval_pk).count()
    if amount_ctw <= 0:
        messages_list.append('No se encuentran registradas las aptitudes del trabajo en equipo a evaluar')
        return messages_list

    amount_tce = TypeContractsEvaluate.objects.filter(performance_evaluation__pk = perform_eval_pk).count()
    if amount_tce <= 0:
        messages_list.append('No se encuentran registrados los tipos de contratos a evaluar')
        return messages_list

    return messages_list

def finalize_performance_eval(request, perform_eval_pk):
    print('>>> finalize_performance_eval')
    performance_eval = PerformanceEvaluation.objects.get(pk = perform_eval_pk)
    list_inst_unit_eval = InstitutionalUnitEvaluation.objects.filter(
        Q(performance_evaluation__pk = perform_eval_pk) &
        Q(state='GENERATED') |
        Q(state='INITIATED')
        )
    if list_inst_unit_eval:
        messages.error(request, "No se puede finalizar la Evaluación de desempeño, existen evaluaciones pendientes por Unidad Institucional.")
    else:
        list_inst_unit_eval = InstitutionalUnitEvaluation.objects.filter(performance_evaluation__pk = perform_eval_pk)
        count = 0
        total_amount = 0
        total_evaluated = 0
        for inst_unit_eval in list_inst_unit_eval:
            count = count + 1
            total_evaluated += inst_unit_eval.quantity_evaluated
            total_amount += inst_unit_eval.percentage_evaluation
        print(">>> total_amount: %s - count: %s - total_evaluated:%s" % (total_amount, count, total_evaluated))
        performance_eval.quantity_evaluated = total_evaluated
        performance_eval.percentage_evaluation = total_amount/count
        performance_eval.state = 'FINALIZED'
        performance_eval.save()
        messages.success(request, "La Evaluación se desempeño ha sido finalizada correctamente.")
    url = reverse('evaluation:performance_evaluation_all', kwargs={})
    return HttpResponseRedirect(url)

def initiate_inst_unit_eval(request, inst_unit_eval_pk):
    """
     Metodo donde se inicia el proceso para la evaluacion por unidad institucional, el proceso inicia con la busqueda de contratos activos 
     que pertenecen a la unidad institucional, filtrados por los tipos de contratos seleccionados a evaluar, posteriormente se crea una
     evaluacion de personal para cada uno de los contratos como resultado de la busqueda de los contratos.
    """
    print('>>> intro initiate_inst_unit_eval')
    time.sleep(1)
    #inst_unit_eval_id = request.GET.get('inst_unit_eval_pk')
    inst_unit_eval = InstitutionalUnitEvaluation.objects.get(pk=inst_unit_eval_pk)
    data_validation = validate_initiate_inst_unit_eval(inst_unit_eval)

    if data_validation:
        for n in data_validation:
            messages.error(request, n)
    else:
        institutional_unit = inst_unit_eval.institutional_unit
        performance_eval = inst_unit_eval.performance_evaluation
        history_director_unit = HistoryDirectorInstitutionalUnit.objects.filter(institutional_unit__id=institutional_unit.id, status=True)[0]
        institutional_unit_director = history_director_unit.employee
        print('>>>>> HISTORY DIRECTOR UNIT %s' % (history_director_unit.employee))
        department_eval_list = list(Department.objects.filter(institutional_unit__id = institutional_unit.id))
        department_list = []
        if department_eval_list:
            for var in department_eval_list:
                department_list.append(var.id)
        print(department_list)

        type_contracts_eval_list = list(TypeContractsEvaluate.objects.filter(performance_evaluation = performance_eval))
        contract_type_list = []
        if type_contracts_eval_list:
            for var in type_contracts_eval_list:
                contract_type_list.append(var.type_contract)
        print(contract_type_list)
        list_contract = Contract.objects.filter(
                        # Q(institutional_unit__id=int(inst_unit_eval.institutional_unit.pk)), 
                        Q(status='Firmado'),
                        Q(start_date__lte=date(performance_eval.year, 12, 31))
                        ).filter(department__in=list(department_list)
                        ).filter(type_of_contract__in=list(contract_type_list))
        print(list_contract)
        for contract in list_contract:
            evaluated = contract.employee
            # immediate_superior = Employee.objects.get(pk=1)
            # institutional_unit_director = Employee.objects.get(pk=1)
            personal_evaluation = PersonalEvaluation(
                start_date=now(),
                period_from= get_period_from_to_evaluate(performance_eval, contract),
                period_to=date(performance_eval.year, 12, 31),
                state='GENERATED',
                job_management_indicators=Decimal('0.00'),
                knowledge=Decimal('0.00'),
                technical_competence=Decimal('0.00'), 
                universal_competence=Decimal('0.00'),
                teamwork=Decimal('0.00'),
                evaluation_citizens=Decimal('0.00'),
                final_evaluation_percentage=Decimal('0.00'),
                final_evaluation_result='PROCESO INCORRECTO',
                evaluated=evaluated,
                contract= contract,
                immediate_superior=institutional_unit_director,
                institutional_unit_director=institutional_unit_director,
                institutional_unit_evaluation = inst_unit_eval
            )
            personal_evaluation.save()
        inst_unit_eval.quantity_evaluated= inst_unit_eval.personalevaluation_set.count()
        inst_unit_eval.state='INITIATED'
        inst_unit_eval.save()
        messages.success(request, "La evaluación para la Unidad institucional ha iniciada correctamente")
    
    message = 'Request success'
    data = {"message":message}
    return JsonResponse(data)

def create_personal_evaluation_for_recovery(request, person_eval_pk):
    print('>>> intro create_personal_evaluation_for_recovery')
    person_eval_previus = PersonalEvaluation.objects.get(pk=person_eval_pk)
    inst_unit_eval = person_eval_previus.institutional_unit_evaluation
    # contract = Contract.objects.get(
    #                     Q(institutional_unit__id=int(inst_unit_eval.institutional_unit.pk)), 
    #                     Q(status='Firmado'),
    #                     Q(start_date__lte=date(performance_eval.year, 12, 31))
    #                     ).filter(type_of_contract__in=list(contract_type_list))
    evaluated = person_eval_previus.evaluated
    immediate_superior = person_eval_previus.immediate_superior
    institutional_unit_director = person_eval_previus.institutional_unit_director
    person_eval = PersonalEvaluation(
        start_date=now(),
        period_from= person_eval_previus.period_from,
        period_to=person_eval_previus.period_to,
        state='GENERATED',
        job_management_indicators=Decimal('0.00'),
        knowledge=Decimal('0.00'),
        technical_competence=Decimal('0.00'), 
        universal_competence=Decimal('0.00'),
        teamwork=Decimal('0.00'),
        evaluation_citizens=Decimal('0.00'),
        final_evaluation_percentage=Decimal('0.00'),
        final_evaluation_result='',
        evaluated=evaluated,
        immediate_superior=immediate_superior,
        institutional_unit_director=institutional_unit_director,
        institutional_unit_evaluation = inst_unit_eval
    )
    person_eval.save()
    person_eval_previus.state='CANCELED'
    person_eval_previus.save()
    messages.success(request, 'La nueva evaluación de desempeño ha sido generada exitosamente.')
    url = reverse('evaluation:personal_evaluation_low_all', kwargs={})
    return HttpResponseRedirect(url)

def get_period_from_to_evaluate(performance_eval, contract):
    """
    Metodo en el cual se verifica la fecha de inicio del contrato, si el contratto ha iniciado en el año de evaluacion,
    se tomara esta fecha como fecha de inicio de la evaluacion y si el contrato ha iniciado antes del año a evaluar se
    tomara como fecha de inicio para la evaluacion el primer dia, primer mes del año a evaluar
    """
    print('>>> get_period_from_to_evaluate')
    signature_date = contract.start_date
    signature_year = signature_date.year
    eval_year = performance_eval.year
    period_from = None
    if signature_year == eval_year:
        period_from = signature_date
    
    if signature_year < eval_year:
        period_from = date(eval_year, 1, 1)
    print(period_from)
    return period_from

def validate_initiate_inst_unit_eval(inst_unit_eval):
    """
    Metodo donde se valida el inicio de la evalucion por unidad institucional, para ello se verifica que la evaluacion
    general se encuentre en una estado INITIATED
    """
    print('>>> validate_initiate_inst_unit_eval')
    messages_list = []

    performance_evaluation = inst_unit_eval.performance_evaluation
    if performance_evaluation.state != 'INITIATED':
        print('>>>>> NO INITIATED')
        messages_list.append('La evaluación de desempeño debe estar Iniciada para continuar con la evaluación por unidades.')
    
    institutional_unit = inst_unit_eval.institutional_unit
    institutional_unit_director = HistoryDirectorInstitutionalUnit.objects.filter(institutional_unit__id=institutional_unit.id, status=True)
    print('>>>>> Director %s' % (institutional_unit_director))
    if not institutional_unit_director:
        print('>>>>> NO DIRECTOR')
        messages_list.append("No existe asignado un Director en " + institutional_unit.institutional_unit + ", favor asignar alguien para continuar." )

    return messages_list

def finalize_inst_unit_eval(request, inst_unit_eval_pk):
    print('>>> finalize_inst_unit_eval')
    time.sleep(3)
    inst_unit_eval = InstitutionalUnitEvaluation.objects.get(pk=inst_unit_eval_pk)
    performance_evaluation = inst_unit_eval.performance_evaluation
    person_eval_list = list(PersonalEvaluation.objects.filter(
        institutional_unit_evaluation__id = inst_unit_eval_pk))
    total_result = 0
    total_number_person_eval = PersonalEvaluation.objects.filter(institutional_unit_evaluation__id=inst_unit_eval_pk).count()
    finalized_number_person_eval = PersonalEvaluation.objects.filter(
        institutional_unit_evaluation__id=inst_unit_eval_pk, state='FINALIZED').count()
    print('total: %s - finalized: %s' % (total_number_person_eval, finalized_number_person_eval))
    if(total_number_person_eval == finalized_number_person_eval):
        # total_result = reduce(lambda x, y: x.final_evaluation_percentage + y.final_evaluation_percentage, person_eval_list)
        print("continue")
        total_result = PersonalEvaluation.objects.filter(
            institutional_unit_evaluation__id = inst_unit_eval_pk).aggregate(
            total=Sum('final_evaluation_percentage'))['total']
        print("total >>> %s" % total_result)

        # Person.objects.raw('SELECT * FROM evaluation_personalevaluation WHERE institutional_unit_evaluation_id = %s', [inst_unit_eval_pk])
        # query = 'SELECT SUM(final_evaluation_percentage) as total FROM evaluation_personalevaluation WHERE institutional_unit_evaluation_id = %s' % inst_unit_eval_pk
        # cursor = connection.cursor()
        # cursor.execute(query)
        # row = cursor.fetchone()
        # print(row[0])
        # print("row >>> %s" % (row))
        # sum_total = Person.objects.raw('SELECT SUM(final_evaluation_percentage) FROM evaluation_personalevaluation WHERE institutional_unit_evaluation_id = %s', 
        #     [inst_unit_eval_pk])
        # print(">>> %s" % (sum_total))
    
        # print(PersonalEvaluation.objects
        #     .filter(institutional_unit_evaluation__id = inst_unit_efinal_evaluation_percentageval_pk)
        #     .values('evaluated__person__name')institutional_unit_evaluation_id
        #     .annotate(total=Sum('final_evaluation_percentage'))
        #     .order_by('-total').query)
        # print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")

        print("# evaluados : %s - resultado: %s" % (total_number_person_eval, total_result))
        inst_unit_eval.quantity_evaluated = total_number_person_eval
        print("# resultado total : %s - # total de evaluadios: %s" % (total_result, total_number_person_eval))
        inst_unit_eval.percentage_evaluation = total_result/total_number_person_eval
        inst_unit_eval.state = 'FINALIZED'
        inst_unit_eval.save()
        messages.success(request, "La evaluación para la Unidad Institucional ha finalizado correctamente")
    else:
        print('break')
        messages.error(request, "La evaluación para la Unidad Institucional no se finalizó porque existen evaluaciones personales pendientes.")
    # b= [elem for elem in person_eval_list if elem.state != 'FINALIZED']
    # print("elementos : %s" % (b))    
    data = {"message":'ok'}
    return JsonResponse(data)

def pe_list(request):
    performance_evaluation_list = PerformanceEvaluation.objects.all()
    print(performance_evaluation_list)
    return render(request, "performance_evaluation/pe_list.html",
                  {"performance_evaluation_list": performance_evaluation_list})

def pe_detail(request, slug):
    personal_evaluation = get_object_or_404(PerformanceEvaluation, pk=slug)
    return render(request, "performance_evaluation/pe_detail.html",
                  {"object": personal_evaluation})

def pe_popup(request, slug):
    personal_evaluation = get_object_or_404(PerformanceEvaluation, pk=slug)
    return render(request, "performance_evaluation/pe_popup.html",
                  {"personal_evaluation": personal_evaluation})

def performance_eval_to_evaluate(request):
    print('>>> intro performance_eval_to_evaluate')
    identification = request.user.username
    print(identification)
    try:
        employee = Employee.objects.get(person__cedula =identification)
    except ObjectDoesNotExist:
        print('object_no_exist')
        page_obj = PerformanceEvaluation.objects.none()
        context = {'page_obj': page_obj}
        return render(request, 'personal_evaluation/perform_eval_to_evaluate_list.html', context)

    list_perform_eval = PerformanceEvaluation.objects.none()
    if employee:
        institution_id = request.session['institution_id']
        print('>>>>> institucion %s' % (institution_id))
        list_perform_eval = PerformanceEvaluation.objects.filter(
            Q(institution__pk=institution_id) &
            (
                Q(institutionalunitevaluation__personalevaluation__immediate_superior__pk=employee.id) 
                | Q(institutionalunitevaluation__personalevaluation__institutional_unit_director__pk=employee.id)
            ) &
            (
                Q(institutionalunitevaluation__state='INITIATED') 
                | Q(institutionalunitevaluation__temporary_activation=True)
            )
        ).distinct()
    print('evaluaciones : %s' % (len(list_perform_eval)))
    # for var in list_perform_eval:
    #    result =  InstitutionalUnitEvaluation.objects.filter(
    #         Q(performance_evaluation__pk=var.id) &
    #         Q(personalevaluation__immediate_superior__pk=employee.id) |
    #         Q(personalevaluation__institutional_unit_director__pk=employee.id)
    #     ).distinct()
    #    print(result)
    #    var.units_eval = result
    paginator = Paginator(list_perform_eval, 20) # Show 25 objects per page
    page = request.GET.get('page')
    try:
        page_obj = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        page_obj = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        page_obj = paginator.page(paginator.num_pages)
    context = {'page_obj': page_obj}
    return render(request, 'personal_evaluation/perform_eval_to_evaluate_list.html', context)

def performance_eval_to_evaluate_aux(request):
    print('>>> intro performance_eval_to_evaluate')
    identification = request.user.username
    print(identification)
    try:
        employee = Employee.objects.get(person__cedula =identification)
    except ObjectDoesNotExist:
        print('object_no_exist')
        page_obj = PerformanceEvaluation.objects.none()
        context = {'page_obj': page_obj}
        return render(request, 'personal_evaluation/perform_eval_to_evaluate_list.html', context)

    list_perform_eval = PerformanceEvaluation.objects.none()
    # print(PerformanceEvaluation.objects.filter(
    #         Q(institutionalunitevaluation__personalevaluation__immediate_superior__pk=person.id) | 
    #         Q(institutionalunitevaluation__personalevaluation__institutional_unit_director__pk=person.id)
    #     ).distinct().query)
    if employee:
        list_perform_eval = PerformanceEvaluation.objects.filter(
            Q(institutionalunitevaluation__personalevaluation__immediate_superior__pk=employee.id) | 
            Q(institutionalunitevaluation__personalevaluation__institutional_unit_director__pk=employee.id)
        ).distinct()
    for var in list_perform_eval:
       result =  InstitutionalUnitEvaluation.objects.filter(
            Q(performance_evaluation__pk=var.id) &
            Q(personalevaluation__immediate_superior__pk=employee.id) |
            Q(personalevaluation__institutional_unit_director__pk=employee.id)
        ).distinct()
       print(result)
       var.units_eval = result

    paginator = Paginator(list_perform_eval, 20) # Show 25 objects per page
    page = request.GET.get('page')
    try:
        page_obj = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        page_obj = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        page_obj = paginator.page(paginator.num_pages)
    context = {'page_obj': page_obj}
    return render(request, 'personal_evaluation/perform_eval_to_evaluate_list.html', context)

# def institutional_unit_to_evaluate(request):
#     print('institutional_unit_to_evaluate')
#     identification = request.user.username
#     institutional_unit_eval = request.GET.get('insitutional_unit')
#     person = Person.objects.get(cedula=identification)
#     list_personal_to_evaluate = PersonalEvaluation.objects.filter(
#             Q(institutional_unit_evaluation__id=institutional_unit_eval)
#             Q(immediate_superior__id=person.id) |
#             Q(institutional_unit_director__id=person.id)
#         )

def personal_eval_create(request):
    form = PersonalEvaluationForm(request.POST or None)
    institutional_unit_eval_id = request.GET.get('insitutional_unit_eval')
    institutional_unit_eval = InstitutionalUnitEvaluation.objects.get(pk=institutional_unit_eval_id)
    if form.is_valid():
        form.save()
        url = reverse('evaluation:personal_eval_to_evaluate')
        return HttpResponseRedirect('%s?insitutional_unit_eval=%s' % (url, institutional_unit_eval_id))
    context = {'form':form, 'insitutional_unit_eval': institutional_unit_eval_id}
    return render(request, template_name, {'form':form})

def personal_eval_update(request, pk):
    print('personal_eval_update')
    # personal_evaluation_id = request.GET.get('personal_evaluation')
    personal_evaluation = get_object_or_404(PersonalEvaluation, pk=pk)
    form = PersonalEvaluationForm(request.POST or None, instance=personal_evaluation, inst_unit_id=1)    
    institutional_unit_eval = personal_evaluation.institutional_unit_evaluation
    perform_eval = institutional_unit_eval.performance_evaluation
    print(institutional_unit_eval.id)
    if request.method=='POST':
        if form.is_valid():
            print('form is valid >>>')
            form.save()
            messages.success(request, 'La evaluación personal ha sido actualizada correctamente.')
            # url = reverse('evaluation:personal_eval_to_evaluate')
            # return HttpResponseRedirect('%s?institutional_unit_eval=%s' % (url, institutional_unit_eval.id))
            url = reverse('evaluation:personal_eval_to_evaluate_all')
            return HttpResponseRedirect('%s?perform_eval=%s' % (url, perform_eval.id))
        else:            
            print('form is invalid >>>')
            print(form.errors)
            # url = reverse('evaluation:personal_eval_update')
            # return HttpResponseRedirect('%s?perform_eval=%s' % (url, perform_eval.id))
            # context = RequestContext(request, {'form': form, 'perform_eval': perform_eval})
            # return render_to_respose('personal_evaluation/personal_eval_form.html', context)
            # context = RequestContext(request, {'form': form, 'personal_evaluation': personal_evaluation})
            # return render_to_response('personal_evaluation/personal_eval_form.html', context, status=400)

    # context = {'form':form, 'institutional_unit_eval': institutional_unit_eval}
    context = {'form':form, 'perform_eval': perform_eval}
    return render(request, 'personal_evaluation/personal_eval_form.html', context)

def personal_eval_update_aux(request):
    print('personal_eval_update')
    personal_evaluation_id = request.GET.get('personal_evaluation')
    personal_evaluation = get_object_or_404(PersonalEvaluation, pk=personal_evaluation_id)
    # form = PersonalEvaluationForm(request.POST or None, instance=personal_evaluation, 
    #     inst_unit_id=personal_evaluation.institutional_unit_evaluation.institutional_unit.pk)
    form = PersonalEvaluationForm(request.POST or None, instance=personal_evaluation)
    #institutional_unit_eval_id = request.GET.get('insitutional_unit_eval')
    #institutional_unit_eval = InstitutionalUnitEvaluation.objects.get(pk=institutional_unit_id)
    institutional_unit_eval = personal_evaluation.institutional_unit_evaluation
    perform_eval = institutional_unit_eval.performance_evaluation
    print(institutional_unit_eval.id)
    if request.method=='POST':
        if form.is_valid():
            print('form is valid >>>')
            form.save()
            # url = reverse('evaluation:personal_eval_to_evaluate')
            # return HttpResponseRedirect('%s?institutional_unit_eval=%s' % (url, institutional_unit_eval.id))
            url = reverse('evaluation:personal_eval_to_evaluate_all')
            return HttpResponseRedirect('%s?perform_eval=%s' % (url, perform_eval.id))
        else:            
            print('form is invalid >>>')
            print(form.errors)
            context = RequestContext(request, {'form': form, 'perform_eval': perform_eval})
            return render_to_response('personal_evaluation/personal_eval_form.html', context, status=400)

    # context = {'form':form, 'institutional_unit_eval': institutional_unit_eval}
    context = {'form':form, 'perform_eval': perform_eval}
    return render(request, 'personal_evaluation/personal_eval_form.html', context)

def personal_eval_to_evaluate(request):
    print('personal_eval_to_evaluate')
    institutional_unit_eval_id = request.GET.get('institutional_unit_eval')
    print(institutional_unit_eval_id)
    institutional_unit_eval = InstitutionalUnitEvaluation.objects.get(pk=institutional_unit_eval_id)
    identification = str(request.user.username)
    print(identification)
    try:
        employee = Employee.objects.get(person__cedula=identification)
    except ObjectDoesNotExist:
        print('employe no exist')
        page_obj = PersonalEvaluation.objects.none()
        context = {'page_obj': page_obj, 'institutional_unit_eval':institutional_unit_eval}
        return render(request, 'personal_evaluation/personal_eval_to_evaluate_list.html', context)

    list_personal_to_evaluate = PersonalEvaluation.objects.filter(
            Q(institutional_unit_evaluation__id=institutional_unit_eval.id) &
            Q(immediate_superior__id=employee.id) |
            Q(institutional_unit_director__id=employee.id)
        )
    print(len(list_personal_to_evaluate))
    paginator = Paginator(list_personal_to_evaluate, 20)    
    page = request.GET.get('page')
    try:
        page_obj = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        page_obj = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        page_obj = paginator.page(paginator.num_pages)
    context = {'page_obj': page_obj, 'institutional_unit_eval':institutional_unit_eval}
    return render(request, 'personal_evaluation/personal_eval_to_evaluate_list.html', context)


def personal_eval_to_evaluate_all(request):
    print('intro >>> personal_eval_to_evaluate_all')
    perform_eval_id = request.GET.get('perform_eval')
    print(perform_eval_id)
    perform_eval = PerformanceEvaluation.objects.get(pk=perform_eval_id)
    identification = str(request.user.username)
    print(identification)
    try:
        employee = Employee.objects.get(person__cedula=identification)
    except ObjectDoesNotExist:
        print('employe no exist')
        page_obj = PersonalEvaluation.objects.none()
        context = {'page_obj': page_obj, 'institutional_unit_eval':perform_eval}
        return render(request, 'personal_evaluation/personal_eval_to_evaluate_list.html', context)

    # list_personal_to_evaluate = PersonalEvaluation.objects.filter(
    #         Q(institutional_unit_evaluation__id=institutional_unit_eval.id) &
    #         Q(immediate_superior__id=employee.id) |
    #         Q(institutional_unit_director__id=employee.id)
    #     )

    list_personal_to_evaluate = PersonalEvaluation.objects.filter(
            Q(institutional_unit_evaluation__performance_evaluation_id=perform_eval.id) &
            (
                Q(immediate_superior__id=employee.id) |
                Q(institutional_unit_director__id=employee.id)
            ) &
            (
                Q(institutional_unit_evaluation__state='INITIATED') |
                Q(institutional_unit_evaluation__temporary_activation=True)
            )

        )
    print(len(list_personal_to_evaluate))
    paginator = Paginator(list_personal_to_evaluate, 20)    
    page = request.GET.get('page')
    try:
        page_obj = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        page_obj = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        page_obj = paginator.page(paginator.num_pages)
    context = {'page_obj': page_obj, 'perform_eval':perform_eval}
    return render(request, 'personal_evaluation/personal_eval_to_evaluate_list.html', context)


def personal_evaluation_all(request):
    print('intro personal_evaluation_all')
    user = User.objects.get(username=request.user.username)
    personal_evaluation_list = PersonalEvaluation.objects.all()
    context = {'personal_evaluation_list': personal_evaluation_list}
    return render(request, 'personal_evaluation/evaluation_list.html', context)

def personal_evaluation_generate(request, personal_eval_pk):
    print('intro personal_evaluation_generate')
    personal_evaluation = PersonalEvaluation.objects.get(pk= personal_eval_pk)
    institutional_unit_evaluation = personal_evaluation.institutional_unit_evaluation  
    perform_eval = institutional_unit_evaluation.performance_evaluation
    activity_eval = ActivityEvaluation()
    activity_eval.factor = perform_eval.activity_factor
    activity_eval.percentage_increase = perform_eval.advance_goals_percentage
    activity_eval.personal_evaluation = personal_evaluation
    activity_eval.save()
    knowledge_eval = KnowledgeEvaluation()
    knowledge_eval.factor = perform_eval.knowledge_factor
    knowledge_eval.personal_evaluation = personal_evaluation
    knowledge_eval.save()
    technical_competence_eval = TechnicalCompetenceEvaluation()
    technical_competence_eval.factor = perform_eval.technical_competence_factor
    technical_competence_eval.personal_evaluation = personal_evaluation
    technical_competence_eval.save()
    universal_competence_eval = UniversalCompetenceEvaluation()
    universal_competence_eval.factor = perform_eval.universal_competence_factor
    universal_competence_eval.personal_evaluation = personal_evaluation
    universal_competence_eval.save()
    teamwork_eval = TeamworkEvaluation()
    teamwork_eval.factor = perform_eval.teamwork_factor
    teamwork_eval.personal_evaluation = personal_evaluation
    teamwork_eval.save()
    complaint_eval = ComplaintEvaluation()
    complaint_eval.percentage_reduction = perform_eval.reduction_complaints_percentage
    complaint_eval.personal_evaluation = personal_evaluation
    complaint_eval.save()
    personal_evaluation.state = "INITIATED"
    personal_evaluation.start_date = date.today()
    personal_evaluation.save()
    messages.success(request, "La Evaluación para el servidor público se ha iniciado correctamente.")
    # message = 'Su evaluación de desempeño ha sido generada.'
    # data = {"message":message}
    # return JsonResponse(data)
    # url = reverse('evaluation:personal_eval_to_evaluate')
    url = reverse('evaluation:personal_eval_to_evaluate_all')
    return HttpResponseRedirect('%s?perform_eval=%s' % (url, perform_eval.id))

def personal_evaluation_evaluate(request, pk):
    print('intro personal_evaluation_evaluate')
    personal_eval = get_object_or_404(PersonalEvaluation, pk=pk)
    perform_eval = personal_eval.institutional_unit_evaluation.performance_evaluation
    inst_unit_eval = personal_eval.institutional_unit_evaluation
    # contract = Contract.objects.get(
    #     Q(employee__pk = personal_eval.evaluated.id) &
    #     Q(status='Firmado')
    #     )
    contract = personal_eval.contract
    curriculum_vitae = CurriculumVitae.objects.get(person__pk = personal_eval.evaluated.person.id)
    formal_inst_list = FormalInstruction.objects.filter(curriculum_vitae__pk = curriculum_vitae.id)
    request.session['personal_evaluation'] = personal_eval.id
    user = User.objects.get(username=request.user.username)
    activity_eval = ActivityEvaluation.objects.get(personal_evaluation=personal_eval)
    knowledge_eval = KnowledgeEvaluation.objects.get(personal_evaluation=personal_eval)
    technical_c_eval = TechnicalCompetenceEvaluation.objects.get(personal_evaluation=personal_eval)
    universal_c_eval = UniversalCompetenceEvaluation.objects.get(personal_evaluation=personal_eval)
    teamwork_eval = TeamworkEvaluation.objects.get(personal_evaluation=personal_eval)
    complaint_eval = ComplaintEvaluation.objects.get(personal_evaluation= personal_eval)
    activity_list = activity_eval.activity_set.all()
    knowledge_list = knowledge_eval.knowledge_set.all()   
    technical_c_list = technical_c_eval.technicalcompetence_set.all()
    universal_c_list = universal_c_eval.universalcompetence_set.all()
    teamwork_list = teamwork_eval.teamwork_set.all()
    chief_observation_list = ChiefObservation.objects.filter(personal_evaluation=personal_eval)
    print('observations list = %s' % len(chief_observation_list))
    complaint_list = complaint_eval.complaint_set.all()
    context = {'perform_eval': perform_eval, 'inst_unit_eval':inst_unit_eval, 'personal_eval': personal_eval,
    'contract': contract, 'activity_eval': activity_eval, 'activity_list': activity_list, 'knowledge_eval': knowledge_eval,
    'knowledge_list': knowledge_list, 'technical_c_eval': technical_c_eval, 'technical_c_list': technical_c_list, 
    'universal_c_eval': universal_c_eval, 'universal_c_list': universal_c_list, 'teamwork_eval': teamwork_eval, 
    'teamwork_list': teamwork_list, 'chief_observation_list': chief_observation_list,
    'complaint_eval': complaint_eval , 'complaint_list': complaint_list, 'user': user, 'formal_inst_list': formal_inst_list}
    #return render(request, 'personal_evaluation/evaluation_form.html', context)
    return render(request, 'personal_evaluation/person_eval_form.html', context)

def finalize_personal_evaluation(request, pk):
    print("intro >>> finalize_personal_evaluation")
    personal_eval = get_object_or_404(PersonalEvaluation, pk=pk)
    personal_eval.state = 'FINALIZED'
    personal_eval.save()
    messages.success(request, "La evaluación personal ha finalizado correctamente")
    message = 'ok'
    data = {"message":message}
    return JsonResponse(data)

def personal_evaluation_view(request, pk):
    print('intro personal_evaluation_view')
    personal_eval = get_object_or_404(PersonalEvaluation, pk=pk)
    performance_eval = personal_eval.institutional_unit_evaluation.performance_evaluation
    inst_unit_eval = personal_eval.institutional_unit_evaluation
    contract = personal_eval.contract
    curriculum_vitae = CurriculumVitae.objects.get(person__pk = personal_eval.evaluated.person.id)
    formal_inst_list = FormalInstruction.objects.filter(curriculum_vitae__pk = curriculum_vitae.id)
    request.session['personal_evaluation'] = personal_eval.id
    user = User.objects.get(username=request.user.username)
    activity_eval = ActivityEvaluation.objects.get(personal_evaluation=personal_eval)
    knowledge_eval = KnowledgeEvaluation.objects.get(personal_evaluation=personal_eval)
    technical_c_eval = TechnicalCompetenceEvaluation.objects.get(personal_evaluation=personal_eval)
    universal_c_eval = UniversalCompetenceEvaluation.objects.get(personal_evaluation=personal_eval)
    teamwork_eval = TeamworkEvaluation.objects.get(personal_evaluation=personal_eval)
    complaint_eval = ComplaintEvaluation.objects.get(personal_evaluation= personal_eval)
    activity_list = activity_eval.activity_set.all()
    knowledge_list = knowledge_eval.knowledge_set.all()   
    technical_c_list = technical_c_eval.technicalcompetence_set.all()
    universal_c_list = universal_c_eval.universalcompetence_set.all()
    teamwork_list = teamwork_eval.teamwork_set.all()
    chief_observation_list = ChiefObservation.objects.filter(personal_evaluation=personal_eval)
    print('observations list = %s' % len(chief_observation_list))
    complaint_list = complaint_eval.complaint_set.all()
    context = {'performance_eval': performance_eval, 'inst_unit_eval':inst_unit_eval, 'personal_eval': personal_eval,
    'contract': contract, 'activity_eval': activity_eval, 'activity_list': activity_list, 'knowledge_eval': knowledge_eval,
    'knowledge_list': knowledge_list, 'technical_c_eval': technical_c_eval, 'technical_c_list': technical_c_list, 
    'universal_c_eval': universal_c_eval, 'universal_c_list': universal_c_list, 'teamwork_eval': teamwork_eval, 
    'teamwork_list': teamwork_list, 'chief_observation_list': chief_observation_list,
    'complaint_eval': complaint_eval , 'complaint_list': complaint_list, 'user': user, 'formal_inst_list': formal_inst_list}
    #return render(request, 'personal_evaluation/evaluation_form.html', context)
    return render(request, 'personal_evaluation/personal_eval_view.html', context)

def activity_create(request, ae_pk=None):
    print('intro activity_create')
    print(ae_pk)
    activity_eval = get_object_or_404(ActivityEvaluation, pk=ae_pk)
    if request.POST:
        if request.POST.get('cancel', None):
            return redirect('evaluation:personal_evaluation_evaluate', pk=activity_eval.personal_evaluation.pk)
        form = ActivityForm(data=request.POST)
        if form.is_valid():
            activity = form.save(commit=False)
            activity.activity_evaluation = activity_eval
            activity = form.save()
            update_activity_eval(activity_eval)
            update_personal_evaluation(activity_eval.personal_evaluation)
            data = {'state': 'correct'}
            return HttpResponse(json.dumps(data), content_type='application/json', status=200)
        else:
            context = RequestContext(request, {'form': form, 'activity_eval': activity_eval})
            return render_to_response('personal_evaluation/activity.html', context, status=400)
    form = ActivityForm()
    vars = RequestContext(request, {'form':form, 'activity_eval': activity_eval})
    return render_to_response("personal_evaluation/activity.html", vars)

def activity_delete(request):
    print('intro activity_delete')
    if request.method == 'DELETE':
        activity_id = int(QueryDict(request.body).get('activity_id'))
        activity = Activity.objects.get(pk=activity_id)
        activity_eval = activity.activity_evaluation
        activity.delete()
        update_activity_eval(activity_eval)
        update_personal_evaluation(activity_eval.personal_evaluation)
        response_data = {}
        response_data['msg'] = 'La actividad ha sido borrada.'
        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

def update_activity_eval(activity_eval = None):
    """
        N12 = numero de actividades =SUMA(N13:N32)
        M13 =SI(K13<>"",SI(K13=5,60,SI(K13=4,45,SI(K13=3,30,SI(K13=2,15,SI(K13=1,0,""))))),0)
        S13 =SI(M13=0,0,(M13/N12))
        S12 =SUMA(S13:S35) (Valor del porcentaje total de actividades)
        S35 = L35
        H11 = N12
        resultado =CONCATENAR(REDONDEAR(S12,1),"%")
        L35 =SI(Y(P33=H11,I35="SI",R33>=1),4,0) (Valor del porcentaje de incremento)
        P33 =SUMA(P13:P32)
        P13 =SI(Y(I13>=H13,O13=1),1,0)
        I35 = SI-NO
        R33=SUMA(R13:R32)
        R13 =SI(O(I13<0,I13="",H13=""),"",SI(Y(I13>H13),1,0))
        I13 = valor de cumplidos
        H13 = % de cumplimiento
    """
    print('intro update_activity_eval')
    activity_list = activity_eval.activity_set.all()
    i35 = activity_eval.applies_advance_goals
    n12 = Activity.objects.filter(activity_evaluation__pk=activity_eval.pk).count()
    h11 = n12
    s12 = 0
    mn = 0
    sn = 0
    s35 = 0
    l35 = 0
    p33 = 0
    r33 = 0
    for activity in activity_list:
        mn = get_level_fulfillment(activity)
        pn = 1 if activity.complete_goal >= activity.period_goal else 0
        rn = 1 if activity.complete_goal > activity.period_goal else 0
        sn = mn / n12
        s12 = s12 + sn
        p33 = p33 + pn
        r33 = r33 + rn
        print("mn = {}, pn = {}, rn = {}, sn = {}".format(mn, pn, rn, sn))
    print("s12 = {}, p33 = {}, h11 = {}, i35 = {}, r33 = {}".format(s12, p33, h11, i35, r33))
    l35 = 4 if (p33 == h11 and i35 == 'YES' and r33 >=1) else 0
    s12 = s12 + l35
    print("FINAL >>>  l35 = {}, s12 = {}".format(l35, s12))
    activity_eval.number_activities = n12
    activity_eval.percentage_increase = l35
    activity_eval.percentage_final = s12
    activity_eval.save()

def get_level_fulfillment(activity):
    print('intro get_level_fulfillment')
    mn = None
    if activity.level_fulfillment == 5:
        mn = 60
    elif activity.level_fulfillment == 4:
        mn = 45
    elif activity.level_fulfillment == 3:
        mn = 30
    elif activity.level_fulfillment == 2:
        mn = 15
    else:
        mn = 0
    return mn

def knowledge_create(request, ke_pk=None):
    print('intro knowledge_create')
    print(ke_pk)
    ke = get_object_or_404(KnowledgeEvaluation, pk=ke_pk)
    if request.POST:
        if request.POST.get('cancel', None):
            return redirect('evaluation:personal_evaluation_evaluate', pk=ke.personal_evaluation.pk)
        form = KnowledgeForm(data=request.POST)
        if form.is_valid():
            knowledge = form.save(commit=False)
            knowledge.knowledge_evaluation = ke
            knowledge = form.save()
            update_knowledge_eval(ke)
            update_personal_evaluation(ke.personal_evaluation)
            data = {'state': 'correct'}
            #return HttpResponse(json.dumps(data), content_type='application/json', status=200)
            #return JsonResponse({'foo':'bar'})
            return HttpResponse(status=200);
        else:
            #json_data = json.dumps(form.errors)
            #return HttpResponseBadRequest(json_data, content_type='application/json')
            context = RequestContext(request, {'form': form, 'create': True})
            return render_to_response('personal_evaluation/knowledge.html', context, status=400)
    form = KnowledgeForm()
    vars = RequestContext(request, {'form':form, 'ke': ke})
    return render_to_response("personal_evaluation/knowledge.html", vars)

def knowledge_delete(request):
    print('intro knowledge_delete')
    if request.method == 'DELETE':
        k_id = int(QueryDict(request.body).get('k_id'))
        k = Knowledge.objects.get(pk=k_id)
        ke = k.knowledge_evaluation
        k.delete()
        update_knowledge_eval(ke)
        update_personal_evaluation(ke.personal_evaluation)
        response_data = {}
        response_data['msg'] = 'La actividad ha sido borrada.'
        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

def update_knowledge_eval(ke = None):
    """
        O37=SUMA(O38:O47) (Valor del porcentaje final de conocimientos)
        N37=SUMA(N38:N47) (Total de conocimientos)
        O38=SI(M38=0,0,(M38/N37))
        M38=SI(K38<>"",SI(K38=5,8,SI(K38=4,6,SI(K38=3,4,SI(K38=2,2,SI(K38=1,0,""))))),0)
        K38=SI(D38="","",SI(L38="Sobresaliente",5,SI(L38="Muy Bueno",4,SI(L38="Bueno",3,SI(L38="Regular",2,SI(L38="Insuficiente",1,""))))))
    """
    print('intro update_knowledge_eval')
    knowledge_list = ke.knowledge_set.all()   
    n37 = Knowledge.objects.filter(knowledge_evaluation__pk=ke.pk).count()
    o37 = 0
    on = 0
    kn = 0
    mn = 0
    for knowledge in knowledge_list:
        vlk = get_value_level_knowledge(knowledge)
        kn = vlk['kn']
        mn = vlk['mn']
        on = 0 if mn == 0 else (mn / n37)
        o37 = o37 + on
    ke.number_knowledge = n37
    ke.percentage_final = round(o37, 1)
    ke.save()


def get_value_level_knowledge(knowledge):
    print('intro get_value_level_knowledge')
    vlk = {}
    if knowledge.level_knowledge == 'OUTSTANDING':
        vlk['kn'] = 5
        vlk['mn'] = 8
    elif knowledge.level_knowledge == 'VERYGOOD':
        vlk['kn'] = 4
        vlk['mn'] = 6
    elif knowledge.level_knowledge == 'GOOD':
        vlk['kn'] = 3
        vlk['mn'] = 4
    elif knowledge.level_knowledge == 'REGULAR':
        vlk['kn'] = 2
        vlk['mn'] = 2
    else:
        vlk['kn'] = 1
        vlk['mn'] = 0
    return vlk

def technical_competence_create(request, tce_pk=None):
    print('>>>>> intro technical_competence_create')
    tech_compet_eval = get_object_or_404(TechnicalCompetenceEvaluation, pk=tce_pk)
    person_eval = tech_compet_eval.personal_evaluation
    perform_eval = tech_compet_eval.personal_evaluation.institutional_unit_evaluation.performance_evaluation
    catalogs_tech_compet = CatalogTechnicalCompetence.objects.filter(performance_evaluation__pk=perform_eval.pk).distinct('skill')
    ids_items_tech_compet = []
    for catalog in catalogs_tech_compet:
        ids_items_tech_compet.append(catalog.skill.pk)
    # print(ids_items_tech_compet)
    if request.POST:
        if request.POST.get('cancel', None):
            return redirect('evaluation:personal_evaluation_evaluate', pk=tech_compet_eval.personal_evaluation.pk)
        form = TechnicalCompetenceForm(data=request.POST, perform_eval=perform_eval.pk, list_tc=ids_items_tech_compet)
        # print(request.POST)
        if form.is_valid():
            print('>>>>> intro valid')
            skill = request.POST['skill']
            print(skill)
            tech_comp = TechnicalCompetence.objects.filter(
                catalog_technical_competence__skill__id=skill, technical_competence_evaluation__id=tech_compet_eval.id)
            if tech_comp:
                print('>>>>> existe')
                messages.error(request, 'Ya se encuentra registrada una Competencia técnica con la destreza seleccionada.')
                context = RequestContext(request, {'form': form, 'technical_c_eval': tech_compet_eval})
                return render_to_response('personal_evaluation/technical_competence.html', context, status=400)
            else:
                technical_competence = form.save(commit=False)
                # print(technical_competence)
                technical_competence.technical_competence_evaluation = tech_compet_eval
                # technical_competence.skill = skill
                technical_competence = form.save()
                update_technical_competence_eval(tech_compet_eval)
                update_personal_evaluation(tech_compet_eval.personal_evaluation)
                data = {'state': 'correct'}
                return HttpResponse(json.dumps(data), content_type='application/json', status=200)
        else:
            print('>>>>> intro invalid')
            context = RequestContext(request, {'form': form, 'technical_c_eval': tech_compet_eval})
            return render_to_response('personal_evaluation/technical_competence.html', context, status=400)
    form = TechnicalCompetenceForm(perform_eval = perform_eval.pk, list_tc = ids_items_tech_compet)
    vars = RequestContext(request, {'form':form, 'technical_c_eval': tech_compet_eval, 'performance_eval': perform_eval})
    return render_to_response("personal_evaluation/technical_competence.html", vars)

def catalog_tech_comp(request):
    print('intro catalog_tech_comp')
    if request.method == 'POST':
        sel_skill = QueryDict(request.body).get('sel_skill')
        sel_relevance = QueryDict(request.body).get('sel_relevance')
        sel_performance_eval = QueryDict(request.body).get('sel_performance_eval')                 
        print(sel_skill)
        print(sel_relevance)
        print(sel_performance_eval)
        #CatalogTechnicalCompetence.objects.all()
        catalog_list = CatalogTechnicalCompetence.objects.filter(
                Q(skill__id=int(sel_skill)) & 
                Q(relevance=sel_relevance) & 
                Q(performance_evaluation__id=int(sel_performance_eval))
            )
        print(catalog_list)
        # data = serializers.serialize('json', catalog_list)
        # return HttpResponse(data, content_type='application/json')
        if catalog_list:
            print('>>> true')
            data = serializers.serialize('json', catalog_list)
            struct = json.loads(data)
            data = json.dumps(struct[0])
            return HttpResponse(data, content_type='application/json')
        else:
            print('>>> false')
            message = 'No existe la competencia técnica dentro del catálogo configurado para la evaluación de desempeño'
            return JsonResponse({'status':'false','message':message}, status=500)
        
    message = 'Process success'
    data = {"message":message}
    return JsonResponse(data)

def technical_competence_delete(request):
    print('intro technical_competence_delete')
    if request.method == 'DELETE':
        tc_id = int(QueryDict(request.body).get('tc_id'))
        tc = TechnicalCompetence.objects.get(pk=tc_id)
        tce = tc.technical_competence_evaluation
        tc.delete()
        update_technical_competence_eval(tce)
        update_personal_evaluation(tce.personal_evaluation)
        response_data = {}
        response_data['msg'] = 'La competencia técnica ha sido borrada.'
        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

def update_technical_competence_eval(tce = None):
    """
        O49 =SUMA(O51:O60) (Valor del porcentaje final de competencias tecnicas)
        N49 = SUMA(N51:N60) (Total de competencias tecnicas)
        O51 = SI(M51=0,0,(M51/N49))
        M51 = SI(K51<>"",SI(K51=5,8,SI(K51=4,6,SI(K51=3,4,SI(K51=2,2,SI(K51=1,0,""))))),0)
        K51 =SI(O(D51="",F51=""),"",SI(L51="Altamente Desarrollada",5,SI(L51="Desarrollada",4,SI(L51="Medianamente Desarrollada",3,SI(L51="Poco Desarrollada",2,SI(L51="No Desarrollada",1,""))))))
    """
    print('intro update_technical_competence')
    technical_competence_list = tce.technicalcompetence_set.all()  
    n49 = TechnicalCompetence.objects.filter(technical_competence_evaluation__pk=tce.pk).count()
    o49 = 0
    on = 0
    mn = 0
    kn = 0
    for tc in technical_competence_list:
        mn = get_equivalent_value_tc(tc)
        on = 0 if mn == 0 else (mn/n49)
        print("FINAL >>>  mn = {}, on = {}".format(mn, on))
        o49 = o49 + on
    tce.number_competence = n49
    tce.percentage_final =  round(o49,1) 
    tce.save()

def get_equivalent_value_tc(tc):
    print('intro get_equivalent_value_tc')
    val = 0
    if tc.development_level == 5:
        val = 8
    elif tc.development_level == 4:
        val = 6
    elif tc.development_level == 3:
        val = 4
    elif tc.development_level == 2:
        val = 2
    else:
        val = 0
    return val

def universal_competence_create(request, uce_pk=None):
    print('intro universal_competence_create')
    universal_c_eval = get_object_or_404(UniversalCompetenceEvaluation, pk=uce_pk)
    person_eval = universal_c_eval.personal_evaluation
    perform_eval = universal_c_eval.personal_evaluation.institutional_unit_evaluation.performance_evaluation
    list_uc = [12, 13, 14, 15]
    catalogs_univ_comp = CatalogUniversalCompetence.objects.filter(performance_evaluation__pk=perform_eval.pk).distinct('skill')
    ids_items_univ_compet = []
    for catalog in catalogs_univ_comp:
        ids_items_univ_compet.append(catalog.skill.pk)
    print(ids_items_univ_compet)
    if request.POST:
        if request.POST.get('cancel', None):
            return redirect('evaluation:personal_evaluation_evaluate', pk=person_eval.id)
        form = UniversalCompetenceForm(data=request.POST, perform_eval=perform_eval.pk, list_uc=ids_items_univ_compet)
        skill = request.POST['skill']
        if form.is_valid():
            print('>>>>> intro valid')
            print(request.POST['skill'])
            catalog_universal_competence_id = request.POST['catalog_universal_competence']
            print('>>>>> catalog universal competence id %s' % (catalog_universal_competence_id))
            # cat_univ_comp = CatalogUniversalCompetence.objects.get(id=catalog_universal_competence.id)
            univ_comp = UniversalCompetence.objects.filter(
                catalog_universal_competence__skill__id=skill, universal_competence_evaluation__id=universal_c_eval.id)
            if univ_comp:
                print('>>>>> existe')
                messages.error(request, 'Ya se encuentra registrada una Competencia universal con la destreza seleccionada.')
                context = RequestContext(request, {'form': form, 'universal_c_eval': universal_c_eval})
                return render_to_response('personal_evaluation/universal_competence.html', context, status=400)
            else:
                universal_competence = form.save(commit=False)
                universal_competence.universal_competence_evaluation = universal_c_eval
                #universal_competence.skill = skill
                universal_competence = form.save()
                update_universal_competence_eval(universal_c_eval)
                update_personal_evaluation(person_eval)
                data = {'state': 'correct'}
                return HttpResponse(json.dumps(data), content_type='application/json', status=200)
        else:
            context = RequestContext(request, {'form': form, 'universal_c_eval': universal_c_eval})
            return render_to_response('personal_evaluation/universal_competence.html', context, status=400)
    #form = UniversalCompetenceForm(catalog_code='DESTREZAUNIVERSAL', list_uc=list_uc)
    form = UniversalCompetenceForm(perform_eval=perform_eval.pk, list_uc=ids_items_univ_compet)
    vars = RequestContext(request, {'form':form, 'universal_c_eval': universal_c_eval})
    return render_to_response("personal_evaluation/universal_competence.html", vars)

def catalog_univ_comp(request):
    print('intro catalog_univ_comp')
    if request.method == 'POST':
        sel_skill = QueryDict(request.body).get('sel_skill')
        sel_relevance = QueryDict(request.body).get('sel_relevance')
        sel_performance_eval = QueryDict(request.body).get('sel_performance_eval')                 
        print(sel_skill)
        print(sel_relevance)
        print(sel_performance_eval)
        catalog_list = CatalogUniversalCompetence.objects.filter(
                Q(skill__id=int(sel_skill)) & 
                Q(relevance=sel_relevance) & 
                Q(performance_evaluation__id=int(sel_performance_eval))
            )
        print(catalog_list)
        if catalog_list:
            data = serializers.serialize('json', catalog_list)
            struct = json.loads(data)
            data = json.dumps(struct[0])
            return HttpResponse(data, content_type='application/json')
        else:
            print('>>> false')
            message = 'No existe la competencia universal dentro del catálogo configurado para la evaluación de desempeño'
            return JsonResponse({'status':'false','message':message}, status=500)
        
    message = 'Process success'
    data = {"message":message}
    return JsonResponse(data)

def universal_competence_delete(request):
    print('intro universal_competence_delete')
    if request.method == 'DELETE':
        uc_id = int(QueryDict(request.body).get('uc_id'))
        uc = UniversalCompetence.objects.get(pk=uc_id)
        uce = uc.universal_competence_evaluation
        uc.delete()
        update_universal_competence_eval(uce)
        update_personal_evaluation(uce.personal_evaluation)
        response_data = {}
        response_data['msg'] = 'La competencia universal ha sido borrada.'
        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

def update_universal_competence_eval(uce = None):
    """
        M62 =SI(N62=6,SUMA(M65:M70),0%) (Valor del porcentaje final de competencias universales)
        N62 =SUMA(N65:N70) (Total de competencias universales)
        M65 =SI(K65=5,1.33,SI(K65=4,0.99,SI(K65=3,0.66,SI(K65=2,0.33,SI(K65=1,0,"")))))
        K65 =SI(O(D65="",F65=""),"",SI(L65="Siempre",5,SI(L65="Frecuentemente",4,SI(L65="Alguna vez",3,SI(L65="Rara vez",2,SI(L65="Nunca",1,""))))))
    """
    print('intro update_universal_competence_eval')    
    universal_competence_list = uce.universalcompetence_set.all()  
    n62 = UniversalCompetence.objects.filter(universal_competence_evaluation__pk=uce.pk).count()
    m62 = 0
    mn = 0
    if n62 == 6:
        for uc in universal_competence_list:
            mn = get_equivalent_value_uc(uc)
            m62 = m62 + mn
            print("FINAL >>>  mn = {}, m62 = {}".format(mn, m62))
    uce.number_competence = n62
    uce.percentage_final = round(m62,1)
    # uce.percentage_final = m62
    uce.save()


def get_equivalent_value_uc(uc):
    print('intro get_equivalent_value_uc')
    val = 0
    if uc.frequency_application == 5:
        val = 1.33
    elif uc.frequency_application == 4:
        val = 0.99
    elif uc.frequency_application == 3:
        val = 0.66
    elif uc.frequency_application == 2:
        val = 0.33
    else:
        val = 0
    return val

def teamkork_create(request, twe_pk=None):
    print('intro teamkork_create')
    print(twe_pk)
    teamwork_eval = get_object_or_404(TeamworkEvaluation, pk=twe_pk)
    person_eval = teamwork_eval.personal_evaluation
    perform_eval = teamwork_eval.personal_evaluation.institutional_unit_evaluation.performance_evaluation
    catalogs_teamwork = CatalogTeamwork.objects.filter(performance_evaluation__pk=perform_eval.pk).distinct('ability')
    ids_items_teamwork = []
    for catalog in catalogs_teamwork:
        ids_items_teamwork.append(catalog.ability.pk)
    print(ids_items_teamwork)
    if request.POST:
        if request.POST.get('cancel', None):
            return redirect('evaluation:personal_evaluation_evaluate', pk=person_eval.id)
        form = TeamworkForm(data=request.POST, perform_eval=perform_eval.pk, list_tw=ids_items_teamwork)
       
        print(form)
        if form.is_valid():
            print('>>>>> intro valid')
            ability = request.POST['ability']
            print(ability)
            teamw =  Teamwork.objects.filter(
                catalog_teamwork__ability__id=ability, teamwork_evaluation__id=teamwork_eval.id)
            if teamw:
                print('>>>>> existe')
                messages.error(request, 'Ya se encuentra registrada una Aptitud de trabajo en equipo con la destreza seleccionada.')
                context = RequestContext(request, {'form': form, 'teamwork_eval': teamwork_eval})
                return render_to_response('personal_evaluation/teamwork.html', context, status=400)
            else:
                teamwork = form.save(commit=False)
                teamwork.teamwork_evaluation = teamwork_eval
                teamwork.ability = ability
                teamwork = form.save()
                print('>>> before teamwork save')
                update_teamwork_eval(teamwork_eval)
                update_personal_evaluation(person_eval)
                data = {'state': 'correct'}
                return HttpResponse(json.dumps(data), content_type='application/json', status=200)
        else:
            context = RequestContext(request, {'form': form, 'teamwork_eval': teamwork_eval})
            return render_to_response('personal_evaluation/teamwork.html', context, status=400)
    form = TeamworkForm(perform_eval=perform_eval.pk, list_tw=ids_items_teamwork)
    vars = RequestContext(request, {'form':form, 'teamwork_eval': teamwork_eval})
    return render_to_response("personal_evaluation/teamwork.html", vars)

def teamwork_delete(request):
    print('intro teamwork_delete')
    if request.method == 'DELETE':
        tw_id = int(QueryDict(request.body).get('tw_id'))
        tw = Teamwork.objects.get(pk=tw_id)
        twe = tw.teamwork_evaluation
        tw.delete()
        update_teamwork_eval(twe)
        update_personal_evaluation(twe.personal_evaluation)
        response_data = {}
        response_data['msg'] = 'La habilidad de trabajo en equipo ha sido borrada.'
        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

def catalog_teamwork(request):
    print('intro catalog_teamwork')
    if request.method == 'POST':
        sel_ability = QueryDict(request.body).get('sel_ability')
        sel_relevance = QueryDict(request.body).get('sel_relevance')
        sel_performance_eval = QueryDict(request.body).get('sel_performance_eval')                 
        print(sel_ability)
        print(sel_relevance)
        print(sel_performance_eval)
        catalog_list = CatalogTeamwork.objects.filter(
                Q(ability__id=int(sel_ability)) & 
                Q(relevance=sel_relevance) & 
                Q(performance_evaluation__id=int(sel_performance_eval))
            )
        print(catalog_list)
        data = serializers.serialize('json', catalog_list)
        struct = json.loads(data)
        data = json.dumps(struct[0])
        return HttpResponse(data, content_type='application/json')
        
    message = 'Su evaluación de desempeño ha sido generada.'
    data = {"message":message}
    return JsonResponse(data)

def update_teamwork_eval(twe = None):
    #M72 =SI(N78=2,SUMA(M74:M75),O72) (Valor del porcentaje final de trabajo en equipo, considerando si se tiene a cargo personal subordinado)
    #N78 =SUMA(N74:N77) (Total de habilidades de trabajo en equipo)
    #M74 =SI(K74=5,8,SI(K74=4,6,SI(K74=3,4,SI(K74=2,2,SI(K74=1,0,"")))))
    #K74 =SI(D74="","",SI(L74="Siempre",5,SI(L74="Frecuentemente",4,SI(L74="Alguna vez",3,SI(L74="Rara vez",2,SI(L74="Nunca",1,""))))))
    #O72 =SUMA(O74:O77) (Se suman las tres habilidades de trabajo en equipo, cuando se tiene a cargo personal subordinado)
    #O74 =SI(K74=5,5.33,SI(K74=4,3.99,SI(K74=3,2.66,SI(K74=2,1.33,SI(K74=1,0,"")))))
    print('>>> intro update_teamwork_eval')
    teamwork_list = twe.teamwork_set.all()  
    n78 = Teamwork.objects.filter(teamwork_evaluation__pk=twe.pk).count()
    m72 = 0
    mn = 0
    kn = 0
    for tw in teamwork_list:
        if n78 < 3:
            mn = get_equivalent_value_tw(tw, False)
            m72 = m72 + mn
            print("FINAL >>>  mn = {}, m72 = {}".format(mn, m72))    
        else:
            mn = get_equivalent_value_tw(tw, True)
            m72 = m72 + mn
            print("FINAL >>>  mn = {}, m72 = {}".format(mn, m72))
    twe.number_variables = n78
    twe.percentage_final = round(m72,1)
    twe.save()

def get_equivalent_value_tw(tw, leader = False):
    val = 0
    if leader:
        if tw.frequency_application == 5:
            val = 5.33
        elif tw.frequency_application == 4:
            val = 3.99
        elif tw.frequency_application == 3:
            val = 2.66
        elif tw.frequency_application == 2:
            val = 1.33
        else:
            val = 0
    else:
        if tw.frequency_application == 5:
            val = 8
        elif tw.frequency_application == 4:
            val = 6
        elif tw.frequency_application == 3:
            val = 4
        elif tw.frequency_application == 2:
            val = 2
        else:
            val = 0

    return val

def chief_observation_create(request, pe_pk):
    print('intro chief_observation_create')
    print(pe_pk)
    personal_eval = PersonalEvaluation.objects.get(pk = pe_pk)
    #form = ComplaintForm(data=request.POST or None)
    form = ChiefObservationForm(request.POST or None)
    if request.POST:
        if form.is_valid():
            chief_observation = form.save(commit=False)
            chief_observation.personal_evaluation = personal_eval
            chief_observation.save()
            #return redirect('evaluation:institution_list')
            data = {'state': 'correct'}
            return HttpResponse(json.dumps(data), content_type='application/json', status=200)
        else:
            #return render(request, 'personal_evaluation/chief_observation.html', {'form':form, 'personal_eval': personal_eval})
            context = RequestContext(request, {'form': form, 'personal_eval': personal_eval})
            return render_to_response('personal_evaluation/chief_observation.html', context, status=400)
    #return render(request, 'personal_evaluation/complaint.html', {'form':form, 'complaint_eval': complaint_eval})
    return render(request, 'personal_evaluation/chief_observation.html', {'form':form, 'personal_eval': personal_eval})

def chief_observation_delete(request):
    print('chief_observation_delete')
    if request.method == 'DELETE':
        cho_id = int(QueryDict(request.body).get('cho_id'))
        chief_observation = ChiefObservation.objects.get(pk=cho_id)
        chief_observation.delete()
        response_data = {}
        response_data['msg'] = 'La observación seleccionada ha sido borrada exitosamente.'
        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

def complaint_create(request, ce_pk=None):
    print('complaint_create')
    print(ce_pk)
    complaint_eval = get_object_or_404(ComplaintEvaluation, pk=ce_pk)
    if request.POST:
        if request.POST.get('cancel', None):
            return redirect('evaluation:personal_evaluation_evaluate', pk=complaint_eval.personal_evaluation.id)
        form = ComplaintForm(data=request.POST)
        if form.is_valid():
            complaint = form.save(commit=False)
            complaint.complaint_evaluation = complaint_eval
            complaint = form.save()
            update_complaint_eval(complaint_eval)
            update_personal_evaluation(complaint_eval.personal_evaluation)
            data = {'state': 'correct'}
            return HttpResponse(json.dumps(data), content_type='application/json', status=200)
        else:
            context = RequestContext(request, {'form': form, 'complaint_eval': complaint_eval})
            return render_to_response('personal_evaluation/complaint.html', context, status=400)
    form = ComplaintForm()
    vars = RequestContext(request, {'form':form, 'complaint_eval': complaint_eval})
    return render_to_response("personal_evaluation/complaint.html", vars)

def complaint_delete(request):
    print('complaint_delete')
    if request.method == 'DELETE':
        c_id = int(QueryDict(request.body).get('c_id'))
        c = Complaint.objects.get(pk=c_id)
        ce = c.complaint_evaluation
        c.delete()
        update_complaint_eval(ce)
        update_personal_evaluation(ce.personal_evaluation)
        response_data = {}
        response_data['msg'] = 'La queja ha sido borrada.'
        return HttpResponse(
            json.dumps(response_data),
            content_type="application/json"
        )

def update_complaint_eval(ce = None):
    #L92 =SUMA(L84:L91)(Valor del porcentaje final de reduccion por quejas)
    #L84 =SI(Y(D85<>"",E85<>"",H85<>""),SI(Y(I85="SI"),-4,0),"")
    #M82 =SUMA(M84:M91)(Total de quejas registradas)
    complaint_list = ce.complaint_set.all()  
    m82 = Complaint.objects.filter(complaint_evaluation__pk=ce.pk).count()
    l92 = 0
    ln = 0
    ooo = True
    for c in complaint_list:
        ln = ln + c.percentage_reduction 
    l92 = ln
    ce.number_complaint = m82
    ce.percentage_final = round(l92,1)
    ce.save()

def update_personal_evaluation(pe = None):
    print('>>> intro update_personal_evaluation')
    activity_eval = pe.activityevaluation_set.all()[0]
    knowledge_eval = pe.knowledgeevaluation_set.all()[0]
    technical_c_eval = pe.technicalcompetenceevaluation_set.all()[0]
    universal_c_eval = pe.universalcompetenceevaluation_set.all()[0]
    teamwork_eval = pe.teamworkevaluation_set.all()[0]
    complaint_eval = pe.complaintevaluation_set.all()[0]
    job_management_indicators = activity_eval.percentage_final
    knowledge = knowledge_eval.percentage_final
    technical_competence = technical_c_eval.percentage_final
    universal_competence = universal_c_eval.percentage_final
    teamwork = teamwork_eval.percentage_final
    evaluation_citizens = complaint_eval.percentage_final
    final_evaluation_percentage = job_management_indicators + knowledge + technical_competence + universal_competence + teamwork + evaluation_citizens
    final_evaluation_result = get_evaluation_result(final_evaluation_percentage)
    print('%s %s %s %s %s %s %s %s' % (job_management_indicators, knowledge, technical_competence, universal_competence, teamwork, 
        evaluation_citizens, final_evaluation_percentage, final_evaluation_result))
    pe.job_management_indicators = job_management_indicators
    pe.knowledge = knowledge
    pe.technical_competence = technical_competence
    pe.universal_competence = universal_competence
    pe.teamwork = teamwork
    pe.evaluation_citizens = evaluation_citizens
    pe.final_evaluation_percentage = final_evaluation_percentage
    pe.final_evaluation_result = final_evaluation_result
    print('>>> before save')
    pe.save()

def get_evaluation_result(final_percentage):
    # D102 =SI(Y(REDONDEAR(M100,1)>=90.5,REDONDEAR(M100,1)<=104),"EXCELENTE: DESEMPEÑO ALTO",
    # SI(Y(REDONDEAR(M100,1)>=80.5,REDONDEAR(M100,1)<=90.4),"MUY BUENO: DESEMPEÑO MEJOR A LO ESPERADO",
    # SI(Y(REDONDEAR(M100,1)>=70.5,REDONDEAR(M100,0)<=80.4),"SATISFACTORIO: DESEMPEÑO ESPERADO",
    # SI(Y(REDONDEAR(M100,1)>=60.5,REDONDEAR(M100,1)<=70.4),"REGULAR: DESEMPEÑO BAJO LO ESPERADO",
    # SI(Y(REDONDEAR(M100,1)>0,REDONDEAR(M100,1)<=60.4),"INSUFICIENTE: DESEMPEÑO MUY BAJO A LO ESPERADO","PROCESO INCORRECTO")))))
    print('>>> intro get_evaluation_result')
    print(type(final_percentage))
    print(final_percentage)
    resutl = None
    if final_percentage >= Decimal('90.5') and final_percentage <= Decimal('104'):
        result = "EXCELENTE: DESEMPEÑO ALTO"
    elif final_percentage >= Decimal('80.5') and final_percentage <= Decimal('90.4'):
        result = "MUY BUENO: DESEMPEÑO MEJOR A LO ESPERADO"
    elif final_percentage >= Decimal('70.5') and final_percentage <= Decimal('80.4'):
        result = "SATISFACTORIO: DESEMPEÑO ESPERADO"
    elif final_percentage >= Decimal('60.5') and final_percentage <= Decimal('70.4'):
        result = "REGULAR: DESEMPEÑO BAJO LO ESPERADO"
    elif final_percentage > Decimal('0') and final_percentage <= Decimal('60.4'):
        result = "INSUFICIENTE: DESEMPEÑO MUY BAJO A LO ESPERADO"   
    else:
        result = "ERROR: PROCESO INCORRECTO"
    return result;

def some_view(request):
    # Create the HttpResponse object with the appropriate PDF headers.
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="somefilename.pdf"'

    # Create the PDF object, using the response object as its "file."
    p = canvas.Canvas(response)

    # Draw things on the PDF. Here's where the PDF generation happens.
    # See the ReportLab documentation for the full list of functionality.
    p.drawString(100, 100, "Hello world.")

    # Close the PDF object cleanly, and we're done.
    p.showPage()
    p.save()
    return response



class PersonalEvaluationList(ListView):
    states = {
        '': '----------',
        'GENERATED': 'Generada',
        'INITIATED': 'Iniciada',
        'FINALIZED': 'Finalizada',
        'CANCELED': 'Anulada',
    }

    scores = {
        '': '----------',
        'A': 'EXCELENTE (menor o igual a 104 y mayor o igual a 90.5)',
        'B': 'MUY BUENO (menor o igual a 90.4 y mayor o igual a 80.5)',
        'C': 'SATISFACTORIO (menor o igual a 80.4 y mayor o igual a 70.5)',
        'D': 'REGULAR (menor o igual a 70.4 Y mayor o igual a 60.5 )',
        'E': 'INSUFICIENTE (menor o igual a 60.4 y mayor a 0)',
        'F': 'ERROR (igual a 0)'
    }
    model = PersonalEvaluation
    template_name = 'personal_evaluation/personal_eval_list.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(PersonalEvaluationList, self).get_context_data(**kwargs)
        print('intro >>> get_context_data')
        context['list_states'] = self.states
        context['list_scores'] = self.scores
        context['list_inst_unit'] = self.get_list_institutional_unit()
        state = self.request.GET.get('state')
        score = self.request.GET.get('score')
        inst_unit = self.request.GET.get('inst_unit')
        if check_empty(inst_unit):
            inst_unit = int(inst_unit)
        year = self.request.GET.get('year')
        identification = self.request.GET.get('identification')
        names = self.request.GET.get('names')
        set_if_not_none(context, 'state', state)
        set_if_not_none(context, 'score', score)
        set_if_not_none(context, 'inst_unit', inst_unit)
        set_if_not_none(context, 'year', year)
        set_if_not_none(context, 'identification', identification)
        set_if_not_none(context, 'names', names)
        return context

    def get_queryset(self):
        print('intro >>> get_queryset')
        state =  self.request.GET.get('state')
        score =  self.request.GET.get('score')
        inst_unit = self.request.GET.get('inst_unit')
        year = self.request.GET.get('year')
        identification = self.request.GET.get('identification')
        names = self.request.GET.get('names')
        params = {}
        # kwargs = { 'deleted_datetime__isnull': True }
        # args = ( Q( title__icontains = 'Foo' ) | Q( title__icontains = 'Bar' ) )
        # entries = Entry.objects.filter( *args, **kwargs )
        apply_filter = False
        q_object = Q()
        if check_empty(state):
            q_object.add(Q(state = state), Q.AND)
        if check_empty(score):
            if score == 'A':
                q_object.add(Q(final_evaluation_percentage__range=(Decimal('90.50'), Decimal('104.00'))), Q.AND)
            elif score == 'B':
                q_object.add(Q(final_evaluation_percentage__range=(Decimal('80.50'), Decimal('90.40'))), Q.AND)
            elif score == 'C':
                q_object.add(Q(final_evaluation_percentage__range=(Decimal('70.50'), Decimal('80.40'))), Q.AND)
            elif score == 'D':
                q_object.add(Q(final_evaluation_percentage__range=(Decimal('60.50'), Decimal('70.40'))), Q.AND)
            elif score == 'E':
                q_object.add(Q(final_evaluation_percentage__lte=Decimal('60.40')), Q.AND)
                q_object.add(Q(final_evaluation_percentage__gt=Decimal('0.00')), Q.AND)
            else:
                q_object.add(Q(final_evaluation_percentage=Decimal('0.00')), Q.AND)
        if check_empty(inst_unit):
            q_object.add(Q(institutional_unit_evaluation__institutional_unit__pk = inst_unit), Q.AND)
        if check_empty(year):
            q_object.add(Q(institutional_unit_evaluation__performance_evaluation__year = year), Q.AND)
        if check_empty(identification):
            q_object.add(Q(evaluated__cedula__icontains = identification), Q.AND)
        if check_empty(names):
            q_object.add(Q(evaluated__name__icontains = names), Q.AND)
            q_object.add(Q(evaluated__lastname__icontains = names), Q.OR)
        print(PersonalEvaluation.objects.filter(q_object).order_by('-institutional_unit_evaluation__performance_evaluation__year').query)  
        queryset = PersonalEvaluation.objects.filter(q_object).order_by('-institutional_unit_evaluation__performance_evaluation__year')
        return queryset

    def get_list_institutional_unit(self):
        print('intro >>> get_list_institutional_unit')
        result = InstitutionalUnit.objects.filter(institution__pk=2).values_list('id', 'institutional_unit')
        result = list(result)
        result.insert(0,('',"---------"))
        return result

class PersonalEvaluationLowList(ListView):
    scores = {
        '': '----------',
        'A': 'REGULAR (mayor o igual a 60.5 y menor o igual a 70.4)',
        'B': 'INSUFICIENTE (mayor a 0 y menor o igual a 60.4)',
        'C': 'ERROR (igual a 0)'
    }
    model = PersonalEvaluation
    template_name = 'personal_evaluation/personal_eval_low_list.html'
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super(PersonalEvaluationLowList, self).get_context_data(**kwargs)
        print('intro >>> get_context_data')
        context['list_score'] = self.scores
        context['list_inst_unit'] = self.get_list_institutional_unit()
        score = self.request.GET.get('score')
        inst_unit = self.request.GET.get('inst_unit')
        if check_empty(inst_unit):
            inst_unit = int(inst_unit)
        year = self.request.GET.get('year')
        identification = self.request.GET.get('identification')
        names = self.request.GET.get('names')
        set_if_not_none(context, 'score', score)
        set_if_not_none(context, 'inst_unit', inst_unit)
        set_if_not_none(context, 'year', year)
        set_if_not_none(context, 'identification', identification)
        set_if_not_none(context, 'names', names)
        return context

    def get_queryset(self):
        print('intro >>> get_queryset')
        score =  self.request.GET.get('score')
        inst_unit = self.request.GET.get('inst_unit')
        year = self.request.GET.get('year')
        identification = self.request.GET.get('identification')
        names = self.request.GET.get('names')
        params = {}
        # kwargs = { 'deleted_datetime__isnull': True }
        # args = ( Q( title__icontains = 'Foo' ) | Q( title__icontains = 'Bar' ) )
        # entries = Entry.objects.filter( *args, **kwargs )
        # 60.39999999999999857891452847979962825775146484375
        # 60.39999999999999857891452847979962825775146484375
        # 
        apply_filter = False
        q_object = Q()
        print('>>> score: %s' % (score))
        print(getcontext())
        if check_empty(score):
            if score == 'A':
                q_object.add(Q(final_evaluation_percentage__range=(Decimal('60.50'), Decimal('70.40'))), Q.AND)
            elif score == 'B':
                q_object.add(Q(final_evaluation_percentage__lte=Decimal('60.40')), Q.AND)
                q_object.add(Q(final_evaluation_percentage__gt=Decimal('0.00')), Q.AND)
            else:
                q_object.add(Q(final_evaluation_percentage=Decimal('0.00')), Q.AND)

        else:
            q_object.add(Q(final_evaluation_percentage__lte=format(70.40, '.2f')), Q.AND)
        if check_empty(inst_unit):
            q_object.add(Q(institutional_unit_evaluation__institutional_unit__pk = inst_unit), Q.AND)
        if check_empty(year):
            q_object.add(Q(institutional_unit_evaluation__performance_evaluation__year = year), Q.AND)
        if check_empty(identification):
            q_object.add(Q(evaluated__cedula__icontains = identification), Q.AND)
        if check_empty(names):
            q_object.add(Q(evaluated__name__icontains = names), Q.AND)
            q_object.add(Q(evaluated__lastname__icontains = names), Q.OR)
        print(PersonalEvaluation.objects.filter(q_object).order_by('-institutional_unit_evaluation__performance_evaluation__year').query)  
        queryset = PersonalEvaluation.objects.filter(q_object).order_by('-institutional_unit_evaluation__performance_evaluation__year')
        return queryset

    def get_list_institutional_unit(self):
        print('intro >>> get_list_institutional_unit')
        result = InstitutionalUnit.objects.filter(institution__pk=2).values_list('id', 'institutional_unit')
        result = list(result)
        result.insert(0,('',"---------"))
        return result

def check_empty(value):
    if not (value is None or value is ""):
        return True;
    else:
        return False;

# def hola_pdf(request):
#     # Create the HttpResponse object with the appropriate PDF headers.
#     response = HttpResponse(content_type='application/pdf')
#     response['Content-Disposition'] = 'attachment; filename="somefilename.pdf"'

#     # Create the PDF object, using the response object as its "file."
#     p = canvas.Canvas(response)

#     # Draw things on the PDF. Here's where the PDF generation happens.
#     # See the ReportLab documentation for the full list of functionality.
#     p.drawString(100, 100, "Hello world.")

#     # Close the PDF object cleanly, and we're done.
#     p.showPage()
#     p.save()
#     return response

def hola_pdf_aux(request): 
    # Crea un objeto HttpResponse con las cabeceras del PDF correctas. 
    response = HttpResponse(content_type='application/pdf') 
    # Abre el PDF en la ventana del navegador 
    response['Content-Disposition'] = 'filename="test.pdf"' 
    # inicia el cuadro de dialogo "abrir con:" 
    #response['Content-Disposition'] = 'attachment; filename="test.pdf"' 
    # Crea el archivo PDF, usando el objeto response como un "archivo". 
    p = canvas.Canvas(response) 
    # Dibuja cosas en el PDF. Aqui se genera el PDF. Consulta la  
    # documentación para obtener una lista completa de funcionalidades. 
    p.roundRect(0, 750, 694, 120, 20, stroke=0, fill=1) 
    p.setFillColorRGB(0,1,0) 
    # La fuente y el tamaño 
    # p.setFont('Times­Bold',28)
    p.setFont("Times-Roman", 28)
    p.drawString(50, 800, "Bienvenidos a Django") 
    #p.setFont('Times­Roman', 12) 
    p.setFont("Helvetica", 12) 
    p.drawString(250, 780, "Hola mundo") 
    # p.setFont('Times­Bold',150) 
    p.setFont("Times-Roman", 150)
    p.setFillColorRGB(0,0,0) 
    p.drawString(70, 400, "Django") 
    # Cierra el objeto PDF limpiamente y termina. 
    p.showPage() 
    p.save() 
    return response

def hola_pdf_ant(request):
    print('>>> intro hola_pdf')
    # Crea un objeto HttpResponse con las cabeceras del PDF correctas. 
    response = HttpResponse(content_type='application/pdf') 
    # Abre el PDF en la ventana del navegador 
    response['Content-Disposition'] = 'filename="test.pdf"' 
    # inicia el cuadro de dialogo "abrir con:" 
    # response['Content-Disposition'] = 'attachment; filename="test.pdf"'
    # A4 es una tupla, podemos extraer sus valores
    # ancho, alto = A4 como también de la siguiente forma 
    sheet_width = A4[0]
    sheet_high = A4[1]
    print(sheet_width, sheet_high)
    # Crea el archivo PDF, usando el objeto response como un "archivo"
    # pagesize es una tupla, por eso le pasamos A4 de lib 
    c = canvas.Canvas(response, pagesize = A4) 
    # Escribimos en la primera hoja
    # drawString(x,y, texto)
    image_path = path_relative_to_file('/static/img/logo-iml.png')
    c.drawImage(image_path, 0, 700, width=100, height=100)

    #FUENTES EN CANVAS
    #-----------------
    texto = "Lorem ipsum dolor sit amet"
    x, y = 150, 650
    for fuente in c.getAvailableFonts():  # Lista disponible de fuentes
        c.setFont(fuente, 10)  # Cambiamos la fuente y el tamaño
        c.drawString(x, y, texto)
        c.setFont("Helvetica", 12) 
        c.drawRightString(x-10, y, fuente + ":")
        y -= 20
    # c.drawString(50,500, " Mi PRIMER PDF")
    # c.drawString(250,300,"Coordenada=(250,300) ")
    # c.drawString(350,200,"(350, 10)")
    # c.drawString(150,400,"Aprendiendo REPORTLAB")

    #Grabamos la página presente del canvas
    c.showPage()  

    #Escribimos en la segunda hoja
    c.drawString(50,420, "Segunda Hoja")
    c.showPage()

    #Archivamos y cerramos el canvas
    c.save()  
    return response

#Definimos las caracteristicas fijas de la primera página
def myFirstPage(canvas, doc):
    canvas.saveState()
    canvas.setFont('Times-Bold', 16)
    canvas.drawCentredString(PAGE_WIDTH/2.0, PAGE_HEIGHT - 108, Title)
    canvas.setFont('Times-Roman', 9)
    canvas.drawString(inch, 0.75 * inch, "Primera pagina / %s" %pageinfo)
    canvas.restoreState()

#Definimos disposiciones alternas para las caracteristicas de las otras páginas
def myLaterPages(canvas, doc):
    canvas.saveState()
    canvas.setFont('Times-Roman', 9)
    canvas.drawString(inch, 0.75 * inch, "Página %d / %s" %(doc.page, pageinfo))
    canvas.restoreState()

def hola_pdf_xxx(request):
    print('>>> intro hola_pdf')
    # Crea un objeto HttpResponse con las cabeceras del PDF correctas. 
    response = HttpResponse(content_type='application/pdf') 
    pdf_name = "clientes.pdf"  # llamado clientes 
    # Abre el PDF en la ventana del navegador 
    response['Content-Disposition'] = 'filename=%s' % pdf_name
    # inicia el cuadro de dialogo "abrir con:" 
    # response['Content-Disposition'] = 'attachment; filename="test.pdf"'
    # A4 es una tupla, podemos extraer sus valores
    # ancho, alto = A4 como también de la siguiente forma 
    sheet_width = A4[0]
    sheet_high = A4[1]
    print(sheet_width, sheet_high)

    buff = BytesIO()
    doc = SimpleDocTemplate(buff,
                            pagesize=letter,
                            rightMargin=40,
                            leftMargin=40,
                            topMargin=60,
                            bottomMargin=18,
                            )
    clientes = []
    styles = getSampleStyleSheet()
    header = Paragraph("Listado de Clientes", styles['Heading1'])
    clientes.append(header)
    headings = ('Id', 'Relevance', 'Performance')
    allclientes = [(p.id, p.relevance, p.performance_evaluation) for p in  CatalogTechnicalCompetence.objects.all()]
    print(allclientes)
    t = Table([headings] + allclientes)
    t.setStyle(TableStyle(
        [
            ('GRID', (0, 0), (3, -1), 1, colors.dodgerblue),
            ('LINEBELOW', (0, 0), (-1, 0), 2, colors.darkblue),
            ('BACKGROUND', (0, 0), (-1, 0), colors.dodgerblue),
            ('BACKGROUND', (0, 1), (-1, 1),colors.lawngreen)
        ]
    ))
    clientes.append(t)
    doc.build(clientes)
    response.write(buff.getvalue())
    buff.close()
    return response

def hola_pdf(request):
    print('>>> intro hola_pdf')
    # Crea un objeto HttpResponse con las cabeceras del PDF correctas. 
    response = HttpResponse(content_type='application/pdf') 
    pdf_name = "clientes.pdf"  # llamado clientes 
    # Abre el PDF en la ventana del navegador 
    response['Content-Disposition'] = 'filename=%s' % pdf_name
    # inicia el cuadro de dialogo "abrir con:" 
    # response['Content-Disposition'] = 'attachment; filename="test.pdf"'
    # A4 es una tupla, podemos extraer sus valores
    # ancho, alto = A4 como también de la siguiente forma 
    sheet_width = A4[0]
    sheet_high = A4[1]
    print(sheet_width, sheet_high)
    buff = BytesIO()
    go(buff)
    response.write(buff.getvalue())
    buff.close()
    return response


def personal_evaluation_report(request, person_eval_pk):
    print('>>> intro personal_evaluation_report')
    # Crea un objeto HttpResponse con las cabeceras del PDF correctas. 
    response = HttpResponse(content_type='application/pdf') 
    pdf_name = "clientes.pdf"  # llamado clientes 
    # Abre el PDF en la ventana del navegador 
    response['Content-Disposition'] = 'filename=%s' % pdf_name
    # inicia el cuadro de dialogo "abrir con:" 
    # response['Content-Disposition'] = 'attachment; filename="test.pdf"'
    # A4 es una tupla, podemos extraer sus valores
    # ancho, alto = A4 como también de la siguiente forma 
    sheet_width = A4[0]
    sheet_high = A4[1]
    # print(sheet_width, sheet_high)
    buff = BytesIO()
    doc = SimpleDocTemplate(buff, pagesize = A4)
    story=[]
    
    # Configuraciones iniciales, imagenes, estilos de texto, etc.
    image_path = path_relative_to_file('/static/img/logo-mrl.png')
    estilo=getSampleStyleSheet()
    estilo.add(ParagraphStyle(name="TitleMain", alignment=TA_CENTER, fontName='Times-Bold', fontSize=8))
    estilo.add(ParagraphStyle(name="TitleSmall", alignment=TA_CENTER, fontName='Times-Bold', fontSize=7))
    estilo.add(ParagraphStyle(name="DefaultNormal", alignment=TA_LEFT, fontName='Times', fontSize=8))
    estilo.add(ParagraphStyle(name="DefaultBold", alignment=TA_LEFT, fontName='Times-Bold', fontSize=8))
    estilo.add(ParagraphStyle(name="DefaultNormalColor", alignment=TA_CENTER, fontName='Times', fontSize=8, textColor=colors.Color(0.000, 0.000, 0.545)))
    estilo.add(ParagraphStyle(name="DefaultBoldColor", alignment=TA_CENTER, fontName='Times-Bold', fontSize=8, textColor=colors.Color(0.000, 0.000, 0.545)))
    estilo.add(ParagraphStyle(name="Result", alignment=TA_RIGHT, fontName='Times-Bold', fontSize=8, textColor=colors.Color(0.502, 0.000, 0.000)))

    title_color =  colors.Color(0.412, 0.412, 0.412)
    subtitle_color = colors.Color(0.753, 0.753, 0.753)
    result_color = colors.Color(0.941, 0.902, 0.549)

    # Linea vacia
    empty_line = ['', '', '', '', '', '']

    # Cabezera del documento, logo del ministerio y titulo del formulario
    tbl_data = [
        [Image(image_path, width=100, height=25), Paragraph("FORMULARIO MRL-EVAL-01 - MODIFICADO", estilo['TitleMain'])],
    ]
    tbl = Table(tbl_data, colWidths=[150, 300])
    tbl.setStyle([
        ('TEXTCOLOR', (0, 1), (0, -1), colors.blue),
        ('TEXTCOLOR', (1, 1), (2, -1), colors.green),
        # ('BACKGROUND',(1,1),(-1,-1),colors.cyan),
        # ('BOX',(1,1),(-1,-1),1.25,colors.yellow),
        # ('INNERGRID',(1,1),(-1,-1),1,colors.red),
        ('VALIGN', (0,0), (-1, -1), 'MIDDLE'),
    ])
    story.append(tbl)

    # Datos iniciales de la evaluacion personal con los que se arma el reporte
    personal_eval = PersonalEvaluation.objects.get(pk=person_eval_pk)
    performance_eval = personal_eval.institutional_unit_evaluation.performance_evaluation
    inst_unit_eval = personal_eval.institutional_unit_evaluation
    contract = personal_eval.contract
    curriculum_vitae = CurriculumVitae.objects.get(person__pk = personal_eval.evaluated.person.id)
    formal_inst_list = FormalInstruction.objects.filter(curriculum_vitae__pk = curriculum_vitae.id)
    request.session['personal_evaluation'] = personal_eval.id
    user = User.objects.get(username=request.user.username)
    
    # Datos generales del servidor
    formulario_evaluacion = Paragraph( 'FORMULARIO PARA LA EVALUACIÓN DEL DESEMPEÑO POR COMPETENCIAS' + 
            ' PARA USO DEL JEFE INMEDIATO', estilo['TitleMain'])
    datos_servidor = Paragraph('DATOS DEL SERVIDOR:', estilo['DefaultBold'])
    apellidos_nombres_evaluado = Paragraph('Apellidos y Nombres del Servidor (Evaluado):', estilo['DefaultBold'])
    denominacion_puesto = Paragraph('Denominación del Puesto que Desempeña:', estilo['DefaultBold'])
    titulo_profesion = Paragraph('Título o profesión:', estilo['DefaultBold'])
    jefe_departamento = Paragraph('Apellidos o nombre del fefe departamental:', estilo['DefaultBold'])
    jefe_imediato = Paragraph('Apellidos o nombre del jefe inmediato o superior inmediato (Evaluador):', estilo['DefaultBold']) 
    periodo_eval_desde = Paragraph('Período de evaluación (dd/mm/aaaa): Desde:', estilo['DefaultBold'])
    periodo_eval_hasta = Paragraph('Hasta:',estilo['DefaultBold'])
    instructions = ''
    for var in formal_inst_list:
        instructions = instructions + var.area_studies + ' ' 
    datos = (
            (formulario_evaluacion, '', '', ''),
            (datos_servidor, '', '', ''),
            (apellidos_nombres_evaluado, style_paragraph_color(personal_eval.evaluated.person), '', ''),
            (denominacion_puesto, style_paragraph_color(contract.public_office), '', ''),
            (titulo_profesion, style_paragraph_color(instructions), '', ''),
            (jefe_departamento, style_paragraph_color(personal_eval.institutional_unit_director), '', ''),
            (jefe_imediato, style_paragraph_color(personal_eval.immediate_superior.person), '', ''),
            (
                periodo_eval_desde, style_paragraph_color(personal_eval.period_from.strftime('%Y-%m-%d')), 
                periodo_eval_hasta, style_paragraph_color(personal_eval.period_to.strftime('%Y-%m-%d'))
            )

        )
    tabla = Table(data = datos,
                    colWidths=[250,100,100,100],
                    style=[
                            ('GRID',(0,0), (-1,-1), 0.9, colors.grey),
                            ('BOX',(0,0), (-1,-1), 2, colors.black),
                            ('SPAN',(0,0),(-1,0)), # Titulo principal del formulario para la evaluacion
                            ('SPAN',(0,1),(-1,1)), # Titulo Datos del servidor
                            ('SPAN',(1,2),(-1,2)), # Datos del evaluado
                            ('SPAN',(1,3),(-1,3)), # Datos del puesto que desempenia
                            ('SPAN',(1,4),(-1,4)), # Datos titulo o profesion
                            ('SPAN',(1,5),(-1,5)), # Datos del jefe de departamento
                            ('SPAN',(1,6),(-1,6)), # Datos del jefe inmediato
                            ('BACKGROUND',(0,0), (-1,0), colors.Color(0.412, 0.412, 0.412)),
                            ('BACKGROUND',(0,1),(-1,1), colors.Color(0.753, 0.753, 0.753)),
                            ('BACKGROUND',(0,2),(0,-1), colors.Color(0.753, 0.753, 0.753)),
                            ('BACKGROUND',(2,7),(2,7), colors.Color(0.753, 0.753, 0.753)),
                        ]
                  )
    story.append(tabla)
    story.append(Spacer(0,15))

    count_rows = 0
    report_data = []
    report_style = [
                    ('GRID',(0,0), (-1,-1), 0.9, colors.grey),
                    ('GRID',(0,53),(-1,56), 3,colors.white),
                    ('BOX',(0,0), (-1,-1), 2, colors.black),
                ]
      
    # Titulo Evaluacion de actividades del puesto
    job_evaluation_activities = Paragraph('EVALUACIÓN DE ACTIVIDADES DEL PUESTO', estilo['TitleMain'])
    report_data.append([job_evaluation_activities, '', '', '', '', '']) 
    report_style.append(('SPAN',(0,count_rows),(-1,count_rows)))
    report_style.append(('BACKGROUND',(0,count_rows), (-1,count_rows), title_color))
    count_rows = count_rows + 1

    # Sutitulo de los Indicadores de gestion del puesto
    activity_eval = ActivityEvaluation.objects.get(personal_evaluation=personal_eval)
    activity_list = activity_eval.activity_set.all()
    job_management_indicator = Paragraph('INDICADORES DE GESTIÓN DEL PUESTO:', estilo['DefaultBold'])
    activities_number = Paragraph('# Actividades:', estilo['DefaultNormalColor'])
    value_activities_number = Paragraph(str(activity_eval.number_activities), estilo['DefaultNormalColor'])
    factor = Paragraph('Factor:', estilo['DefaultNormalColor'])
    value_factor = Paragraph(str(activity_eval.factor), estilo['DefaultNormalColor'])
    report_data.append([
            job_management_indicator, 
            '', 
            activities_number, 
            value_activities_number, 
            factor, 
            value_factor
        ]) 
    report_style.append(('SPAN',(0,count_rows),(1,count_rows)))
    report_style.append(('BACKGROUND',(0,count_rows), (-1,count_rows), subtitle_color))
    count_rows = count_rows + 1
    
    # Subtitulo del Nombre de la columnas de las actividades
    description_activities = Paragraph("Descripción de actividades", estilo['DefaultBold'])
    indicator = Paragraph("Indicador", estilo['DefaultBold'])
    goal = Paragraph("Meta del período evaluado", estilo['DefaultBold'])
    completed = Paragraph("Cumplidos", estilo['DefaultBold'])
    comply_percentage = Paragraph("% de cumplimiento", estilo['DefaultBold'])
    comply_level = Paragraph("Nivel de cumplimiento", estilo['DefaultBold'])
    report_data.append([
            description_activities, 
            indicator, 
            goal, 
            completed, 
            comply_percentage, 
            comply_level
        ])
    report_style.append(('BACKGROUND',(0,count_rows), (-1,count_rows), subtitle_color))
    count_rows = count_rows + 1
    
    # Datos de las actividades para los indicadores de gestion del puesto
    if activity_list:
        for activity in activity_list:
            report_data.append(
                [
                    style_paragraph_color(activity.description), 
                    style_paragraph_color(activity.indicator), 
                    style_paragraph_color(activity.period_goal), 
                    style_paragraph_color(activity.complete_goal), 
                    style_paragraph_color(activity.percentage_fulfillment), 
                    style_paragraph_color(activity.level_fulfillment)
                ]
            )
            count_rows = count_rows + 1
    else:
        report_data.append(empty_line)
        report_style.append(('SPAN',(0,count_rows),(-1,count_rows)))
        report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), colors.white))
        count_rows = count_rows + 1
    
    # Subtitulo para cuando se adelante con objetivos y metas del proximo periodo
    advance_objectives = Paragraph("¿A más del cumplimeinto de la totalidad de las metas y objetivos se adelantó y cumplió" + 
        " con objetivos y metas provistas para el siguiente período de evaluación?", estilo['DefaultBold'])
    increase_applies = Paragraph("APLICA EL + 4%", estilo['DefaultBold'])
    increase_porcentage = Paragraph("% DE AUMENTO", estilo['DefaultBold'])
    report_data.append([advance_objectives, '', '',increase_applies, '', increase_porcentage])
    report_style.append(('SPAN',(3,count_rows),(4,count_rows)))
    report_style.append(('BACKGROUND',(0,count_rows), (-1,count_rows), subtitle_color))
    report_style.append(('SPAN',(0,count_rows),(2,count_rows + 1)))
    count_rows = count_rows + 1
   
    # Valores para cuando se adelante con objetivos o metas del proximo periodo
    report_data.append([
        '', '', '',
        style_paragraph_color(activity_eval.get_applies_advance_goals_display()),
        '',
        style_paragraph_color(activity_eval.percentage_increase),])
    report_style.append(('SPAN',(3,count_rows),(4,count_rows)))
    report_style.append(('BACKGROUND',(0,count_rows), (2,count_rows), subtitle_color))
    count_rows = count_rows + 1
    
    # Valor final respecto a las actividades  para los indicadores de gestion
    activities_total = Paragraph("Total actividades escenciales:", estilo['Result'])
    report_data.append([activities_total, '', '','', '', style_paragraph_result(activity_eval.percentage_final)])
    report_style.append(('SPAN',(0,count_rows),(4,count_rows)))
    report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), result_color))
    count_rows = count_rows + 1
    
    # Subtitulo de conocimientos
    knowledge = Paragraph('CONOCIMIENTOS', estilo['DefaultBold'])
    knowledge_number = Paragraph('# de Conocimientos:', estilo['DefaultNormalColor'])
    knowledge_level = Paragraph('Nivel de conocimiento', estilo['DefaultBold'])
    knowledge_eval = KnowledgeEvaluation.objects.get(personal_evaluation=personal_eval)
    knowledge_list = knowledge_eval.knowledge_set.all() 
    report_data.append([
            knowledge, 
            knowledge_number, 
            style_paragraph_color(knowledge_eval.number_knowledge),
            factor, 
            style_paragraph_color(knowledge_eval.factor), 
            knowledge_level
        ])
    report_style.append(('BACKGROUND',(0,count_rows), (-1,count_rows), subtitle_color))
    count_rows = count_rows + 1

    # Conocimientos registrados 
    if knowledge_list:
        for knowledge in knowledge_list:
            report_data.append(
                [
                    style_paragraph_color(knowledge.knowledge), 
                    '', '', '', '',
                    style_paragraph_color(knowledge.get_level_knowledge_display())
                ]
            )
            report_style.append(
                    ('SPAN',(0,count_rows),(4,count_rows)),
                )
            count_rows = count_rows + 1
    else:
        report_data.append(empty_line)
        report_style.append(('SPAN',(0,count_rows),(-1,count_rows)))
        report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), colors.white))
        count_rows = count_rows + 1

    # Valor final respecto a los conocimientos
    knowledge_total = Paragraph("Total de conocimientos:", estilo['Result'])
    report_data.append([knowledge_total, '', '', '', '', style_paragraph_result(knowledge_eval.percentage_final)])
    report_style.append(('SPAN',(0,count_rows),(4,count_rows)))
    report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), result_color))
    count_rows = count_rows + 1

    # Subtitulo competencias tecnicas
    technical_competence = Paragraph("COMPETENCIAS TÉCNICAS DEL PUESTO", estilo['DefaultBold'])
    competence_number = Paragraph("# Competencias", estilo['DefaultNormalColor'])
    technical_c_eval = TechnicalCompetenceEvaluation.objects.get(personal_evaluation=personal_eval)
    technical_c_list = technical_c_eval.technicalcompetence_set.all()
    report_data.append([
            technical_competence, 
            competence_number, 
            style_paragraph_color(technical_c_eval.number_competence),
            factor, 
            style_paragraph_color(technical_c_eval.factor), 
            ''
        ])
    report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), subtitle_color))
    count_rows = count_rows + 1

    # Subtitulo del nombre de las columnas de las competencias tecnicas
    skill = Paragraph("DESTREZAS", estilo['DefaultBold'])
    relevance = Paragraph("Relevancia", estilo['DefaultBold'])
    observable_behavior = Paragraph("Comportamiento observable", estilo['DefaultBold'])
    development_level = Paragraph("Nivel de desarrollo", estilo['DefaultBold'])
    report_data.append([
            skill, 
            relevance, 
            observable_behavior,
             '',
            '',
            development_level
        ])
    report_style.append(('SPAN',(2,count_rows),(4,count_rows)))
    report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), subtitle_color))
    count_rows = count_rows + 1

    # Competencias tecnicas ingresadas
    if technical_c_list:
        for technical in technical_c_list:
            report_data.append(
                [
                    style_paragraph_color(technical.catalog_technical_competence.skill),
                    style_paragraph_color(technical.catalog_technical_competence.get_relevance_display()),
                    style_paragraph_color(technical.catalog_technical_competence.observable_behavior),
                    '', '',
                    style_paragraph_color(technical.development_level),
                ]
            )
            report_style.append(
                    ('SPAN',(2,count_rows),(4,count_rows)),
                )
            count_rows = count_rows + 1
    else:
        report_data.append(empty_line)
        report_style.append(('SPAN',(0,count_rows),(-1,count_rows)))
        report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), colors.white))
        count_rows = count_rows + 1

    # Valor final respecto a las competencias tecnicas
    total_technical_competence = Paragraph("Total Competencias Técnicas del Puesto :", estilo['Result'])
    report_data.append([total_technical_competence, '', '', '', '', style_paragraph_result(technical_c_eval.percentage_final)])
    report_style.append(('SPAN',(0,count_rows),(4,count_rows)))
    report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), result_color))
    count_rows = count_rows + 1

    # Subtitulo competencias universales
    universal_competence = Paragraph("COMPETENCIAS UNIVERSALES", estilo['DefaultBold'])
    universal_c_eval = UniversalCompetenceEvaluation.objects.get(personal_evaluation=personal_eval)
    universal_c_list = universal_c_eval.universalcompetence_set.all()
    report_data.append([universal_competence, competence_number, 
                style_paragraph_color(universal_c_eval.number_competence), 
                factor, 
                style_paragraph_color(universal_c_eval.factor), 
                ''
            ])
    report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), subtitle_color))
    count_rows = count_rows + 1

    # Subtitulo de requerimiento de campos para competencias universales
    required_fields = Paragraph("ESTOS CAMPOS DEBEN SER LLENADOS OBLIGATORIAMENTE:", estilo['TitleMain'])
    report_data.append([required_fields, '', '', '', '', ''])
    report_style.append(('SPAN',(0,count_rows),(-1,count_rows)))
    report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), subtitle_color))
    count_rows = count_rows + 1

    # Subtitulo del nombre de las columnas de las competencias universales
    frequency_application = Paragraph("Frecuencia de aplicación", estilo['DefaultBold'])
    report_data.append([skill, relevance, observable_behavior, '', '', frequency_application])
    report_style.append(('SPAN',(2,count_rows),(4,count_rows)))
    report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), subtitle_color))
    count_rows = count_rows + 1

    # Competencias universales registradas
    for universal in universal_c_list:
        report_data.append(
            [
                style_paragraph_color(universal.catalog_universal_competence.skill),
                style_paragraph_color(universal.catalog_universal_competence.get_relevance_display()),
                style_paragraph_color(universal.catalog_universal_competence.observable_behavior),
                '', '',
                style_paragraph_color(universal.frequency_application),
            ]
        )
        report_style.append(
                ('SPAN',(2,count_rows),(4,count_rows)),
            )
        count_rows = count_rows + 1
    
    # Valor final respecto a las competencias universales
    total_universal_competence = Paragraph("Total competencias universales", estilo['Result'])
    report_data.append([total_universal_competence, '', '', '', '', style_paragraph_result(universal_c_eval.percentage_final)])
    report_style.append(('SPAN',(0,count_rows),(4,count_rows)))
    report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), result_color))
    count_rows = count_rows + 1

    # Subtitulo trabajo en equipo, iniciativa y liderazgo
    teamwork = Paragraph("TRABAJO EN EQUIPO, INICIATIVA Y LIDERAZGO", estilo['DefaultBold'])
    teamwork_eval = TeamworkEvaluation.objects.get(personal_evaluation=personal_eval)
    teamwork_list = teamwork_eval.teamwork_set.all()
    report_data.append([teamwork, '', '', factor, style_paragraph_color(teamwork_eval.factor), ''])
    report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), subtitle_color))
    report_style.append(('SPAN',(0,count_rows),(2,count_rows)))
    count_rows = count_rows + 1

    # Subtitulo de condicion para el campo liderazgo
    field_leadership = Paragraph(
                "LLENAR EL CAMPO DE LIDERAZGO, SOLO PARA QUIENES TENGAN SERVIDORES S" + 
                "UBORDINADOS BAJO SU RESPONSABILIDAD DE GESTIÓN", estilo['TitleSmall']
                )    
    report_data.append([field_leadership, '', '', '', '', ''])
    report_style.append(('SPAN',(0,count_rows),(-1,count_rows)))
    report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), subtitle_color))
    count_rows = count_rows + 1

    # Subtitulo del nombre de las columnas del trabajo en equipo, iniciativa y liderazgo
    description = Paragraph("DESCRIPCIÓN", estilo['DefaultBold'])
    report_data.append([
            description, 
            relevance, 
            observable_behavior, 
            '', '', 
            frequency_application
        ])
    report_style.append(('SPAN',(2,count_rows),(4,count_rows)))
    report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), subtitle_color))
    count_rows = count_rows + 1

    # Valor de los datos para el factor de trabajo en equipo 
    if teamwork_list:
        for teamwork in teamwork_list:
            report_data.append(
                [
                    style_paragraph_color(teamwork.catalog_teamwork.ability),
                    style_paragraph_color(teamwork.catalog_teamwork.get_relevance_display()),
                    style_paragraph_color(teamwork.catalog_teamwork.observable_behavior),
                    '', '',
                    style_paragraph_color(teamwork.frequency_application),
                ]
            )
            report_style.append(
                    ('SPAN',(2,count_rows),(4,count_rows)),
                )
            count_rows = count_rows + 1
    else:
        report_data.append(empty_line)
        report_style.append(('SPAN',(0,count_rows),(-1,count_rows)))
        report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), colors.white))
        count_rows = count_rows + 1
    
    # Valor final respecto al trabajo en equipo, iniciatiba y liderazgo
    total_teamwork = Paragraph("Total Trabajo en Equipo, Iniciativa y Liderazgo:", estilo['Result'])
    report_data.append([
                total_teamwork, 
                '',
                '', 
                '', 
                '', 
                style_paragraph_result(teamwork_eval.percentage_final)
            ])
    report_style.append(('SPAN',(0,count_rows),(4,count_rows)))
    report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), result_color))
    count_rows = count_rows + 1

    # Subtitulo Observaciones del jefe inmediato
    observations = Paragraph("OBSERVACIONES DEL JEFE INMEDIATO (EN CASO DE QUE LAS TENGA)", estilo['DefaultBold'])
    chief_observation_list = ChiefObservation.objects.filter(personal_evaluation=personal_eval)
    report_data.append([observations, '', '', '', '', ''])
    report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), title_color))
    report_style.append(('SPAN',(0,count_rows),(-1,count_rows)))
    count_rows = count_rows + 1
    if chief_observation_list:
        for observation in chief_observation_list:
            report_data.append(
                [
                    style_paragraph_color(observation.description),
                    '',
                    '',
                    '',
                    '',
                    ''
                ]
            )
            report_style.append(
                    ('SPAN',(0,count_rows),(-1,count_rows)),
                )
            count_rows = count_rows + 1
    else:
        report_data.append(empty_line)
        report_style.append(('SPAN',(0,count_rows),(-1,count_rows)))
        report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), colors.white))
        count_rows = count_rows + 1

    # Subtitulo Quejas del ciudadano
    complaints = Paragraph("QUEJAS DEL CIUDADANO (PARA USO DE LAS UARHS) " + 
            "INFORMACIÓN PROVENIENTE DEL FORMULARIO EVAL-02", estilo['DefaultBold'])
    complaint_eval = ComplaintEvaluation.objects.get(personal_evaluation= personal_eval)
    complaint_list = complaint_eval.complaint_set.all()
    report_data.append([complaints, '', '', '', '', ''])
    report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), title_color))
    report_style.append(('SPAN',(0,count_rows),(-1,count_rows)))
    count_rows = count_rows + 1

    # Subtitulo del nombre de las columnas del trabajo en equipo, iniciativa y liderazgo
    name_complainant = Paragraph("Nombre de la persona que realiza la queja", estilo['DefaultBold'])
    form_number = Paragraph("No. de formulario", estilo['DefaultBold'])
    applies_discount = Paragraph("Aplica descuento a la evaluación del desempeño", estilo['DefaultBold'])
    reduction_percentage = Paragraph("% de reducción", estilo['DefaultBold'])
    report_data.append([
                name_complainant, 
                description, 
                '', 
                form_number, 
                applies_discount, 
                reduction_percentage
            ])
    report_style.append(('SPAN',(1,count_rows),(2,count_rows)))
    report_style.append(('BACKGROUND',(0, count_rows),(-1,count_rows), subtitle_color))
    count_rows = count_rows + 1

    # Quejas registradas por los ciudadanos en contra del servidor publico
    if complaint_list:
        for complaint in complaint_list:
            report_data.append(
                [
                    style_paragraph_color(complaint.person_making_complaint),
                    style_paragraph_color(complaint.description),
                    '',
                    style_paragraph_color(complaint.form_number),
                    style_paragraph_color(complaint.discount_applies),
                    style_paragraph_color(complaint.percentage_reduction),
                ]
            )
            report_style.append(
                    ('SPAN',(1,count_rows),(2,count_rows)),
                )
            count_rows = count_rows + 1
    else:
        report_data.append(empty_line)
        report_style.append(('SPAN',(0,count_rows),(-1,count_rows)))
        report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), colors.white))
        count_rows = count_rows + 1

    # Valor final respecto quejas del ciudadano
    total_complaints = Paragraph("Total", estilo['Result'])
    report_data.append([total_complaints, '', '', '', '', style_paragraph_result(complaint_eval.percentage_final)])
    report_style.append(('SPAN',(0,count_rows),(4,count_rows)))
    report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), result_color))
    count_rows = count_rows + 1

    # Subtitulo resultado de la evaluacion
    evaluation_result = Paragraph("RESULTADO DE LA EVALUACIÓN", estilo['TitleMain'])
    report_data.append([evaluation_result, '', '', '', '', ''])
    report_style.append(('BACKGROUND',(0,count_rows),(-1,count_rows), title_color))
    report_style.append(('SPAN',(0,count_rows),(-1,count_rows)))
    count_rows = count_rows + 1

    # Subtitulo del nombre de las columnas de los factores de evaluacion
    evaluation_factors = Paragraph("FACTORES DE EVALUACIÓN", estilo['DefaultBold'])
    score = Paragraph("CALIFICACIÓN ALCANZADA", estilo['DefaultBold'])
    report_data.append([evaluation_factors, '', '', '', '', score])
    report_style.append(('SPAN',(0,count_rows),(4,count_rows)))
    report_style.append(('BACKGROUND',(0, count_rows),(-1,count_rows), subtitle_color))
    count_rows = count_rows + 1

    # Subtitulo y valor para el factor de Gestion del puesto
    subtotal_job_management_indicators = Paragraph("Indicadores de Gestión del puesto", estilo['DefaultNormalColor'])
    report_data.append([
            subtotal_job_management_indicators, 
            '', '', '', '', 
            style_paragraph_color(personal_eval.job_management_indicators)
        ])
    report_style.append(('SPAN',(0,count_rows),(4,count_rows)))
    report_style.append(('BACKGROUND',(0, count_rows),(-1,count_rows), result_color))
    count_rows = count_rows + 1

    # Subtitulo y valor para el factor de Conocimientos
    subtotal_knowledge = Paragraph("Conocimientos", estilo['DefaultNormalColor'])
    report_data.append([
            subtotal_knowledge, 
            '', '', '', '',
            style_paragraph_color(personal_eval.knowledge)
        ])
    report_style.append(('SPAN',(0,count_rows),(4,count_rows)))
    report_style.append(('BACKGROUND',(0, count_rows),(-1,count_rows), result_color))
    count_rows = count_rows + 1

    # Subtitulo y valor para el factor de Competencias tecnicas
    subtotal_technical_competence = Paragraph("Competencias técnicas del puesto", estilo['DefaultNormalColor'])
    report_data.append([
            subtotal_technical_competence, 
            '', '', '', '', 
            style_paragraph_color(personal_eval.technical_competence)
        ])
    report_style.append(('SPAN',(0,count_rows),(4,count_rows)))
    report_style.append(('BACKGROUND',(0, count_rows),(-1,count_rows), result_color))
    count_rows = count_rows + 1

    # Subtitulo y valor para el factor de Competencias universales
    subtotal_universal_competence = Paragraph("Competencias universales", estilo['DefaultNormalColor'])
    report_data.append([
            subtotal_universal_competence, 
            '', '', '', '',
            style_paragraph_color(personal_eval.universal_competence)
        ])
    report_style.append(('SPAN',(0,count_rows),(4,count_rows)))
    report_style.append(('BACKGROUND',(0, count_rows),(-1,count_rows), result_color))
    count_rows = count_rows + 1

    # Subtitulo y valor para el factor de Trabajo en equipo, iniciativa y liderazgo
    subtotal_teamwork = Paragraph("Trabajo en equipo, iniciativa y liderazgo", estilo['DefaultNormalColor'])
    report_data.append([
            subtotal_teamwork,
            '', '', '', '', 
            style_paragraph_color(personal_eval.teamwork)
        ])
    report_style.append(('SPAN',(0,count_rows),(4,count_rows)))
    report_style.append(('BACKGROUND',(0, count_rows),(-1,count_rows), result_color))
    count_rows = count_rows + 1

    # Subtitulo y valor para el factor quejas o evaluacion del ciudadano
    subtotal_citizen_evaluation = Paragraph("Evaluación del ciudadano (-)", estilo['DefaultNormalColor'])
    report_data.append([
            subtotal_citizen_evaluation,
            '', '', '', '',
            style_paragraph_color(personal_eval.evaluation_citizens)
        ])
    report_style.append(('SPAN',(0,count_rows),(4,count_rows)))
    report_style.append(('BACKGROUND',(0, count_rows),(-1,count_rows), result_color))
    count_rows = count_rows + 1

    # Valor del porcentaje final de la evaluacion
    report_data.append([style_paragraph_result_final(personal_eval.get_evaluation_percentage()), '', '', '', '', ''])
    report_style.append(('SPAN',(0,count_rows),(-1,count_rows)))
    report_style.append(('BACKGROUND',(0, count_rows),(-1,count_rows), subtitle_color))
    count_rows = count_rows + 1

    # Valor del resultado final de la evaluacion
    report_data.append([style_paragraph_result_final(personal_eval.final_evaluation_result), '', '', '', '', ''])
    report_style.append(('SPAN',(0,count_rows),(-1,count_rows)))
    report_style.append(('BACKGROUND',(0, count_rows),(-1,count_rows), subtitle_color))
    count_rows = count_rows + 1

    # Subtitulo Funcionario evaluador
    evaluator_official = Paragraph("FUNCIONARIO (A) EVALUADOR (A)", estilo['TitleMain'])
    report_data.append([evaluator_official, '', '', '', '', ''])
    report_style.append(('SPAN',(0,count_rows),(-1,count_rows)))
    report_style.append(('BACKGROUND',(0, count_rows),(-1,count_rows), subtitle_color))
    count_rows = count_rows + 1

    # Subtitulo y valor fecha
    evaluation_date = Paragraph("Fecha (dd/mm/aaaa)", estilo['DefaultBoldColor'])
    report_data.append([evaluation_date, '', '', '', '', ''])
    report_style.append(('SPAN',(0,count_rows),(2,count_rows)))
    report_style.append(('SPAN',(3,count_rows),(-1,count_rows)))
    report_style.append(('BACKGROUND',(0, count_rows),(-1,count_rows), subtitle_color))
    count_rows = count_rows + 1

    # Subtitulo Certifico
    certificate = Paragraph(
            "CERTIFICO: Que he evaluado al (a la) servidor (a) acorde al" + 
            " procedimiento de la norma de Evaluación del Desempeño",
            estilo['DefaultNormalColor']
        )
    report_data.append([certificate, '', '', '', '', ''])
    report_style.append(('SPAN',(0,count_rows),(-1,count_rows)))
    report_style.append(('BACKGROUND',(0, count_rows),(-1,count_rows), subtitle_color))
    count_rows = count_rows + 1

    # Espacio vacio para firma
    report_data.append(['', '', '', '', '', ''])
    report_style.append(('INNERGRID',(0,count_rows),(-1,count_rows),2,colors.white))
    report_style.append(('INNERGRID',(0,count_rows),(-1,-1), 1,colors.white))    
    count_rows = count_rows + 1
    
    # Lineas para la firma
    lines = Paragraph("__________________________________", estilo['DefaultNormalColor'])
    report_data.append([lines, '', '', lines, '', ''])
    report_style.append(('SPAN',(0,count_rows),(2,count_rows)))
    report_style.append(('SPAN',(3,count_rows),(-1,count_rows)))
    count_rows = count_rows + 1    
    
    # Subtitulo Firma
    signature = Paragraph("FIRMA", estilo['DefaultBoldColor'])
    report_data.append([signature, '', '', signature, '', ''])
    report_style.append(('SPAN',(0,count_rows),(2,count_rows)))
    report_style.append(('SPAN',(3,count_rows),(-1,count_rows)))
    count_rows = count_rows + 1       


    # Subtitulo de la funcion de desempenian los evaluadores
    top_boss = Paragraph("Evaluador o Jefe inmediato", estilo['DefaultNormalColor'])
    inst_unit_boss = Paragraph("Jefe de la unidad", estilo['DefaultNormalColor'])
    report_data.append([top_boss, '', '', inst_unit_boss, '', ''])
    report_style.append(('SPAN',(0,count_rows),(2,count_rows)))
    report_style.append(('SPAN',(3,count_rows),(-1,count_rows)))
    count_rows = count_rows + 1       

    # Linea final
    report_data.append(['', '', '', '', '', ''])
    report_style.append(('BACKGROUND',(0, count_rows),(-1,count_rows), subtitle_color))
    
    tabla = Table( data = report_data,
                colWidths=[125,125,75,75,75,75],
                style=report_style
                )
    story.append(tabla)
    story.append(Spacer(0,15))

    doc.build(story)
    response.write(buff.getvalue())
    buff.close()
    return response

def style_paragraph_color(value):
    return Paragraph(str(value), 
        ParagraphStyle(
            name="DefaultNormalColor", 
            alignment=TA_LEFT, 
            fontName='Times', 
            fontSize=8, 
            textColor=colors.Color(0.000, 0.000, 0.545)
            )
        )

def style_paragraph_result(value):
    return Paragraph(str(value), 
        ParagraphStyle(
            name="Result", 
            alignment=TA_LEFT, 
            fontName='Times-Bold', 
            fontSize=8, 
            textColor=colors.Color(0.502, 0.000, 0.000)
            )
        )

def style_paragraph_result_final(value):
    return Paragraph(str(value), 
        ParagraphStyle(
            name="Result", 
            alignment=TA_CENTER, 
            fontName='Times-Bold', 
            fontSize=8, 
            textColor=colors.Color(0.502, 0.000, 0.000)
            )
        )

def path_relative_to_file(relative_path):
    print('>>> intro path_relative_to_file')
    project_path = os.path.abspath(os.path.dirname(__name__))
    return (project_path + relative_path)

def simple_upload(request):
    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']
        fs = FileSystemStorage()
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
        return render(request, 'personal_evaluation/document_form_simple.html', {
            'uploaded_file_url': uploaded_file_url
        })
    return render(request, 'personal_evaluation/document_form_simple.html')

def model_form_upload(request, pe_pk):
    if request.method == 'POST':
        form = ReportForm(request.POST, request.FILES)
        # form = DocumentForm(data = request.POST)
        if form.is_valid():
            report = form.save(commit=False)
            report.personal_evaluation = PersonalEvaluation.objects.get(id=pe_pk) 
            report.save()
            # form.save()
            # return redirect('home')
            return redirect('evaluation:personal_evaluation_all')
        else:
            context = RequestContext(request, {'form': form})
            return render_to_response('personal_evaluation/document_form.html', context)
    else:
        form = ReportForm()
    return render(request, 'personal_evaluation/document_form.html', {'form': form})

def model_form_upload_edit(request, pe_pk, doc_pk):
    report = get_object_or_404(Report, pk=doc_pk)
    if request.method == 'POST':
        # form = DocumentForm(instance=document, request.POST, request.FILES)
        form = ReportForm(request.POST or None, request.FILES or None, instance=report)
        # form = DocumentForm(instance=document, data = request.POST)
        if form.is_valid():
            report = form.save(commit=False)
            report.personal_evaluation = PersonalEvaluation.objects.get(id=pe_pk) 
            report.save()
            # form.save()
            # return redirect('home')
            return redirect('evaluation:personal_evaluation_all')
        else:
            context = RequestContext(request, {'form': form})
            return render_to_response('personal_evaluation/document_form.html', context)
    else:
        form = ReportForm(instance=report)
    return render(request, 'personal_evaluation/document_form.html', {'form': form})


def insert_items_catalog_AREAESTUDIO(request):
    print('intro >>> insert_items_catalog_AREAESTUDIO')
    if request.method == 'POST':
        print('>>> INTRO POST')
        list_items = {
            'ABASTECEDOR DE MONTEROS',
            'ACTIVIDADES ARTISTICAS',
            'ADMINISTRACION',
            'ADMINISTRACION DE EMPRESAS',
            'ADMINISTRACION PUBLICA',
            'AFINAMIENTO DE MOTORES',
            'AGRICULTURA',
            'AGROFORESTERIA',
            'AGRONOMIA',
            'AGROPECUARIA',
            'ALIMENTOS',
            'AMBIENTE',
            'ANTROPOLOGIA',
            'APICULTURA',
            'AREA JURIDICA',
            'ARQUITECTURA',
            'ARTE',
            'ASFALTO',
            'ASISTENTE',
            'ATENCION AL CLIENTE',
            'AUDIOVISUALES',
            'AUDITORIA',
            'AUTOCAD',
            'AUTOESTIMA',
            'AUTOMOTRIZ',
            'AUTOSUPERACION',
            'AUXILIAR DE ENFERMERIA',
            'AVALUOS Y CATASTROS',
            'BELLAS ARTES',
            'BIBLIOTECOLOGIA',
            'BIOQUIMICA',
            'BIOQUIMICA CLINICA',
            'BOMBEROS',
            'BOMBEROTECNIA',
            'CALIDAD',
            'CAMBIO DE COMPORTAMIENTO',
            'CAPACITACION',
            'CARNICOS',
            'CARPINTERIA',
            'CARRERA DOCENTE',
            'CATASTROS',
            'CHOFER',
            'CIENCIA Y TECNOLOGIA',
            'CIENCIAS DE LA EDUCACION',
            'CIENCIAS HUMANISTICAS',
            'CIENCIAS SOCIALES',
            'CINE Y TELEVISION',
            'CIRUGIA',
            'CLARINETE SOLFEO',
            'COMERCIALIZACION',
            'COMERCIO',
            'COMPRAS PUBLICAS',
            'COMPUTACION',
            'COMUNICACION',
            'COMUNICACION SOCIAL',
            'CONCILIACION TRIBUTARIA',
            'CONDUCCION',
            'CONFECCION DE TITERES',
            'CONTABILIDAD',
            'CONTABILIDAD BASICA Y COSTOS',
            'CONTRATACION PUBLICA',
            'CONTROL',
            'CONTROL DE RABIA',
            'CONTROL GUBERNAMENTAL MODERNO',
            'CONTROL INTERNO',
            'COOPERATIVISMO',
            'CULTURA FISICA',
            'CURRICULAR',
            'DANZA',
            'DEFENSA PERSONAL',
            'DEPORTES',
            'DERECHO',
            'DERECHO ADMINISTRATIVO',
            'DERECHO AMBIENTAL',
            'DERECHO CONSTITUCIONAL',
            'DERECHO LABORAL',
            'DERECHO PENAL',
            'DERECHO PENAL Y CRIMINOLOGIA',
            'DERECHO PROCESAL',
            'DERECHO TRIBUTARIO',
            'DERECHOS HUMANOS',
            'DESARROLLO ARTESANAL',
            'DESARROLLO HUMANO',
            'DESARROLLO ORGANIZACIONAL',
            'DESARROLLO PERSONAL',
            'DIDACTICA',
            'DIGITADOR',
            'DISEÑO',
            'DISEÑO GRAFICO',
            'DOCENCIA',
            'DOCENCIA PRIMARIA',
            'DOS WORD QPRO',
            'E-GOV',
            'ECONOMIA',
            'EDUCACION',
            'EDUCACION BASICA',
            'EDUCACION ESPECIAL',
            'EDUCACION FISICA',
            'EDUCACION PREESCOLAR',
            'EDUCACION RURAL',
            'ELECTRICIDAD',
            'ELECTROMECANICA',
            'ELECTRONICA',
            'EMERGENCIAS',
            'ENERGIA',
            'ENFERMERIA',
            'ESTIMULACION TEMPRANA',
            'ESTRUCTURAS',
            'ETICA',
            'EXCEL',
            'FACULTAD DE ARQUITECTURA',
            'FACULTAD DE CIENCIAS',
            'FARMACEUTICA Y BIOQUIMICA',
            'FAUNA SILVESTRE',
            'FIEBRE AFTOSA',
            'FINANZAS',
            'FISICO MATEMATICO',
            'FORESTAL',
            'FORMACION',
            'FOTOGRAFIA',
            'FUERZA TERRESTRE',
            'FUERZAS ARMADAS',
            'FUNCIONARIOS PUBLICOS',
            'FUSILERO',
            'FUTBOL',
            'GASTRONOMIA Y TURISMO',
            'GENETICA',
            'GERENCIA',
            'GESTION PUBLICA',
            'GINECOLOGIA',
            'GRADUADOR DE ARTILLERIA',
            'HIDRAULICA',
            'HORMIGON',
            'HUMANIDADES',
            'IDIOMAS',
            'ILUMINACION',
            'IMPORTACION Y EXPORTACION',
            'INDUSTRIAS',
            'INFANTERIA',
            'INFORMATICA',
            'INGENERIA CIVIL',
            'INGENIERA COMERCIAL',
            'INGENIERIA',
            'INGENIERIA CIVIL',
            'INGENIERIA COMERCIAL',
            'INGENIERIA ELECTROMECANICA',
            'INGENIERIA EN ALIMENTOS',
            'INGENIERIA EN SISTEMAS',
            'INGENIERIA QUIMICA',
            'INGLES',
            'INTELIGENCIA MILITAR',
            'INTERNET',
            'JUECES',
            'LA JUVENTUD PIENSA',
            'LABORATORIOS',
            'LEGISLACION',
            'LENGUA Y LITERATURA',
            'LEYES',
            'LIDERAZGO',
            'LIMPIEZA',
            'LITERATURA',
            'LOCUCION PARA RADIO Y TV',
            'MANEJO AGROECOLOGICO DE CAFE',
            'MANEJO DE PLAGAS DE CAFE',
            'MANUALIDADES',
            'MANUFACTURA ALIMENTARIA',
            'MARINERO',
            'MARKETING',
            'MATERNO INFANTIL',
            'MECANICA',
            'MEDICINA',
            'MEDIO AMBIENTE',
            'MERCADOTECCNIA',
            'METODOLOGIA',
            'MIGRACION',
            'MILITAR',
            'MINADOR',
            'MOTIVACION',
            'MOTIVACION Y COMUNICACION',
            'MOTONIVELADOR',
            'MUSICA',
            'NUTRICION',
            'OBRAS VIALES',
            'ODONTOLOGIA',
            'ONCOLOGIA',
            'OPERADOR DE CARGA FRONTAL',
            'ORATORIA',
            'ORDENAMIENTO TERRITORIAL',
            'ORGANIZACION Y PRACTICAS',
            'ORIENTACION FAMILIAR',
            'PARVULARIA',
            'PATRIMONIO CULTURAL',
            'PAVIMENTOS',
            'PEDAGOGIA',
            'PEDIATRIA',
            'PERIODISMO',
            'PERIODONCIA',
            'PERSONAL',
            'PINTURA DE SUSPERFICIES',
            'PLANIFICACION',
            'PLANIFICACION CURRICULAR',
            'PLANIFICACION ESTRATEGICA',
            'PLANIFICACION Y CREATIVIDAD',
            'POLICIA',
            'POLICIA MILITAR',
            'POLITICA',
            'POLITICAS PUBLICAS',
            'PRESENTADOR DE ARTILLERIA',
            'PREVENCION DE RIESGOS',
            'PRIMEROS AUXILIOS',
            'PRINCIPIOS FUNDAMENTALES',
            'PROCESAMIENTO DE ALIMENTOS',
            'PRODUCCION CINEMATOGRAFICA',
            'PRODUCCION DE CAFE ORGANICO',
            'PRODUCCION RADIAL',
            'PRODUCCION RADIO TELEVISION',
            'PROFESORA',
            'PROGRAMACION',
            'PROMOSION SOCIAL',
            'PROTOCOLO',
            'PROYECTOS',
            'PSICOLOGIA',
            'PSICOLOGIA CLINICA',
            'PSICOLOGIA INFANTIL',
            'PSICORREHABILITACION',
            'PSICOTERAPIA',
            'PSIQUIATRIA',
            'RECURSOS HUMANOS',
            'REDACCION',
            'REDACION Y ORTOGRAFIA',
            'REDES CISCO',
            'REFORMAS CODIGO DE TRABAJO',
            'RELACIONES HUMANAS',
            'RELACIONES INTERNACIONALES',
            'RENTAS INTERNAS',
            'RESCATE',
            'RESTAURACION',
            'RIESGOS LABORALES',
            'RIESGOS SECTOR ELECTRICO',
            'SALUD',
            'SANITARIA',
            'SECRETARIA EJCUTIVA',
            'SECRETARIADO',
            'SECRETARIADO BILINGUE',
            'SECRETARIADO GERENCIAL',
            'SECTOR RURAL',
            'SEGURIDAD',
            'SEGURIDAD INDUSTRIAL',
            'SEGURIDAD PERSONAL',
            'SEGURIDAD SOCIAL',
            'SEGURIDAD Y SALUD',
            'SEMAFOROS',
            'SERVICIO AL CLIENTE',
            'SERVICIO SOCIAL',
            'SERVICIOS PUBLICOS',
            'SIG',
            'SIN AREA DE ESTUDIO',
            'SLDOS. A CBOS',
            'SOCIALES',
            'SOLDADURA',
            'SRI',
            'SUDAMERICANO',
            'TALENTO HUMANO',
            'TECNICA FISICA',
            'TECNOLOGIA EN MINAS',
            'TELEVISION',
            'TENDEDOR DE LINEAS ALAMBRICAS',
            'TOPOGRAFIA',
            'TRABAJO SOCIAL',
            'TRANSITO',
            'TRANSITO Y TRANSPORTE',
            'TRANSPORTE',
            'TRIBUTACION',
            'TRIPLE C',
            'TURISMO',
            'VENTAS',
            'VETERINARIA',
            'WINDOWS Y WORD'
        }

        catalog = Catalog.objects.get(code='AREAESTUDIO')
        for a in list_items:
            item = Item(name=a, description=None, catalog=catalog, parent_item=None)
            item.save()
            print(a)
        response_data = {}
        response_data['msg'] = 'Ok'
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    else:
        # return render(request, 'insert_data.html', {})
        template = loader.get_template('insert_data.html')
        context = {}
        return HttpResponse(template.render(context, request))

   
