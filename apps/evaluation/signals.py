from django.db.models.signals import pre_save, pre_delete, post_save, post_delete
from django.dispatch import receiver
from django.conf import settings
from .models import Report
import os.path

@receiver(post_save, sender=Report)
def model_post_save(sender, **kwargs):
    # instance = kwargs['instance']
    print('Saved: {}'.format(kwargs['instance'].__dict__))
    print(kwargs['created'])
    # if instance.id:
    #     old_instance = Document.objects.get(pk=instance.id)
    #     print(old_instance.document)