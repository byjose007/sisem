from django import forms
from django.forms import ModelForm
from .models import (PerformanceEvaluation, InstitutionalUnitEvaluation, CatalogTechnicalCompetence, CatalogUniversalCompetence,
    CatalogTeamwork, PersonalEvaluation, Report, Activity, Knowledge, TechnicalCompetence, UniversalCompetence,
    Teamwork, Complaint, ChiefObservation)
from apps.settings.models import Item
from apps.employee.models import Employee
from django.core.exceptions import NON_FIELD_ERRORS

BIRTH_YEAR_CHOICES = ('1980', '1981', '1982')
FAVORITE_COLORS_CHOICES = (
    ('blue', 'Blue'),
    ('green', 'Green'),
    ('black', 'Black'),
)

RELEVANCE_LEVEL_CHOICES = (
        ('','---------'),
        ('HIGH', 'Alta'),
        ('HALF', 'Media'),
        ('SHORT', 'Baja'),
    )

class PerformanceEvaluationForm(ModelForm):
    
    # institution = forms.ModelChoiceField(label="Institución",
    #                     queryset=Institution.objects.all(),
    #                     widget=forms.HiddenInput)

    start_date = forms.DateField(label='Fecha de inicio', required=True,  
        widget=forms.DateTimeInput(format='%d/%m/%Y', attrs={
            'placeholder':'Seleccione una fecha'
        })
    )
    end_date = forms.DateField(label='Fecha de fin', required=True,  
        widget=forms.DateTimeInput(format='%d/%m/%Y', attrs={
            'placeholder':'Seleccione una fecha'
        })
    )
    # end_date = forms.DateField(
    #     label='Fecha de fin',
    #     widget=forms.DateInput(format = '%d/%m/%Y',
    #     attrs={'placeholder':'Seleccione una fecha'}),
    #     input_formats=('%d/%m/%Y'),
    #     required=False
    # )

    class Meta:
        model = PerformanceEvaluation
        fields = '__all__'
        exclude=[]
        error_messages = {
            NON_FIELD_ERRORS: {
                'unique_together': "Ya existe una evaluación registrada para el año seleccionado.",
                # 'unique_together': "%(model_name)s's %(field_labels)s are not unique.",
            }
        }

    # def __init__(self, *args, **kwargs):
    #     # print('intro __init__ -> institution: %s' % (kwargs.pop('institution')))
    #     self.institution = kwargs.pop('institution')
    #     super(PerformanceEvaluationForm, self).__init__(*args, **kwargs)
    #     #self.fields['end_date'].widget = forms.DateInput(format=('%d/%m/%Y'), attrs={'placeholder':'Select a date'})
    
    # def clean(self):
    #     print('intro >>> clean')
    #     cleaned_data = self.cleaned_data
    #     print('>>>>> year: %s, insitution: %s' % (cleaned_data['year'], self.institution ))

    #     if PerformanceEvaluation.objects.filter(year=cleaned_data['year'], institution=self.institution).exists():
    #         print('>>>>> EXIST PERFORMANCE EVALUATION')
    #         # self.add_error('year', 'Ya se encuentra registrada una Evaluación de desempeño para el año seleccionado!')
    #         raise forms.ValidationError('Ya se encuentra registrada una evaluación de desempeño con el año seleccionado!')

    #     # Always return cleaned_data
    #     return cleaned_data


class InstitutionalUnitEvaluationForm(forms.ModelForm):
    class Meta:
        model = InstitutionalUnitEvaluation
        fields = '__all__'
        #exclude=['performance_evaluation']

class PersonalEvaluationForm(ModelForm):
    field_order = ['evaluated', 'immediate_superior', 'institutional_unit_director', 'period_from', 'period_to', 'state', 
        'job_management_indicators', 'knowledge', 'technical_competence', 'universal_competence', 'teamwork', 'evaluation_citizens',
        'final_evaluation_percentage', 'final_evalution_result']
    class Meta:
        model = PersonalEvaluation
        fields = '__all__'
        exclude=['institutional_unit_evaluation', 'contract', 'start_date', 'end_date']

    def __init__(self, *args, **kwargs):
        inst_unit_id = kwargs.pop('inst_unit_id', None)
        super(PersonalEvaluationForm, self).__init__(*args, **kwargs)
        self.fields['immediate_superior'].queryset = Employee.objects.filter(institutional_unit__pk=inst_unit_id)

class PersonalAuxEvaluationForm(ModelForm):
    field_order = ['evaluated', 'immediate_superior', 'institutional_unit_director', 'period_from', 'period_to', 'state', 
        'job_management_indicators', 'knowledge', 'technical_competence', 'universal_competence', 'teamwork', 'evaluation_citizens',
        'final_evaluation_percentage', 'final_evalution_result']
    class Meta:
        model = PersonalEvaluation
        fields = '__all__'
        exclude=['institutional_unit_evaluation', 'start_date', 'end_date']

    # def __init__(self, inst_unit, *args, **kwargs):
    #     inst_unit_id = kwargs.pop('inst_unit_id', None)
    #     super(PersonalEvaluationForm, self).__init__(*args, **kwargs)
    #     self.fields['immediate_superior'].queryset = Employee.objects.filter(institutional_unit__pk=inst_unit_id)

class ReportForm(forms.ModelForm):
    class Meta:
        model = Report
        fields = ('description', 'document', )

    # def save(self, *args, **kwargs):
    #     # instance = super(DocumentForm, self).save(commit=False)
    #     # instance.course = self.course
    #     # instance.user = self.user
    #     # if commit:
    #     #     instance.save()
    #     # return instance
    #     super(MyModel, self).save(*args, **kw)

class CatalogTechnicalCompetenceForm(ModelForm):
    class Meta:
        model = CatalogTechnicalCompetence
        fields = '__all__'
        exclude=['performance_evaluation']

class CatalogUniversalCompetenceForm(ModelForm):
    class Meta:
        model = CatalogUniversalCompetence
        fields = '__all__'
        exclude=['performance_evaluation']

class CatalogTeamworkForm(ModelForm):
    class Meta:
        model = CatalogTeamwork
        fields = '__all__'
        exclude = ['performance_evaluation']

class InstitutionalUnitEvaluationForm(ModelForm):
    class Meta:
        model = InstitutionalUnitEvaluation
        fields = '__all__'
        exclude = ['performance_evaluation']

class ActivityForm(ModelForm):
    class Meta:
        model = Activity
        fields = '__all__' # Or a list of the fields that you want to include in your form
        exclude = ['activity_evaluation']

    #def clean_apply_percentage(self):
    #    data = self.cleaned_data['apply_percentage']
    #    if len(data) < 2:
    #        raise forms.ValidationError("El campo aplica porcentage debe contener 2 caracteres")
    #    return data

class KnowledgeForm(ModelForm):
    class Meta:
        model = Knowledge
        fields = '__all__'
        exclude = ['knowledge_evaluation']

class TechnicalCompetenceForm(ModelForm):
    field_order = ['skill', 'relevance', 'observable_behavior', 'frequency_application', 'technical_competence_evaluation']
    skill = forms.ModelChoiceField(
        queryset=Item.objects.none(),
        label="Destrezas"
        )
    relevance = forms.ChoiceField(
        choices = RELEVANCE_LEVEL_CHOICES,
        label = "Relevancia",
        initial="",
        widget=forms.Select(),
        required=True
        )
    observable_behavior = forms.CharField(
        widget=forms.Textarea, label="Comportamiento observable"
        )
    catalog_technical_competence = forms.ModelChoiceField(
        queryset=CatalogTechnicalCompetence.objects.none(),
        label="Catálogo de competencias técnicas"
        )
    class Meta:
        model = TechnicalCompetence
        fields = '__all__'
        exclude = ['technical_competence_evaluation']
        error_messages = {
            NON_FIELD_ERRORS: {
                'unique_together': "Ya existe una evaluación registrada para el año seleccionado.",
                # 'unique_together': "%(model_name)s's %(field_labels)s are not unique.",
            }
        }

    def __init__(self, perform_eval, list_tc, *args, **kwargs):
        super(TechnicalCompetenceForm, self).__init__(*args, **kwargs)
        #self.fields['skill'].queryset = Item.objects.filter(catalog__code=catalog_code)
        self.fields['catalog_technical_competence'].queryset = CatalogTechnicalCompetence.objects.filter(
            performance_evaluation__pk=perform_eval)
        self.fields['skill'].queryset = Item.objects.filter(pk__in=list_tc)


class UniversalCompetenceForm(ModelForm):
    # skill = forms.IntegerField(
    #     label='Opción destreza',
    #     widget=forms.Select(
    #         choices=Item.objects.all().values_list('id', 'name')
    #     )
    #  )
    field_order = ['skill', 'relevance', 'observable_behavior', 'frequency_application', 'catalog_universal_competence']
    skill = forms.ModelChoiceField(
        queryset=Item.objects.none(), 
        label="Destrezas"
        )
    relevance = forms.ChoiceField(
        choices = RELEVANCE_LEVEL_CHOICES, 
        label="Relevancia", 
        initial='', 
        widget=forms.Select(), 
        required=True
        )
    observable_behavior = forms.CharField(
        widget=forms.Textarea,
        label="Comportamiento observable"
        )
    catalog_universal_competence = forms.ModelChoiceField(
        queryset=CatalogUniversalCompetence.objects.none(),
        label="Catálogo de competencias universales"
        )
    class Meta:
        model = UniversalCompetence
        fields = '__all__'
        exclude = ['universal_competence_evaluation']
    def __init__(self, perform_eval, list_uc, *args, **kwargs):
        super(UniversalCompetenceForm, self).__init__(*args, **kwargs)
        self.fields['catalog_universal_competence'].queryset = CatalogUniversalCompetence.objects.filter(
            performance_evaluation__pk=perform_eval)
        self.fields['skill'].queryset = Item.objects.filter(pk__in=list_uc)

class TeamworkForm(ModelForm):
    field_order = ['ability', 'relevance', 'observable_behavior', 'frequency_application', 'catalog_teamwork']
    ability = forms.ModelChoiceField(
        queryset=Item.objects.none(), 
        label="Habilidad")
    relevance = forms.ChoiceField(
        choices=RELEVANCE_LEVEL_CHOICES,
        label="Relevancia",
        initial='',
        widget=forms.Select(),
        required=True
        )
    observable_behavior=forms.CharField(
        widget=forms.Textarea,
        label='Comportamiento observalbe'
        )
    catalog_teamwork = forms.ModelChoiceField(
        queryset=CatalogTeamwork.objects.none(),
        label='Catálogo de trabajo en equipo'
        )
    class Meta:
        model = Teamwork
        fields = '__all__'
        exclude = ['teamwork_evaluation']
    def __init__(self, perform_eval, list_tw, *args, **kwargs):
        super(TeamworkForm, self).__init__(*args, **kwargs)
        self.fields['catalog_teamwork'].queryset = CatalogTeamwork.objects.filter(
            performance_evaluation__pk=perform_eval)
        self.fields['ability'].queryset = Item.objects.filter(pk__in=list_tw)

class ChiefObservationForm(ModelForm):
    class Meta:
        model = ChiefObservation
        fields = '__all__'
        exclude = ['personal_evaluation']

class ComplaintForm(ModelForm):
    class Meta:
        model = Complaint
        fields = '__all__'
        exclude = ['complaint_evaluation']
