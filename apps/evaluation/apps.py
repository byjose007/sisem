from django.apps import AppConfig


class EvaluationConfig(AppConfig):
    name = 'apps.evaluation'
    verbose_name = 'EVALUACIÓN'

    def ready(self):
    	import apps.evaluation.signals