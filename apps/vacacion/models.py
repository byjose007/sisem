from django.db import models

from apps.employee.models import Employee
from apps.settings.models import Item


class Vacacion(models.Model):
    empleado = models.ForeignKey(Employee, verbose_name='Empleado', on_delete=models.PROTECT)
    motivo = models.TextField(max_length=100, verbose_name='Motivo')
    fecha_desde = models.DateField(verbose_name='Fecha desde', blank=True, null=True)
    fecha_hasta = models.DateField(verbose_name='Fecha hasta', blank=True, null=True)
    hora_inicio = models.TimeField(verbose_name='Hora de inicio')
    hora_fin = models.TimeField(verbose_name='Hora de fin')
    n_horas = models.IntegerField(blank=True, null=True, verbose_name='Nº Horas')
    registro_por = models.CharField(verbose_name='Registrado por', max_length=100)
    tipo_accion = models.ForeignKey(Item, on_delete=models.PROTECT, limit_choices_to={'catalog__code': 'VACACIONES'}, max_length=10,
                            verbose_name='Tipo de Acción', blank=True, null=True)

    class Meta:
        # ordering = ['empleado']
        verbose_name = 'Vacación'
        verbose_name_plural = 'Vacaciones'

    def __str__(self):
        return u'{0}'.format(self.empleado)

    def __unicode__(self):
        return u'{0}'.format(self.empleado)
