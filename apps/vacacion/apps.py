from django.apps import AppConfig


class VacacionConfig(AppConfig):
    name = 'apps.vacacion'
