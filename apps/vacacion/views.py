import datetime

from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from django.urls import reverse, reverse_lazy
from django.views.decorators.csrf import csrf_exempt

from apps.employee.models import Employee
from apps.vacacion.forms import VacacionForm
from apps.vacacion.models import Vacacion


def vacacion_list(request, template_name='vacacion/vacacion_list.html'):
    vacacion = Vacacion.objects.all().order_by('-id')
    data = {}
    data['object_list'] = vacacion
    return render(request, template_name, data)


def vacacion_create(request, cedula, template_name='vacacion/vacacion_form.html'):
    usuario_actual = request.user.first_name +' '+ request.user.last_name + ' - '+request.user.username
    form = VacacionForm(request.POST or None, initial={"registro_por": usuario_actual}, cedula=cedula)
    if form.is_valid():
        form.save()
        return redirect('vacacion:vacacion_list')
    return render(request, template_name, {'form': form, 'create':True})


def vacacion_update(request, pk, template_name='vacacion/vacacion_form.html'):
    vacacion = get_object_or_404(Vacacion, pk=pk)
    cedula = vacacion.empleado.person.cedula
    form = VacacionForm(request.POST or None, instance=vacacion,cedula=cedula)


    if form.is_valid():
        form.save()
        return redirect('vacacion:vacacion_list')
    return render(request, template_name, {'form': form})


#No borrar
# def buscarEmpleado(request, cedula):
#     empleado = Employee.objects.get(person__cedula=cedula)
#     url = reverse_lazy('vacacion:vacacion_new',kwargs={'cedula': empleado.person.cedula})
#     return redirect(url)
