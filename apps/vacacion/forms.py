from django import forms
from django.conf import settings
from django.forms import ModelForm, TextInput, ModelChoiceField
# from bootstrap_datepicker.widgets import DatePicker

from apps.employee.models import Employee
from apps.vacacion.models import Vacacion


class VacacionForm(ModelForm):
    class Meta:
        model = Vacacion
        fields = '__all__'
        exclude = ('id',)

    def __init__(self, *args, **kwargs):

        # print(kwargs['cedula'])

        cedula = kwargs.pop('cedula')
        empleado_actual = Employee.objects.filter(person__cedula=cedula)

        super(VacacionForm, self).__init__(*args, **kwargs)

        self.fields['fecha_desde'].widget = TextInput(attrs={'type': 'date'})
        self.fields['fecha_hasta'].widget = TextInput(attrs={'type': 'date'})
        self.fields['hora_inicio'].widget = TextInput(attrs={'type': 'time'})
        self.fields['hora_fin'].widget = TextInput(attrs={'type': 'time'})
        self.fields['registro_por'].widget = TextInput(attrs={'readonly': 'readonly'})
        self.fields['empleado'].queryset= empleado_actual
        if(empleado_actual):
            self.fields['empleado'].initial = empleado_actual[0].pk





        # self.fields['registro_por'].initial = "harvard"

        # if(kwargs):
        #     self.fields['fecha_hasta'].widget = TextInput(attrs={
        #         'id': '',
        #     })