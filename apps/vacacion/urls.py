from django.conf.urls import url

from .views import *

urlpatterns = [
    url(r'^$', vacacion_list, name='vacacion_list'),
    url(r'^new/(?P<cedula>\d+)$', vacacion_create, name='vacacion_new'),
    url(r'^editar/(?P<pk>\d+)$', vacacion_update, name='vacacion_edit'),

]
