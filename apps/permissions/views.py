# -*- coding: utf-8 -*-
from django.shortcuts import render
import json
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
	CreateView,
	UpdateView,
	DeleteView
)
from apps.permissions.models import TypeOfPermit, Permission
from .forms import TypeOfPermitForm, PermissionForm
from django.http import HttpResponse
from django.views.generic import FormView, TemplateView
from django.contrib.messages.views import SuccessMessageMixin
from apps.accounts.models import UserProfile
# Create your views here.


#Tipos de permiso

def type_of_permit_list(request, template_name='permissions/typeofpermit_list.html'):
    user_profile = UserProfile.objects.get(user=request.user)
    type_of_permit = TypeOfPermit.objects.all()
    data = {}
    data['object_list'] = type_of_permit
    return render(request, template_name, data)


def type_of_permit_create(request, template_name='permissions/typeofpermit_form.html'):
    
    form = TypeOfPermitForm(request.POST or None)  
    if form.is_valid():
        type_of_permit = form.save()
        return redirect('permissions:type_of_permit_list')
    return render(request, template_name, {'form':form})


def type_of_permit_update(request, pk, template_name='permissions/typeofpermit_form.html'):
    type_of_permit = get_object_or_404(TypeOfPermit, pk=pk)
    form = TypeOfPermitForm(request.POST or None, instance=type_of_permit)
    if form.is_valid():
        form.save()
        return redirect('permissions:type_of_permit_list')
    return render(request, template_name, {'form':form})

def type_of_permit_delete(request, pk):
    type_of_permit = get_object_or_404(TypeOfPermit, pk=pk)    
    type_of_permit.delete()
    return redirect('permissions:type_of_permit_list')
    # return render(request, template_name, {'object':type_of_permit})


#Permisos

def permission_list_user(request, template_name='permissions/permission_list.html'):
    user_profile = UserProfile.objects.get(user=request.user)
    permission = Permission.objects.all().order_by('-id')
    data = {}
    data['object_list'] = permission
    return render(request, template_name, data)


def permission_user_create(request,cedula, template_name='permissions/permission_form.html'):
    usuario_actual = request.user.first_name + ' ' + request.user.last_name + ' - ' + request.user.username
    form = PermissionForm(request.POST or None,initial={"registered_by": usuario_actual}, cedula=cedula)
    if form.is_valid():
        permission = form.save()
        return redirect('permissions:permission_list')
    return render(request, template_name, {'form':form, 'create': True})


def permission_user_update(request, pk, template_name='permissions/permission_form.html'):
    permission = get_object_or_404(Permission, pk=pk)
    cedula = permission.employee.person.cedula
    form = PermissionForm(request.POST or None, instance=permission, cedula=cedula)
    if form.is_valid():
        form.save()
        return redirect('permissions:permission_list')
    return render(request, template_name, {'form':form})

def permission_user_delete(request, pk):
    permission = get_object_or_404(Permission, pk=pk)    
    permission.delete()
    return redirect('permissions:permission_list')
    # return render(request, template_name)
