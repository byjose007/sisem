
from django.contrib import admin
from apps.permissions.models import TypeOfPermit, Permission



# Register your models here.

class TypeOfPermitAdmin(admin.ModelAdmin):
	list_display = ('type_of_permit','status')
	search_fields = ('type_of_permit','status')

class PermissionAdmin(admin.ModelAdmin):
	list_display = ('employee','type_of_permit')
	search_fields = ('employee','employee','registration_date','date_permiso')


	


admin.site.register(TypeOfPermit, TypeOfPermitAdmin)
admin.site.register(Permission, PermissionAdmin)
