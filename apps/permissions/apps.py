from django.apps import AppConfig


class PermissionsConfig(AppConfig):
    name = 'apps.permissions'
