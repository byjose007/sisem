# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from .views import (
    type_of_permit_list,
    type_of_permit_create,
    type_of_permit_update,
    type_of_permit_delete,

    permission_list_user,
    permission_user_create,
    permission_user_update,
    permission_user_delete,
)


urlpatterns = [

    #urls admin
    url(r'^admin/$', type_of_permit_list, name='type_of_permit_list'),
    url(r'^admin/new$', type_of_permit_create, name='type_of_permit_new'),
    url(r'^admin/editar/(?P<pk>\d+)$', type_of_permit_update, name='type_of_permit_edit'),      
    url(r'^admin/borrar/(?P<pk>\d+)$', type_of_permit_delete, name='type_of_permit_delete'),


    url(r'^$', permission_list_user, name='permission_list'),
    url(r'^new/(?P<cedula>\d+)$', permission_user_create, name='permission_new'),
    url(r'^editar/(?P<pk>\d+)$', permission_user_update, name='permission_edit'),      
    url(r'^borrar/(?P<pk>\d+)$', permission_user_delete, name='permission_delete'),
    #url(r'^admin/(?P<pk>\d+)$',institution_detail, name='institution_detail'),
    #url(r'^admin/2/(?P<pk>\d+)$',InstitutionDetail.as_view(), name='institution_detail2'),
    
    

    
]
