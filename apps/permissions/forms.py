from django import forms
from django.forms import ModelForm, TextInput

from apps.employee.models import Employee
from .models import Permission, TypeOfPermit


class TypeOfPermitForm(ModelForm):
    class Meta:
        model = TypeOfPermit
        fields = '__all__'
        exclude = ('id',)


class PermissionForm(ModelForm):
    class Meta:
        model = Permission
        fields = '__all__'
        exclude = ('id',)


    def __init__(self, *args, **kwargs):
        # print(kwargs['cedula'])
        cedula = kwargs.pop('cedula')
        empleado_actual = Employee.objects.filter(person__cedula=cedula)
        super(PermissionForm, self).__init__(*args, **kwargs)
        self.fields['registration_date'].widget = TextInput(attrs={'type': 'date'})
        self.fields['date_permiso'].widget = TextInput(attrs={'type': 'date'})
        self.fields['start_time'].widget = TextInput(attrs={'type': 'time'})
        self.fields['end_time'].widget = TextInput(attrs={'type': 'time'})
        self.fields['registered_by'].widget = TextInput(attrs={'readonly': 'readonly'})
        self.fields['employee'].queryset= empleado_actual
        if(empleado_actual):
            self.fields['employee'].initial = empleado_actual[0].pk
