from django.db import models
from apps.employee.models import Employee


# Create your models here.
class TypeOfPermit(models.Model):
    type_of_permit = models.CharField(verbose_name='Tipo de permiso', max_length=100)
    status = models.BooleanField(verbose_name='Estado', default=True)

    class Meta:
        ordering = ['type_of_permit']
        verbose_name = 'Tipo de permiso'
        verbose_name_plural = 'Tipos de permiso'

    def __str__(self):
        return u'{0}'.format(self.type_of_permit)

    def __unicode__(self):
        return u'{0}'.format(self.type_of_permit)


class Permission(models.Model):
    descuento_rol = 'descuento a rol'
    cargo_vacaciones = 'cargo a vacaciones'
    ninguna = 'ninguna'
    action_list = (
        (descuento_rol, 'Descuento a rol'),
        (cargo_vacaciones, 'Cargo a vacaciones'),
        (ninguna, 'Ninguna'),
    )

    employee = models.ForeignKey(Employee, verbose_name='Empleado', on_delete=models.PROTECT)
    type_of_permit = models.ForeignKey('TypeOfPermit', verbose_name='Tipo de Permiso', on_delete=models.PROTECT)
    action = models.CharField(verbose_name='Acción a ejecutar', choices=action_list, default=cargo_vacaciones,
                              max_length=50)
    registration_date = models.DateField(verbose_name='Fecha de registro')
    date_permiso = models.DateField(verbose_name='Fecha de permiso')
    start_time = models.TimeField(verbose_name='Hora de inicio')
    end_time = models.TimeField(verbose_name='Hora de fin')
    reason = models.TextField(verbose_name='Motivo')
    registered_by = models.CharField(verbose_name='Registrado por', max_length=100)

    class Meta:
        # ordering = ['employee', 'date_permiso']
        verbose_name = 'Permiso'
        verbose_name_plural = 'Permisos'

    def __str__(self):
        return u'{0}-{1}'.format(self.employee, self.type_of_permit)

    def __unicode__(self):
        return u'{0}-{1}'.format(self.employee, self.type_of_permit)
