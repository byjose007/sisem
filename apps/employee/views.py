# from dal import autocomplete
from django.db import transaction, IntegrityError, OperationalError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse_lazy, reverse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import FormView, TemplateView
from django.contrib.messages.views import SuccessMessageMixin

from apps.accounts.models import UserProfile

from apps.employee.models import Employee, DatosInstitucionales, ServiceCommission, Observations, \
    EmployeeScheduleHistory
from apps.employee.forms import EmployeeForm, InstitutionalDataForm, ServiceCommissionForm, ObservationsForm, \
    ObservationsFormUser, ObservationsFormUserEmployee, EmployeeScheduleHistoryForm, EmployeeScheduleHistoryDataForm, \
    PersonForm, InstruccionForm, OficiosForm, LenguajeForm, CapacitacionForm, ExperienciaForm, DatosInstitucionalesForm, \
    ReferenciaForm, DatosEconomicosForm, FilterInstitucionForm, cuentaBancariaForm
from django.contrib.messages.views import SuccessMessageMixin
from apps.curriculum.models import CurriculumVitae, FormalInstruction, OfficesOrsubactivities, Language, Training, \
    WorkExperience, PerformanceEvaluation, PersonalAchievement, AffirmativeAction, PersonalReferences

from apps.institution.models import Institucion, JefaturaCoordinacion, Directivo, Direccion
# from apps.institution.models import Institution, InstitutionalUnit, Department
from apps.partidas.models import Partida, EmpleadoHistorial
from apps.permissions.models import Permission
import time
from django.shortcuts import render_to_response
from apps.economicdata.models import EconomicData, BankAccounts
from apps.person.models import Person
from apps.personnel_actions.models import PersonnelActions
from apps.contract.models import Contract
from django.db.models import Q
from datetime import datetime

# buscador
from apps.settings.models import Item, Catalog

from humanresource.html_to_pdf import render_to_pdf


@csrf_exempt
def api_organigrama(request):
    # Estado del empleado

    nivel_estudios = Catalog.objects.get(code='NIVELESTUDIOS')
    niveles = Item.objects.filter(catalog_id=nivel_estudios).values('name', 'pk')
    estado_empledo = Catalog.objects.get(code='ESTADOEMPLEADO')
    estados = Item.objects.filter(catalog_id=estado_empledo).values('name', 'pk')
    genero_empleado = Catalog.objects.get(code='SEXO')
    generos = Item.objects.filter(catalog_id=genero_empleado).values('name', 'pk')
    estado_civil_cat = Catalog.objects.get(code='ESTADO CIVIL')
    estado_civil = Item.objects.filter(catalog_id=estado_civil_cat).values('name', 'pk')

    # Organigrama
    instituciones = Institucion.objects.all().values('nombre', 'pk')
    directivos = Directivo.objects.all().values('nombre', 'pk', 'institucion__id')
    direcciones = Direccion.objects.all().values('nombre', 'pk', 'directivo__id')
    jefaturas = JefaturaCoordinacion.objects.all().values('nombre', 'pk', 'direccion__id')
    organigrama = {
        'instituciones': list(instituciones),
        'directivos': list(directivos),
        'direcciones': list(direcciones),
        'jefaturas': list(jefaturas),
        'estados': list(estados),
        'niveles_esdudio': list(niveles),
        'generos': list(generos),
        'estado_civil': list(estado_civil)
    }
    return JsonResponse(organigrama, safe=False)


# Funciones admistrador
# -------Employee----------
def employee_list(request, template_name='employee/employee_list_admin.html'):
    usuario = request.user
    user_profile = UserProfile.objects.get(user=request.user)

    employee = Employee.objects.all()
    data = {}
    data['object_list'] = employee
    return render(request, template_name, data)


def employee_create(request, template_name='employee/nuevo_empleado.html'):
    if request.POST:
        formPerson = PersonForm(request.POST, request.FILES, prefix='formPerson')
        formEmpleado = EmployeeForm(request.POST or None, prefix='formEmpleado')
        formDatosInstitcionales = DatosInstitucionalesForm(request.POST or None, prefix='formDatosInstitcionales')
        formIstruccion = InstruccionForm(request.POST or None, prefix='formIstruccion')
        formOficios = OficiosForm(request.POST or None, prefix='formOficios')
        formLenguajes = LenguajeForm(request.POST or None, prefix='formLenguajes')
        formCapacitacion = CapacitacionForm(request.POST, request.FILES, prefix='formCapacitacion')
        formExperiencia = ExperienciaForm(request.POST, request.FILES, prefix='formExperiencia')
        formReferencia = ReferenciaForm(request.POST or None, prefix='formReferencia')
        formDatosEconomicos = DatosEconomicosForm(request.POST or None, prefix='formDatosEconomicos')
        formCuentaBancaria = cuentaBancariaForm(request.POST or None, prefix='formCuentaBancaria')

        if all([formEmpleado.is_valid(), formPerson.is_valid(), formDatosInstitcionales.is_valid()]):
            # Guardar Persona
            print(formPerson.cleaned_data)
            persona = formPerson.save(commit=False)
            print(request.FILES.get('photo'))
            persona.photo = request.FILES.get('photo')
            persona.save()

            if request.POST.get('institucion') and request.POST.get('direccion'):
                # Guardar Empleado
                catalogo = Catalog.objects.get(code='ESTADOEMPLEADO')
                estado_empleado = Item.objects.get(name='Empleado', catalog=catalogo)

                empleado = formEmpleado.save(commit=False)
                empleado.person = persona
                empleado.status = estado_empleado

                empleado.institution_id = int(request.POST.get('institucion'))
                empleado.directivo_id = int(request.POST.get('directivo'))
                empleado.direccion_id = int(request.POST.get('direccion'))
                if request.POST.get('jefatura'):
                    empleado.jefatura_id = int(request.POST.get('jefatura'))

                empleado.save()
                # Guardar datos institucionales
                datos_institucionales = formDatosInstitcionales.save(commit=False)
                datos_institucionales.employee = empleado
                datos_institucionales.save()

                # Guardar Curriculum
                currriculum = CurriculumVitae.objects.create(person=persona)
                num_instrucciones = int(request.POST.get('num_instrucciones'))
                if num_instrucciones > 0:
                    for i in range(num_instrucciones):
                        instruccion = formIstruccion.save(commit=False)
                        instruccion.curriculum_vitae = currriculum
                        instruccion.level_of_instruction_id = int(request.POST.get("nivel_" + str(i)))
                        instruccion.obtained_title = request.POST.get('titulo_' + str(i))
                        instruccion.educational_institution = request.POST.get('institucion_edu_' + str(i))
                        instruccion.date_of_graduation = request.POST.get('año_' + str(i))
                        instruccion.senecyt_registration_number = request.POST.get('reg_' + str(i))
                        instruccion.number_acta = request.POST.get('acta_' + str(i))
                        instruccion.number_refrendacion = request.POST.get('refrendacion_' + str(i))
                        instruccion.save()

                # Guardar Oficios
                num_oficios = int(request.POST.get('num_oficios'))
                if num_oficios > 0:
                    for i in range(num_oficios):
                        oficio = formOficios.save(commit=False)
                        oficio.curriculum_vitae = currriculum
                        oficio.job = request.POST.get('oficio_' + str(i))
                        oficio.description = request.POST.get('descripcion_' + str(i))
                        oficio.save()

                # Guardar idiomas
                num_idiomas = int(request.POST.get('num_idiomas'))
                if num_idiomas > 0:
                    for i in range(num_idiomas):
                        idioma = formLenguajes.save(commit=False)
                        idioma.curriculum_vitae = currriculum
                        idioma.languaje = request.POST.get('language' + str(i))
                        idioma.spoken_level = request.POST.get('spoken_level' + str(i))
                        idioma.written = request.POST.get('written' + str(i))
                        idioma.save()

                if formCapacitacion.is_valid():
                    files = request.FILES.getlist('formCapacitacion-documento_pdf')
                    for pdf in files:
                        Training.objects.create(curriculum_vitae=currriculum, documento_pdf=pdf)

                if formExperiencia.is_valid():
                    files = request.FILES.getlist('formExperiencia-documento_pdf')
                    for pdf in files:
                        WorkExperience.objects.create(curriculum_vitae=currriculum, documento_pdf=pdf)

                # Guardar referencias
                num_referencias = int(request.POST.get('num_referencias'))
                if num_referencias > 0:
                    for i in range(num_referencias):
                        referencia = formReferencia.save(commit=False)
                        referencia.curriculum_vitae = currriculum
                        referencia.name = request.POST.get('name' + str(i))
                        referencia.lastname = request.POST.get('lastname' + str(i))
                        referencia.phone = request.POST.get('phone' + str(i))
                        referencia.email = request.POST.get('email' + str(i))
                        referencia.save()

                # Guardar Datos Economicos
                if formDatosEconomicos.is_valid():
                    datos_economicos = formDatosEconomicos.save(commit=False)
                    datos_economicos.employee = empleado
                    datos_economicos.save()

                if formCuentaBancaria.is_valid():
                    cuenta_bancaria = formCuentaBancaria.save(commit=False)
                    cuenta_bancaria.employee = empleado
                    cuenta_bancaria.save()
                return redirect('employee:employee_list_user')
    else:
        formPerson = PersonForm(prefix='formPerson')
        formEmpleado = EmployeeForm(prefix='formEmpleado')
        formDatosInstitcionales = DatosInstitucionalesForm(prefix='formDatosInstitcionales')
        formIstruccion = InstruccionForm(prefix='formIstruccion')
        formOficios = OficiosForm(prefix='formOficios')
        formLenguajes = LenguajeForm(prefix='formLenguajes')
        formCapacitacion = CapacitacionForm(prefix='formCapacitacion')
        formExperiencia = ExperienciaForm(prefix='formExperiencia')
        formReferencia = ReferenciaForm(prefix='formReferencia')
        formDatosEconomicos = DatosEconomicosForm(prefix='formDatosEconomicos')
        formCuentaBancaria = cuentaBancariaForm(prefix='formCuentaBancaria')
    return render(request, template_name,
                  {'formEmpleado': formEmpleado, 'formPerson': formPerson,
                   'formDatosInstitcionales': formDatosInstitcionales,
                   'formIstruccion': formIstruccion, 'formExperiencia': formExperiencia,
                   'formCapacitacion': formCapacitacion, 'formOficios': formOficios,
                   'formLenguajes': formLenguajes, 'formReferencia': formReferencia,
                   'formDatosEconomicos': formDatosEconomicos,
                   'formCuentaBancaria': formCuentaBancaria

                   })


def addExperiencia(request, template_name='employee/nuevo_empleado.html'):
    formExperiencia = ExperienciaForm(request.POST, request.FILES)
    if formExperiencia.is_valid():
        formExperiencia.add()
        return render(request, template_name,
                      {'formExperiencia': formExperiencia
                       })


def employee_update(request, pk, template_name='employee/employee_form_admin.html'):
    # employee = Employee.objects.get(pk=pk)
    employee = get_object_or_404(Employee, pk=pk)
    form = EmployeeForm(request.POST or None, instance=employee)
    if form.is_valid():
        form.save()
        return redirect('employee:employee_list_user')
    return render(request, template_name, {'form': form})


def datos_personales_update(request, template_name='employee/employee_form_admin.html'):
    if request.POST:
        form = PersonForm(request.POST, request.FILES)
    else:
        form = PersonForm()

    if form.is_valid():
        form.save()
        return redirect('employee:employee_list_user')
    return render(request, template_name, {'form': form})


def employee_delete(request, pk, template_name='employee/employee_confirm_delete_admin.html'):
    employee = get_object_or_404(Employee, pk=pk)
    if request.method == 'POST':
        employee.delete()
        return redirect('employee:employee_list')
    return render(request, template_name, {'object': employee})


def employee_detail(request, pk, template_name='employee/employee_detail_admin.html'):
    employee = Employee.objects.get(id=pk)
    # employee = Employee.objects.filter(employee=pk)
    # persona = Person.objects.get(id=curriculum.)
    context = {
        "employee": employee,
    }
    return render(request, template_name, context)


# Funciones Usuario
# -------Empleado----------
def employee_list_user(request, template_name='employee/employee_list.html'):
    user_profile = UserProfile.objects.get(user=request.user)
    if request.POST:
        # institucion = request.POST.get('institucion')
        directivo = request.POST.get('directivo')
        direccion = request.POST.get('direccion')
        jefatura = request.POST.get('jefatura')
        cedula = request.POST.get('cedula')
        apellido = request.POST.get('apellido')
        estado = request.POST.get('estado')
        genero = request.POST.get('genero')
        estado_civil = request.POST.get('estado_civil')
        data = {
            # 'institucion_pk': institucion,
            'directivo_pk': directivo,
            'direccion_pk': direccion,
            'jefatura_pk': jefatura,
            'cedula': cedula,
            'apellido': apellido,
            'estado_pk': estado,
            'genero': genero,
            'estado_civil': estado_civil
        }

        qset = Q(institution=user_profile.institution_default)
        if directivo:
            qset = (qset & Q(directivo__pk=int(directivo)))
        if directivo and direccion:
            qset = (qset & Q(direccion__pk=int(direccion)))
        if directivo and direccion and jefatura:
            qset = (qset & Q(jefatura__pk=int(jefatura)))
        if estado:
            qset = (qset & Q(status__pk=int(estado)))
        if genero:
            qset = (qset & Q(person__sex__pk=int(genero)))
        if estado_civil:
            qset = (qset & Q(person__civil_status__pk=int(estado_civil)))

        employees = Employee.objects.filter(qset)
        if cedula:
            employees = Employee.objects.filter(Q(person__cedula=cedula) & Q(institution=user_profile.institution_default))
        if apellido:
            employees = Employee.objects.filter(
                Q(person__name__icontains=apellido) | Q(person__lastname__icontains=apellido) & Q(institution=user_profile.institution_default))
    else:
        employees = Employee.objects.filter(institution=user_profile.institution_default).order_by('-pk')[:30]
        data = {}

    data['object_list'] = employees

    if request.POST.get('reporte'):

        if request.POST.get('sueldo'):
            data['reporte_avanzado'] = True
            empleados = []
            for empleado in employees:
                partida = Partida.objects.filter(currentemployee=empleado)
                # contrato = Contract.objects.filter(employee=empleado)

                if partida:
                    empleados.append({'empleado': empleado, 'partida': partida[0]})
                else:
                    empleados.append({'empleado': empleado, 'partida': None})
            data['object_list'] = empleados

        data['user'] = request.user
        data['total_registros'] = employees.count()
        if request.POST.get('estado'):
            data['estado_emp'] = Item.objects.get(pk=int(estado), catalog__code='ESTADOEMPLEADO')
        if request.POST.get('genero'):
            data['genero_emp'] = Item.objects.get(pk=int(genero), catalog__code='SEXO')
        if request.POST.get('estado_civil'):
            data['civil_emp'] = Item.objects.get(pk=int(estado_civil), catalog__code='ESTADO CIVIL')
        if request.POST.get('address'):
            data['address'] = True
        if request.POST.get('telefono'):
            data['telefono'] = True
        if request.POST.get('fecha_nac'):
            data['fecha_nac'] = True
        if request.POST.get('sueldo'):
            data['sueldo'] = True
        if request.POST.get('cargo'):
            data['cargo'] = True
        if request.POST.get('nivel_instruccion'):
            data['nivel_instruccion'] = True

        return render_to_pdf('employee/reportes.html', data)

    # paginator = Paginator(employee, 20)  # Show 25 contract per pager
    # try:
    #     employees = paginator.page(page)
    # except PageNotAnInteger:
    #     employees = paginator.page(1)
    # except EmptyPage:
    #     employees = paginator.page(paginator.num_pages)

    return render(request, template_name, data)


def employee_create_user(request, template_name='employee/employee_form.html'):
    form = EmployeeForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('employee:employee_list_user')
    return render(request, template_name, {'form': form})


def employee_update_user(request, pk, template_name='employee/employee_form.html'):
    employee = get_object_or_404(Employee, pk=pk)
    form = EmployeeForm(request.POST or None, instance=employee)
    if form.is_valid():
        form.save()
        return redirect('employee:employee_list_user')
    return render(request, template_name, {'form': form})


def employee_delete_user(request, pk, template_name='employee/employee_confirm_delete.html'):
    employee = get_object_or_404(Employee, pk=pk)
    if request.method == 'POST':
        employee.delete()
        return redirect('employee:employee_list_user')
    return render(request, template_name, {'object': employee})


def employee_detail_user(request, pk, template_name='employee/employee_detail_admin.html'):
    employee = Employee.objects.get(id=pk)
    # employee = Employee.objects.filter(employee=pk)
    # persona = Person.objects.get(id=curriculum.)
    context = {
        "employee": employee,

    }
    return render(request, template_name, context)


def employee_detail_user_personel_data(request, pk, template_name='employee/employee_detail.html'):
    employee = get_object_or_404(Employee, pk=pk)
    person = get_object_or_404(Person, pk=employee.person.pk)
    titulo = "Datos Personales"
    funcion = "Datos Personales"

    if request.POST:
        formPerson = PersonForm(request.POST, request.FILES, instance=person)
        if formPerson.is_valid():
            formPerson.save()
            return redirect('employee:employee_detail_user', pk)
    else:
        formPerson = PersonForm(instance=person)

    context = {
        "titulo": titulo,
        "employee": employee,
        "funcion": funcion,
        'formPerson': formPerson
    }

    return render(request, template_name, context)


# -----------------------CURRICULUM--------------------------
def employee_detail_user_curriculo(request, pk, template_name='employee/employee_detail.html'):
    employee = get_object_or_404(Employee, pk=pk)
    funcion = "Currículo"
    instruccion = FormalInstruction.objects.filter(curriculum_vitae__person=employee.person)
    oficios = OfficesOrsubactivities.objects.filter(curriculum_vitae__person=employee.person)
    lenguajes = Language.objects.filter(curriculum_vitae__person=employee.person)
    capacitaciones = Training.objects.filter(curriculum_vitae__person=employee.person)
    experiencias = WorkExperience.objects.filter(curriculum_vitae__person=employee.person)
    # performance_evaluation = PerformanceEvaluation.objects.filter(curriculum_vitae=pk)
    # personal_achievement = PersonalAchievement.objects.filter(curriculum_vitae=pk)
    # affirmative_action = AffirmativeAction.objects.filter(curriculum_vitae=pk)
    personal_references = PersonalReferences.objects.filter(curriculum_vitae__person=employee.person)
    titulo = "Curriculo Vitae"
    funcion = "Curriculo Vitae"

    context = {
        "titulo": titulo,
        # "curriculum_vitae": curriculum_vitae,
        "formal_instruction": instruccion,
        "oficios": oficios,
        "lenguajes": lenguajes,
        "capacitaciones": capacitaciones,
        "experiencias": experiencias,
        # "performance_evaluation": performance_evaluation,
        # "personal_achievement": personal_achievement,
        # "affirmative_action": affirmative_action,
        "personal_references": personal_references,
        "employee": employee,
        "funcion": funcion,
    }

    return render(request, template_name, context)


# ------------------------------ Modal Crud  Curriculum -----------------------------

# ----- Edit
def editModal(request, pk, model, template_name='employee/modal_edit.html'):
    if model == 'instruccion':
        instance = FormalInstruction.objects.get(pk=pk)
        form = InstruccionForm(request.POST or None, instance=instance)
        titulo_modal = 'Editar Instrucción'
    elif model == 'oficio':
        instance = OfficesOrsubactivities.objects.get(pk=pk)
        form = OficiosForm(request.POST or None, instance=instance)
        titulo_modal = 'Editar Oficio'
    elif model == 'lenguaje':
        instance = Language.objects.get(pk=pk)
        form = LenguajeForm(request.POST or None, instance=instance)
        titulo_modal = 'Editar Idioma'
    elif model == 'capacitacion':
        # TODO por hacer
        form = CapacitacionForm(request.POST or None)
        titulo_modal = 'Editar Capacitación'
    elif model == 'referencia':
        instance = PersonalReferences.objects.get(pk=pk)
        form = ReferenciaForm(request.POST or None, instance=instance)
        titulo_modal = 'Editar refencia'
    else:
        form = None

    url_action = 'employee:edit_modal'
    empleado = Employee.objects.get(person=instance.curriculum_vitae.person)
    if request.POST:
        if instance:
            form.save()
            return redirect('employee:employee_detail_user_curriculo', empleado.pk)
    data = {'id': instance.pk, 'form': form, 'url_action': url_action, 'titulo_modal': titulo_modal, 'model': model}
    return render(request, template_name, data)


# ----- Add
def addModal(request, empleado_id, model, template_name='employee/modal_add.html'):
    if model == 'instruccion':
        form = InstruccionForm(request.POST or None)
        titulo_modal = 'Nueva Instrucción'
    elif model == 'oficio':
        form = OficiosForm(request.POST or None)
        titulo_modal = 'Nuevo Oficio'
    elif model == 'lenguaje':
        form = LenguajeForm(request.POST or None)
        titulo_modal = 'Nuevo Idioma'
    elif model == 'capacitacion':
        form = CapacitacionForm(request.POST or None, request.FILES, prefix='formCapacitacion')
        titulo_modal = 'Nueva Capacitación'
    elif model == 'experiencia':
        form = ExperienciaForm(request.POST or None, request.FILES, prefix='formExperiencia')
        titulo_modal = 'Nueva Experiencia'
    elif model == 'referencia':
        form = ReferenciaForm(request.POST or None)
        titulo_modal = 'Nueva Referencia'
    else:
        form = None

    url_action = 'employee:add_modal'
    empleado = get_object_or_404(Employee, pk=empleado_id)
    if CurriculumVitae.objects.filter(person=empleado.person).count() > 0:
        print('ya tiene curriculum ')
        currriculum = CurriculumVitae.objects.get(person=empleado.person)
    else:
        currriculum = CurriculumVitae.objects.create(person=empleado.person)

    if request.POST:
        if model == "capacitacion":
            files = request.FILES.getlist('formCapacitacion-documento_pdf')
            for pdf in files:
                Training.objects.create(curriculum_vitae=currriculum, documento_pdf=pdf)
        elif model == "experiencia":
            files = request.FILES.getlist('formExperiencia-documento_pdf')
            for pdf in files:
                WorkExperience.objects.create(curriculum_vitae=currriculum, documento_pdf=pdf)
        else:
            ref_model = form.save(commit=False)
            ref_model.curriculum_vitae = currriculum
            ref_model.save()
        return redirect('employee:employee_detail_user_curriculo', empleado_id)
    data = {'form': form, 'url_action': url_action, 'titulo_modal': titulo_modal, 'id': empleado_id, 'model': model}
    return render(request, template_name, data)


# ----- Eliminar
def eliminarModal(request, pk, model, template_name='employee/modal_eliminar.html'):
    if model == 'instruccion':
        instance = FormalInstruction.objects.get(pk=pk)
        titulo_modal = 'Eliminar Instrucción'
    elif model == 'oficio':
        instance = OfficesOrsubactivities.objects.get(pk=pk)
        titulo_modal = 'Eliminar Oficio'
    elif model == 'lenguaje':
        instance = Language.objects.get(pk=pk)
        titulo_modal = 'Eliminar Idioma'
    elif model == 'capacitacion':
        instance = Training.objects.get(pk=pk)
        titulo_modal = 'Eliminar capacitación'
    elif model == 'experiencia':
        instance = WorkExperience.objects.get(pk=pk)
        titulo_modal = 'Eliminar capacitación'
    elif model == 'referencia':
        instance = PersonalReferences.objects.get(pk=pk)
        titulo_modal = 'Eliminar refencia'
    else:
        instance = None

    empleado = Employee.objects.get(person=instance.curriculum_vitae.person)

    url_action = 'employee:eliminar_modal'
    if request.POST:
        instance.delete()
        return redirect('employee:employee_detail_user_curriculo', empleado.pk)
    data = {'url_action': url_action, 'titulo_modal': titulo_modal, 'id': pk, 'model': model}
    return render(request, template_name, data)


# ------------------------------ Datos Institucionales -----------------------------

def employee_detail_user_institutional_data(request, pk, template_name='employee/employee_detail.html'):
    titulo = "Datos Institucionales"

    try:
        employee = get_object_or_404(Employee, pk=pk)
        institutional_data = DatosInstitucionales.objects.get(employee=pk)
        formEmpleado = EmployeeForm(request.POST or None, prefix='formEmpleado', instance=employee)
        formDatosInstitucionales = DatosInstitucionalesForm(request.POST or None, prefix='formDatosInstitucionales', instance=institutional_data)

        if request.POST:
            # formEmpleado = EmployeeForm(request.POST, prefix='formEmpleado')
            directivo = request.POST.get('directivo')
            direccion = request.POST.get('direccion')
            jefatura = request.POST.get('jefatura')

            form_empleado = formEmpleado.save(commit=False)
            form_empleado.directivo_id = int(directivo)
            form_empleado.direccion_id = int(direccion)
            form_empleado.jefatura_id = int(jefatura)
            form_empleado.save()
            formDatosInstitucionales.save()
            return redirect('employee:employee_detail_user_institutional_data', employee.pk)

        service_commission = ServiceCommission.objects.filter(employee=institutional_data.employee)
        observations = Observations.objects.filter(employee=institutional_data.employee)
    except DatosInstitucionales.DoesNotExist:
        institutional_data = {}
        service_commission = {}
        observations = {}

    context = {
        "titulo": titulo,
        "employee": employee,
        "institutional_data": institutional_data,
        "service_commission": service_commission,
        "observations": observations,
        "formDatosInstitucionales": formDatosInstitucionales,
        'formEmpleado': formEmpleado

    }
    return render(request, template_name, context)


def employee_detail_user_economic_data(request, pk, template_name='employee/employee_detail.html'):
    titulo = "Datos Económicos"
    funcion = "Datos Económicos"

    try:
        employee = get_object_or_404(Employee, pk=pk)
        datos_economicos = EconomicData.objects.get(employee=employee)
        formDatosEconomicos = DatosEconomicosForm(request.POST or None, instance=datos_economicos)
    except:
        datos_economicos = {}
        formDatosEconomicos = DatosEconomicosForm()

    try:
        bank_account = BankAccounts.objects.get(employee=employee)
        formCuentasBancaria = cuentaBancariaForm(request.POST or None, instance=bank_account)
    except:
        bank_account = {}
        formCuentasBancaria = cuentaBancariaForm()

    if request.POST:
        if request.POST.get('guardar') == 'datos_economicos':
            formDatosEconomicos.save()
        else:
            formCuentasBancaria.save()

    context = {
        "titulo": titulo,
        "employee": employee,
        "funcion": funcion,
        "datos_economicos": datos_economicos,
        "bank_account": bank_account,
        'formDatosEconomicos': formDatosEconomicos,
        'formCuentasBancaria': formCuentasBancaria,

    }

    return render(request, template_name, context)


def employee_detail_user_contract_data(request, pk, template_name='employee/employee_detail.html'):
    employee = get_object_or_404(Employee, pk=pk)
    titulo = "Contratos"
    funcion = "Contratos"
    try:
        contract = Contract.objects.filter(employee=employee)

    except Contract.DoesNotExist:
        contract = 'No existen datos'
    context = {
        "titulo": titulo,
        "employee": employee,
        "funcion": funcion,
        "contract": contract,

    }

    return render(request, template_name, context)


def historial_partidas_presupuestarias(request, pk, template_name='employee/employee_detail.html'):
    employee = get_object_or_404(Employee, pk=pk)
    partidas = EmpleadoHistorial.objects.filter(empleado=pk)
    context = {
        "titulo": 'Partidas',
        "partidasHistorial": partidas,
        "employee": employee,
    }

    return render(request, template_name, context)


def employee_detail_user_personnel_actions_data(request, pk, template_name='employee/employee_detail.html'):
    employee = get_object_or_404(Employee, pk=pk)
    titulo = "Acciones de personal"
    funcion = "Acciones de personal"
    personnel_actions = PersonnelActions.objects.filter(employee=employee)
    context = {
        "titulo": titulo,
        "employee": employee,
        "funcion": funcion,
        "personnel_actions": personnel_actions,
    }

    return render(request, template_name, context)


def employee_detail_user_schedule_data(request, pk, template_name='employee/employee_detail.html'):
    employee = get_object_or_404(Employee, pk=pk)
    titulo = "Horarios"
    funcion = "Horarios"

    schedule = EmployeeScheduleHistory.objects.filter(employee=employee.id)
    context = {
        "titulo": titulo,
        "employee": employee,
        "funcion": funcion,
        "schedule": schedule,

    }

    return render(request, template_name, context)


def employee_detail_user_permission_data(request, pk, template_name='employee/employee_detail.html'):
    print('Estoy en permisos')
    employee = get_object_or_404(Employee, pk=pk)
    titulo = "Permisos Por Hora"
    funcion = "Permisos Por Hora"
    permissions = Permission.objects.filter(employee=employee)
    context = {
        "titulo": titulo,
        "employee": employee,
        "funcion": funcion,
        "permissions": permissions,

    }

    return render(request, template_name, context)


def institutional_data_detail(request, pk, template_name='employee/institutional_data_detail_admin.html'):
    institutional_data = DatosInstitucionales.objects.get(id=pk)
    service_commission = ServiceCommission.objects.filter(employee=institutional_data.employee)
    observations = Observations.objects.filter(employee=institutional_data.employee)
    # employee = Employee.objects.filter(employee=pk)
    # persona = Person.objects.get(id=curriculum.)
    context = {
        "institutional_data": institutional_data,
        "service_commission": service_commission,
        "observations": observations,
    }

    return render(request, template_name, context)


def create_or_update_institutional_data(contract):
    if (contract.status == 'Firmado'):
        try:
            institutional_data = DatosInstitucionales.objects.get(employee=contract.employee.id)
            institutional_data.institution = contract.department.institutional_unit.institution
            institutional_data.institutional_unit = contract.department.institutional_unit
            institutional_data.department = contract.department
            institutional_data.employee = contract.employee
            institutional_data.place_of_work = contract.place_of_work
            institutional_data.telephone_extension = ""
            # service_time=1,
            institutional_data.schedule = contract.schedule
            institutional_data.file_number = ""
            institutional_data.biometric_identification = ""
            institutional_data.impediment_code = ""
            institutional_data.public_office = contract.public_office
            institutional_data.functions = contract.functions
            institutional_data.email_institutional = ""
            institutional_data.status = contract.status
            institutional_data.start_date = contract.start_date
            institutional_data.end_date = contract.end_date
            institutional_data.collective_contract = contract.collective_contract
            institutional_data.save()

        except DatosInstitucionales.DoesNotExist:
            institutional_data = DatosInstitucionales()
            institutional_data.institucion = contract.department.institutional_unit.institution
            institutional_data.direccion = contract.department.institutional_unit
            institutional_data.jefatura = contract.department
            institutional_data.employee = contract.employee
            institutional_data.place_of_work = contract.place_of_work
            institutional_data.telephone_extension = ""
            # service_time=1,
            institutional_data.schedule = contract.schedule
            institutional_data.file_number = ""
            institutional_data.biometric_identification = ""
            institutional_data.impediment_code = ""
            institutional_data.public_office = contract.public_office
            institutional_data.functions = contract.functions
            institutional_data.email_institutional = ""
            institutional_data.status = contract.status
            institutional_data.start_date = contract.start_date
            institutional_data.end_date = contract.end_date
            institutional_data.collective_contract = contract.collective_contract
            institutional_data.save()
    return institutional_data


# -------Datos institucionales - InstitutionalData ---------- service_commission
def service_commission_list(request, template_name='employee/service_commission_admin.html'):
    service_commission = ServiceCommission.objects.all()
    # data['object_list'] = service_commission
    context = {
        "data": service_commission,
        "accion": 'listar',
    }
    return render(request, template_name, context)


def service_commission_create(request, template_name='employee/service_commission_admin.html'):
    form = ServiceCommissionForm(request.POST or None)
    context = {
        "accion": 'nuevo',
        "form": form,
    }
    if form.is_valid():
        form.save()
        return redirect('employee:service_commission_list')
    return render(request, template_name, context)


def service_commission_update(request, pk, template_name='employee/service_commission_admin.html'):
    service_commission = get_object_or_404(ServiceCommission, pk=pk)
    form = ServiceCommissionForm(request.POST or None, instance=service_commission)
    if form.is_valid():
        form.save()
        return redirect('employee:service_commission_list')

    context = {
        "accion": 'editar',
        "form": form,
    }
    return render(request, template_name, context)


def service_commission_delete(request, pk, template_name='employee/service_commission_admin.html'):
    service_commission = get_object_or_404(ServiceCommission, pk=pk)
    if request.method == 'POST':
        service_commission.delete()
        return redirect('employee:service_commission_list')
    context = {
        "accion": 'eliminar',
        "form": service_commission,
    }
    return render(request, template_name, context)


# -------Datos institucionales - InstitutionalData ---------- observaciones
def observations_list(request, template_name='employee/observations_admin.html'):
    observations = Observations.objects.all()
    # data['object_list'] = service_commission
    context = {
        "data": observations,
        "accion": 'listar',
    }
    return render(request, template_name, context)


def observations_create(request, template_name='employee/observations_admin.html'):
    form = ObservationsForm(request.POST or None)
    context = {
        "accion": 'nuevo',
        "form": form,
    }
    if form.is_valid():
        form.save()
        return redirect('employee:observations_list')
    return render(request, template_name, context)


def observations_update(request, pk, template_name='employee/observations_admin.html'):
    observations = get_object_or_404(Observations, pk=pk)
    form = ObservationsForm(request.POST or None, instance=observations)
    if form.is_valid():
        form.save()
        return redirect('employee:service_commission_list')

    context = {
        "accion": 'editar',
        "form": form,
    }
    return render(request, template_name, context)


def observations_delete(request, pk, template_name='employee/observations_admin.html'):
    observations = get_object_or_404(Observations, pk=pk)
    if request.method == 'POST':
        observations.delete()
        return redirect('employee:observations_list')
    context = {
        "accion": 'eliminar',
        "form": observations,
    }
    return render(request, template_name, context)


# Funciones usuario
# -------Datos institucionales - InstitutionalData ---------- User
def institutional_data_list_user(request, template_name='employee/institutional_data_list.html'):
    user_profile = UserProfile.objects.get(user=request.user)
    institutional_data = DatosInstitucionales.objects.filter(employee__institution=user_profile.institution_default)
    data = {}
    data['object_list'] = institutional_data
    return render(request, template_name, data)


def institutional_data_create_user(request, template_name='employee/institutional_data_form.html'):
    form = InstitutionalDataForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('employee:institutionaldata_list_user')
    return render(request, template_name, {'form': form})


def institutional_data_update_user(request, pk, template_name='employee/institutional_data_form.html'):
    institutional_data = get_object_or_404(DatosInstitucionales, pk=pk)
    form = InstitutionalDataForm(request.POST or None, instance=institutional_data)
    if form.is_valid():
        form.save()
        return redirect('employee:institutionaldata_list_user')
    return render(request, template_name, {'form': form})


def institutional_data_delete_user(request, pk, template_name='employee/institutional_data_confirm_delete.html'):
    institutional_data = get_object_or_404(DatosInstitucionales, pk=pk)
    if request.method == 'POST':
        institutional_data.delete()
        return redirect('employee:employee_list_user')
    return render(request, template_name, {'object': institutional_data})


def institutional_data_detail_user(request, pk, template_name='employee/institutional_data_detail.html'):
    institutional_data = DatosInstitucionales.objects.get(id=pk)
    service_commission = ServiceCommission.objects.filter(employee=institutional_data.employee)
    observations = Observations.objects.filter(employee=institutional_data.employee)
    # employee = Employee.objects.filter(employee=pk)
    # persona = Person.objects.get(id=curriculum.)
    context = {
        "institutional_data": institutional_data,
        "service_commission": service_commission,
        "observations": observations,
    }
    return render(request, template_name, context)


# -------Datos institucionales - InstitutionalData ---------- service_commission---------User
def service_commission_list_user(request, template_name='employee/service_commission.html'):
    user_profile = UserProfile.objects.get(user=request.user)
    service_commission = ServiceCommission.objects.filter(employee__institution=user_profile.institution_default)
    # data['object_list'] = service_commission
    context = {
        "data": service_commission,
        "accion": 'listar',
    }
    return render(request, template_name, context)


def service_commission_create_user(request, template_name='employee/service_commission.html'):
    form = ServiceCommissionForm(request.POST or None)
    context = {
        "accion": 'nuevo',
        "form": form,
    }
    if form.is_valid():
        form.save()
        return redirect('employee:service_commission_list_user')
    return render(request, template_name, context)


def service_commission_update_user(request, pk, template_name='employee/service_commission.html'):
    service_commission = get_object_or_404(ServiceCommission, pk=pk)
    form = ServiceCommissionForm(request.POST or None, instance=service_commission)
    if form.is_valid():
        form.save()
        return redirect('employee:service_commission_list_user')

    context = {
        "accion": 'editar',
        "form": form,
    }
    return render(request, template_name, context)


def service_commission_delete_user(request, pk, template_name='employee/service_commission.html'):
    service_commission = get_object_or_404(ServiceCommission, pk=pk)
    if request.method == 'POST':
        service_commission.delete()
        return redirect('employee:service_commission_list_user')
    context = {
        "accion": 'eliminar',
        "form": service_commission,
    }
    return render(request, template_name, context)


# -------Datos institucionales - InstitutionalData ---------- observaciones------User
def observations_list_user(request, template_name='employee/observations.html'):
    user_profile = UserProfile.objects.get(user=request.user)
    observations = Observations.objects.filter(employee__institution=user_profile.institution_default)
    # data['object_list'] = service_commission
    context = {
        "data": observations,
        "accion": 'listar',
    }
    return render(request, template_name, context)


def observations_create_user(request, template_name='employee/observations.html'):
    form = ObservationsFormUser(request.POST or None)
    context = {
        "accion": 'nuevo',
        "form": form,
    }
    if form.is_valid():
        observations = form.save(commit=False)
        user_profile = UserProfile.objects.get(user=request.user)
        observations.date = time.strftime("%Y-%m-%d")
        observations.created_by = user_profile.user
        form.save()
        return redirect('employee:observations_list_user')
    return render(request, template_name, context)


def observations_create_user_employee(request, pk, template_name='employee/observations.html'):
    form = ObservationsFormUserEmployee(request.POST or None)
    context = {
        "accion": 'nuevo',
        "form": form,
    }
    if form.is_valid():
        observations = form.save(commit=False)
        user_profile = UserProfile.objects.get(user=request.user)
        employee = get_object_or_404(Employee, pk=pk)
        observations.employee = employee
        observations.type_of_observation = 'manual'
        observations.date = time.strftime("%Y-%m-%d")
        observations.created_by = user_profile.user
        form.save()
        return HttpResponseRedirect(reverse('employee:employee_detail_user_institutional_data', kwargs={'pk': pk}))
    return render(request, template_name, context)


def observations_create_automatic(employee, titulo, observacion, contract_number, user_profile_autent):
    employee = Employee.objects.get(pk=employee)
    date = time.strftime("%Y-%m-%d")
    type_of_observation = 'automática'
    number_personel_action = contract_number
    created_by = user_profile_autent.user
    observacion_obj = Observations.objects.create(employee=employee, title=titulo, date=date, observations=observacion,
                                                  type_of_observation=type_of_observation,
                                                  number_personel_action=contract_number, created_by=created_by)
    observacion_obj.save()
    return redirect('employee:observations_list')
    # return render(request, template_name, context)


def observations_update_user(request, pk, template_name='employee/observations.html'):
    observations = get_object_or_404(Observations, pk=pk)
    form = ObservationsForm(request.POST or None, instance=observations)
    if form.is_valid():
        form.save()
        return redirect('employee:service_commission_list_user')

    context = {
        "accion": 'editar',
        "form": form,
    }
    return render(request, template_name, context)


def observations_delete_user(request, pk, template_name='employee/observations.html'):
    observations = get_object_or_404(Observations, pk=pk)
    if request.method == 'POST':
        observations.delete()
        return redirect('employee:observations_list_user')
    context = {
        "accion": 'eliminar',
        "form": observations,
    }
    return render(request, template_name, context)


# -------Datos institucionales - InstitutionalData ---------- Historial de horarios ------User
def employee_schedule_history_list_user(request, template_name='employee/employee_schedule_history.html'):
    user_profile = UserProfile.objects.get(user=request.user)
    employee_schedule_history = EmployeeScheduleHistory.objects.filter(
        employee__institution=user_profile.institution_default)
    # data['object_list'] = service_commission
    context = {
        "data": employee_schedule_history,
        "accion": 'listar',
    }
    return render(request, template_name, context)


def employee_schedule_history_list_user_employee(request, pk, template_name='employee/employee_schedule_history.html'):
    user_profile = UserProfile.objects.get(user=request.user)
    employee = Employee.objects.get(id=pk)
    employee_schedule_history = EmployeeScheduleHistory.objects.filter(employee=employee)
    context = {
        "data": employee_schedule_history,
        "accion": 'listar',
    }
    return render(request, template_name, context)


def employee_schedule_history_create_user_employee(request, pk,
                                                   template_name='employee/employee_schedule_history.html'):
    employee = get_object_or_404(Employee, pk=pk)
    economic_data = EconomicData.objects.get(employee=employee)
    contract = Contract.objects.get(id=economic_data.contract_number)
    form = EmployeeScheduleHistoryDataForm(request.POST or None)
    accion = 'asignar nuevo horario'
    context = {
        "accion": accion,
        "mensaje": 'Este empleado no es encesario asignar un horario',
    }
    print(contract.type_of_contract.horario)
    print(contract.type_of_contract)
    if contract.type_of_contract.horario:
        form = EmployeeScheduleHistoryDataForm(request.POST or None)
        context = {
            "accion": accion,
            "form": form,
            "mensaje": '',
        }
        if form.is_valid():
            # employee_schedule_history = form.save(commit=False)
            # user_profile = UserProfile.objects.get(user=request.user)
            # employee_schedule_history.date_of_assignment = time.strftime("%Y-%m-%d")
            # employee_schedule_history.user_change = user_profile.user
            form.save()
            return HttpResponseRedirect(reverse('employee:employee_detail_user_institutional_data', kwargs={'pk': pk}))
        print('actualizar horario')
        return render(request, template_name, context)
    else:
        print('no se puede actualizar')

    return render(request, template_name, context)


def employee_schedule_history_create_user(request, template_name='employee/employee_schedule_history.html'):
    form = EmployeeScheduleHistoryForm(request.POST or None)
    context = {
        "accion": 'nuevo',
        "form": form,
    }
    if form.is_valid():
        employee_schedule_history = form.save(commit=False)
        user_profile = UserProfile.objects.get(user=request.user)
        employee_schedule_history.date_of_assignment = time.strftime("%Y-%m-%d")
        employee_schedule_history.user_change = user_profile.user
        form.save()
        return redirect('employee:employee_schedule_history_user_list_user')
    return render(request, template_name, context)


def employee_schedule_history_ascreate_user(request, pk, template_name='employee/employee_schedule_history.html'):
    form = ObservationsFormUserEmployee(request.POST or None)
    context = {
        "accion": 'nuevo',
        "form": form,
    }
    if form.is_valid():
        observations = form.save(commit=False)
        user_profile = UserProfile.objects.get(user=request.user)
        employee = get_object_or_404(Employee, pk=pk)
        observations.employee = employee
        observations.type_of_observation = 'manual'
        observations.date = time.strftime("%Y-%m-%d")
        observations.created_by = user_profile.user
        form.save()
        return HttpResponseRedirect(reverse('employee:employee_detail_user_institutional_data', kwargs={'pk': pk}))
    return render(request, template_name, context)


def employee_schedule_history_create_automatic(employee, titulo, observacion, contract_number, user_profile_autent):
    employee = Employee.objects.get(pk=employee)
    date = time.strftime("%Y-%m-%d")
    type_of_observation = 'automática'
    number_personel_action = contract_number
    created_by = user_profile_autent.user
    observacion_obj = Observations.objects.create(employee=employee, title=titulo, date=date, observations=observacion,
                                                  type_of_observation=type_of_observation,
                                                  number_personel_action=contract_number, created_by=created_by)
    observacion_obj.save()
    return redirect('employee:observations_list')
    # return render(request, template_name, context)


def employee_schedule_history_update_user(request, pk, template_name='employee/observations.html'):
    observations = get_object_or_404(Observations, pk=pk)
    form = ObservationsForm(request.POST or None, instance=observations)
    if form.is_valid():
        form.save()
        return redirect('employee:service_commission_list_user')

    context = {
        "accion": 'editar',
        "form": form,
    }
    return render(request, template_name, context)


def employee_schedule_history_delete_user(request, pk, template_name='employee/observations.html'):
    observations = get_object_or_404(Observations, pk=pk)
    if request.method == 'POST':
        observations.delete()
        return redirect('employee:observations_list_user')
    context = {
        "accion": 'eliminar',
        "form": observations,
    }
    return render(request, template_name, context)


def verPdf(request, id, nombre):
    if nombre == 'experiencia':
        pdf = WorkExperience.objects.get(pk=id)
    elif nombre == 'capacitacion':
        pdf = Training.objects.get(pk=id)
    else:
        pdf = None
    pdf_data = open(pdf.documento_pdf.path, 'rb').read()
    return HttpResponse(pdf_data, content_type='application/pdf')


# -------------- REPORTES PDF -------------------

def report_pdf(request, context_dict={}):
    # pasar parametros a imprimir
    return render_to_pdf('employee/reportes.html', context_dict)

# -------------- REPORTES PDF -------------------
