from django.contrib import admin
from apps.employee.models import Employee, DatosInstitucionales, ServiceCommission, Observations, \
    EmployeeScheduleHistory


# Register your models here.

# class EmployeeAdmin(admin.ModelAdmin):
#     list_display = ('person', 'employee_type', 'institution', 'institutional_unit', 'department', 'status')
#     search_fields = ('person', 'employee_type', 'institution', 'institutional_unit', 'department', 'status')
#
#
# class InstitutionalDataAdmin(admin.ModelAdmin):
#     list_display = ('employee', 'institucion')
#     search_fields = ('employee', 'institucion')
#
#
# # list_filter = ('institucion',)
#
# class ServiceCommissionAdmin(admin.ModelAdmin):
#     list_display = ('employee', 'institution_source')
#     search_fields = ('employee', 'institution_source')
#
#
# class ObservationsAdmin(admin.ModelAdmin):
#     list_display = ('employee', 'type_of_observation')
#     search_fields = ('employee', 'type_of_observation')
#
#
# class EmployeeScheduleHistoryAdmin(admin.ModelAdmin):
#     list_display = ('employee', 'schedule')
#     search_fields = ('employee', 'schedule')
#
#
# admin.site.register(Employee, EmployeeAdmin)
# admin.site.register(DatosInstitucionales, InstitutionalDataAdmin)
# admin.site.register(ServiceCommission, ServiceCommissionAdmin)
# admin.site.register(Observations, ObservationsAdmin)
# admin.site.register(EmployeeScheduleHistory, EmployeeScheduleHistoryAdmin)


admin.site.register(Employee)
admin.site.register(DatosInstitucionales)
admin.site.register(ServiceCommission)
admin.site.register(Observations)
admin.site.register(EmployeeScheduleHistory)