from django.db import models
from apps.institution.models import Institucion, JefaturaCoordinacion, Directivo, Direccion
# Institution, InstitutionalUnit, Department
from apps.person.models import Person
from apps.schedule.models import Schedule
# from contract.models import Contract
from django.conf import settings


from apps.settings.models import Item


class Employee(models.Model):
    employee_type = models.ForeignKey(Item, blank=True, null=True, max_length=100, on_delete=models.CASCADE,
                                      limit_choices_to={'catalog__code':'TIPOEMPLEADO'},
                                      related_name='catalog_tipo_empleado', verbose_name='Tipo Empleado')

    status = models.ForeignKey(Item, blank=True, null=True, max_length=100, on_delete=models.CASCADE,
                                      limit_choices_to={'catalog__code':'ESTADOEMPLEADO'},
                                      related_name='catalog_estado_emp', verbose_name='Estado')
    person = models.OneToOneField(Person, verbose_name='Persona')
    institution = models.ForeignKey(Institucion, blank=True, null=True, verbose_name='Institución',
                                    on_delete=models.CASCADE)
    directivo = models.ForeignKey(Directivo, blank=True, null=True, verbose_name='Directivo',
                                  on_delete=models.CASCADE)
    direccion = models.ForeignKey(Direccion, blank=True, null=True, verbose_name='Dirección Institucional',
                                 on_delete=models.CASCADE)
    jefatura = models.ForeignKey(JefaturaCoordinacion, blank=True, null=True, verbose_name='Jefatura/Coordinación', on_delete=models.CASCADE)

    class Meta:
        # ordering = ['person', 'employee_type', 'employee_type']
        verbose_name = 'Empleado'
        verbose_name_plural = 'Empleados'


    def __str__(self):
        return u'{0}'.format(self.person)

    def __unicode__(self):
        return u'{0}'.format(self.person)


class DatosInstitucionales(models.Model):
    employee = models.OneToOneField(Employee, verbose_name='Empleado', on_delete=models.PROTECT, blank=True, null=True)
    place_of_work = models.CharField(verbose_name='Lugar de trabajo', max_length=100, blank=True, null=True)
    telephone_extension = models.CharField(max_length=100, blank=True, null=True, verbose_name='Nro. Extensión')
    service_time = models.DecimalField(verbose_name='Tiempo de servicio', max_digits=2, decimal_places=2, null=True,
                                       blank=True)  # Factor evaluacion de porcentage de conocimientos
    schedule = models.ForeignKey(Schedule, verbose_name='Jornada Laboral', null=True, blank=True)
    file_number = models.CharField(max_length=5, blank=True, null=True, verbose_name='Número de Expediente')
    biometric_identification = models.CharField(max_length=5, blank=True, null=True,
                                                verbose_name='Identificador del Biométrico')

    impediment_code = models.CharField(max_length=50, blank=True, null=True, verbose_name='Código de impedimento')
    public_office = models.CharField(verbose_name='Cargo público', max_length=100, blank=True, null=True)
    functions = models.TextField(verbose_name='Funciones', blank=True, null=True)
    email_institutional = models.EmailField(verbose_name='Correo Institucional', blank=True, null=True)
    status = models.CharField(max_length=100, blank=True, null=True, verbose_name='Estado')

    # labor_system = models.ForeignKey(LaborSystem,verbose_name='Régimen Laboral')
    # type_of_contract = models.ForeignKey(TypeOfContract,verbose_name='Tipo de contrato')

    start_date = models.DateField(verbose_name='Fecha de inicio', blank=True, null=True)
    end_date = models.DateField(verbose_name='Fecha de fin', blank=True, null=True)

    collective_contract = models.BooleanField(verbose_name='Contrato Colectivo', default=False)

    class Meta:
        # ordering = ['institucion', 'direccion', 'jefatura', 'employee']
        verbose_name = 'Datos Institucionales'
        verbose_name_plural = 'Datos Institucionales'

    def __str__(self):
        return u'{0}'.format(self.employee)

    def __unicode__(self):
        return u'{0}'.format(self.employee)


class ServiceCommission(models.Model):
    employee = models.ForeignKey(Employee, verbose_name='Empleado', on_delete=models.PROTECT)
    institution_source = models.CharField(verbose_name='Institución Origen', max_length=100)
    institution_destination = models.CharField(verbose_name='Institución Destino', max_length=100)
    start_date = models.DateField(verbose_name='Fecha de inicio')
    end_date = models.DateField(verbose_name='Fecha de fin', blank=True, null=True)

    class Meta:
        ordering = ['employee', 'start_date']
        verbose_name = 'Comisión de Servicio'
        verbose_name_plural = 'Comisiones de Servicio'

    def __str__(self):
        return u'{0}'.format(self.employee)

    def __unicode__(self):
        return u'{0}'.format(self.employee)


class Observations(models.Model):
    manual = 'manual'
    automatica = 'automática'
    type_of_observation_list = (
        (manual, 'Manual'),
        (automatica, 'Automática'),
    )

    employee = models.ForeignKey(Employee, verbose_name='Empleado', on_delete=models.PROTECT)
    title = models.CharField(verbose_name='Título', max_length=50)
    date = models.DateField(verbose_name='Fecha')
    observations = models.TextField(verbose_name='Observación')
    type_of_observation = models.CharField(verbose_name='Tipo', max_length=10, choices=type_of_observation_list,
                                           default=manual)

    number_personel_action = models.CharField(verbose_name='Número de Acción de Personal', max_length=20, blank=True,
                                              null=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Creado por:')

    class Meta:
        ordering = ['employee', 'date']
        verbose_name = 'Observación'
        verbose_name_plural = 'Observaciones'

    def __str__(self):
        return u'{0}'.format(self.employee)

    def __unicode__(self):
        return u'{0}'.format(self.employee)


class EmployeeScheduleHistory(models.Model):
    employee = models.ForeignKey(Employee, verbose_name='Empleado', on_delete=models.PROTECT)
    schedule = models.ForeignKey(Schedule, verbose_name='Horario', on_delete=models.PROTECT)
    date_of_assignment = models.DateField(verbose_name='Fecha asignación del horario')
    user_change = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Creado por:')
    reason = models.TextField(verbose_name='Motivo')
    status = models.BooleanField(verbose_name='Estado')

    class Meta:
        ordering = ['employee', 'date_of_assignment']
        verbose_name = 'Historial de horario del empleado'
        verbose_name_plural = 'Historial de horarios de los empleados'
