# -*- coding: utf-8 -*-
from django.conf.urls import url, include

from apps.permissions.views import permission_user_update
from .views import (employee_list, employee_create,
                    employee_update,
                    employee_delete,
                    employee_detail,

                    employee_list_user,
                    employee_create_user,
                    employee_update_user,
                    employee_delete_user,
                    employee_detail_user_personel_data,
                    employee_detail_user_curriculo,
                    employee_detail_user_institutional_data,
                    employee_detail_user_economic_data,
                    employee_detail_user_personnel_actions_data,
                    employee_detail_user_contract_data,
                    employee_detail_user_schedule_data,
                    employee_detail_user_permission_data,

    # institutional_data_list,
    # institutional_data_create,
    # institutional_data_update,
    # institutional_data_delete,
                    institutional_data_detail,

                    institutional_data_list_user,
                    institutional_data_create_user,
                    institutional_data_update_user,
                    institutional_data_delete_user,
                    institutional_data_detail_user,

                    service_commission_delete,
                    service_commission_list,
                    service_commission_create,
                    service_commission_update,

                    service_commission_delete_user,
                    service_commission_list_user,
                    service_commission_create_user,
                    service_commission_update_user,

                    observations_delete,
                    observations_list,
                    observations_create,
                    observations_update,

                    observations_delete_user,
                    observations_list_user,
                    observations_create_user,
                    observations_create_user_employee,
                    observations_update_user,

                    employee_schedule_history_list_user,
                    employee_schedule_history_create_user,
                    employee_schedule_history_create_user_employee,

                    api_organigrama, addExperiencia, datos_personales_update, editModal, eliminarModal, addModal, verPdf, report_pdf,
                    historial_partidas_presupuestarias)

urlpatterns = [

    # --------------API --- VUE
    # url(r'^api_empleados/$', api_empleados, name='api_empleados'),
    url(r'^api_organigrama/$', api_organigrama, name='api_instituciones'),

    # urls adminc
    url(r'^admin/$', employee_list, name='employee_list'),
    url(r'^admin/add_experiencia$', addExperiencia, name='add_experiencia'),
    url(r'^admin/new$', employee_create, name='employee_new'),
    url(r'^admin/editar/(?P<pk>\d+)$', employee_update, name='employee_edit'),
    url(r'^admin/(?P<pk>\d+)$', employee_delete, name='employee_delete'),
    url(r'^admin/detail/(?P<pk>\d+)$', employee_detail, name='employee_detail'),

    # url(r'^institutionaldata/admin/$', institutional_data_list, name='institutionaldata_list'),
    # url(r'^institutionaldata/admin/new$', institutional_data_create, name='institutionaldata_new'),
    # url(r'^institutionaldata/admin/editar/(?P<pk>\d+)$', institutional_data_update, name='institutionaldata_edit'),
    # url(r'^institutionaldata/admin/(?P<pk>\d+)$',institutional_data_delete, name='institutionaldata_delete'),
    url(r'^institutionaldata/admin/detail/(?P<pk>\d+)$', institutional_data_detail, name='institutionaldata_detail'),

    # url(r'^institutionaldata/sc/admin/$', service_commission_list, name='service_commission_list'),
    # url(r'^institutionaldata/sc/admin/new$', service_commission_create, name='service_commission_new'),
    # url(r'^institutionaldata/sc/admin/editar/(?P<pk>\d+)$', service_commission_update, name='service_commission_edit'),
    # url(r'^institutionaldata/sc/admin/(?P<pk>\d+)$', service_commission_delete, name='service_commission_delete'),

    # url(r'^institutionaldata/observations/admin/$', observations_list, name='observations_list'),
    # url(r'^institutionaldata/observations/admin/new$', observations_create, name='observations_new'),
    # url(r'^institutionaldata/observations/admin/editar/(?P<pk>\d+)$', observations_update, name='observations_edit'),
    # url(r'^institutionaldata/observations/admin/(?P<pk>\d+)$', observations_delete, name='observations_delete'),

    # urls user
    url(r'^$', employee_list_user, name='employee_list_user'),
    url(r'^new$', employee_create_user, name='employee_new_user'),
    url(r'^editar/(?P<pk>\d+)$', employee_update_user, name='employee_edit_user'),
    url(r'^(?P<pk>\d+)$', employee_delete_user, name='employee_delete_user'),

    url(r'^institutionaldata/$', institutional_data_list_user, name='institutionaldata_list_user'),
    url(r'^institutionaldata/new$', institutional_data_create_user, name='institutionaldata_new_user'),
    url(r'^institutionaldata/editar/(?P<pk>\d+)$', institutional_data_update_user, name='institutionaldata_edit_user'),
    url(r'^institutionaldata/(?P<pk>\d+)$', institutional_data_delete_user, name='institutionaldata_delete_user'),
    url(r'^institutionaldata/detail/(?P<pk>\d+)$', institutional_data_detail_user, name='institutionaldata_detail_user'),

    url(r'^institutionaldata/sc/$', service_commission_list_user, name='service_commission_list_user'),
    url(r'^institutionaldata/sc/new$', service_commission_create_user, name='service_commission_new_user'),
    url(r'^institutionaldata/sc/editar/(?P<pk>\d+)$', service_commission_update_user, name='service_commission_edit_user'),
    url(r'^institutionaldata/sc/(?P<pk>\d+)$', service_commission_delete_user, name='service_commission_delete_user'),

    url(r'^institutionaldata/observations/$', observations_list_user, name='observations_list_user'),
    url(r'^institutionaldata/observations/new$', observations_create_user, name='observations_new_user'),
    url(r'^institutionaldata/observations/employee/(?P<pk>\d+)$', observations_create_user_employee, name='observations_new_user_employee'),
    url(r'^institutionaldata/observations/editar/(?P<pk>\d+)$', observations_update_user, name='observations_edit_user'),
    url(r'^institutionaldata/observations/(?P<pk>\d+)$', observations_delete_user, name='observations_delete_user'),

    url(r'^institutionaldata/schedule_history/$', employee_schedule_history_list_user, name='employee_schedule_history_user_list_user'),
    url(r'^institutionaldata/schedule_history/new$', employee_schedule_history_create_user, name='employee_schedule_history_user_new_user'),
    url(r'^institutionaldata/schedule_history/employee/(?P<pk>\d+)$', employee_schedule_history_create_user_employee,
        name='employee_schedule_history_new_user_employee'),
    # url(r'^institutionaldata/schedule_history/employee/(?P<pk>\d+)$', employee_schedule_history_create_user_employee, name='observations_new_user_employee'),
    # url(r'^institutionaldata/schedule_history/editar/(?P<pk>\d+)$', observations_update_user, name='observations_edit_user'),
    # url(r'^institutionaldata/schedule_history/(?P<pk>\d+)$',observations_delete_user, name='observations_delete_user'),

    url(r'^detail/(?P<pk>\d+)$', employee_detail_user_personel_data, name='employee_detail_user'),
    url(r'^detail/personales/(?P<pk>\d+)$', employee_detail_user_personel_data, name='employee_detail_user_personel'),
    url(r'^detail/curriculo/(?P<pk>\d+)$', employee_detail_user_curriculo, name='employee_detail_user_curriculo'),
    url(r'^detail/institutional_data/(?P<pk>\d+)$', employee_detail_user_institutional_data, name='employee_detail_user_institutional_data'),
    url(r'^detail/economic_data/(?P<pk>\d+)$', employee_detail_user_economic_data, name='employee_detail_user_economic_data'),
    url(r'^detail/contract/(?P<pk>\d+)$', employee_detail_user_contract_data, name='employee_detail_user_contract'),
    url(r'^detail/personnel_actions_data/(?P<pk>\d+)$', employee_detail_user_personnel_actions_data,name='employee_detail_user_personnel_actions_data'),
    url(r'^detail/partidas/(?P<pk>\d+)$', historial_partidas_presupuestarias, name='historial_partidas_presupuestarias'),

    url(r'^detail/schedule_data/(?P<pk>\d+)$', employee_detail_user_schedule_data, name='employee_detail_user_schedule_data'),
    url(r'^detail/permissions_data/(?P<pk>\d+)$', employee_detail_user_permission_data, name='employee_detail_user_permissions_data'),

    url(r'^datospersonales/update/$', datos_personales_update, name='datos_personales_update'),

    # ------- CRUD Modal Curriculum --------
    url(r'^modal/edit_modal/(?P<pk>\d+)/(?P<model>\w+)$', editModal, name='edit_modal'),
    url(r'^modal/add_modal/(?P<empleado_id>\d+)/(?P<model>\w+)$', addModal, name='add_modal'),
    url(r'^modal/eliminar_modal/(?P<pk>\d+)/(?P<model>\w+)$', eliminarModal, name='eliminar_modal'),
    url(r'^view_pdf/(?P<id>\d+)/(?P<nombre>\w+)$', verPdf, name='ver_pdf'),

    # -------- REPORTES -----------
    url(r'^report_pdf/$', report_pdf, name='report_pdf'),

]
