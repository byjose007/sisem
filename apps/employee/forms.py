# -*- coding: utf-8 -*-

from django import forms
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout

from apps.curriculum.models import CurriculumVitae, FormalInstruction, OfficesOrsubactivities, Language, Training, \
    WorkExperience, PersonalReferences
from apps.economicdata.models import EconomicData, BankAccounts
from apps.institution.models import Institucion, Directivo, Direccion, JefaturaCoordinacion
from apps.person.models import Person
from .models import Employee, DatosInstitucionales, ServiceCommission, Observations, EmployeeScheduleHistory
from django.db.models.query import EmptyQuerySet


class InstitutionalDataForm(ModelForm):
    class Meta:
        model = DatosInstitucionales
        fields = '__all__'
        exclude = ('id',)



class ServiceCommissionForm(ModelForm):
    class Meta:
        model = ServiceCommission
        fields = '__all__'
        exclude = ('id',)


class ObservationsForm(ModelForm):
    class Meta:
        model = Observations
        fields = '__all__'
        exclude = ('id',)


class ObservationsFormUser(ModelForm):
    class Meta:
        model = Observations
        fields = '__all__'
        exclude = ('id', 'date', 'created_by')


class ObservationsFormUserEmployee(ModelForm):
    class Meta:
        model = Observations
        fields = '__all__'
        exclude = ('id', 'date', 'created_by', 'employee', 'type_of_observation')


class EmployeeScheduleHistoryForm(ModelForm):
    class Meta:
        model = EmployeeScheduleHistory
        fields = '__all__'
        exclude = ('id',)


class EmployeeScheduleHistoryDataForm(ModelForm):
    class Meta:
        model = EmployeeScheduleHistory
        fields = '__all__'
        exclude = ('id', 'employee', 'date_of_assignment', 'user_change', 'status')


class EmployeeForm(ModelForm):
    class Meta:
        model = Employee
        fields = '__all__'
        exclude = ('id','status','person')

class DatosInstitucionalesForm(ModelForm):
    # functions = forms.CharField(label='Funciones',required=False)
    # start_date = forms.DateField(label='Fecha de inicio', widget=forms.TextInput(attrs={'type': 'date'}))
    # end_date = forms.DateField(label='Fecha de fin', widget=forms.TextInput(attrs={'type': 'date'}))
    class Meta:
        model = DatosInstitucionales
        fields = '__all__'
        exclude = ('id', 'employee','status','collective_contract','start_date','end_date','service_time', 'functions','public_office')





class PersonForm(ModelForm):
    cedula = forms.CharField(label='# Documento *', required=True, max_length=10)
    address = forms.CharField(label='Dirección', required=False)
    reference_address = forms.CharField(label='Referencia', required=False)
    birthdate = forms.DateField(label='Fecha de nacimiento', required=False, widget=forms.TextInput(attrs={'type': 'date'}))

    # disability_has = forms.BooleanField(label='Posee alguna discapacidad ', widget=forms.CheckboxInput(attrs={'class': 'icheckbox_flat-green'}))



    class Meta:
        model = Person
        fields = '__all__'
        exclude = ('id','country','province_of_birth',
                   'province_of_residence', 'canton_of_residence',
                   'parroquia_of_residence','canton_of_birth', 'province_of_birth',
                   'parroquia_of_birth', 'avatar')



    def clean_cedula(self):
        ruc = self.cleaned_data['cedula']
        l = len(ruc)
        total = 0
        if l == 10 or l == 13:  # verificar la longitud correcta
            cp = int(ruc[0:2])
            if cp >= 1 and cp <= 22:  # verificar codigo de provincia
                tercer_dig = int(ruc[2])
                if tercer_dig >= 0 and tercer_dig < 6:  # numeros enter 0 y 6
                    if l == 10:
                        base = 10
                        d_ver = int(ruc[9])  # digito verificador
                        multip = (2, 1, 2, 1, 2, 1, 2, 1, 2)
                        tipo = 0
                    elif l == 13:
                        base = 10
                        d_ver = int(ruc[9])  # digito verificador
                        multip = (2, 1, 2, 1, 2, 1, 2, 1, 2)
                        tipo = 0
                elif tercer_dig == 6:
                    base = 11
                    d_ver = int(ruc[8])
                    multip = (3, 2, 7, 6, 5, 4, 3, 2)
                    tipo = 1
                elif tercer_dig == 9:  # si es ruc
                    base = 11
                    d_ver = int(ruc[9])
                    multip = (4, 3, 2, 7, 6, 5, 4, 3, 2)
                    tipo = 2
                else:
                    raise forms.ValidationError('Tercer digito invalido')
            else:
                raise forms.ValidationError('Codigo de provincia incorrecto')
        else:
            raise forms.ValidationError('Longitud incorrecta del numero ingresado')

        for i in range(0, len(multip)):
            p = int(ruc[i]) * multip[i]
            if tipo == 0:
                total += p if p < 10 else int(str(p)[0]) + int(str(p)[1])
            else:
                total += p
        mod = total % base
        val = base - mod if mod != 0 else 0
        return ruc




        # Formularios Curriculum
class InstruccionForm(ModelForm):

    class Meta:
        model = FormalInstruction
        fields = '__all__'
        exclude = ('id', 'curriculum_vitae')

class OficiosForm(ModelForm):
    class Meta:
        model = OfficesOrsubactivities
        fields = '__all__'
        exclude = ('id', 'curriculum_vitae')

class LenguajeForm(ModelForm):
    class Meta:
        model = Language
        fields = '__all__'
        exclude = ('id', 'curriculum_vitae')

class CapacitacionForm(ModelForm):
    documento_pdf = forms.FileField(label='Capacitaciones',widget=forms.ClearableFileInput(attrs={'multiple': True}))
    class Meta:
        model = Training
        fields = '__all__'
        exclude = ('id', 'curriculum_vitae')


class ExperienciaForm(ModelForm):
    documento_pdf = forms.FileField(label='Experiencias', widget=forms.ClearableFileInput(attrs={'multiple': True}))
    class Meta:
        model = WorkExperience
        fields = '__all__'
        exclude = ('id', 'curriculum_vitae')

    def add(self):
        documento_pdf = forms.FileField(label='Experiencia')
        return documento_pdf


class ReferenciaForm(ModelForm):
    class Meta:
        model = PersonalReferences
        fields = '__all__'
        exclude = ('id', 'curriculum_vitae')

# Datos Económicos

class DatosEconomicosForm(ModelForm):
    class Meta:
        model = EconomicData
        fields = '__all__'
        exclude = ('id', 'employee', 'collective_contract', 'budget_item', 'date_of_entry_into_roles',
                   'contract_termination_date', 'contract_number', 'contract_start_date',
                   'date_of_admission','start_date','end_date','labor_system','remuneration','post',
                   'occupational_group','grade','type_used')


class cuentaBancariaForm(ModelForm):
    class Meta:
        model = BankAccounts
        fields = '__all__'
        exclude = ('id', 'employee')


class FilterInstitucionForm(forms.Form):
    institucion = forms.ModelChoiceField(queryset=Institucion.objects.all(), label='Institución',  widget=forms.Select(attrs={"onChange":'obtenerItem()'}))
    directivo = forms.ModelChoiceField(queryset=Directivo.objects, required=False, disabled=True, label='Directivo', widget=forms.Select(attrs={"onChange": 'obtenerItem()'}))
    direccion = forms.ModelChoiceField(queryset=Direccion.objects, required=False, disabled=True, label='Dirección', widget=forms.Select(attrs={"onChange": 'obtenerItem()'}))
    jefatura = forms.ModelChoiceField(queryset=JefaturaCoordinacion.objects, required=False, disabled=True, label='Jefatura', widget=forms.Select(attrs={"onChange": 'obtenerItem()'}))


    def __init__(self, *args,data=None, **kwargs):
        if data:
            super(FilterInstitucionForm, self).__init__(*args,**kwargs)

            if data['institucion']:
                id_institucion = data['institucion']

                directivos = Directivo.objects.filter(institucion=id_institucion)
                if directivos:
                    self.fields['directivo'].disabled = False
                    self.fields['directivo'].queryset = directivos

            if data['directivo']:
                id_directivo = data['directivo']
                direcciones = Direccion.objects.filter(directivo=id_directivo)
                if direcciones:
                    self.fields['direccion'].disabled = False
                    self.fields['direccion'].queryset = direcciones
                    self.fields['jefatura'].queryset = JefaturaCoordinacion.objects
                else:
                    self.fields['direccion'].queryset = direcciones
                    self.fields['jefatura'].queryset = JefaturaCoordinacion.objects


            else:
                self.fields['direccion'].disabled = True

            if data['direccion']:
                id_direccion = data['direccion']
                jefaturas = JefaturaCoordinacion.objects.filter(direccion=id_direccion)
                if jefaturas:
                    self.fields['jefatura'].disabled = False
                    self.fields['jefatura'].queryset = jefaturas
            else:
                self.fields['jefatura'].disabled = True


            if data['directivo'] == "" or data['direccion'] == "":
                # self.fields['direccion'].disabled = True
                self.fields['direccion'].disabled = True
                self.fields['jefatura'].disabled = True

        # self.fields['direccion'].widget=forms.ChoiceField(choises=direcciones)
            # self.fields['direccion'].widget.attrs['choises'] = direcciones

        else:
            super(FilterInstitucionForm, self).__init__(*args,  data=data, **kwargs)




