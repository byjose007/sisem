from django.apps import AppConfig


class PersonnelActionsConfig(AppConfig):
    name = 'apps.personnel_actions'
