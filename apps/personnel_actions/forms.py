# -*- coding: utf-8 -*-
from django import forms
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout
from .models import KindPersonnelActions, PersonnelActions


class KindPersonnelActionsForm(ModelForm):
	
	class Meta:
		model = KindPersonnelActions
		fields = '__all__'
		exclude = ('id',)

class PersonnelActionsForm(ModelForm):
	
	class Meta:
		model = PersonnelActions
		fields = '__all__'
		exclude = ('id',)


