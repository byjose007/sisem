# -*- coding: utf-8 -*-
from django.conf.urls import url, include

from .views import (
    kind_personnel_actions_list,
    kind_personnel_actions_create,
    kind_personnel_actions_update,
    kind_personnel_actions_delete,

    personnel_actions_list,
    personnel_actions_create,
    personnel_actions_update,
    personnel_actions_delete,
    personnel_actions_pdf,
    list_new_personnel_action_type,
)


urlpatterns = [

    #urls admin
    
    url(r'^kindpersonnelactions/admin/$', kind_personnel_actions_list, name='kindpersonnelactions_list'),
    url(r'^kindpersonnelactions/admin/new$', kind_personnel_actions_create, name='kindpersonnelactions_new'),
    url(r'^kindpersonnelactions/admin/editar/(?P<pk>\d+)$', kind_personnel_actions_update, name='kindpersonnelactions_edit'),      
    url(r'^kindpersonnelactions/admin/(?P<pk>\d+)$',kind_personnel_actions_delete, name='kindpersonnelactions_delete'),

    url(r'^admin/$', personnel_actions_list, name='personnelactions_list'),
    url(r'^admin/new/(?P<pk>\d+)$$', personnel_actions_create, name='personnelactions_new'),
    url(r'^admin/personnelactions/new/list/$', list_new_personnel_action_type, name='personnelactions_new_list'),
    url(r'^admin/editar/(?P<pk>\d+)$', personnel_actions_update, name='personnelactions_edit'),      
    url(r'^admin/(?P<pk>\d+)$',personnel_actions_delete, name='personnelactions_delete'),
    url(r'^admin/pdf/(?P<pk>\d+)$',personnel_actions_pdf, name='personnelactions_pdf'),
        
]
