from django.db import models
from apps.institution.models import Institucion, JefaturaCoordinacion, Directivo, Direccion
# from apps.institution.models import InstitutionalUnit, Department
from apps.employee.models import Employee
from apps.contract.models import LaborSystem, PayScalesSp


# Create your models here.
class KindPersonnelActions(models.Model):
    action = models.CharField(max_length=50, verbose_name='Acción')
    labor_system = models.ForeignKey(LaborSystem, verbose_name='Régimen Laboral', on_delete=models.PROTECT)
    valid = models.BooleanField(verbose_name='Estado de la acción', default=True)

    cs_institutional_unit = models.BooleanField(verbose_name='Unidad administrativa act.', default=True)
    cs_department = models.BooleanField(verbose_name='Departamento act.', default=True)
    cs_public_office = models.BooleanField(verbose_name='Cargo público act.', default=True)
    cs_occupational_group = models.BooleanField(verbose_name='Grupo Ocupacional act.', default=True)
    cs_workplace = models.BooleanField(verbose_name='Lugar de trabajo act.', default=True)
    cs_remuneration = models.BooleanField(verbose_name='Remuneración act', default=True)
    cs_budget_item = models.BooleanField(verbose_name='Partida presupuestaria act.', default=True)

    ps_institutional_unit = models.BooleanField(verbose_name='Unidad administrativa pro.', default=True)
    ps_department = models.BooleanField(verbose_name='Departamento pro.', default=True)
    ps_public_office = models.BooleanField(verbose_name='Cargo público pro.', default=True)
    ps_occupational_group = models.BooleanField(verbose_name='Grupo Ocupacional pro.', default=True)
    ps_workplace = models.BooleanField(verbose_name='Lugar de trabajo pro.', default=True)
    ps_remuneration = models.BooleanField(verbose_name='Remuneración pro', default=True)
    ps_budget_item = models.BooleanField(verbose_name='Partida presupuestaria pro.', default=True)

    class Meta:
        ordering = ['action', 'labor_system']
        verbose_name = 'Tipo Acción de Personal'
        verbose_name_plural = 'Tipo de Acciones de Personal'

    def __str__(self):
        return u'{0} - {1}'.format(self.action, self.labor_system)

    def __unicode__(self):
        return u'{0} - {1}'.format(self.action, self.labor_system)


class PersonnelActions(models.Model):
    decreto = 'decreto'
    acuerdo = 'acuerdo'
    resolucion = 'resolución'
    reason_decision_list = (
        (decreto, 'Decreto'),
        (acuerdo, 'Acuerdo'),
        (resolucion, 'Resolución'),
    )
    emitida = 'emitida'
    registrada = 'registrada'

    status_list = (
        (emitida, 'Emitida'),
        (registrada, 'Registrada'),
    )

    reason_decision = models.CharField(max_length=13, verbose_name='Tipo', choices=reason_decision_list,
                                       default=decreto, blank=True, null=True)
    number_personnel_action = models.IntegerField(verbose_name='Número', blank=True, null=True)
    date_of_elaboration = models.DateField(verbose_name='Fecha de elaboración', blank=True, null=True)
    effective_date = models.DateField(verbose_name='Fecha vigencia')
    employee = models.ForeignKey(Employee, verbose_name='Empleado')
    action = models.ForeignKey('KindPersonnelActions', verbose_name='Acción', on_delete=models.PROTECT)
    document_generates_pa = models.CharField(max_length=250, verbose_name='Documento que genera la acción de personal')
    explanation = models.TextField(verbose_name='Explicación')

    cs_institutional_unit = models.ForeignKey(Directivo, verbose_name='Unidad administrativa actual',
                                              blank=True, null=True, on_delete=models.PROTECT)
    cs_department = models.ForeignKey(Direccion, verbose_name='Departamento actual', blank=True, null=True,
                                      on_delete=models.PROTECT)
    cs_public_office = models.CharField(max_length=50, verbose_name='Cargo Público actual', blank=True, null=True)
    cs_occupational_group = models.ForeignKey(PayScalesSp, verbose_name='Grupo Ocupacional actual', blank=True,
                                              null=True)
    cs_workplace = models.CharField(max_length=100, verbose_name='Lugar de trabajo actual', blank=True, null=True)
    cs_remuneration = models.DecimalField(verbose_name='Remuneración actual', max_digits=6, decimal_places=2,
                                          blank=True, null=True)
    cs_budget_item = models.CharField(verbose_name='Partida presupuestaria actual', max_length=100, blank=True,
                                      null=True)

    ps_institutional_unit = models.ForeignKey(Directivo, verbose_name='Unidad administrativa propuesta',
                                              related_name="ps_institutional_unit", blank=True, null=True,
                                              on_delete=models.PROTECT)
    ps_department = models.ForeignKey(Direccion, verbose_name='Departamento propuesta', related_name="ps_department",
                                      blank=True, null=True, on_delete=models.PROTECT)
    ps_public_office = models.CharField(max_length=50, verbose_name='Cargo Público propuesta', blank=True, null=True)
    ps_occupational_group = models.ForeignKey(PayScalesSp, verbose_name='Grupo Ocupacional propuesta',
                                              related_name="ps_occupational_group", blank=True, null=True,
                                              on_delete=models.PROTECT)
    ps_workplace = models.CharField(max_length=100, verbose_name='Lugar de trabajo propuesta', blank=True, null=True)
    ps_remuneration = models.DecimalField(verbose_name='Remuneración propuesta', max_digits=6, decimal_places=2,
                                          blank=True, null=True)
    ps_budget_item = models.CharField(verbose_name='Partida presupuestaria propuesta', max_length=100, blank=True,
                                      null=True)

    created_by = models.CharField(max_length=13, verbose_name='Elaborado Por', blank=True, null=True)
    disposed = models.CharField(max_length=13, verbose_name='Dispuesto por')
    registration_and_control = models.CharField(max_length=13, verbose_name='Registro y Control')
    status = models.CharField(max_length=15, verbose_name='Estado', choices=status_list, default=emitida)

    class Meta:
        ordering = ['employee', 'pk', 'number_personnel_action']
        verbose_name = 'Acción de Personal'
        verbose_name_plural = 'Acciones de Personal'

    def __str__(self):
        return u'{0}'.format(self.employee)

    def __unicode__(self):
        return u'{0}'.format(self.employee)
