from django.contrib import admin
from apps.personnel_actions.models import KindPersonnelActions
from apps.personnel_actions.models import PersonnelActions

# Register your models here.

class KindPersonnelActionsAdmin(admin.ModelAdmin):
	list_display = ('action','labor_system','valid')
	search_fields = ('action','labor_system','valid')

class PersonnelActionsAdmin(admin.ModelAdmin):
	list_display = ('employee','action','number_personnel_action')
	search_fields = ('employee','action','number_personnel_action')


admin.site.register(KindPersonnelActions,KindPersonnelActionsAdmin)
admin.site.register(PersonnelActions,PersonnelActionsAdmin)


