# -*- coding: utf-8 -*-
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from apps.institution.models import Institucion,JefaturaCoordinacion,Directivo,Direccion
from .forms import KindPersonnelActionsForm, PersonnelActionsForm
from .models import KindPersonnelActions, PersonnelActions
from django.http import HttpResponse
from django.views.generic import FormView, TemplateView
from django.contrib.messages.views import SuccessMessageMixin
from apps.accounts.models import UserProfile
from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4, cm
from reportlab.lib.colors import pink, black, red, blue, green
from apps.contract.models import LaborSystem
# Create your views here.

#Funciones admistrador

#-------Acciones de personal  - Tipos de accion de personal  ----------
def kind_personnel_actions_list(request, template_name='personnelactions/kind_personnel_actions_admin.html'):
    kind_personnel_actions = KindPersonnelActions.objects.all()
    context = {
        "data": kind_personnel_actions,
        "accion": 'listar',
    }
    return render(request,template_name , context)

def kind_personnel_actions_create(request, template_name='personnelactions/kind_personnel_actions_admin.html'):
    form = KindPersonnelActionsForm(request.POST or None)
    context = {
        "accion": 'nuevo',
        "form": form,
    }
    if form.is_valid():
        form.save()
        return redirect('personnelactions:kindpersonnelactions_list')
    return render(request, template_name, context)

def kind_personnel_actions_update(request, pk, template_name='personnelactions/kind_personnel_actions_admin.html'):
    kind_personnel_actions = get_object_or_404(KindPersonnelActions, pk=pk)
    form = KindPersonnelActionsForm(request.POST or None, instance=kind_personnel_actions)
    if form.is_valid():
        form.save()
        return redirect('personnelactions:kindpersonnelactions_list')

    context = {
        "accion": 'editar',
        "form": form,
    }
    return render(request, template_name, context)

def kind_personnel_actions_delete(request, pk, template_name='personnelactions/kind_personnel_actions_admin.html'):
    kind_personnel_actions = get_object_or_404(KindPersonnelActions, pk=pk)
    if request.method=='POST':
        kind_personnel_actions.delete()
        return redirect('personnelactions:kindpersonnelactions_list')
    context = {
        "accion": 'eliminar',
        "form": kind_personnel_actions,
    }
    return render(request, template_name, context)



#-------Acciones de personal 
def personnel_actions_list(request, template_name='personnelactions/personnel_actions_admin.html'):
    personnel_actions = PersonnelActions.objects.all()
    paginator = Paginator(personnel_actions, 10) # Show 25 contacts per page
    page = request.GET.get('page')
    query =   'asd'
    data = {}
    try:
        personnel_actions = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        personnel_actions = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        personnel_actions = paginator.page(paginator.num_pages)

    

    data['object_list'] = personnel_actions
    data['query'] = query
    data['accion'] = 'listar'
    data['titulo'] = 'Acciones de Personal'

    return render(request,template_name , data)

#    context = {
#        "data": personnel_actions_p,
#        #data['object_list']: personnel_actions_p,
#        "accion": 'listar',
#    }
 #   return render(request,template_name , context)







def list_new_personnel_action_type(request, template_name='personnelactions/new_personnel_actions_list.html'):
    data = {}
    labor_system = LaborSystem.objects.all()
    kind_personnel_actions = KindPersonnelActions.objects.filter(valid=True)
    data['kind_personnel_actions'] = kind_personnel_actions
    data['labor_system'] = labor_system
    return render(request,template_name, data)

def personnel_actions_create(request, pk,template_name='personnelactions/personnel_actions_admin.html'):
    print ("Estamos en crear accion")
    try:
        kind_personnel_actions = KindPersonnelActions.objects.get(pk=pk)
        form = PersonnelActionsForm(request.POST or None)
        titulo = "Acción de personal - "+kind_personnel_actions.labor_system.labor_system+" - "+kind_personnel_actions.action
        context = {
            "accion": 'nuevo',
            "form": form,
            "titulo": titulo,
            "kind_personnel_actions":kind_personnel_actions,
        }

        if form.is_valid():
            print ('Formulario valido')
            personnel_actions = form.save(commit=False)
            personnel_actions.created_by = 'Cambiar'
            print (personnel_actions)
            personnel_actions.save()
            return redirect('personnelactions:personnelactions_list')
        else:
            print ('Formulario invalido')
            print (form)
    except Exception as e:
        print ("Error")
        print (e)
    finally:
        print ("final")
        Final="Todas las operaciones ejecutadas con exito"
    
    return render(request, template_name, context)

    



def personnel_actions_update(request, pk, template_name='personnelactions/personnel_actions_admin.html'):
    personnel_actions = get_object_or_404(PersonnelActions, pk=pk)
    form = PersonnelActionsForm(request.POST or None, instance=personnel_actions)
    if form.is_valid():
        form.save()
        return redirect('personnelactions:personnelactions_list')

    context = {
        "accion": 'editar',
        "form": form,
    }
    return render(request, template_name, context)

def personnel_actions_delete(request, pk, template_name='personnelactions/personnel_actions_admin.html'):
    personnel_actions = get_object_or_404(PersonnelActions, pk=pk)
    if request.method=='POST':
        personnel_actions.delete()
        return redirect('personnelactions:personnelactions_list')
    context = {
        "accion": 'eliminar',
        "form": personnel_actions,
    }
    return render(request, template_name, context)


def personnel_actions_pdf(request, pk):
    personnel_actions = get_object_or_404(PersonnelActions, pk=pk)
    form = PersonnelActionsForm(request.POST or None, instance=personnel_actions)
    
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename='+personnel_actions.employee.person.name+'_'+personnel_actions.employee.person.lastname+'.pdf'
    buffer = BytesIO()
    c = canvas.Canvas(buffer,pagesize=A4)
    #Header 
    c.setLineWidth(.3)
    c.setFont('Helvetica',64)
    c.drawString(80,500,'Acción de Personal')
    c.setFont('Helvetica', 18)
    #c.drawString(200,450,curriculum_vitae.person.name+" "+curriculum_vitae.person.lastname)
    c.showPage()
    c.setFont('Helvetica',10)
    #c.drawString(300,810,'Currículo Vitae - '+curriculum_vitae.person.name+" "+curriculum_vitae.person.lastname)
    
    high = 765
    
    
    #-------------------------------------Datos Personales------------------
 
    c.save()
    pdf = buffer.getvalue()
    buffer.close()
    response.write(pdf)
    

    context = {
        "accion": 'editar',
        "form": form,
    }
    return response