from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse_lazy, reverse
from django.shortcuts import get_object_or_404, redirect, render
from django.http import HttpResponse, Http404
# from .forms import BookmarkForm
# from .models import Bookmark
import datetime
from django.db.models.query_utils import Q
from django import template
from django.contrib.auth.models import Group
from django.template.loader import get_template
from django.template import Context
from apps.accounts.models import UserProfile
from apps.employee.models import Employee
from apps.partidas.models import Partida, Catalogo, Item

HTML = """
<!DOCTYPE html>
<html lang="es">
<head>
<meta httpequiv="
contenttype"
content="text/html; charset=utf8">
<meta name="robots" content="NONE,NOARCHIVE">
<title>Hola mundo</title>
<style type="text/css">
html * { padding:0; margin:0; }
body * { padding:10px 20px; }
body * * { padding:0; }
body { font:small sansserif;
}
body>div { borderbottom:
1px solid #ddd; }
h1 { fontweight:
normal; }
#summary { background: #e0ebff; }
</style>
</head>
<body>
<div id="summary">
<h1 style="color:black; background-color: green;" >¡Hola Mundo!</h1>
</div>
</body></html> """


def security_welcome(request):
    return render(request, 'welcome.html')




@login_required
def security_main(request):
    # print('>>> intro security_main')
    qset = (Q())
    u = request.user
    perms = list(u.user_permissions.all())
    ctypes = list(ContentType.objects.all().distinct('app_label'))
    print('----------------------')
    print(perms)
    print('>>>>>>>>>>>>>>>>>>>>>>')
    print(ctypes)
    print('>>>>>>>>>>>>>>>>>>>>>>')

    l = []
    for p in perms:
        l.append(p.content_type.id)

    print(l)
    print('>>>>>>>>>>>>>>>>>>>>>>')

    x = list(ContentType.objects.filter(pk__in=l).distinct('app_label'))
    y = list(ContentType.objects.filter(pk__in=l))

    print(x)
    print('>>>>>>>>>>>>>>>>>>>>>>')
    print(y)
    print('>>>>>>>>>>>>>>>>>>>>>>')

    menu = []

    for a in x:
        submenu = []
        for b in y:
            if a.app_label == b.app_label:
                print('Entro' + a.app_label)
                submenu.append(b)

        menu.append(submenu)

    print(menu)
    print('----------------------')

    user_profile = UserProfile.objects.get(user=request.user)
    institution = user_profile.institution_default
    request.session['institution_id'] = institution.id

    request.session['evaluation_id'] = 5

    photo = user_profile.photo
    topEmpleados = Employee.objects.all().order_by('-id')[:5]

    # Contador de partidas

    partidasT = Partida.objects.filter(clonedto=None)
    partidasLib = Partida.objects.filter(clonedto=None,status_itm=Item.objects.get(code='EST_LIBRE'))
    partidasO = Partida.objects.filter(clonedto=None,status_itm=Item.objects.get(code='EST_OCUPADA'))
    partidasLit = Partida.objects.filter(clonedto=None,status_itm=Item.objects.get(code='EST_LITIGIO'))
    partidasC = Partida.objects.filter(clonedto=None,status_itm=Item.objects.get(code='EST_CONCURSO'))
    partidasI = Partida.objects.filter(clonedto=None,status_itm=Item.objects.get(code='EST_INACTIVA'))

    partidasTotal=partidasT.count()
    partidasLibres= partidasLib.count()
    partidasOcupadas = partidasO.count()
    partidasLitigio = partidasLit.count()
    partidasConcurso = partidasC.count()
    partidasInactivas = partidasI.count()

    porcLibres = round(((partidasLibres*100)/partidasTotal),2)
    porcOcupadas = round(((partidasOcupadas*100)/partidasTotal),2)
    porcLitigio = round(((partidasLitigio*100)/partidasTotal),2)
    porcConcurso = round(((partidasConcurso*100)/partidasTotal),2)
    porcInactivas = round(((partidasInactivas*100)/partidasTotal),2)

    # fin contador partidas

    context = {'per': perms, 'n': len(menu), 'user_photo': photo, 'empleados': topEmpleados, 'partidasLibres':partidasLibres
        ,'partidasOcupadas':partidasOcupadas,'partidasLitigio':partidasLitigio,'partidasConcurso':partidasConcurso
        ,'partidasInactivas':partidasInactivas,'partidasTotal':partidasTotal, 'porcLibres':porcLibres , 'porcOcupadas':porcOcupadas
        , 'porcLitigio': porcLitigio, 'porcInactivas':porcInactivas, 'porcConcurso':porcConcurso}
    return render(request, 'app/index2.html', context)  # main.html


def hola(request):
    return HttpResponse(HTML)


def fecha_actual(request):
    ahora = datetime.datetime.now()
    # t = get_template('mi_plantilla.html')
    # html = t.render(Context({'ahora':ahora}))
    # return HttpResponse(html)
    context = {'ahora': ahora}
    return render(request, 'fecha.django', context)


def horas_adelante(request, horas):
    try:
        horas = int(horas)
    except ValueError:
        raise Http404()
    dt = datetime.datetime.now() + datetime.timedelta(hours=horas)
    # assert False
    # html = "<html><body><h1>En %s hora(s), seran:</h1> <h3>%s</h3></body></html>" % (offset, dt)
    # return HttpResponse(html)
    return render(request, 'horas_adelante.django', {'hora_siguiente': dt, 'horas': horas})


def security_logout(request):
    print('>>> intro security_logout')
    print('evaluation_id: %s' % (request.session['evaluation_id']))
    try:
        del request.session['evaluation_id']
        del request.session['institution_id']
    except KeyError:
        print('>>> KeyError')
        pass
    logout(request)
    # messages.success(request, 'La session ha finalizado correctamente.')
    # url = reverse('security_logout')
    # return HttpResponseRedirect(url)
    return redirect(reverse('security_main'))
