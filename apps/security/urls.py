from django.conf.urls import url
import django.contrib.auth.views as auth_views
import apps.security.views as security


from django.contrib.auth import views as auth_views
from apps.accounts.forms import CustomAuthForm


urlpatterns = [
    url(r'^$', security.security_main, name='security_main'),
    # url(r'^login/$', auth_views.login, {'template_name': 'app/login.html'}, name='security_login'),
    url(r'^login/$', auth_views.login, name='login', kwargs={"authentication_form":CustomAuthForm, 'template_name': 'app/login.html'}),
    # url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page': reverse_lazy('security_main')}, name='security_logout'),
    url(r'^logout/$', security.security_logout, name='security_logout'),
    #url(r'^marcador/$', 'marcador.views.bookmark_list', name='marcador_bookmark_list'),
    #url(r'^user/(?P<username>[-\w]+)/$', 'marcador.views.bookmark_user',
    #    name='marcador_bookmark_user'),
    #url(r'^create/$', 'marcador.views.bookmark_create',
    #    name='marcador_bookmark_create'),
    #url(r'^edit/(?P<pk>\d+)/$', 'marcador.views.bookmark_edit',
    #    name='marcador_bookmark_edit'),
    url(r'^hola/$', security.hola, name='security_hola'),
    url(r'^security/fecha/$', security.fecha_actual, name='security_fecha'),
    url(r'^security/fecha/mas/(\d{1,2})/$', security.horas_adelante, name='security_horas_adelante'),
]


