from django.apps import AppConfig


class InstitutionConfig(AppConfig):
    name = 'apps.institution'
