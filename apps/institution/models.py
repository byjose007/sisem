# -*- coding: utf-8 -*-
from django.db import models


# Create your models here.
from django.forms.widgets import CheckboxInput


class Institucion(models.Model):
    ruc = models.CharField(verbose_name="Ruc*",max_length=13, unique=True)
    nombre = models.CharField(verbose_name="Nombre*", max_length=100)
    locacion = models.CharField(verbose_name='Dirección', max_length=100, blank=True, null=True)
    telefono = models.CharField(verbose_name='Teléfono', max_length=13, blank=True, null=True)
    estado = models.CharField(verbose_name='Estado', default='ACTIVO', max_length=13, choices=[('ACTIVO', 'ACTIVO'), ('INACTIVO', 'INACTIVO')])
    # estado =models.(verbose_name='Estado', blank=True, max_length=13, choices=[('ACTIVO', 'ACTIVO'), ('INACTIVO', 'INACTIVO')])
    class Meta:
        ordering = ['nombre']
        verbose_name = 'Institución'
        verbose_name_plural = 'Instituciones'

    def __str__(self):
        return u'{0}'.format(self.nombre)

    def __unicode__(self):
        return u'{0}'.format(self.nombre)

#  Modelo Direccion representa al Nivel Directivo
class Directivo(models.Model):
    institucion = models.ForeignKey('Institucion', verbose_name='Institución*', on_delete=models.PROTECT)
    nombre = models.CharField(verbose_name='Nombre*', max_length=100)
    locacion = models.CharField(verbose_name='Dirección', max_length=100, blank=True, null=True)
    telefono = models.CharField(verbose_name='Teléfono', max_length=13, blank=True, null=True)
    estado = models.CharField(verbose_name='Estado', default='ACTIVO', blank=True, max_length=13, choices=[('ACTIVO', 'ACTIVO'), ('INACTIVO', 'INACTIVO')])


    class Meta:
        ordering = ['institucion', 'nombre']
        verbose_name = 'Nivel Directivo'
        verbose_name_plural = 'Niveles Directivos'

    def __str__(self):
        return u'{0}'.format(self.nombre)

    def __unicode__(self):
        return u'{0}'.format(self.nombre)

#  Modelo Direccion representa a DireccionesInstitucionales
class Direccion(models.Model):
    directivo = models.ForeignKey(Directivo, verbose_name='Nivel Directivo*',
                                  on_delete=models.PROTECT)
    nombre = models.CharField(verbose_name='Nombre*', max_length=100)
    locacion = models.CharField(verbose_name='Dirección', max_length=100, blank=True, null=True)
    telefono = models.CharField(verbose_name='Teléfono', max_length=13, blank=True, null=True)
    estado = models.CharField(verbose_name='Estado', default='ACTIVO', blank=True, max_length=13, choices=[('ACTIVO', 'ACTIVO'), ('INACTIVO', 'INACTIVO')])

    class Meta:
        ordering = ['directivo', 'nombre']
        verbose_name = 'Direccion'
        verbose_name_plural = 'Direcciones'

    def __str__(self):
        return u'{0} '.format(self.nombre)

    def __unicode__(self):
        return u'{0}'.format(self.nombre)

#  Modelo Direccion representa al jefaturas o corrdinaciones
class JefaturaCoordinacion(models.Model):
    direccion = models.ForeignKey(Direccion, verbose_name='Dirección Institucional',
                                  on_delete=models.PROTECT)
    nombre = models.CharField(verbose_name='Nombre', max_length=100)
    locacion = models.CharField(verbose_name='Dirección', max_length=100, blank=True, null=True)
    telefono = models.CharField(verbose_name='Teléfono', max_length=13, blank=True, null=True)
    estado = models.CharField(verbose_name='Estado', default='ACTIVO', blank=True, max_length=13, choices=[('ACTIVO', 'ACTIVO'), ('INACTIVO', 'INACTIVO')])

    class Meta:
        ordering = ['direccion', 'nombre']
        verbose_name = 'Dirección'
        verbose_name_plural = 'Direciones'

    def __str__(self):
        return u'{0}'.format(self.nombre)

    def __unicode__(self):
        return u'{0}'.format(self.nombre)
