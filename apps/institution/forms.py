# -*- coding: utf-8 -*-
# from dal import autocomplete
from django import forms
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout
from .models import Institucion, JefaturaCoordinacion, Direccion, Directivo
from django.contrib import messages


class InstitucionForm(ModelForm):
    def clean(self):
        # Sobrecargar clean devuelve un diccionario con los campos
        cleaned_data = super(InstitucionForm, self).clean()
        ruc = cleaned_data.get("ruc")
        institucion = cleaned_data.get("nombre")

        if ruc == None:
            # messages.warning('ruc', 'Ruc no válido. Debe contener 13 caracteres!')
            self.add_error('ruc', 'Ruc no válido. Debe contener 13 caracteres!')
        elif len(ruc) != 13:
            # messages.warning('ruc', 'Ruc no válido. Debe contener 13 caracteres!')
            self.add_error('ruc', 'Ruc no válido. Debe contener 13 caracteres!')

        elif institucion == None:
            self.add_error('nombre', 'Nombre de la institución debe contener al menos 3 caracteres!')

        elif len(institucion) < 3:
            self.add_error('nombre', 'Nombre de la institución debe contener al menos 3 caracteres!')


        elif isfloat(ruc):
            self.add_error('ruc', 'Caracteres no válidos!')

        # raise forms.ValidationError("")
        return cleaned_data

    class Meta:
        model = Institucion
        fields = '__all__'
        exclude = ('id',)



        # widgets = {
        #     'nombre': autocomplete.ModelSelect2(url='employee:instituciones-autocomplete')
        # }


class InstitucionEditarForm(ModelForm):
    # def __init__(self, *args, **kwargs):
    #     super(InstitucionEditarForm, self).__init__(*args, **kwargs)
    #     instance = getattr(self, 'instance', None)
    #     if instance and instance.id:
    #         self.fields['ruc'].widget.attrs['readonly'] = True

    def clean(self):
        # Sobrecargar clean devuelve un diccionario con los campos
        cleaned_data = super(InstitucionEditarForm, self).clean()
        ruc = cleaned_data.get("ruc")
        institucion = cleaned_data.get("nombre")

        if ruc == None:
            self.add_error('ruc', 'Ruc no válido. Debe contener 13 caracteres!')
        elif len(ruc) != 13:
            self.add_error('ruc', 'Ruc no válido. Debe contener 13 caracteres!')

        elif institucion == None:
            self.add_error('institucion', 'Nombre de la institución debe contener al menos 3 caracteres!')

        elif len(institucion) < 3:
            self.add_error('institucion', 'Nombre de la institución debe contener al menos 3 caracteres!')


        elif isfloat(ruc):
            self.add_error('ruc', 'Caracteres no válidos!')

        # raise forms.ValidationError("")
        return cleaned_data

    class Meta:
        model = Institucion
        fields = '__all__'
        exclude = ('id',)
        widgets = {
            'ruc': forms.TextInput(attrs={'readonly': 'readonly'})
        }


def isfloat(value):
    try:
        float(value)
        return False
    except:
        return True


class DirectivoForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DirectivoForm, self).__init__(*args, **kwargs)
        self.fields['institucion'].queryset = Institucion.objects.filter(estado='ACTIVO')

    def clean(self):
        cleaned_data = super(DirectivoForm, self).clean()
        directivoNuevo = cleaned_data.get("nombre")
        institucionNueva = cleaned_data.get("institucion")
        directivoExistente = Directivo.objects.filter(nombre=directivoNuevo).values('nombre')
        institucionExistente = Institucion.objects.filter(nombre=institucionNueva).values('nombre')

        if len(directivoNuevo) < 3:
            self.add_error('nombre',
                           'Nombre del nivel directivo debe contener al menos 3 caracteres!')
        elif directivoExistente and institucionExistente:
            self.add_error('nombre',
                           'El nivel directivo ya existe. Por favor ingrese otro nombre')

        return cleaned_data

    class Meta:
        model = Directivo
        fields = '__all__'
        exclude = ('id',)


class DirectivoEditarForm(ModelForm):
    def clean(self):
        # Sobrecargar clean devuelve un diccionario con los campos
        cleaned_data = super(DirectivoEditarForm, self).clean()
        directivo = cleaned_data.get("nombre")

        if directivo == None:
            self.add_error('nombre',
                           'Nombre del nivel directivo debe contener al menos 3 caracteres!')
        elif len(directivo) < 3:
            self.add_error('nombre',
                           'Nombre del nivel directivo debe contener al menos 3 caracteres!')

        # raise forms.ValidationError("")
        return cleaned_data

    class Meta:
        model = Directivo
        fields = '__all__'
        exclude = ('id',)


class DirecccionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(DirecccionForm, self).__init__(*args, **kwargs)
        self.fields['directivo'].queryset = Directivo.objects.filter(estado='ACTIVO')

    def clean(self):
        cleaned_data = super(DirecccionForm, self).clean()
        direccionNueva = cleaned_data.get("nombre")
        directivoNuevo = cleaned_data.get("directivo")
        direccionExistente = Direccion.objects.filter(nombre=direccionNueva).values('nombre')
        directivoExistente = Directivo.objects.filter(nombre=directivoNuevo).values('nombre')

        if direccionNueva == None:
            self.add_error('nombre',
                           'Nombre de la dirección institucional debe contener al menos 3 caracteres!')
        elif len(direccionNueva) < 3:
            self.add_error('nombre',
                           'Nombre de la dirección institucional debe contener al menos 3 caracteres!')
        elif direccionExistente and directivoExistente:
            self.add_error('nombre',
                           'La dirección institucional ya existe. Por favor ingrese otro nombre')
        return cleaned_data

    class Meta:
        model = Direccion
        fields = '__all__'
        exclude = ('id',)


class JefaturanEditarForm(ModelForm):
    def clean(self):
        # Sobrecargar clean devuelve un diccionario con los campos
        cleaned_data = super(JefaturanEditarForm, self).clean()
        jefatura = cleaned_data.get("nombre")

        if jefatura == None:
            self.add_error('nombre',
                           'Nombre de la dirección debe contener al menos 3 caracteres!')
        elif len(jefatura) < 3:
            self.add_error('nombre',
                           'Nombre de la Dirección debe contener al menos 3 caracteres!')

        # raise forms.ValidationError("")
        return cleaned_data

    class Meta:
        model = Direccion
        fields = '__all__'
        exclude = ('id',)


class CoordinacionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(CoordinacionForm, self).__init__(*args, **kwargs)
        self.fields['direccion'].queryset = Direccion.objects.filter(estado='ACTIVO')

    def clean(self):
        cleaned_data = super(CoordinacionForm, self).clean()


        coordinacionNueva = cleaned_data.get("nombre")
        jefaturaNueva = cleaned_data.get("direccion")
        coordinacionExistente = JefaturaCoordinacion.objects.filter(nombre=coordinacionNueva, direccion=jefaturaNueva.id).values('nombre')


        if coordinacionNueva == None:
            self.add_error('nombre', 'Nombre de la Jefatura debe contener al menos 3 caracteres!')
        elif len(coordinacionNueva) < 3:
            self.add_error('nombre', 'Nombre de la Jefatura debe contener al menos 3 caracteres!')
        elif coordinacionExistente:
            self.add_error('nombre',
                           'La Jefatura o Coordinación ya existe. Por favor ingrese un nombre diferente')
        return cleaned_data

    class Meta:
        model = JefaturaCoordinacion
        fields = '__all__'
        exclude = ('id',)


class CoordinacionEditarForm(ModelForm):
    def clean(self):
        # Sobrecargar clean devuelve un diccionario con los campos
        cleaned_data = super(CoordinacionEditarForm, self).clean()
        coordinacion = cleaned_data.get("nombre")

        if coordinacion == None:
            self.add_error('nombre', 'Nombre de la coordinación debe contener al menos 3 caracteres!')
        elif len(coordinacion) < 3:
            self.add_error('nombre', 'Nombre de la coordinación debe contener al menos 3 caracteres!')

        return cleaned_data

    class Meta:
        model = JefaturaCoordinacion
        fields = '__all__'
        exclude = ('id',)


class DireccionFormUsuario(ModelForm):
    def clean(self):
        cleaned_data = super(DireccionFormUsuario, self).clean()
        direccion = cleaned_data.get("nombre")

        if direccion == None:
            self.add_error('nombre',
                           'Nombre de la unidad institucional debe contener al menos 3 caracteres!')
        elif len(direccion) < 3:
            self.add_error('nombre',
                           'Nombre de la unidad institucional debe contener al menos 3 caracteres!')

        return cleaned_data

    class Meta:
        model = Directivo
        fields = '__all__'
        exclude = ('id', 'institucion',)


class JefaturaFormUsuario(ModelForm):
    def clean(self):
        cleaned_data = super(JefaturaFormUsuario, self).clean()
        jefatura = cleaned_data.get("nombre")

        if jefatura == None:
            self.add_error('nombre', 'Nombre de la unidad institucional debe contener al menos 3 caracteres!')
        elif len(jefatura) < 3:
            self.add_error('nombre', 'Nombre de la unidad institucional debe contener al menos 3 caracteres!')

        return cleaned_data

    def __init__(self, *args, **kwargs):
        direccion = kwargs.pop('nombre')
        print('asasssss')
        super(JefaturaFormUsuario, self).__init__(*args, **kwargs)
        self.fields['direccion'].queryset = Directivo.objects.filter(institucion=direccion)

    class Meta:
        model = Direccion
        fields = '__all__'
        exclude = ('id',)


class CoordinacionFormUsuario(ModelForm):
    def clean(self):
        cleaned_data = super(CoordinacionFormUsuario, self).clean()
        coordinacion = cleaned_data.get("nombre")

        if coordinacion == None:
            self.add_error('nombre', 'Nombre de la unidad institucional debe contener al menos 3 caracteres!')
        elif len(coordinacion) < 3:
            self.add_error('nombre', 'Nombre de la unidad institucional debe contener al menos 3 caracteres!')

        return cleaned_data

    def __init__(self, *args, **kwargs):
        jefatura = kwargs.pop('jefatura')
        print('asasssss')
        super(JefaturaFormUsuario, self).__init__(*args, **kwargs)
        self.fields['jefatura'].queryset = JefaturaCoordinacion.objects.filter(jefatura=jefatura)

    class Meta:
        model = Direccion
        fields = '__all__'
        exclude = ('id',)


class TestForm(forms.Form):
    name = forms.CharField(required=True)
    email = forms.EmailField(required=True)
    url = forms.URLField(required=False)
    comment = forms.CharField(required=True, widget=forms.Textarea)

    @property
    def helper(self):
        helper = FormHelper()
        helper.form_tag = False  # don't render form DOM element
        helper.render_unmentioned_fields = True  # render all fields
        helper.label_class = 'col-md-2'
        helper.field_class = 'col-md-10'
        return helper
