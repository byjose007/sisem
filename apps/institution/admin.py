from django.contrib import admin
# from apps.institution.models import Institution
# from apps.institution.models import InstitutionalUnit
# from apps.institution.models import Department
from apps.institution.models import Institucion, JefaturaCoordinacion, Directivo, Direccion


# Register your models here.
#
# class InstitucionAdmin(admin.ModelAdmin):
#     list_display = ('institution', 'email', 'website')
#     search_fields = ('institution', 'ruc')
#
#
# class DireccionInstitucionalAdmin(admin.ModelAdmin):
#     list_display = ('institutional_unit', 'institution')
#     search_fields = ('institutional_unit', 'institution')
#
#
# # list_filter = ('institucion',)
#
# class DepartmentAdmin(admin.ModelAdmin):
#     list_display = ('department', 'institutional_unit')
#     search_fields = ('department', 'institutional_unit')


admin.site.register(Institucion)
admin.site.register(Direccion)
admin.site.register(Directivo)
admin.site.register(JefaturaCoordinacion)
