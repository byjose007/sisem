# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static

from .views import (

    listar_institucion,
    # listar_institucion_inactiva,
    crear_institucion,
    actualizar_institucion,
    eliminar_institucion,
    dar_baja_institucion,
    ver_institucion,
    listar_coordinacion,
    ver_coordinacion,
    crear_coordinacion,
    actualizar_coordinacion,
    eliminar_coordinacion,

    listar_directivo,
    crear_directivo,
    actualizar_directivo,
    ver_directivo,

    listar_direccion,
    crear_direccion,
    actualizar_jefatura,
    eliminar_jefatura,
    ver_direccion,

    # DirectivoDetail,
    institution_list_user,
    institution_create_user,
    institution_update_user,
    institution_delete_user,
    institution_detail_user,
    institutional_unit_list_user,
    institutional_unit_create_user,
    institutional_unit_update_user,
    institutional_unit_delete_user,
    institutional_unit_detail_user,
    department_create_user,
    department_list_user,
    department_delete_user,
    department_update_user,

    crear_coordinacion2)

urlpatterns = [

    # urls admin
    # urls instituciones
    url(r'^admin/$', listar_institucion, name='institution_list'),
    # url(r'^admin/$', listar_institucion, name='institution_list'),
    # url(r'^admin/inactivas$', listar_institucion_inactiva, name='institution_list_inactive'),
    url(r'^admin/new$', crear_institucion, name='institution_new'),
    url(r'^admin/editar/(?P<pk>\d+)$', actualizar_institucion, name='institution_edit'),
    url(r'^admin/borrar/(?P<pk>\d+)/(?P<estado>\d+)/$', eliminar_institucion, name='institution_delete2'),
    url(r'^admin/borrar/(?P<pk>\d+)$', dar_baja_institucion, name='institution_delete'),
    url(r'^admin/(?P<pk>\d+)$', ver_institucion, name='institution_detail'),
    # urls direcciones
    # url(r'^admin/institutionalunit$', listar_directivo, name='institutionalunit_list'),
    # url(r'^admin/institutionalunit/(?P<pk>\d+)$', ver_directivo, name='institutionalunit_detail'),
    # url(r'^admin/direccioninstitucional2/(?P<pk>\d+)$', DirectivoDetail.as_view(), name='direccioninstitucional_detail2'),

    # url(r'^admin/direccioninstitucional/edit/(?P<pk>\d+)$', actualizar_directivo, name='direccioninstitucional_edit'),

    url(r'^admin/directivo$', listar_directivo, name='directivo_listar'),
    url(r'^admin/directivo/(?P<pk>\d+)$', ver_directivo, name='directivo_ver'),
    # url(r'^admin/direccioninstitucional2/(?P<pk>\d+)$', DirectivoDetail.as_view(), name='direccioninstitucional_detail2'),
    url(r'^admin/directivo/new$', crear_directivo, name='directivo_nuevo'),
    url(r'^admin/directivo/edit/(?P<pk>\d+)$', actualizar_directivo, name='directivo_editar'),

    url(r'^admin/direccion$', listar_direccion, name='direccion_listar'),
    url(r'^admin/direccion/detail/(?P<pk>\d+)$', ver_direccion, name='direccion_ver'),
    url(r'^admin/direccion/new$', crear_direccion, name='direccion_nuevo'),

    url(r'^admin/direccion/edit/(?P<pk>\d+)$', actualizar_jefatura, name='direccion_editar'),
    url(r'^admin/jefatura/delete/(?P<pk>\d+)$', eliminar_jefatura, name='jefatura_delete'),

    # Codigo nuevo
    url(r'^admin/coordinacion$', listar_coordinacion, name='coordinacion_list'),
    url(r'^admin/coordinacion/detail/(?P<pk>\d+)$', ver_coordinacion, name='coordinacion_detail'),
    url(r'^admin/coordinacion/new$', crear_coordinacion, name='coordinacion_new'),
    url(r'^admin/coordinacion2/new/(?P<id>\d+)$', crear_coordinacion2, name='coordinacion_new2'),
    url(r'^admin/coordinacion/edit/(?P<pk>\d+)$', actualizar_coordinacion, name='coordinacion_edit'),
    url(r'^admin/coordinacion/delete/(?P<pk>\d+)$', eliminar_coordinacion, name='coordinacion_delete'),

    # Fin codigo nuevo


    # urls user

    url(r'^$', institution_list_user, name='institution_list_user'),
    url(r'^new$', institution_create_user, name='institution_new_user'),
    url(r'^(?P<pk>\d+)$', institution_detail_user, name='institution_detail_user'),
    url(r'^editar/(?P<pk>\d+)$', institution_update_user, name='institution_edit_user'),
    url(r'^borrar/(?P<pk>\d+)$', institution_delete_user, name='institution_delete_user'),

    url(r'^institutionalunit$', institutional_unit_list_user, name='institutionalunit_list_user'),
    url(r'^institutionalunit/new$', institutional_unit_create_user, name='institutionalunit_new_user'),
    url(r'^institutionalunit/(?P<pk>\d+)$', institutional_unit_detail_user, name='institutionalunit_detail_user'),
    url(r'^institutionalunit/edit/(?P<pk>\d+)$', institutional_unit_update_user, name='institutionalunit_edit_user'),
    url(r'^institutionalunit/delete/(?P<pk>\d+)$', institutional_unit_delete_user,
        name='institutionalunit_delete_user'),

    url(r'^department$', department_list_user, name='department_list_user'),
    url(r'^department/new$', department_create_user, name='department_new_user'),
    url(r'^department/edit/(?P<pk>\d+)$', department_update_user, name='department_edit_user'),
    url(r'^department/delete/(?P<pk>\d+)$', department_delete_user, name='department_delete_user'),

]
