# -*- coding: utf-8 -*-
# from dal import autocomplete
from django.db import transaction, IntegrityError, OperationalError
from django.contrib import messages
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.db.models import Q
import json
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView
)


from apps.employee.models import Employee
from apps.institution.forms import DirectivoEditarForm, JefaturanEditarForm, CoordinacionEditarForm
from apps.institution.models import Directivo, Direccion, Institucion, JefaturaCoordinacion
from apps.settings.models import Item
from .forms import InstitucionForm, InstitucionEditarForm
from .forms import DirectivoForm, CoordinacionForm
from .forms import DirecccionForm, DireccionFormUsuario, CoordinacionFormUsuario, JefaturaFormUsuario
from django.http import HttpResponse
from django.views.generic import FormView, TemplateView
from django.contrib.messages.views import SuccessMessageMixin
from .forms import TestForm
from apps.accounts.models import UserProfile


# Create your views here.


# Funciones admistrador
# -------Institución----------
@login_required()
def listar_institucion(request, template_name='institution/institution_list.html'):
    usuario = request.user
    data = {}
    query = request.GET.get('q', '')
    user_profile = UserProfile.objects.get(user=request.user)
    if query == '':
        # user_profile = UserProfile.objects.filter(user=request.user)
        # print (user_profile.institution_default)
        # institucion = user_profile.institution_default
        institucion = Institucion.objects.filter(estado='ACTIVO').values('ruc', 'nombre', 'telefono', 'id', 'estado')
        paginator = Paginator(institucion, 10)  # Show 25 contract per page
        page = request.GET.get('page')
        try:
            institucion = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            institucion = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            institucion = paginator.page(paginator.num_pages)
    else:
        if query:
            qset = (
                Q(institucion__icontains=query) |
                Q(ruc__icontains=query)
            )
            institucion = Institucion.objects.filter(qset).values('ruc', 'nombre', 'telefono', 'id')
            paginator = Paginator(institucion, 20)  # Show 25 contract per page
            page = request.GET.get('page')
            try:
                institucion = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                institucion = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                institucion = paginator.page(paginator.num_pages)

    data['object_list'] = institucion
    data['query'] = query
    data['titulo'] = 'Acciones de Personal'

    instituciones = Institucion.objects.filter(estado='ACTIVO')
    instituciones_inactivas = Institucion.objects.filter(estado='INACTIVO')
    data = {'instituciones_inactivas': instituciones_inactivas}
    data['instituciones'] = instituciones

    return render(request, template_name, data)
#
# @login_required()
# def listar_institucion_inactiva(request, template_name='institution/institution_list.html'):
#     data = {}
#     instituciones_inactivas = Institucion.objects.filter(estado='INACTIVO')
#     data['instituciones_inactivas'] = instituciones_inactivas
#     # data = {'instituciones_inactivas':instituciones_inactivas }
#
#
#     return render(request, template_name, data)

@login_required()
def crear_institucion(request, template_name='institution/institution_form.html'):
    form = InstitucionForm(request.POST or None)
    titulo = 'Nueva Institución'

    if form.is_valid():
        form.save()
        messages.success(request, 'Buen Trabajo. Datos ingresados correctamente.')
        return redirect('institution:institution_list')
    return render(request, template_name, {'form': form, 'titulo':titulo})


@login_required()
def actualizar_institucion(request, pk, template_name='institution/institution_form.html'):
    institucion = get_object_or_404(Institucion, pk=pk)
    titulo = 'Editar Institución'
    form = InstitucionEditarForm(request.POST or None, instance=institucion)
    if form.is_valid():
        form.save()
        messages.success(request, 'Datos actualizados correctamente')
        return redirect('institution:institution_list')
    return render(request, template_name, {'form': form, 'titulo':titulo})


@login_required()
def eliminar_institucion(request, pk, estado, template_name='institution/institution_confirm_delete_admin.html'):
    try:
        institucion = get_object_or_404(Institucion, pk=pk, estado=estado)
        institucion.estado=estado
        institucion.save()
        messages.success(request, 'Buen Trabajo. Los datos han sido eliminados.')
    except Exception as inst:

        messages.error(request,
                       'Mal. Los datos no se han podido eliminar. Verificar que la institución no tenga información asociada como Unidades Institucionales o Evaluaciones de Desempeño')

    return redirect('institution:institution_list')

# clase dar de baja institucion
@login_required()
def dar_baja_institucion(request, pk, template_name='institution/institution_confirm_delete_admin.html'):
    try:
        institucion = get_object_or_404(Institucion, pk=pk)

        institucion.save()
        messages.success(request, 'Buen Trabajo. Los datos han sido eliminados.')
    except Exception as inst:

        messages.error(request,
                       'Mal. Los datos no se han podido eliminar. Verificar que la institución no tenga información asociada como Unidades Institucionales o Evaluaciones de Desempeño')

    return redirect('institution:institution_list')


@login_required()
def ver_institucion(request, pk, template_name='institution/institution_detail.html'):
    institucion = Institucion.objects.get(id=pk)
    directivos = Directivo.objects.filter(institucion=pk, estado='ACTIVO')

    # persona = Person.objects.get(id=curriculum.)
    context = {
        "institucion": institucion,
        "directivos": directivos,
    }
    return render(request, template_name, context)


# -------Directivo----------
@login_required()
def listar_directivo(request, template_name='institution/directivo_list.html'):
    # institutional_unit = InstitutionalUnit.objects.all()
    data = {}

    query = request.GET.get('q', '')
    if query == '':
        print('q=vacio')
        directivo = Directivo.objects.all()
        paginator = Paginator(directivo, 10)  # Show 25 contract per page
        page = request.GET.get('page')
        try:
            directivo = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            directivo = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            directivo = paginator.page(paginator.num_pages)
    else:
        if query:
            qset = (
                Q(institutional_unit__icontains=query) |
                Q(institutional_unit__icontains=query)
            )
            directivo = Directivo.objects.filter(qset)
            paginator = Paginator(directivo, 10)  # Show 25 contract per page
            page = request.GET.get('page')
            try:
                direccion = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                directivo = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                directivo = paginator.page(paginator.num_pages)

    data['object_list'] = directivo
    data['query'] = query

    directivos = Directivo.objects.filter(estado='ACTIVO')
    directivos_inactivos = Directivo.objects.filter(estado='INACTIVO')
    data = {'directivos_inactivos': directivos_inactivos}
    data['directivos'] = directivos



    return render(request, template_name, data)


@login_required()
def crear_directivo(request, template_name='institution/directivo_form.html'):
    form = DirectivoForm(request.POST or None)
    titulo = 'Crear Nivel Directivo'
    if form.is_valid():
        form.save()
        messages.success(request, 'Datos ingresados correctamente')
        return redirect('institution:directivo_listar')
    return render(request, template_name, {'form': form,'titulo':titulo})


@login_required()
def actualizar_directivo(request, pk, template_name='institution/directivo_form.html'):
    directivo = get_object_or_404(Directivo, pk=pk)
    titulo = 'Editar Nivel Directivo'
    form = DirectivoEditarForm(request.POST or None, instance=directivo)

    if form.is_valid():
        form.save()
        messages.success(request, 'Los datos se actualizaron correctamente')
        return redirect('institution:directivo_listar')
    return render(request, template_name, {'form': form, 'titulo':titulo})

@login_required()
def ver_directivo(request, pk, template_name='institution/directivo_detail.html'):
    directivo = Directivo.objects.get(id=pk)
    direcciones = Direccion.objects.filter(directivo=pk, estado='ACTIVO')
    context = {
        "directivo": directivo,
        "direcciones": direcciones,
    }
    return render(request, template_name, context)

# -------Jefatura----------
@login_required()
def listar_direccion(request, template_name='institution/direccion_list.html'):
    data = {}
    query = request.GET.get('q', '')
    if query == '':
        direccion = Direccion.objects.all()
        paginator = Paginator(direccion, 10)  # Show 25 contract per page
        page = request.GET.get('page')
        try:
            direccion = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            direccion = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            direccion = paginator.page(paginator.num_pages)
    else:
        if query:
            qset = (
                Q(direccion__icontains=query) |
                Q(direccion__icontains=query)
            )
            direccion = Direccion.objects.filter(qset)
            paginator = Paginator(direccion, 10)  # Show 25 contract per page
            page = request.GET.get('page')
            try:
                direccion = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                direccion = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                direccion = paginator.page(paginator.num_pages)

    data['object_list'] = direccion
    data['query'] = query

    direcciones = Direccion.objects.filter(estado='ACTIVO')
    direcciones_inactivas = Direccion.objects.filter(estado='INACTIVO')
    data = {'direcciones_inactivas': direcciones_inactivas}
    data['direcciones'] = direcciones

    return render(request, template_name, data)



@login_required()
def crear_jefatura2(request, template_name='institution/direccion_form.html'):
    form = DirecccionForm(request.POST or None)

    if form.is_valid():
        form.save()
        messages.success(request, 'Se ha registrado correctamente')
        return redirect('institution:department_list')
    return render(request, template_name, {'form': form})




@login_required()
def crear_direccion(request, template_name='institution/direccion_form.html'):
    form = DirecccionForm(request.POST or None)
    titulo = 'Nueva Dirección Institucional'
    if form.is_valid():
        form.save()
        messages.success(request, 'Se ha registrado correctamente')
        return redirect('institution:direccion_listar')
    return render(request, template_name, {'form': form,'titulo': titulo})


@login_required()
def actualizar_jefatura(request, pk, template_name='institution/direccion_form.html'):
    jefatura = get_object_or_404(Direccion, pk=pk)
    titulo = 'Editar Jefatura'
    form = JefaturanEditarForm(request.POST or None, instance=jefatura)
    if form.is_valid():
        form.save()
        messages.success(request, 'Se ha actualizado correctamente')
        return redirect('institution:direccion_listar')
    return render(request, template_name, {'form': form,'titulo': titulo})


@login_required()
def eliminar_jefatura(request, pk, template_name='institution/jefatura_delete.html'):
    try:
        department = Direccion.objects.get(pk=pk)
        department.delete()
        messages.success(request, 'Buen Trabajo. Los datos han sido eliminados.')
    except Exception as inst:
        messages.error(request,
                       'Mal. Los datos no se han podido eliminar. Verificar que el departamento no tenga información asociada')
    return redirect('institution:department_list')


@login_required()
def ver_direccion(request, pk, template_name='institution/direccion_detail.html'):
    direccion = Direccion.objects.get(id=pk)
    jefaturas= JefaturaCoordinacion.objects.filter(direccion=pk, estado='ACTIVO')
    context={
        "direccion":direccion,
        "jefaturas":jefaturas,
    }
    return render(request, template_name, context)


# -------Coordinaciones----------
@login_required()
def listar_coordinacion(request, template_name='institution/coordinacion_list.html'):
    data = {}
    query = request.GET.get('q', '')
    if query == '':
        coordinacion = JefaturaCoordinacion.objects.all()
        paginator = Paginator(coordinacion, 10)  # Show 25 contract per page
        page = request.GET.get('page')
        try:
            coordinacion = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            coordinacion = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            coordinacion = paginator.page(paginator.num_pages)
    else:
        if query:
            qset = (
                Q(coordinacion__icontains=query) |
                Q(coordinacion__icontains=query)
            )
            coordinacion = JefaturaCoordinacion.objects.filter(qset)
            paginator = Paginator(coordinacion, 10)  # Show 25 contract per page
            page = request.GET.get('page')
            try:
                coordinacion = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                coordinacion = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                coordinacion = paginator.page(paginator.num_pages)

    data['object_list'] = coordinacion
    data['query'] = query

    coordinaciones = JefaturaCoordinacion.objects.filter(estado='ACTIVO')
    data['coordinaciones'] = coordinaciones
    coordinaciones_inactivas = JefaturaCoordinacion.objects.filter(estado='INACTIVO')
    data['coordinaciones_inactivas'] = coordinaciones_inactivas
    return render(request, template_name, data)


@login_required()
def crear_coordinacion(request, template_name='institution/coordinacion_form.html'):
    form = CoordinacionForm(request.POST or None)
    titulo = 'Nueva Coordinación'
    if form.is_valid():
        form.save()
        messages.success(request, 'Se ha registrado correctamente')
        return redirect('institution:coordinacion_list')
    return render(request, template_name, {'form': form, 'titulo':titulo})



@login_required()
def crear_coordinacion2(request, id , template_name='institution/coordinacion_form.html'):
    jefatura =  get_object_or_404(Direccion, pk=id)

    titulo = 'Nueva Coordinación'
    form = CoordinacionForm(request.POST or None, instance=jefatura)

    if form.is_valid():
        form.save()
        messages.success(request, 'Se ha registrado correctamente')
        return redirect('institution:coordinacion_list')
    return render(request, template_name, {'form': form, 'titulo':titulo})


@login_required()
def actualizar_coordinacion(request, pk, template_name='institution/coordinacion_form.html'):
    coordinacion = get_object_or_404(JefaturaCoordinacion, pk=pk)
    titulo='Editar Coordinación'
    form = CoordinacionEditarForm(request.POST or None, instance=coordinacion)
    if form.is_valid():
        form.save()
        messages.success(request, 'Se ha registrado correctamente')
        return redirect('institution:coordinacion_list')
    return render(request, template_name, {'form': form, 'titulo':titulo})


@login_required()
def eliminar_coordinacion(request, pk, template_name='institution/jefatura_delete.html'):
    try:
        department = JefaturaCoordinacion.objects.get(pk=pk)
        department.delete()
        messages.success(request, 'Buen Trabajo. Los datos han sido eliminados.')
    except Exception as inst:
        messages.error(request,
                       'Mal. Los datos no se han podido eliminar. Verificar que el departamento no tenga información asociada')
    return redirect('institution:coordinacion_list')


@login_required()
def ver_coordinacion(request, pk, template_name='institution/coordinacion_detail.html'):
    coordinacion=JefaturaCoordinacion.objects.get(id=pk)
    # estado= Item.objects.get(catalog__code='ESTADOEMPLEADO',name='empleado')
    # empleados=Employee.objects.filter(jefatura=pk, status=estado)
    empleados = Employee.objects.filter(jefatura=pk)
    # empleados=Employee.objects.filter(coordinacion=coordinacion)
    context={
        'coordinacion':coordinacion,
        'empleados':empleados
    }
    return render(request, template_name, context)


# -------------------------Funciones Usuario--------------------------------------------
# institucion

@login_required()
def institution_list_user(request, template_name='institution/institution_list.html'):
    user_profile = UserProfile.objects.get(user=request.user)
    data = {}
    data['object_list'] = user_profile.institucion
    return render(request, template_name, data)


@login_required()
def institution_update_user(request, pk, template_name='institution/institution_form.html'):
    institucion = get_object_or_404(Institucion, pk=pk)
    form = InstitucionForm(request.POST or None, instance=institucion)
    if form.is_valid():
        form.save()
        messages.success(request, 'Buen Trabajo. Datos actualizados correctamente.')
        return redirect('institution:institution_list_user')
    return render(request, template_name, {'form': form})


@login_required()
def institution_create_user(request, template_name='institution/institution_form.html'):
    form = InstitucionForm(request.POST or None)
    if form.is_valid():
        form.save()
        messages.success(request, 'Buen Trabajo. Los datos han ingresados correctamente.')
        return redirect('institution:institution_list_user')
    return render(request, template_name, {'form': form})


@login_required()
def institution_update_user(request, pk, template_name='institution/institution_form.html'):
    institucion = get_object_or_404(Institucion, pk=pk)
    form = InstitucionForm(request.POST or None, instance=institucion)
    if form.is_valid():
        form.save()
        messages.success(request, 'Buen Trabajo. Los datos han actualizados correctamente.')
        return redirect('institution:institution_list_user')
    return render(request, template_name, {'form': form})


@login_required()
def institution_delete_user(request, pk, template_name='institution/institution_confirm_delete.html'):
    try:
        institucion = get_object_or_404(Institucion, pk=pk)
        institucion.delete()
        messages.success(request, 'Buen Trabajo. Los datos han sido eliminados.')
    except Exception as inst:

        messages.error(request,
                       'Mal. Los datos no se han podido eliminar. Verificar que la institución no tenga información asociada como Unidades Institucionales o Evaluaciones de Desempeño')

    return redirect('institution:institution_list_user')


@login_required()
def institution_detail_user(request, pk, template_name='institution/institution_detail.html'):
    institucion = Institucion.objects.get(id=pk)
    print(template_name)
    institutional_unit = Directivo.objects.filter(institucion=pk)
    # persona = Person.objects.get(id=curriculum.)
    context = {
        "institucion": institucion,
        "institutional_unit": institutional_unit,
    }
    return render(request, template_name, context)


# Unidad Institucional

@login_required()
def institutional_unit_list_user(request, template_name='institution/direccion_list.html'):
    user_profile = UserProfile.objects.get(user=request.user)
    data = {}
    query = request.GET.get('q', '')
    if query == '':
        institutional_unit = Directivo.objects.filter(institucion=user_profile.institution_default)
        paginator = Paginator(institutional_unit, 10)  # Show 25 contract per page
        page = request.GET.get('page')
        try:
            institutional_unit = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            institutional_unit = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            institutional_unit = paginator.page(paginator.num_pages)
    else:
        if query:
            qset = (
                Q(institutional_unit__icontains=query)
            )
            institutional_unit = Directivo.objects.filter(qset, institucion=user_profile.institution_default)
            paginator = Paginator(institutional_unit, 10)  # Show 25 contract per page
            page = request.GET.get('page')
            try:
                institutional_unit = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                institutional_unit = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                institutional_unit = paginator.page(paginator.num_pages)

    data['object_list'] = institutional_unit
    data['query'] = query
    return render(request, template_name, data)


@login_required()
def institutional_unit_create_user(request, template_name='institution/direccion_form.html'):
    form = DireccionFormUsuario(request.POST or None)

    if form.is_valid():
        user_profile = UserProfile.objects.get(user=request.user)
        institutional_unit = form.save(commit=False)
        institutional_unit.institution = user_profile.institution_default
        institutional_unit.save()
        messages.success(request, 'Los datos se han ingresado correctamente.')
        return redirect('institution:institutionalunit_list_user')
    return render(request, template_name, {'form': form})


@login_required()
def institutional_unit_update_user(request, pk, template_name='institution/direccion_form.html'):
    institutional_unit = get_object_or_404(Directivo, pk=pk)
    form = DireccionFormUsuario(request.POST or None, instance=institutional_unit)
    if form.is_valid():
        form.save()
        messages.success(request, 'Los datos se han actualizado correctamente.')
        return redirect('institution:institutionalunit_list_user')
    return render(request, template_name, {'form': form})


@login_required()
def institutional_unit_delete_user(request, pk):
    print("Estamos aqui")
    try:
        direccion = get_object_or_404(Directivo, pk=pk)
        jefatura = Direccion.objects.filter(direccion=direccion)
        if jefatura:
            messages.error(request,
                           'Mal. Los datos no se han podido eliminar. Verificar que la Unidad Institucional no tenga información asociada como Departamentos')
        else:
            direccion.delete()
            messages.success(request, 'Los datos han sido eliminados.')
    except Exception as inst:
        messages.error(request,
                       'Mal. Los datos no se han podido eliminar. Verificar que la unidad institucional no tenga información asociada como Unidades Institucionales o Evaluaciones de Desempeño')

    return redirect('institution:institutionalunit_list_user')


@login_required()
def institutional_unit_detail_user(request, pk, template_name='institution/direccion_detail.html'):
    direccion = Directivo.objects.get(id=pk)
    asd = direccion.id
    lista_jefaturas = Direccion.objects.filter(jefatura=asd)
    context = {
        "direccion": direccion,
        "departamentos": lista_jefaturas,

    }
    return render(request, template_name, context)


class InstitutionalUnitDetail(DetailView):
    model = Directivo


# Departament
# @login_required()
# def department_list_user(request, template_name='institution/jefatura_list.html'):
#    user_profile = UserProfile.objects.get(user=request.user)
#    institutional_unit = InstitutionalUnit.objects.filter(institution=user_profile.institution_default)
#    data = {}

#    department = []

# for institutional_units in institutional_unit:
#   department.extend(Department.objects.filter(institutional_unit = institutional_units))


# data['object_list'] = department
# return render(request, template_name, data)


@login_required()
def department_list_user(request, template_name='institution/jefatura_list.html'):
    user_profile = UserProfile.objects.get(user=request.user)
    institutional_unit = Directivo.objects.filter(institution=user_profile.institution_default)
    data = {}
    department = []
    for institutional_units in institutional_unit:
        department.extend(Direccion.objects.filter(institutional_unit=institutional_units))

    query = request.GET.get('q', '')
    if query == '':
        paginator = Paginator(department, 10)  # Show 25 contract per page
        page = request.GET.get('page')
        try:
            department = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            department = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            department = paginator.page(paginator.num_pages)
    else:
        if query:
            qset = (
                Q(department__icontains=query) |
                Q(department__icontains=query)
            )
            department = Direccion.objects.filter(qset,
                                                  institutional_unit__institution__institution=user_profile.institution_default)
            paginator = Paginator(department, 10)  # Show 25 contract per page
            page = request.GET.get('page')
            try:
                department = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                department = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                department = paginator.page(paginator.num_pages)

    data['object_list'] = department
    data['query'] = query

    return render(request, template_name, data)


@login_required()
def department_create_user(request, template_name='institution/jefatura_form.html'):
    user_profile = UserProfile.objects.get(user=request.user)
    institucion = user_profile.institution_default
    form = JefaturaFormUsuario(request.POST or None, institucion=institucion)
    if form.is_valid():
        print('Formulario valido')
        form.save()
        messages.success(request, 'Los datos se han ingresados correctamente.')
        return redirect('institution:department_list_user')
    return render(request, template_name, {'form': form})


@login_required()
def department_update_user(request, pk, template_name='institution/jefatura_form.html'):
    user_profile = UserProfile.objects.get(user=request.user)
    institucion = user_profile.institution_default
    department = get_object_or_404(Direccion, pk=pk)
    form = JefaturaFormUsuario(request.POST or None, instance=department, institucion=institucion)
    if form.is_valid():
        form.save()
        messages.success(request, 'Los datos se han actualizado correctamente.')
        return redirect('institution:department_list_user')
    return render(request, template_name, {'form': form})


@login_required()
def department_delete_user(request, pk, template_name='institution/jefatura_delete.html'):
    try:
        department = Direccion.objects.get(pk=pk)
        department.delete()
        messages.success(request, 'El registro se ha eliminado correctamente')
    except Exception as inst:
        messages.error(request,
                       'Mal. Los datos no se han podido eliminar. Verificar que el departamento no tenga información asociada')

    return redirect('institution:department_list_user')

