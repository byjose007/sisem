from django.apps import AppConfig


class EconomicdataConfig(AppConfig):
    name = 'apps.economicdata'
