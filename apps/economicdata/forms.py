# -*- coding: utf-8 -*-
from django import forms
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout
from .models import EconomicData, BankAccounts


class EconomicDataForm(ModelForm):
	
	class Meta:
		model = EconomicData
		fields = '__all__'
		exclude = ('id',)

class BankAccountsForm(ModelForm):
	
	class Meta:
		model = BankAccounts
		fields = '__all__'
		exclude = ('id',)


