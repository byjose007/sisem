# -*- coding: utf-8 -*-
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView
from django.views.generic.detail import DetailView

from apps.institution.models import Institucion, JefaturaCoordinacion, Directivo, Direccion
# from apps.institution.models import Institution, InstitutionalUnit, Department
from .forms import EconomicDataForm, BankAccountsForm
from .models import EconomicData, BankAccounts
from django.http import HttpResponse
from django.views.generic import FormView, TemplateView
from django.contrib.messages.views import SuccessMessageMixin
from apps.accounts.models import UserProfile
# Create your views here.
#Funciones admistrador

#-------Acciones de personal  - Tipos de accion de personal  ----------
def economic_data_list(request, template_name='economicdata/economic_data_admin.html'):
    economic_data = EconomicData.objects.all()
    context = {
        "data": economic_data,
        "accion": 'listar',
    }
    return render(request,template_name , context)

def economic_data_create(request, template_name='economicdata/economic_data_admin.html'):
    form = EconomicDataForm(request.POST or None)
    context = {
        "accion": 'nuevo',
        "form": form,
    }
    if form.is_valid():
        form.save()
        return redirect('economicdata:economicdata_list')
    return render(request, template_name, context)

def economic_data_update(request, pk, template_name='economicdata/economic_data_admin.html'):
    economic_data = get_object_or_404(EconomicData, pk=pk)
    form = EconomicDataForm(request.POST or None, instance=economic_data)
    if form.is_valid():
        form.save()
        return redirect('economicdata:economic_data_list')

    context = {
        "accion": 'editar',
        "form": form,
    }
    return render(request, template_name, context)

def economic_data_delete(request, pk, template_name='economicdata/economic_data_admin.html'):
    economic_data = get_object_or_404(EconomicData, pk=pk)
    if request.method=='POST':
        economic_data.delete()
        return redirect('economicdata:economicdata_list')
    context = {
        "accion": 'eliminar',
        "form": economic_data,
    }
    return render(request, template_name, context)

def economic_data_detail(request, pk, template_name='economicdata/economic_data_admin.html'):
    economic_data = get_object_or_404(EconomicData, pk=pk)
    context = {
        "accion": 'detalle',
        "form": economic_data,
    }
    return render(request, template_name, context)

def create_or_update_economic_data_cc(contract):
    if (contract.status=='Firmado'):
        try:
            economic_data = EconomicData.objects.get(employee=contract.employee.id)
            
            economic_data.employee = contract.employee
            economic_data.remuneration = contract.remuneration
            economic_data.post = contract.public_office
            economic_data.budget_item = contract.budget_item 
            economic_data.reserve_funds = -1
            economic_data.date_of_admission = contract.start_date
            economic_data.date_of_entry_into_roles = contract.end_date
            economic_data.type_public_servant = "No está bajo dependencia"
            economic_data.labor_system = contract.labor_system 
            economic_data.payment_method = "Contra factura"
            economic_data.type_used = "Servicios Profesionales"
            economic_data.contract_number = contract.id
            economic_data.contract_start_date = contract.start_date
            economic_data.contract_termination_date = contract.end_date
            economic_data.family_responsibilities = -1
            economic_data.education_by = -1
            economic_data.start_date = contract.start_date
            economic_data.end_date = contract.end_date
            occupational_group = ""
            grade = ""
            collective_contract = ""
            economic_data.save()
        

        except EconomicData.DoesNotExist:
            economic_data = EconomicData()          
            economic_data.employee = contract.employee
            economic_data.remuneration = contract.remuneration
            economic_data.post = contract.public_office
            economic_data.budget_item = contract.budget_item 
            economic_data.reserve_funds = -1
            economic_data.date_of_admission = contract.start_date
            economic_data.date_of_entry_into_roles = contract.end_date
            economic_data.type_public_servant = "No está bajo dependencia"
            economic_data.labor_system = contract.labor_system 
            economic_data.payment_method = "Contra factura"
            economic_data.type_used = "Servicios Profesionales"
            economic_data.contract_number = contract.id
            economic_data.contract_start_date = contract.start_date
            economic_data.contract_termination_date = contract.end_date
            economic_data.family_responsibilities = -1
            economic_data.education_by = -1
            economic_data.start_date = contract.start_date
            economic_data.end_date = contract.end_date
            occupational_group = ""
            grade = ""
            collective_contract = ""
            economic_data.save()

    return economic_data

def create_or_update_economic_data_ct(contract):
    if (contract.status=='Firmado'):
        try:
            economic_data = EconomicData.objects.get(employee=contract.employee.id)
            
            economic_data.employee = contract.employee
            economic_data.remuneration = contract.remuneration
            economic_data.post = contract.public_office
            economic_data.budget_item = contract.budget_item 
            economic_data.reserve_funds = -1
            economic_data.date_of_admission = contract.start_date
            economic_data.date_of_entry_into_roles = contract.end_date
            economic_data.type_public_servant = "No está bajo dependencia"
            economic_data.labor_system = contract.labor_system 
            economic_data.payment_method = "Contra factura"
            economic_data.type_used = "Trabajadores"
            economic_data.contract_number = contract.id
            economic_data.contract_start_date = contract.start_date
            economic_data.contract_termination_date = contract.end_date
            economic_data.family_responsibilities = -1
            economic_data.education_by = -1
            economic_data.start_date = contract.start_date
            economic_data.end_date = contract.end_date
            occupational_group = ""
            grade = ""
            collective_contract = ""
            economic_data.save()
        

        except EconomicData.DoesNotExist:
            economic_data = EconomicData()          
            economic_data.employee = contract.employee
            economic_data.remuneration = contract.remuneration
            economic_data.post = contract.public_office
            economic_data.budget_item = contract.budget_item 
            economic_data.reserve_funds = -1
            economic_data.date_of_admission = contract.start_date
            economic_data.date_of_entry_into_roles = contract.end_date
            economic_data.type_public_servant = "No está bajo dependencia"
            economic_data.labor_system = contract.labor_system 
            economic_data.payment_method = "Contra factura"
            economic_data.type_used = "Servicios Profesionales"
            economic_data.contract_number = contract.id
            economic_data.contract_start_date = contract.start_date
            economic_data.contract_termination_date = contract.end_date
            economic_data.family_responsibilities = -1
            economic_data.education_by = -1
            economic_data.start_date = contract.start_date
            economic_data.end_date = contract.end_date
            occupational_group = ""
            grade = ""
            collective_contract = ""
            economic_data.save()

    return economic_data



    
    
    