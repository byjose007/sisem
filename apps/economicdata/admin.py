
from django.contrib import admin
from apps.economicdata.models import EconomicData
from apps.economicdata.models import BankAccounts



# Register your models here.

class EconomicDataAdmin(admin.ModelAdmin):
	list_display = ('employee',)
	#search_fields = ('employee')

class BankAccountsAdmin(admin.ModelAdmin):
	list_display = ('employee',)
	#search_fields = ('employee')
#	list_filter = ('institucion',)



admin.site.register(EconomicData, EconomicDataAdmin)
admin.site.register(BankAccounts, BankAccountsAdmin)

