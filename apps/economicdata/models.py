from django.db import models

from apps.institution.models import Institucion, JefaturaCoordinacion, Directivo, Direccion
# from apps.institution.models import Institution, InstitutionalUnit, Department
from apps.person.models import Person
from apps.schedule.models import Schedule
from apps.employee.models import Employee
from apps.contract.models import LaborSystem, PayScalesSp


# Create your models here.

class EconomicData(models.Model):
    semanal = 'semanal'
    quicenal = 'quincenal'
    mensual = 'mensual'
    contra_factura = 'contra factura'
    payment_method_list = (
        (semanal, 'Semanal'),
        (mensual, 'Mensual'),
        (quicenal, 'Quincenal'),
        (contra_factura, 'Contra Factura'),
    )

    employee = models.OneToOneField(Employee, verbose_name='Empleado', on_delete=models.PROTECT, null=True, blank=True)
    remuneration = models.DecimalField(verbose_name='Remuneración', max_digits=6, decimal_places=2,null=True, blank=True)
    post = models.CharField(max_length=50, verbose_name='puesto',null=True, blank=True)
    occupational_group = models.ForeignKey(PayScalesSp, verbose_name='Grupo Ocupacional', blank=True, null=True)
    grade = models.CharField(max_length=50, verbose_name='Grado', null=True, blank=True)
    budget_item = models.CharField(verbose_name='Partida presupuestaria', max_length=100,null=True, blank=True)
    reserve_funds = models.DecimalField(verbose_name='Fondos de reserva', max_digits=6, decimal_places=2, blank=True,
                                        null=True)
    date_of_admission = models.DateField(verbose_name='Fecha de ingreso',null=True, blank=True)
    date_of_entry_into_roles = models.DateField(verbose_name='Fecha de ingreso a roles', blank=True, null=True)
    type_public_servant = models.CharField(max_length=50, verbose_name='Tipo servidor publico', blank=True, null=True)
    labor_system = models.ForeignKey(LaborSystem, verbose_name='Régimen laboral', null=True, blank=True)
    payment_method = models.CharField(max_length=50, verbose_name='Modalidad pago', choices=payment_method_list,
                                      default=mensual,  null=True, blank=True)
    type_used = models.CharField(max_length=50, verbose_name='Tipo empleado', null=True, blank=True)
    contract_number = models.CharField(max_length=50, verbose_name='# de contrato',null=True, blank=True)
    contract_start_date = models.DateField(verbose_name='Fecha inicio de contrato',null=True, blank=True)
    contract_termination_date = models.DateField(verbose_name='Fecha terminación de contrato', blank=True, null=True)
    family_responsibilities = models.IntegerField(verbose_name='Cargas familiares', blank=True, null=True)
    education_by = models.IntegerField(verbose_name='Educación a cargo', blank=True, null=True)

    start_date = models.DateField(verbose_name='Fecha de inicio',null=True, blank=True)
    end_date = models.DateField(verbose_name='Fecha de fin', blank=True, null=True)

    collective_contract = models.BooleanField(verbose_name='Contrato Colectivo', default=False)

    class Meta:
        ordering = ['date_of_admission', 'employee']
        verbose_name = 'Datos Económicos'
        verbose_name_plural = 'Datos Económicos'

    def __str__(self):
        return u'{0}'.format(self.employee)

    def __unicode__(self):
        return u'{0}'.format(self.employee)


class BankAccounts(models.Model):
    ahorro = 'ahorro'
    corriente = 'corriente'
    account_type_list = (
        (ahorro, 'Ahorro'),
        (corriente, 'Corriente'),
    )

    employee = models.ForeignKey(Employee, verbose_name='Empleado', on_delete=models.PROTECT)
    bank = models.CharField(max_length=50, verbose_name='Banco')
    number_of_account = models.CharField(max_length=50, verbose_name='Nro. de cuenta')
    account_type = models.CharField(max_length=50, verbose_name='Tipo de cuenta', choices=account_type_list,
                                    default=ahorro)
    headline = models.CharField(max_length=50, verbose_name='Titular')
    active = models.BooleanField(verbose_name='Activa', default=True)

    class Meta:
        ordering = ['employee']
        verbose_name = 'Cuenta Bancaria'
        verbose_name_plural = 'Cuentas Bancarias'

    def __str__(self):
        return u'{0}-{1}'.format(self.bank, self.number_of_account)

    def __unicode__(self):
        return u'{0}-{1}'.format(self.bank, self.number_of_account)
