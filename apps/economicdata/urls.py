# -*- coding: utf-8 -*-
from django.conf.urls import url, include

from .views import (
    economic_data_list,
    economic_data_create,
    economic_data_update,
    economic_data_delete,
    economic_data_detail,

 
)


urlpatterns = [

    #urls admin 
    url(r'^admin/$', economic_data_list, name='economicdata_list'),
    url(r'^admin/new$', economic_data_create, name='economicdata_new'),
    url(r'^admin/editar/(?P<pk>\d+)$', economic_data_update, name='economicdata_edit'),      
    url(r'^admin/(?P<pk>\d+)$',economic_data_delete, name='economicdata_delete'),
    url(r'^admin/detalle/(?P<pk>\d+)$',economic_data_detail, name='economicdata_detalle'),

  
]
