from django import forms
from django.forms import ModelForm
from .models import LaborSystem
from .models import TypeOfContract
from .models import Contract
from .models import PayScaleGroups
from .models import PayScalesSp

from apps.institution.models import Institucion, JefaturaCoordinacion, Directivo, Direccion
# from apps.institution.models import InstitutionalUnit, Department, Institution
from apps.employee.models import Employee


class LaborSystemForm(ModelForm):
    class Meta:
        model = LaborSystem
        fields = '__all__'
        exclude = ('id',)


class TypeOfContractForm(ModelForm):
    class Meta:
        model = TypeOfContract
        fields = '__all__'
        exclude = ('id',)


class PayScaleGroupsForm(ModelForm):
    class Meta:
        model = PayScaleGroups
        fields = '__all__'
        exclude = ('id',)


class PayScalesSpForm(ModelForm):
    class Meta:
        model = PayScalesSp
        fields = '__all__'
        exclude = ('id',)


class ContractForm(ModelForm):
    print('ContractForm')

    class Meta:
        model = Contract
        fields = '__all__'
        exclude = ('id', 'labor_system', 'type_of_contract', 'elaborated_by',)


class ContractFormUser(ModelForm):
    print('ContractFormUser')
    print('llego aqui')

    def __init__(self, *args, **kwargs):
        institution = kwargs.pop('institution')
        super(ContractFormUser, self).__init__(*args, **kwargs)
        self.fields['department'].queryset = Department.objects.filter(institutional_unit__institution=institution)
        self.fields['employee'].queryset = Employee.objects.filter(institution=institution)

    class Meta:
        model = Contract
        fields = '__all__'
        exclude = ('id', 'labor_system', 'type_of_contract', 'elaborated_by',)
