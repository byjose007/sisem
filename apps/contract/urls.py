

from django.conf.urls import url
from .views import (
    LaborSystemDetail,
    labor_system_list,
    labor_system_create,
    labor_system_update,
    labor_system_delete,
    labor_system_detail,
    ContractDetail,
    contract_list,
    contract_list_user,
    contract_update,
    contract_update_firmar,
    contract_create,
    contract_delete,
    TypeOfContractDetail,
    type_of_contract_list,
    type_of_contract_create,
    type_of_contract_delete,
    type_of_contract_update,
    PayScalesSpDetail, 
    pay_scale_sp_create,
    pay_scale_sp_list,
    pay_scale_sp_delete,
    pay_scale_sp_update,
    pay_scale_groups_detail,
    PayScaleGroupsDetail,
    pay_scale_groups_list,
    pay_scale_groups_create,
    pay_scale_groups_delete,
    pay_scale_groups_update,
    list_new_contract_type,
    
    
)


urlpatterns = [
    url(r'^admin/laborsystem$', labor_system_list, name='labor_system_list'),
    #url(r'^laborsystem/(?P<pk>\d+)$', LaborSystemDetail.as_view(), name='labor_system_detail'),
    url(r'^admin/laborsystem/(?P<pk>\d+)$', labor_system_detail, name='labor_system_detail'),
    url(r'^admin/laborsystem/new$', labor_system_create, name='labor_system_new'),
    url(r'^admin/laborsystem/edit/(?P<pk>\d+)$', labor_system_update, name='labor_system_edit'),
    url(r'^admin/laborsystem/delete/(?P<pk>\d+)$', labor_system_delete, name='labor_system_delete'),

    url(r'^admin/typeofcontract$', type_of_contract_list, name='type_of_contract_list'),
    url(r'^admin/typeofcontract/detail/(?P<pk>\d+)$', TypeOfContractDetail.as_view(), name='type_of_contract_detail'),
    url(r'^admin/typeofcontract/new$', type_of_contract_create, name='type_of_contract_new'),
    url(r'^admin/typeofcontract/edit/(?P<pk>\d+)$', type_of_contract_update, name='type_of_contract_edit'),
    url(r'^admin/typeofcontract/delete/(?P<pk>\d+)$', type_of_contract_delete, name='type_of_contract_delete'),

    url(r'^admin/$',contract_list_user, name='contract_list'),
    url(r'^admin/contract/detail/(?P<pk>\d+)$', ContractDetail.as_view(), name='contract_detail'),
    url(r'^admin/contract/new/(?P<pk>\d+)$', contract_create, name='contract_new'),
    url(r'^admin/contract/new/list/$', list_new_contract_type, name='contract_new_list'),
    url(r'^admin/contract/edit/(?P<pk>\d+)$', contract_update, name='contract_edit'),
    url(r'^admin/contract/firmar/(?P<pk>\d+)$', contract_update_firmar, name='contract_firmar'),
    url(r'^admin/contract/delete/(?P<pk>\d+)$', contract_delete, name='contract_delete'),

    
    url(r'^admin/payscalegroups$', pay_scale_groups_list, name='pay_scale_groups_list'),
    url(r'^admin/payscalegroups/detail/(?P<pk>\d+)$', pay_scale_groups_detail, name='pay_scale_groups_detail'),

    url(r'^admin/payscalegroups/new$', pay_scale_groups_create, name='pay_scale_groups_new'),
    url(r'^admin/payscalegroups/edit/(?P<pk>\d+)$', pay_scale_groups_update, name='pay_scale_groups_edit'),
    url(r'^admin/payscalegroups/delete/(?P<pk>\d+)$', pay_scale_groups_delete, name='pay_scale_groups_delete'),

    url(r'^admin/payscalessp$', pay_scale_sp_list, name='pay_scale_sp_list'),
    url(r'^admin/payscalessp/detail/(?P<pk>\d+)$', PayScalesSpDetail.as_view(), name='pay_scale_sp_detail'),
    url(r'^admin/payscalessp/new$', pay_scale_sp_create, name='pay_scale_sp_new'),
    url(r'^admin/payscalessp/edit/(?P<pk>\d+)$', pay_scale_sp_update, name='pay_scale_sp_edit'),
    url(r'^admin/payscalessp/delete/(?P<pk>\d+)$', pay_scale_sp_delete, name='pay_scale_sp_delete'),

    

]

