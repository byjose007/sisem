# -*- coding: utf-8 -*-
from django.db import transaction, IntegrityError, OperationalError
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse
from django.contrib.auth.models import User, Group
from django.contrib.auth.decorators import login_required
import json
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from apps.contract.models import LaborSystem, TypeOfContract, Contract, PayScaleGroups, PayScalesSp

from apps.person.models import Person
from .forms import LaborSystemForm
from .forms import TypeOfContractForm
from .forms import ContractForm, ContractFormUser
from .forms import PayScaleGroupsForm
from .forms import PayScalesSpForm
from apps.accounts.models import UserProfile

from apps.institution.models import Institucion, JefaturaCoordinacion, Directivo, Direccion
# from apps.institution.models import InstitutionalUnit, Institution, Department
from apps.employee.models import Employee, DatosInstitucionales, Observations
from apps.economicdata.models import EconomicData
from apps.economicdata.views import create_or_update_economic_data_cc
from apps.employee.views import create_or_update_institutional_data, observations_create_automatic
from django.db.models import Q
from django.contrib import messages


# Create your views here.

# -----------Regimen Laboral-----------
@login_required()
def labor_system_list(request, template_name='contract/laborsystem_list.html'):
    data = {}
    query = request.GET.get('q', '')
    if query == '':
        labor_system = LaborSystem.objects.all()
        paginator = Paginator(labor_system, 10)  # Show 25 contract per page
        page = request.GET.get('page')

        try:
            labor_system = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            labor_system = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            labor_system = paginator.page(paginator.num_pages)

    else:

        if query:
            qset = (
                    Q(labor_system__icontains=query) |
                    Q(labor_system__icontains=query)
            )
            labor_system = LaborSystem.objects.filter(qset).distinct()
            paginator = Paginator(labor_system, 10)  # Show 25 contract per page
            page = request.GET.get('page')

            try:
                persons = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                labor_system = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                labor_system = paginator.page(paginator.num_pages)
        else:
            labor_system = []

    data['object_list'] = labor_system
    data['query'] = query

    return render(request, template_name, data)


@login_required()
def labor_system_create(request, template_name='contract/laborsystem_form.html'):
    form = LaborSystemForm(request.POST or None)
    try:
        if form.is_valid():
            form.save()
            # messages.add_message(request, messages.INFO, 'Persona Agregada Correctamente.')
            messages.success(request, 'Buen Trabajo. Datos ingresados correctamente.')
            return redirect('contract:labor_system_list')
    except Exception as e:
        messages.error(request, 'Tuvimos un problema. Los Datos no han sido almacenados.')
        return redirect('person:person_list')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"

    return render(request, template_name, {'form': form})


@login_required()
def labor_system_update(request, pk, template_name='contract/laborsystem_form.html'):
    try:
        labor_system = get_object_or_404(LaborSystem, pk=pk)
        form = LaborSystemForm(request.POST or None, instance=labor_system)
        if form.is_valid():
            form.save()
            messages.info(request, 'Buen Trabajo. Datos actualizados correctamente.')
            return redirect('contract:labor_system_list')
    except Exception as inst:

        messages.error(request, 'Error. Los datos no se han podido Actualizar.')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"

    return render(request, template_name, {'form': form})


@login_required()
def labor_system_delete(request, pk, template_name='contract/laborsystem_confirm_delete.html'):
    try:
        labor_system = get_object_or_404(LaborSystem, pk=pk)
        labor_system.delete()
        messages.success(request, 'Bien. Los datos han sido eliminados correctamente.')

    except Exception as inst:
        messages.error(request, 'Error. Los datos no se han podido eliminar.')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"
    return redirect('contract:labor_system_list')


@login_required()
def labor_system_detail(request, pk, template_name='contract/laborsystem_detail.html'):
    labor_system = get_object_or_404(LaborSystem, pk=pk)
    type_of_contract = TypeOfContract.objects.filter(labor_system=pk)

    context = {
        "type_of_contract": type_of_contract,
        "labor_system": labor_system,

    }
    return render(request, template_name, context)


@login_required()
class LaborSystemDetail(DetailView):
    model = LaborSystem


# -----------Tipo Contrato-----------
@login_required()
def type_of_contract_list(request, template_name='contract/typeofcontract_list.html'):
    data = {}
    query = request.GET.get('q', '')
    if query == '':
        type_of_contract = TypeOfContract.objects.all()
        paginator = Paginator(type_of_contract, 10)  # Show 25 contract per page
        page = request.GET.get('page')

        try:
            type_of_contract = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            type_of_contract = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            type_of_contract = paginator.page(paginator.num_pages)

    else:

        if query:
            qset = (
                    Q(kind__icontains=query) |
                    Q(kind__icontains=query)
            )
            type_of_contract = TypeOfContract.objects.filter(qset).distinct()
            paginator = Paginator(type_of_contract, 10)  # Show 25 contract per page
            page = request.GET.get('page')

            try:
                type_of_contract = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                type_of_contract = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                type_of_contract = paginator.page(paginator.num_pages)
        else:
            type_of_contract = []

    data['object_list'] = type_of_contract
    data['query'] = query

    return render(request, template_name, data)


@login_required()
def type_of_contract_create(request, template_name='contract/typeofcontract_form.html'):
    try:
        form = TypeOfContractForm(request.POST or None)
        if form.is_valid():
            form.save()
            messages.success(request, 'Buen Trabajo. Datos ingresados correctamente.')
            return redirect('contract:type_of_contract_list')
    except Exception as inst:
        messages.error(request, 'Error. Los datos no se han podido eliminar.')
        return redirect('contract:type_of_contract_list')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"
    return render(request, template_name, {'form': form})


@login_required()
def type_of_contract_update(request, pk, template_name='contract/typeofcontract_form.html'):
    try:
        type_of_contract = get_object_or_404(TypeOfContract, pk=pk)
        form = TypeOfContractForm(request.POST or None, instance=type_of_contract)
        if form.is_valid():
            form.save()
            messages.success(request, 'Buen Trabajo. Datos actualizados correctamente.')
            return redirect('contract:type_of_contract_list')
    except Exception as inst:
        messages.error(request, 'Error. Los datos no se han podido eliminar.')
        return redirect('contract:type_of_contract_list')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"
    return render(request, template_name, {'form': form})


@login_required()
def type_of_contract_delete(request, pk, template_name='contract/typeofcontract_confirm_delete.html'):
    try:
        type_of_contract = get_object_or_404(TypeOfContract, pk=pk)
        type_of_contract.delete()
        messages.success(request, 'Buen Trabajo. Datos eliminados correctamente.')
        return redirect('contract:type_of_contract_list')
    except Exception as inst:
        messages.error(request, 'Error. Los datos no se han podido eliminar.')
        return redirect('contract:type_of_contract_list')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"
    return render(request, template_name, {'object': type_of_contract})


class TypeOfContractDetail(DetailView):
    model = TypeOfContract


# -----------Grupos Escala de Remuneracion-----------
@login_required()
def pay_scale_groups_list(request, template_name='contract/payscalegroups_list.html'):
    data = {}
    query = request.GET.get('q', '')

    if query == '':
        pay_scale_groups = PayScaleGroups.objects.all()
        paginator = Paginator(pay_scale_groups, 10)  # Show 25 contract per page
        page = request.GET.get('page')

        try:
            pay_scale_groups = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            pay_scale_groups = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            pay_scale_groups = paginator.page(paginator.num_pages)

    else:

        if query:
            qset = (
                    Q(kind__icontains=query) |
                    Q(kind__icontains=query)
            )
            pay_scale_groups = PayScaleGroups.objects.filter(qset).distinct()
            paginator = Paginator(pay_scale_groups, 10)  # Show 25 contract per page
            page = request.GET.get('page')

            try:
                pay_scale_groups = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                pay_scale_groups = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                pay_scale_groups = paginator.page(paginator.num_pages)
        else:
            pay_scale_groups = []

    data['object_list'] = pay_scale_groups
    data['query'] = query

    return render(request, template_name, data)


@login_required()
def pay_scale_groups_create(request, template_name='contract/payscalegroups_form.html'):
    try:
        form = PayScaleGroupsForm(request.POST or None)
        if form.is_valid():
            form.save()
            messages.success(request, 'Buen Trabajo. Datos almacenados correctamente.')
            return redirect('contract:pay_scale_groups_list')
    except Exception as inst:
        messages.error(request, 'Error. Los datos no se han podido eliminar.')
        return redirect('contract:pay_scale_groups_list')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"
    return render(request, template_name, {'form': form})


@login_required()
def pay_scale_groups_update(request, pk, template_name='contract/payscalegroups_form.html'):
    try:
        pay_scale_groups = get_object_or_404(PayScaleGroups, pk=pk)
        form = PayScaleGroupsForm(request.POST or None, instance=pay_scale_groups)
        if form.is_valid():
            form.save()
            messages.success(request, 'Buen Trabajo. Datos actualizados correctamente.')
            return redirect('contract:pay_scale_groups_list')
    except Exception as inst:
        messages.error(request, 'Error. Los datos no se han podido eliminar.')
        return redirect('contract:pay_scale_groups_list')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"

    return render(request, template_name, {'form': form})


@login_required()
def pay_scale_groups_delete(request, pk, template_name='contract/payscalegroups_confirm_delete.html'):
    try:
        pay_scale_groups = get_object_or_404(PayScaleGroups, pk=pk)
        pay_scale_groups.delete()
        messages.success(request, 'Buen Trabajo. Datos eliminados correctamente.')
        return redirect('contract:pay_scale_groups_list')
    except Exception as inst:
        messages.error(request, 'Error. Los datos no se han podido eliminar.')
        return redirect('contract:pay_scale_groups_list')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"
    return render(request, template_name, {'object': pay_scale_groups})


@login_required()
def pay_scale_groups_detail(request, pk, template_name='contract/payscalegroups_detail.html'):
    pay_scale_groups = get_object_or_404(PayScaleGroups, pk=pk)
    pay_scale_sp = PayScalesSp.objects.filter(pay_scale_groups=pk)
    context = {
        "pay_scale_groups": pay_scale_groups,
        "pay_scale_sp": pay_scale_sp,
    }
    return render(request, template_name, context)


@login_required()
class PayScaleGroupsDetail(DetailView):
    model = PayScaleGroups


# -----------Escala de Remuneracion Servidores Publicos-----------
@login_required()
def pay_scale_sp_list(request, template_name='contract/payscalessp_list.html'):
    data = {}
    query = request.GET.get('q', '')

    if query == '':
        pay_scale_sp = PayScalesSp.objects.all()

        paginator = Paginator(pay_scale_sp, 10)  # Show 25 contract per page
        page = request.GET.get('page')

        try:
            pay_scale_sp = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            pay_scale_sp = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            pay_scale_sp = paginator.page(paginator.num_pages)

    else:

        if query:
            qset = (
                    Q(occupational_group__icontains=query) |
                    Q(degree__icontains=query)
            )
            pay_scale_sp = PayScalesSp.objects.filter(qset).distinct()
            paginator = Paginator(pay_scale_sp, 10)  # Show 25 contract per page
            page = request.GET.get('page')

            try:
                pay_scale_sp = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                pay_scale_sp = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                pay_scale_sp = paginator.page(paginator.num_pages)
        else:
            pay_scale_groups = []

    data['object_list'] = pay_scale_sp
    data['query'] = query

    return render(request, template_name, data)


@login_required()
def pay_scale_sp_create(request, template_name='contract/payscalessp_form.html'):
    try:
        form = PayScalesSpForm(request.POST or None)
        if form.is_valid():
            form.save()
            messages.success(request, 'Buen Trabajo. Datos almacenados correctamente.')
            return redirect('contract:pay_scale_sp_list')
    except Exception as inst:
        messages.error(request, 'Error. Los datos no se han podido eliminar.')
        return redirect('contract:pay_scale_sp_list')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"
    return render(request, template_name, {'form': form})


@login_required()
def pay_scale_sp_update(request, pk, template_name='contract/payscalessp_form.html'):
    try:
        pay_scale_sp = get_object_or_404(PayScalesSp, pk=pk)
        form = PayScalesSpForm(request.POST or None, instance=pay_scale_sp)
        if form.is_valid():
            form.save()
            messages.success(request, 'Buen Trabajo. Datos actualizados correctamente.')
            return redirect('contract:pay_scale_sp_list')
    except Exception as inst:
        messages.error(request, 'Error. Los datos no se han podido eliminar.')
        return redirect('contract:pay_scale_sp_list')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"
    return render(request, template_name, {'form': form})


@login_required()
def pay_scale_sp_delete(request, pk, template_name='contract/payscalessp_confirm_delete.html'):
    try:
        pay_scale_sp = get_object_or_404(PayScalesSp, pk=pk)
        pay_scale_sp.delete()
        messages.success(request, 'Buen Trabajo. Datos eliminados correctamente.')
        return redirect('contract:pay_scale_sp_list')
    except Exception as inst:
        messages.error(request, 'Error. Los datos no se han podido eliminar.')
        return redirect('contract:pay_scale_sp_list')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"
    return render(request, template_name, {'object': pay_scale_sp})


class PayScalesSpDetail(DetailView):
    model = PayScalesSp


# -----------Contrato-----------

@login_required()
def contract_list(request, template_name='contract/contract_list.html'):
    user_profile_autent = UserProfile.objects.get(user=request.user)  # Obtengo el perfil del cedula donde esta autenticado
    institution = user_profile_autent.institution_default  # Obtengo la institucion por defecto del cedula autenticado
    query = request.GET.get('q', '')
    data = {}
    labor_system = LaborSystem.objects.all()
    data['institution'] = institution
    data['labor_system'] = labor_system
    if query == '':
        contract = Contract.objects.filter(department__institutional_unit__institution=institution)
        paginator = Paginator(contract, 20)  # Show 25 contract per page
        page = request.GET.get('page')

        try:
            contracts = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            contracts = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            contracts = paginator.page(paginator.num_pages)

        labor_system = LaborSystem.objects.all()
        data = {}


    else:

        if query:
            qset = (
                    Q(employee__person__name__icontains=query) |
                    Q(employee__person__lastname__icontains=query) |
                    Q(employee__person__cedula__icontains=query)
            )
            contract = Contract.objects.filter(qset).distinct()
            contract = contract.filter(department__institutional_unit__institution=institution)
            paginator = Paginator(contract, 20)  # Show 25 contract per page
            page = request.GET.get('page')

            try:
                contracts = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                contracts = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                contracts = paginator.page(paginator.num_pages)

        else:
            contracts = []

    data['object_list'] = contracts
    data['query'] = query
    data['institution'] = institution
    data['labor_system'] = labor_system
    return render(request, template_name, data)


@login_required()
def contract_list_user(request, template_name='contract/contract_list.html'):
    user_profile_autent = UserProfile.objects.get(user=request.user)  # Obtengo el perfil del cedula donde esta autenticado
    institution = user_profile_autent.institution_default  # Obtengo la institucion por defecto del cedula autenticado
    query = request.GET.get('q', '')
    data = {}
    # labor_system = LaborSystem.objects.all()
    # data['institution'] = institution
    # data['labor_system'] = labor_system
    if query == '':
        # contract = Contract.objects.filter(department__institutional_unit__institution=institution)
        contract = Contract.objects.filter(jefatura__direccion__institucion=institution)
        paginator = Paginator(contract, 10)

        page = request.GET.get('page')

        try:
            contracts = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            contracts = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            contracts = paginator.page(paginator.num_pages)

        # labor_system = LaborSystem.objects.all()
        # data = {}


    else:

        if query:
            qset = (
                    Q(employee__person__name__icontains=query) |
                    Q(employee__person__lastname__icontains=query) |
                    Q(employee__person__cedula__icontains=query)
            )
            contract = Contract.objects.filter(qset).distinct()
            contract = contract.filter(jefatura__direccion__institucion=institution)
            paginator = Paginator(contract, 10)  # Show 25 contract per page
            page = request.GET.get('page')

            try:
                contracts = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                contracts = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                contracts = paginator.page(paginator.num_pages)

        else:
            contracts = []

    data['object_list'] = contracts
    data['query'] = query
    # data['institution'] = institution
    # data['labor_system'] = labor_system
    return render(request, template_name, data)


@login_required()
@transaction.atomic
def contract_create(request, pk, template_name='contract/contract_form.html'):
    user_profile_autent = UserProfile.objects.get(user=request.user)  # Obtengo el perfil del cedula donde esta autenticado
    institution = user_profile_autent.institution_default  # Obtengo la institucion por defecto del cedula autenticado
    contract_type = TypeOfContract.objects.get(id=pk)
    titulo = "Contrato - " + contract_type.labor_system.labor_system + " - " + contract_type.kind
    sp1 = transaction.savepoint()
    print('holaaa')

    # if this is a POST request we need to process the form data  
    # create a form instance and populate it with data from the request:
    try:

        form = ContractFormUser(request.POST or None, institution=institution)
        # check whether it's valid:
        if form.is_valid():
            # do something -- save, send, etc
            status = request.POST.get('status')
            contract = form.save(commit=False)
            contract.labor_system = contract_type.labor_system
            contract.type_of_contract = contract_type
            # Guarda el contrato, indistintamente del estado del mismo
            # verificar el estado del contrato 
            contract.save()
            if status == 'Firmado':
                employee = update_employee_contract(request.POST.get('employee'), request.POST.get('department'))

                if contract_type.usuario:
                    username = create_user(employee)

                if contract_type.datos_institucional:
                    institutional_data = create_or_update_institutional_data(contract)

                if contract_type.datos_economicos:
                    economic_data = create_or_update_economic_data_cc(contract)

                if contract_type.observacion:
                    titulo = "Contrato Nuevo -" + contract_type.kind
                    observacion = "Ingresa a trabajar a la institución el bajo el régimen laboral " + contract_type.labor_system.labor_system + " - " + contract_type.kind + ", en el departamento " + contract.department.department
                    contract_number = contract.id
                    observacion = observations_create_automatic(employee=employee.id, titulo=titulo, observacion=observacion,
                                                                contract_number=contract_number, user_profile_autent=user_profile_autent)

            return redirect('contract:contract_list')
        else:
            print(form.errors)
        transaction.savepoint_commit(sp1)
        messages.success(request, 'Buen Trabajo. Datos almacenados correctamente.')

    except Exception as e:

        transaction.savepoint_rollback(sp1)
        messages.errors(request, 'Error. Datos almacenados no han podido ser almacenados.')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"
    # if a GET (or any other method) we'll create a blank form    

    return render(request, template_name, {'form': form, 'titulo': titulo, 'contract_type': contract_type})


@login_required()
@transaction.atomic
def contract_update(request, pk, template_name='contract/contract_form.html'):
    user_profile_autent = UserProfile.objects.get(user=request.user)  # Obtengo el perfil del cedula donde esta autenticado
    institution = user_profile_autent.institution_default  # Obtengo la institucion por defecto del cedula autenticado
    contract = get_object_or_404(Contract, pk=pk)
    contract_type = contract.type_of_contract
    titulo = "Contrato - " + contract_type.labor_system.labor_system + " - " + contract_type.kind
    if contract.status == 'Firmado':
        return redirect('contract:type_of_contract_list')
    else:
        sp1 = transaction.savepoint()

        try:
            form = ContractForm(request.POST or None, instance=contract)
            if form.is_valid():
                # do something -- save, send, etc
                status = request.POST.get('status')
                contract = form.save(commit=False)
                contract.labor_system = contract_type.labor_system
                contract.type_of_contract = contract_type
                # Guarda el contrato, indistintamente del estado del mismo
                # verificar el estado del contrato 
                contract.save()
                if status == 'Firmado':
                    employee = update_employee_contract(request.POST.get('employee'), request.POST.get('department'))

                    if contract_type.usuario:
                        username = create_user(employee)

                    if contract_type.datos_institucional:
                        institutional_data = create_or_update_institutional_data(contract)

                    if contract_type.datos_economicos:
                        economic_data = create_or_update_economic_data_cc(contract)

                    if contract_type.observacion:
                        titulo = "Contrato Nuevo -" + contract_type.kind
                        observacion = "Ingresa a trabajar a la institución el bajo el régimen laboral " + contract_type.labor_system.labor_system + " - " + contract_type.kind + ", en el departamento " + contract.department.department
                        contract_number = contract.id
                        observacion = observations_create_automatic(employee=employee.id, titulo=titulo, observacion=observacion,
                                                                    contract_number=contract_number, user_profile_autent=user_profile_autent)

                return redirect('contract:contract_list')
            else:
                print(form.errors)
            transaction.savepoint_commit(sp1)
            messages.success(request, 'Buen Trabajo. Datos almacenados correctamente.')

        except Exception as e:

            messages.error(request, 'Buen Trabajo. Datos almacenados correctamente, se produjo el siguiente error' + str(e))
            transaction.savepoint_rollback(sp1)
        finally:
            Final = "Todas las operaciones ejecutadas con exito"
            # if a GET (or any other method) we'll create a blank form  

    return render(request, template_name, {'form': form, 'titulo': titulo, 'contract_type': contract_type})


@login_required()
@transaction.atomic
def contract_update_firmar(request, pk, template_name='contract/contract_form.html'):
    user_profile_autent = UserProfile.objects.get(user=request.user)  # Obtengo el perfil del cedula donde esta autenticado
    institution = user_profile_autent.institution_default  # Obtengo la institucion por defecto del cedula autenticado
    contract = get_object_or_404(Contract, pk=pk)
    contract_type = contract.type_of_contract

    if contract.status == 'Firmado':
        return redirect('contract:type_of_contract_list')
    else:
        sp1 = transaction.savepoint()
        try:
            contract.status = 'Firmado'
            contract.elaborated_by = str(user_profile_autent.user)
            contract.save()
            employee = update_employee_contract(contract.employee.id, contract.department.id)
            if contract_type.usuario:
                username = create_user(employee)
            if contract_type.datos_institucional:
                institutional_data = create_or_update_institutional_data(contract)

            if contract_type.datos_economicos:
                economic_data = create_or_update_economic_data_cc(contract)

            if contract_type.observacion:
                titulo = "Contrato Nuevo -" + contract_type.kind
                observacion = "Ingresa a trabajar a la institución el bajo el régimen laboral " + contract_type.labor_system.labor_system + " - " + contract_type.kind + ", en el departamento " + contract.department.department
                contract_number = contract.id
                observacion = observations_create_automatic(employee=employee.id, titulo=titulo, observacion=observacion,
                                                            contract_number=contract_number, user_profile_autent=user_profile_autent)

                return redirect('contract:contract_list')
            transaction.savepoint_commit(sp1)
            messages.success(request, 'Buen Trabajo. Datos almacenados correctamente.')

        except Exception as e:
            mensaje = 'Se produjo el siguiente error ' + str(e)
            messages.error(request, 'Buen Trabajo. Datos almacenados correctamente, se produjo el siguiente error' + str(e))

            transaction.savepoint_rollback(sp1)
        finally:
            Final = "Todas las operaciones ejecutadas con exito"
            # if a GET (or any other method) we'll create a blank form  

    return redirect('contract:contract_list')


@login_required()
def list_new_contract_type(request, template_name='contract/new_contract_list.html'):
    data = {}
    labor_system = LaborSystem.objects.all()
    contract_type = TypeOfContract.objects.filter(type_contract_status=True)
    data['contract_type'] = contract_type
    data['labor_system'] = labor_system
    return render(request, template_name, data)


@login_required()
def update_employee_contract(employee, department):
    print('actualizar empleado')
    employee = get_object_or_404(Employee, pk=employee)

    # Actualizar employee           
    employee.employee_type = 'servicios profesionaless'
    employee.status = 'empleado'
    department = get_object_or_404(Direccion, pk=department)
    employee.institution = department.institutional_unit.institution
    employee.coordinacion = department.institutional_unit
    employee.direccion = department
    employee.save()
    return employee


# Funciones Adicionales para realizar cambios en el sistema cada vez que se ingrese o modifique un contrato


@login_required()
def create_user(employee):
    print('vrear cedula')

    if User.objects.filter(username=employee.person.cedula):
        print(employee.person.cedula)
    else:
        user = User.objects.create_user(username=employee.person.cedula, email=employee.person.email, password=employee.person.cedula + "12345",
                                        first_name=employee.person.name, last_name=employee.person.lastname)
        user.save()
    # Asigno permisos al cedula, asigno el cedula a un grupo por defecto
    usuario = User.objects.get(username=employee.person.cedula)
    grupo, creado = Group.objects.get_or_create(name='Usuarios Nuevos')
    usuario.groups.add(grupo)
    # Crear el profile del cedula - asignando la institucion por defecto del mismo
    user_profile = UserProfile.objects.update_or_create(user=usuario)
    user_profile = UserProfile.objects.get(user=usuario)
    user_profile.institution_default = employee.department.institutional_unit.institution
    user_profile.save()
    return usuario


@login_required()
def update_employee_contract_status_firmado(request, pk):
    contract = get_object_or_404(Contract, pk=pk)
    # contract = Contract.get(pk=pk)
    print(contract)
    contract.status = 'Firmado'
    contract.save()

    return redirect('contract:contract_list')


@login_required()
def contract_delete(request, pk, template_name='contract/contract_confirm_delete.html'):
    contract = get_object_or_404(Contract, pk=pk)
    if request.method == 'POST':
        contract.delete()
        return redirect('contract:contract_list')
    return render(request, template_name, {'object': contract})


@login_required()
def contract_detail(request, pk, template_name='contract/contract_detail.html'):
    contract = Contract.objects.get(id=pk)

    context = {
        "contract": contract,

    }
    return render(request, template_name, context)


class ContractDetail(DetailView):
    model = Contract
