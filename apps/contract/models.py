# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from apps.institution.models import Institucion, JefaturaCoordinacion, Directivo, Direccion
# from apps.institution.models import InstitutionalUnit, Department
from apps.employee.models import Employee
from apps.schedule.models import Schedule

# Create your models here.


firmado = 'Firmado'
sin_firmar = 'Sin firmar'
vencido = 'Vencido'
terminado = 'Terminado'
contract_status_list = (
    (firmado, 'Firmado'),
    (sin_firmar, 'Sin firmar'),
    (vencido, 'Vencido'),
    (terminado, 'Terminado'),
)


class LaborSystem(models.Model):
    labor_system = models.CharField(verbose_name='Régimen laboral', max_length=100)
    status = models.BooleanField(verbose_name='Estado', default=True)

    class Meta:
        ordering = ['labor_system']
        verbose_name = 'Regimen Laboral'
        verbose_name_plural = 'Regimen Laboral'

    def __str__(self):
        return u'{0}'.format(self.labor_system)

    def __unicode__(self):
        return u'{0}'.format(self.labor_system)


class TypeOfContract(models.Model):
    labor_system = models.ForeignKey('LaborSystem', verbose_name='Régimen Laboral', on_delete=models.PROTECT)
    kind = models.CharField(verbose_name='Tipo de contrato', max_length=100)
    employee_type = models.CharField(verbose_name='Tipo empleado que genera', max_length=100, blank=True, null=True)
    force_from = models.DateField(verbose_name='Vigente desde', blank=True, null=True)
    type_contract_status = models.BooleanField(verbose_name='Estado del tipo de contrato', default=True)
    # -------------------Configuración de Funciones-------------------------------------------------
    usuario = models.BooleanField(verbose_name='Crear Usuario', default=True)
    perfil_usuario = models.BooleanField(verbose_name='Crear Perfil de Usuario', default=True)
    datos_institucional = models.BooleanField(verbose_name='Crear Datos Institucionales', default=True)
    datos_economicos = models.BooleanField(verbose_name='Crear Datos Económicos', default=True)
    horario = models.BooleanField(verbose_name='Registrar horario', default=True)
    observacion = models.BooleanField(verbose_name='Generar Observación', default=True)

    # -------------------Configuración de cambios a almacenar contrato-------------------------------
    department = models.BooleanField(verbose_name='Departamento', default=True)
    public_office = models.BooleanField(verbose_name='Cargo Público', default=True)
    functions = models.BooleanField(verbose_name='Funciones', default=True)
    budget_certification = models.BooleanField(verbose_name='Certificación presupuestaria', default=True)
    budget_item = models.BooleanField(verbose_name='Partida presupuestaria', default=True)
    remuneration = models.BooleanField(verbose_name='Remuneración', default=True)
    start_date = models.BooleanField(verbose_name='Fecha Inicio', default=True)
    end_date = models.BooleanField(verbose_name='Fecha terminación', default=True)
    place_of_work = models.BooleanField(verbose_name='Lugar de trabajo', default=True)

    pay_scale_groups = models.BooleanField(verbose_name='Grupo de escala de remuneración', default=True)
    schedule = models.BooleanField(verbose_name='Horario', default=True)
    collective_contract = models.BooleanField(verbose_name='Contrato colectivo', default=True)

    memo_area_director = models.BooleanField(verbose_name='Memo director de área', default=True)
    memo_human_resources_director = models.BooleanField(verbose_name='Memo director de RRHH', default=True)
    technical_report = models.BooleanField(verbose_name='Informe técnico', default=True)
    disposed = models.BooleanField(verbose_name='Dispuesto por', default=True)
    date_of_preparation = models.BooleanField(verbose_name='Fecha de elaboración', default=True)
    checked_by = models.BooleanField(verbose_name='Revisado por', default=True)

    # digitized_contract = models.FileField(verbose_name='Contrato digitalizado',blank=False, null=True)


    class Meta:
        ordering = ['labor_system', 'kind']
        verbose_name = 'Tipo de contrato'
        verbose_name_plural = 'Tipos de contrato'

    def __str__(self):
        return u'{0}'.format(self.kind)

    def __unicode__(self):
        return u'{0}'.format(self.kind)


class PayScaleGroups(models.Model):
    pay_scale_groups = models.CharField(verbose_name='Grupos de escala de remuneración', max_length=100)

    class Meta:
        ordering = ['pay_scale_groups']
        verbose_name = 'Grupo de escala de remuneración'
        verbose_name_plural = 'Grupos de escala de remuneración'

    def __str__(self):
        return u'{0}'.format(self.pay_scale_groups)


class PayScalesSp(models.Model):
    pay_scale_groups = models.ForeignKey('PayScaleGroups', verbose_name='Grupo de escala de remuneración',
                                         on_delete=models.PROTECT)
    occupational_group = models.CharField(verbose_name='Grupo ocupacional', max_length=50)
    degree = models.IntegerField(verbose_name='Grado')
    unified_pay = models.DecimalField(verbose_name='Remuneración unificada', max_digits=7, decimal_places=2)

    class Meta:
        ordering = ['pay_scale_groups', 'degree']
        verbose_name = 'Escala de remuneracion de Servidores Públicos'
        verbose_name_plural = 'Escalas de remuneracion de Servidores Públicos'

    def __str__(self):
        return u'{0}'.format(self.occupational_group)

    def __unicode__(self):
        return u'{0}'.format(self.occupational_group)


class Contract(models.Model):
    employee = models.ForeignKey(Employee, verbose_name='Empleado')

    labor_system = models.ForeignKey('LaborSystem', verbose_name='Régimen Laboral', on_delete=models.PROTECT)
    type_of_contract = models.ForeignKey('TypeOfContract', verbose_name='Tipo de contrato', on_delete=models.PROTECT)

    jefatura = models.ForeignKey(Direccion, verbose_name='Jefatura', blank=True, null=True)
    public_office = models.CharField(verbose_name='Cargo público', max_length=100, blank=True, null=True)
    functions = models.TextField(verbose_name='Funciones', blank=True, null=True)
    place_of_work = models.CharField(verbose_name='Lugar de trabajo', max_length=100, blank=True, null=True)
    schedule = models.ForeignKey(Schedule, verbose_name='Horario', blank=True, null=True)

    budget_certification = models.CharField(verbose_name='Certificación presupestario', max_length=100, blank=True,
                                            null=True)
    budget_item = models.CharField(verbose_name='Partida presupuestaria', max_length=100, blank=True, null=True)
    pay_scale_groups = models.ForeignKey('PayScalesSp', verbose_name='Grupo de escala de remuneración', blank=True,
                                         null=True)
    remuneration = models.DecimalField(verbose_name='Remuneración', max_digits=6, decimal_places=2, blank=True,
                                       null=True)
    collective_contract = models.BooleanField(verbose_name='Contrato Colectivo', default=False, blank=True)

    start_date = models.DateField(verbose_name='Fecha de inicio', blank=True, null=True)
    end_date = models.DateField(verbose_name='Fecha de fin', blank=True, null=True)
    disposed = models.CharField(max_length=100, blank=True, null=True, verbose_name='Dispuesto por')

    memo_area_director = models.CharField(verbose_name='Memorando del director de área', max_length=100, blank=True,
                                          null=True)
    memo_human_resources_director = models.CharField(verbose_name='Memorando del director de RRHH', max_length=100,
                                                     blank=True, null=True)
    technical_report = models.CharField(verbose_name='Informe Técnico', max_length=100, blank=True, null=True)

    date_of_preparation = models.DateField(verbose_name='Fecha de elaboración', blank=True, null=True)
    elaborated_by = models.CharField(verbose_name='Elaborado por', max_length=100)
    checked_by = models.CharField(verbose_name='Revisado por', max_length=100, blank=True, null=True)
    # digitized_contract = models.FileField(verbose_name='Contrato digitalizado',blank=False, null=True)
    status = models.CharField(verbose_name='Estado del contrato', max_length=30, choices=contract_status_list,
                              default=sin_firmar)

    class Meta:
        ordering = ['employee']
        verbose_name = 'Contrato'
        verbose_name_plural = 'Contratos'

    def __str__(self):
        return u'{0}'.format(self.employee)

    def __unicode__(self):
        return u'{0}'.format(self.employee)
