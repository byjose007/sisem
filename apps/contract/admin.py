from django.contrib import admin
from apps.contract.models import LaborSystem
from apps.contract.models import TypeOfContract
from apps.contract.models import Contract
from apps.contract.models import PayScaleGroups
from apps.contract.models import PayScalesSp

# Register your models here.

class TypeOfContractAdmin(admin.ModelAdmin):
	list_display = ('labor_system','kind','force_from','type_contract_status')
	search_fields = ('labor_system','kind','force_from','type_contract_status')

class ContractAdmin(admin.ModelAdmin):
	list_display = ('employee','type_of_contract','jefatura','status')
	search_fields = ('employee','type_of_contract','jefatura','status')



class PayScalesSpAdmin(admin.ModelAdmin):
	list_display = ('pay_scale_groups','occupational_group','degree','unified_pay')
	search_fields = ('pay_scale_groups','occupational_group','degree','unified_pay')
	#list_filter = ('institucion',)

admin.site.register(LaborSystem)
admin.site.register(TypeOfContract, TypeOfContractAdmin)
admin.site.register(Contract, ContractAdmin)	
admin.site.register(PayScalesSp,PayScalesSpAdmin)	
admin.site.register(PayScaleGroups)	

