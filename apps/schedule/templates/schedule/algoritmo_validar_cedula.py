----------------Forms.py
class FormInstitucion2(forms.Form): 
    ruc = forms.CharField(max_length=13)
    institucion = forms.CharField(max_length=100)
    direccion = forms.CharField(max_length=100,required=False)
    telefono = forms.CharField(max_length=10,required=False)
    fax = forms.CharField(max_length=10,required=False)
    correo = forms.EmailField(required=False)
    sitio_web = forms.URLField(required=False)

    def clean_ruc(self):
        ruc=self.cleaned_data['ruc']
        l = len(ruc)
        total = 0
        if l == 10 or l == 13: # verificar la longitud correcta
            cp = int(ruc[0:2])
            if cp >= 1 and cp <= 22: # verificar codigo de provincia
                tercer_dig = int(ruc[2])
                if tercer_dig >= 0 and tercer_dig < 6 : # numeros enter 0 y 6
                    if l == 10:
                        base = 10
                        d_ver = int(ruc[9])# digito verificador
                        multip = (2, 1, 2, 1, 2, 1, 2, 1, 2)
                        tipo = 0                      
                    elif l == 13:
                        base = 10
                        d_ver = int(ruc[9])# digito verificador
                        multip = (2, 1, 2, 1, 2, 1, 2, 1, 2)
                        tipo = 0                         
                elif tercer_dig == 6:
                    base = 11
                    d_ver = int(ruc[8])
                    multip = (3, 2, 7, 6, 5, 4, 3, 2 )
                    tipo = 1
                elif tercer_dig == 9: # si es ruc
                    base = 11
                    d_ver = int(ruc[9])
                    multip = (4, 3, 2, 7, 6, 5, 4, 3, 2)
                    tipo = 2
                else:
                    raise forms.ValidationError('Tercer digito invalido') 
            else:
                raise forms.ValidationError('Codigo de provincia incorrecto')            
        else:
            raise forms.ValidationError('Longitud incorrecta del numero ingresado')                
            
        for i in range(0,len(multip)):
            p = int(ruc[i]) * multip[i]
            if tipo == 0:
                total+=p if p < 10 else int(str(p)[0])+int(str(p)[1])
            else:
                total+=p
        mod = total % base
        val = base - mod if mod != 0 else 0
        return ruc    