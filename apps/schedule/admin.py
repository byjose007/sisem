from django.contrib import admin
from apps.schedule.models import DayHour
from apps.schedule.models import Schedule




# Register your models here.

class DayHourAdmin(admin.ModelAdmin):
	list_display = ('day','time_of_entry','time_of_departure')
	search_fields = ('day','day')

class ScheduleAdmin(admin.ModelAdmin):
	list_display = ('institution','schedule','description')
	search_fields = ('institution','schedule','description')
	filter_horizontal = ('day_hour',)
	#list_filter = ('institucion',)



admin.site.register(DayHour, DayHourAdmin)
admin.site.register(Schedule, ScheduleAdmin)
