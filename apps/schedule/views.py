# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import get_object_or_404
from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from .models import DayHour, Schedule
from .forms import DayHourForm
from .forms import ScheduleForm, ScheduleUserForm
from apps.accounts.models import UserProfile
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.contrib import messages


# Create your views here.
# ------Dia - Hora----------------
@login_required()
def day_hour_list(request, template_name='schedule/dayhour_list.html'):
    data = {}
    query = request.GET.get('q', '')
    if query == '':
        day_hour = DayHour.objects.all()
        paginator = Paginator(day_hour, 10)  # Show 25 contract per page
        page = request.GET.get('page')

        try:
            day_hour = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            day_hour = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            day_hour = paginator.page(paginator.num_pages)

    else:
        if query:
            qset = (
                Q(day__icontains=query) |
                Q(day__icontains=query)

            )
            day_hour = DayHour.objects.filter(qset).distinct()
            paginator = Paginator(day_hour, 10)  # Show 25 contract per page
            page = request.GET.get('page')

            try:
                day_hour = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                day_hour = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                day_hour = paginator.page(paginator.num_pages)
        else:
            day_hour = []

    data['object_list'] = day_hour
    data['query'] = query
    return render(request, template_name, data)


@login_required()
def day_hour_create(request, template_name='schedule/dayhour_form.html'):
    try:
        form = DayHourForm(request.POST or None)
        if form.is_valid():
            form.save()
            messages.success(request, 'Buen Trabajo. Datos ingresados correctamente.')
            return redirect('schedule:dayhour_list')

    except Exception as e:
        messages.error(request, 'Tuvimos un problema. Los Datos no han sido almacenados.')
        return redirect('schedule:schedule_list')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"

    return render(request, template_name, {'form': form})


@login_required()
def day_hour_update(request, pk, template_name='schedule/dayhour_form.html'):
    try:
        day_hour = get_object_or_404(DayHour, pk=pk)
        form = DayHourForm(request.POST or None, instance=day_hour)
        if form.is_valid():
            form.save()
            messages.success(request, 'Buen Trabajo. Datos actualizados correctamente.')
            return redirect('schedule:dayhour_list')
    except Exception as e:
        messages.error(request, 'Tuvimos un problema. Los Datos no han sido almacenados.')
        return redirect('schedule:schedule_list')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"
    return render(request, template_name, {'form': form})


@login_required()
def day_hour_delete(request, pk, template_name='schedule/dayhour_confirm_delete.html'):
    try:
        day_hour = get_object_or_404(DayHour, pk=pk)
        day_hour.delete()
        messages.success(request, 'Buen Trabajo. Datos eliminados correctamente.')
        return redirect('schedule:dayhour_list')
    except Exception as e:
        messages.error(request, 'Tuvimos un problema. Los Datos no han sido almacenados.')
        return redirect('schedule:schedule_list')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"
    return render(request, template_name, {'object': day_hour})


class DayHourDetail(DetailView):
    model = DayHour


# ------Horario----------------
@login_required()
def schedule_list(request, template_name='schedule/schedule_list_admin.html'):
    schedule = Schedule.objects.all()
    data = {}
    query = request.GET.get('q', '')
    if query == '':
        schedule = Schedule.objects.all()
        paginator = Paginator(schedule, 10)  # Show 25 contract per page
        page = request.GET.get('page')

        try:
            schedules = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            schedules = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            schedules = paginator.page(paginator.num_pages)

    else:
        if query:
            qset = (
                Q(schedule__icontains=query) |
                Q(description__icontains=query)

            )
            schedule = Schedule.objects.filter(qset).distinct()
            paginator = Paginator(schedule, 10)  # Show 25 contract per page
            page = request.GET.get('page')

            try:
                schedules = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                schedules = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                schedules = paginator.page(paginator.num_pages)
        else:
            schedules = []

    data['object_list'] = schedules
    data['query'] = query

    horarios = Schedule.objects.all()
    data['horarios'] = horarios

    return render(request, template_name, data)


@login_required()
def schedule_create(request, template_name='schedule/schedule_form_admin.html'):
    try:
        form = ScheduleForm(request.POST or None)
        if form.is_valid():
            form.save()
            messages.success(request, 'Buen Trabajo. Datos ingresados correctamente.')
            return redirect('schedule:schedule_list')
    except Exception as e:
        messages.error(request, 'Tuvimos un problema. Los Datos no han sido almacenados.')
        return redirect('schedule:schedule_list')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"
    return render(request, template_name, {'form': form})


@login_required()
def schedule_update(request, pk, template_name='schedule/schedule_form_admin.html'):
    try:
        schedule = get_object_or_404(Schedule, pk=pk)
        form = ScheduleForm(request.POST or None, instance=schedule)
        if form.is_valid():
            form.save()
            messages.success(request, 'Buen Trabajo. Datos actualizados correctamente.')
            return redirect('schedule:schedule_list')
    except Exception as e:
        messages.error(request, 'Tuvimos un problema. Los Datos no han sido almacenados.')
        return redirect('schedule:schedule_list')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"

    return render(request, template_name, {'form': form})


@login_required()
def schedule_delete(request, pk, template_name='schedule/schedule_confirm_delete_admin.html'):
    try:
        schedule = get_object_or_404(Schedule, pk=pk)
        schedule.delete()
        messages.success(request, 'Buen Trabajo. Datos eliminados correctamente.')
        return redirect('schedule:schedule_list')
    except Exception as e:
        messages.error(request, 'Tuvimos un problema. Los Datos no han sido almacenados.')
        return redirect('schedule:schedule_list')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"
    return render(request, template_name, {'object': schedule})


@login_required()
class ScheduleDetail(DetailView):
    model = Schedule


@login_required()
def schedule_detail(request, pk, template_name='schedule/schedule_detail_admin.html'):
    schedule = Schedule.objects.get(id=pk)
    context = {
        "schedule": schedule,

    }
    return render(request, template_name, context)


# ------------------------Schedule Usuario--------------------------
@login_required()
def schedule_list_user(request, template_name='schedule/schedule_list.html'):
    user_profile = UserProfile.objects.get(user=request.user)

    data = {}
    query = request.GET.get('q', '')
    if query == '':
        schedule = Schedule.objects.filter(institution=user_profile.institution_default)
        paginator = Paginator(schedule, 10)  # Show 25 contract per page
        page = request.GET.get('page')
        try:
            schedules = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            schedules = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            schedules = paginator.page(paginator.num_pages)
    else:
        if query:
            qset = (
                Q(schedule__icontains=query) |
                Q(description__icontains=query)
            )
            schedule = Schedule.objects.filter(qset).distinct()
            schedule = schedule.filter(institution=user_profile.institution_default)
            paginator = Paginator(schedule, 10)  # Show 25 contract per page
            page = request.GET.get('page')
            try:
                schedules = paginator.page(page)
            except PageNotAnInteger:
                # If page is not an integer, deliver first page.
                schedules = paginator.page(1)
            except EmptyPage:
                # If page is out of range (e.g. 9999), deliver last page of results.
                schedules = paginator.page(paginator.num_pages)
        else:
            schedules = []

    data['object_list'] = schedules
    data['query'] = query
    return render(request, template_name, data)


@login_required()
def schedule_create_user(request, template_name='schedule/schedule_form.html'):
    try:
        form = ScheduleUserForm(request.POST or None)
        if form.is_valid():
            user_profile = UserProfile.objects.get(user=request.user)
            schedule = form.save(commit=False)
            schedule.institution = user_profile.institution_default
            schedule.save()
            messages.success(request, 'Buen Trabajo. Datos almacenados correctamente.')
            return redirect('schedule:schedule_list_user')
    except Exception as e:
        messages.error(request, 'Tuvimos un problema. Los Datos no han sido almacenados.')
        return redirect('schedule:schedule_list_user')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"
    return render(request, template_name, {'form': form})


@login_required()
def schedule_update_user(request, pk, template_name='schedule/schedule_form.html'):
    try:
        schedule = get_object_or_404(Schedule, pk=pk)
        form = ScheduleUserForm(request.POST or None, instance=schedule)
        if form.is_valid():
            user_profile = UserProfile.objects.get(user=request.user)
            schedule = form.save(commit=False)
            schedule.institution = user_profile.institution_default
            form.save()
            messages.success(request, 'Buen Trabajo. Datos actualizados correctamente.')
            return redirect('schedule:schedule_list_user')
    except Exception as e:
        messages.error(request, 'Tuvimos un problema. Los Datos no han sido almacenados.')
        return redirect('schedule:schedule_list_user')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"
    return render(request, template_name, {'form': form})


@login_required()
def schedule_delete_user(request, pk, template_name='schedule/schedule_confirm_delete.html'):
    try:
        schedule = get_object_or_404(Schedule, pk=pk)
        if request.method == 'POST':
            schedule.delete()
            messages.success(request, 'Buen Trabajo. Datos eliminados correctamente.')
            return redirect('schedule:schedule_list_user')
    except Exception as e:
        messages.error(request, 'Tuvimos un problema. Los Datos no han sido almacenados.')
        return redirect('schedule:schedule_list_user')
    finally:
        Final = "Todas las operaciones ejecutadas con exito"
    return render(request, template_name, {'object': schedule})


class ScheduleDetailUser(DetailView):
    model = Schedule
