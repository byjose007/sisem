from django import forms
from django.forms import ModelForm
from .models import DayHour, Schedule


class DayHourForm(ModelForm):
    class Meta:
        model = DayHour
        fields = '__all__'
        exclude = ('id',)


class ScheduleForm(ModelForm):
    class Meta:
        model = Schedule
        fields = '__all__'
        exclude = ('id',)


class ScheduleUserForm(ModelForm):
    class Meta:
        model = Schedule
        fields = '__all__'
        exclude = ('id', "institucion",)
