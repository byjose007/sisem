from django.conf.urls import url
from .views import (
    #admin
    DayHourDetail,
    ScheduleDetail,     
    schedule_list,
    schedule_create,
    schedule_delete,
    schedule_update,
    schedule_detail,
    day_hour_list,
    day_hour_create,
    day_hour_update,
    day_hour_delete,
    #user
    schedule_list_user,
    schedule_create_user,
    schedule_delete_user,
    schedule_update_user,
    ScheduleDetailUser
)


urlpatterns = [
    #Admin
 	url(r'^admin/$', schedule_list, name='schedule_list'),
    url(r'^admin/(?P<pk>\d+)$', schedule_detail, name='schedule_detail'),
    url(r'^admin/new$',schedule_create, name='schedule_new'),
    url(r'^admin/edit/(?P<pk>\d+)$', schedule_update, name='schedule_edit'),
    url(r'^admin/delete/(?P<pk>\d+)$', schedule_delete, name='schedule_delete'),

    url(r'^dayhour$',day_hour_list, name='dayhour_list'),
    url(r'^dayhour/detail/(?P<pk>\d+)$', DayHourDetail.as_view(), name='dayhour_detail'),
    url(r'^dayhour/new$',day_hour_create, name='dayhour_new'),
    url(r'^dayhour/edit/(?P<pk>\d+)$',day_hour_update , name='dayhour_edit'),
    url(r'^dayhour/delete/(?P<pk>\d+)$',day_hour_delete , name='dayhour_delete'),
    

    #User
    url(r'^$', schedule_list_user, name='schedule_list_user'),
    url(r'^(?P<pk>\d+)$', ScheduleDetailUser.as_view(), name='schedule_detail_user'),
    url(r'^new$',schedule_create_user, name='schedule_new_user'),
    url(r'^edit/(?P<pk>\d+)$', schedule_update_user, name='schedule_edit_user'),
    url(r'^delete/(?P<pk>\d+)$', schedule_delete_user, name='schedule_delete_user'),


]