# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from apps.institution.models import Institucion


# Create your models here.
class DayHour(models.Model):
    lunes = 'lunes'
    martes = 'martes'
    miercoles = 'miercoles'
    jueves = 'jueves'
    viernes = 'viernes'
    sabado = 'sabado'
    domingo = 'domingo'
    days = (
        (lunes, 'Lunes'),
        (martes, 'Martes'),
        (miercoles, 'Miércoles'),
        (jueves, 'Jueves'),
        (viernes, 'Viernes'),
        (sabado, 'Sábado'),
        (domingo, 'Domingo'),
    )
    day = models.CharField(verbose_name='Día', max_length=9, choices=days, default=lunes)
    time_of_entry = models.TimeField(verbose_name='Hora de entrada')
    time_of_departure = models.TimeField(verbose_name='Hora de salida')

    class Meta:
        db_table = 'Day_hour'
        verbose_name = 'Día - Hora'
        verbose_name_plural = 'Días - Horas'

    def __str__(self):
        return u'{0}-{1}-{2}'.format(self.day, self.time_of_entry, self.time_of_departure)

    def __unicode__(self):
        return u'{0}-{1}-{2}'.format(self.day, self.time_of_entry, self.time_of_departure)


class Schedule(models.Model):
    institution = models.ForeignKey(Institucion, verbose_name='Institución')
    schedule = models.CharField(max_length=100, verbose_name='Horario')
    description = models.TextField(max_length=100, verbose_name='Descripción')
    effect_since = models.DateField(max_length=100, verbose_name='Vigente desde')
    permitted_delay = models.IntegerField(blank=True, null=True, verbose_name='Atraso permitido')
    day_hour = models.ManyToManyField('DayHour', verbose_name='Día - Hora')

    class Meta:
        ordering = ['institution', 'schedule']
        verbose_name = 'Horario'
        verbose_name_plural = 'Horarios'

    def __str__(self):
        return u'{0}'.format(self.schedule)

    def __unicode__(self):
        return u'{0}-{1}-{2}'.format(self.day, self.time_of_entry, self.time_of_departure)
