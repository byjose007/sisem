from django.conf.urls import url

from apps.accounting_finance import views

urlpatterns = [
    url(r'^accounting_period/all/$', views.AccountingPeriodList.as_view(), name='accounting_period_all'),
    url(r'^accounting_period/redirect/$', views.accounting_period_redirect, name='accounting_period_redirect'),
    url(r'^accounting_period/detail/(?P<pk>\d+)/$', views.AccountingPeriodDetail.as_view(), name='accounting_period_detail'),
    url(r'^accounting_period/create/$', views.AccountingPeriodCreation.as_view(), name='accounting_period_create'),
    url(r'^accounting_period/edit/(?P<pk>\d+)$', views.AccountingPeriodUpdate.as_view(), name='accounting_period_edit'),
    #url(r'^accounting_period/delete/(?P<pk>\d+)$', views.AccountingPeriodDelete.as_view(), name='accounting_period_delete'),
    url(r'^accounting_period/delete/$', views.accounting_period_delete, name='accounting_period_delete'),
    url(r'^accounts_plan/all/$', views.AccountsPlanList.as_view(), name='accounts_plan_all'),
    url(r'^accounts_plan/detail/$', views.AccountsPlanDetail.as_view(), name='accounts_plan_detail'),
    url(r'^accounts_plan/create/$', views.AccountsPlanCreation.as_view(), name='accounts_plan_create'),
    url(r'^accounts_plan/edit/$', views.AccountsPlanUpdate.as_view(), name='accounts_plan_edit'),
    #url(r'^accounts_plan/delete/$', views.AccountsPlanDelete.as_view(), name='accounts_plan_delete'),
    url(r'^accounts_plan/delete/(?P<pk>\d+)/$', views.accounts_plan_delete, name='accounts_plan_delete'),
    url(r'^accounting_account/all/$', views.AccountingAccountList.as_view(), name='accounting_account_all'),
    url(r'^accounting_account/create/$', views.AccountingAccountCreation.as_view(), name='accounting_account_create'),
    url(r'^accounting_account/edit/$', views.AccountingAccountUpdate.as_view(), name='accounting_account_edit'),
    url(r'^accounting_account_tree/$', views.accounting_account_tree, name='accounting_account_tree'),
    url(r'^aa_list_json/$', views.aa_list_json, name='aa_list_json'),
    url(r'^aa_object_json/$', views.aa_object_json, name='aa_object_json'),
    ]
