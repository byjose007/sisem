from django.shortcuts import render, redirect, get_object_or_404, render_to_response
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest , Http404, JsonResponse, QueryDict
from django.template import Context, RequestContext
from django.contrib import messages
from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy, reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core import serializers
from django.db.models import Q
import json
from .models import (AccountingPeriod, AccountsPlan, AccountingAccount)
from .forms import (AccountsPlanForm, AccountingAccountForm)
from apps.evaluation.models import *

# Create your views here.

class AccountingPeriodList(ListView):
	model = AccountingPeriod
	template_name = 'accounting_period_list.html'
	paginate_by = 10

	def dispatch(self, *args, **kwargs):
		self.object_list = AccountingPeriod.objects.all()
		return super(AccountingPeriodList, self).dispatch(*args, **kwargs)
		
	def get_context_data(self, **kwargs):
		context = super(AccountingPeriodList, self).get_context_data(**kwargs)
		paginator = Paginator(self.object_list, self.paginate_by)
		page = self.request.GET.get('page')
		try:
			paginator_a_p = paginator.page(page)
		except PageNotAnInteger:
			paginator_a_p = paginator.page(1)
		except EmptyPage:
			paginator_a_p = paginator.page(paginator.num_pages)
		context['object_list'] = paginator_a_p
		return context

class AccountingPeriodDetail(DetailView):
	model = AccountingPeriod
	template_name = 'accounting_period_detail.html'

class AccountingPeriodCreation(CreateView):
	model = AccountingPeriod
	success_url = reverse_lazy('accounting_finance:accounting_period_all')
	template_name = 'accounting_period_form.html'
	fields = '__all__'

class AccountingPeriodUpdate(UpdateView):
	model = AccountingPeriod
	success_url = reverse_lazy('accounting_finance:accounting_period_all')
	template_name = 'accounting_period_form.html'
	fields = '__all__'

class AccountingPeriodDelete(DeleteView):
	model = AccountingPeriod
	success_url = reverse_lazy('accounting_finance:accounting_period_all')
	template_name = 'accounting_period_delete.html'

def accounting_period_delete(request):
	print('accounting_period_delete')
	# if 'HTTP_X_HTTP_METHOD_OVERRIDE' in request.META:
	# 	print('intro HTTP_X_HTTP_METHOD_OVERRIDE')
	# 	http_method = request.META['HTTP_X_HTTP_METHOD_OVERRIDE']
	# 	print(http_method)
	if request.method == 'DELETE':
		print('intro DELETE')
		accounting_period_id = int(QueryDict(request.body).get('accounting_period_id'))
		print(accounting_period_id)
		accounting_period = AccountingPeriod.objects.get(pk=accounting_period_id)
		accounting_period.delete()
		#messages.success(request, "Objeto eliminado correctamente!")
		messages.error(request, "Error al eliminar el objeto")
		return HttpResponse()

def accounting_period_redirect(request):
	url = reverse('accounting_finance:accounting_period_all')
	#return HttpResponseRedirect('%s' % (url))
	return HttpResponse("Here's the text of the Web page.")

class AccountsPlanList(ListView):
	model = AccountsPlan
	template_name = 'accounts_plan_list.html'
	paginate_by = 10

	def dispatch(self, *args, **kwargs):
		print('>>> dispatch')
		accounting_period_id = self.request.GET['accounting_period']
		self.accounting_period = get_object_or_404(AccountingPeriod, pk=accounting_period_id)
		self.accounts_plan_list =  AccountsPlan.objects.filter(accounting_period__pk=accounting_period_id)
		print(type(self.accounts_plan_list))
		print(len(self.accounts_plan_list))
		return super(AccountsPlanList, self).dispatch(*args, **kwargs)

	def get_context_data(self, **kwargs):
		# Call the base implementation first to get a context
		context = super(AccountsPlanList, self).get_context_data(**kwargs)
		paginator = Paginator(self.accounts_plan_list, self.paginate_by)
		page = self.request.GET.get('page')
		try:
			paginator_c_t_c = paginator.page(page)
		except PageNotAnInteger:
			paginator_c_t_c = paginator.page(1)
		except EmptyPage:
			paginator_c_t_c = paginator.page(paginator.num_pages)
		context['object_list'] = paginator_c_t_c
		context['accounting_period'] = self.accounting_period
		return context

class AccountsPlanDetail(DetailView):
	model = AccountsPlan
	template_name = 'accounts_plan_detail.html'

	def dispatch(self, *args, **kwargs):
		self.accounting_period = get_object_or_404(AccountingPeriod, pk=kwargs['ap_pk'])
		return super(AccountsPlanDetail, self).dispatch(*args, **kwargs)

	def get_context_data(self, **kwargs):
		context = super(AccountsPlanDetail, self).get_context_data(**kwargs)
		context['accounting_period'] = self.accounting_period
		return context

class AccountsPlanCreation(CreateView):
	model = AccountsPlan
	success_url = reverse_lazy('accounting_finance:accounts_plan_all')
	template_name = 'accounts_plan_form.html'
	form_class = AccountsPlanForm

	def dispatch(self, *args, **kwargs):
		if self.request.method == 'GET':
			print('GET')
			p_a = self.request.GET['accounting_period']
			self.accounting_period = get_object_or_404(AccountingPeriod, pk=p_a)
			print('>')
			print(self.accounting_period)
		if self.request.method == 'POST':
			print('POST')
			p_a = self.request.GET['accounting_period']
			self.accounting_period = get_object_or_404(AccountingPeriod, pk=p_a)

		return super(AccountsPlanCreation, self).dispatch(*args, **kwargs)

	def get_context_data(self, **kwargs):
		print('get_context_data')
		context = super(AccountsPlanCreation, self).get_context_data(**kwargs)
		context['accounting_period'] = self.accounting_period
		return context

	def form_valid(self, form):
		print('form_valid')
		print(self.request.path)
		self.object = form.save(commit=False)
		self.object.accounting_period = self.accounting_period
		self.object.save()
		url = reverse('accounting_finance:accounts_plan_all')
		return HttpResponseRedirect('%s?accounting_period=%s' % (url, self.accounting_period.pk))

	def form_invalid(self, form):
   		print('form_invalid')
   		print(form)
   		context = RequestContext(self.request, {'form': form, 'accounting_period': self.accounting_period})
   		return render_to_response(self.template_name, context) 		
   		

class AccountsPlanUpdate(UpdateView):
	model = AccountsPlan
	success_url = reverse_lazy('accounting_finance:accounts_plan_all')
	template_name = 'accounts_plan_form.html'
	form_class = AccountsPlanForm

	def get_object(self):
		print('>>> get_object')
		#return Product.objects.get(id=self.kwargs.get('id'))
		#return = Product.objects.get(id=self.kwargs['id'])
		self.object = get_object_or_404(AccountsPlan, pk=self.request.GET.get('accounts_plan')) 
		return self.object

	def dispatch(self, *args, **kwargs):
		print('>>> dispatch')
		if self.request.method == 'GET':
			accounting_period_id = self.request.GET['accounting_period']
			self.accounting_period = get_object_or_404(AccountingPeriod, pk=accounting_period_id)
		if self.request.method == 'POST':
			accounting_period_id = self.request.GET['accounting_period']
			self.accounting_period = get_object_or_404(AccountingPeriod, pk=accounting_period_id)
		return super(AccountsPlanUpdate, self).dispatch(*args, **kwargs)

	def get_context_data(self, **kwargs):
		print('>>> get_context_data')
		context = super(AccountsPlanUpdate, self).get_context_data(**kwargs)
		context['accounting_period'] = self.accounting_period
		return context

	def post(self, request, *args, **kwargs):
		print('>>> post')
		self.object = self.get_object()
		print(self.object)
		form = AccountsPlanForm(instance=self.object, data=self.request.POST)
		if form.is_valid():
			#self.object = form.save()
			#return HttpResponseRedirect(self.get_success_url())
			print('form_valid')
			self.object = form.save(commit=False)
			self.object.accounting_period = self.accounting_period
			self.object.save()
			url = reverse('accounting_finance:accounts_plan_all')
			return HttpResponseRedirect('%s?accounting_period=%s' % (url, self.accounting_period.pk))
		else:
			#return self.render_to_response(self.get_context_data(form=form))
			print('form_invalid')
			print(form)
			context = RequestContext(self.request, {'form': form, 'accounting_period': self.accounting_period})
			return render_to_response(self.template_name, context)

class AccountsPlanDelete(DeleteView):
	model = AccountsPlan
	success_url = reverse_lazy('accounting_finance:accounts_plan_all')
	template_name = 'accounts_plan_delete.html'

def catalog_uc_delete(request, pk):
    print('intro catalog_uc_delete')
    #perform_eval = PerformanceEvaluation.objects.get(pk=pk)
    cata_univ_comp = CatalogUniversalCompetence.objects.get(pk=pk)
    perform_eval = cata_univ_comp.performance_evaluation
    if perform_eval.state == 'GENERATED':
        cata_univ_comp.delete()
        messages.success(request, 'La competencia universal ha sido eliminada correctamente.')
    else:
        messages.error(request, 'La competencia universal no se puedo eliminar, debido a que la evaluación de desempeño se encuentra iniciada o finalizada.')
    url = reverse('evaluation:catalog_uc_all', kwargs={'perform_eval_pk':perform_eval.pk})

    return HttpResponseRedirect(url)

def accounts_plan_delete(request, pk):
	print('>>> intro accounts_plan_delete')
	accounts_plan = AccountsPlan.objects.get(pk=pk)
	accounting_period = accounts_plan.accounting_period
	accounts_plan.delete();
	messages.success(request, 'El plan de cuentas seleccionado ha sido borrado correctamente.')
	url = reverse('accounting_finance:accounts_plan_all')
	return HttpResponseRedirect('%s?accounting_period=%s' % (url, accounting_period.id))

class AccountingAccountList(ListView):
    model = AccountingAccount
    template_name = 'accounting_account_list.html'
    paginate_by = 10

    def dispatch(self, *args, **kwargs):
    	accounts_plan_id = self.request.GET['accounts_plan']
    	self.accounts_plan = get_object_or_404(AccountsPlan, pk=accounts_plan_id)
    	self.accounting_account_list = AccountingAccount.objects.filter(accounts_plan__pk=accounts_plan_id)
    	print(type(self.accounting_account_list))
    	print(len(self.accounting_account_list))
    	return super(AccountingAccountList, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AccountingAccountList, self).get_context_data(**kwargs)
        paginator = Paginator(self.accounting_account_list, self.paginate_by)
        page = self.request.GET.get('page')
        try:
            paginator_c_t_c = paginator.page(page)
        except PageNotAnInteger:
            paginator_c_t_c = paginator.page(1)
        except EmptyPage:
            paginator_c_t_c = paginator.page(paginator.num_pages)
        context['object_list'] = paginator_c_t_c
        context['accounts_plan'] = self.accounts_plan
        return context

class AccountingAccountDetail(DetailView):
    model = AccountingAccount
    template_name = 'accounting_account_detail.html'

    def dispatch(self, *args, **kwargs):
    	accounts_plan_id = self.request.GET['accounts_plan']
    	self.accounts_plan = get_object_or_404(AccountsPlan, pk=accounts_plan_id)
    	return super(AccountingAccountDetail, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AccountingAccountDetail, self).get_context_data(**kwargs)
        context['accounts_plan'] = self.accounts_plan
        return context

class AccountingAccountCreation(CreateView):
    model = AccountingAccount
    success_url = reverse_lazy('accounting_finance:accounting_account_all')
    template_name = 'accounting_account_form.html'
    form_class = AccountingAccountForm

    def dispatch(self, *args, **kwargs):
    	print('intro >>> dispatch')
    	if self.request.method == 'GET':
    		accounts_plan_id = self.request.GET['accounts_plan']
    		self.accounts_plan = get_object_or_404(AccountsPlan, pk=accounts_plan_id)
    	if self.request.method == 'POST':
    		accounts_plan_id = self.request.POST['accounts_plan']
    		self.accounts_plan = get_object_or_404(AccountsPlan, pk=accounts_plan_id)
    	return super(AccountingAccountCreation, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        print('intro >>> get_context_data')
        context = super(AccountingAccountCreation, self).get_context_data(**kwargs)
        context['accounts_plan'] = self.accounts_plan
        return context

    def post(self, request, *args, **kwargs):
    	print(' intro >>> post')
    	form = AccountingAccountForm(data=self.request.POST)
    	if form.is_valid():
    		self.object = form.save(commit=False)
    		self.object.accounts_plan = self.accounts_plan
    		self.object.save()
    		url = reverse('accounting_finance:accounting_account_tree')
    		return HttpResponseRedirect('%s?accounts_plan=%s' % (url, self.accounts_plan.pk))
    	else:
    		context = RequestContext(self.request, {'form': form, 'accounts_plan': self.accounts_plan})
    		return render_to_response(self.template_name, context)

class AccountingAccountUpdate(UpdateView):
    model = AccountingAccount
    success_url = reverse_lazy('accounting_finance:accounting_account_all')
    template_name = 'accounting_account_form.html'
    form_class = AccountingAccountForm

    def get_object(self):
    	print('intro >>> get_object')
    	self.object = get_object_or_404(AccountingAccount, pk=self.request.GET.get('accounting_account'))
    	return self.object

    def dispatch(self, *args, **kwargs):
    	print('intro >>> dispatch')
    	if self.request.method =='GET':
    		accounts_plan_id = self.request.GET['accounts_plan']
    		self.accounts_plan = get_object_or_404(AccountsPlan, pk=accounts_plan_id)
    	if self.request.method=='POST':
    		accounts_plan_id = self.request.POST['accounts_plan']
    		self.accounts_plan = get_object_or_404(AccountsPlan, pk=accounts_plan_id)
    	return super(AccountingAccountUpdate, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
    	print('intro >>> get_context_data')
    	context = super(AccountingAccountUpdate, self).get_context_data(**kwargs)
    	context['accounts_plan'] = self.accounts_plan
    	return context

    def post(self, request, *args, **kwargs):
    	print('intro >>> post')
    	self.object = self.get_object()
    	form = AccountingAccountForm(instance=self.object, data=self.request.POST)
    	if form.is_valid():
    		self.object = form.save(commit=False)
    		self.object.accounts_plan = self.accounts_plan
    		self.object.save()
    		url = reverse('accounting_finance:accounting_account_tree')
    		return HttpResponseRedirect('%s?accounts_plan=%s' % (url, self.accounts_plan.pk))
    	else:
    		context = RequestContext(self.request, {'form': form, 'accounts_plan': self.accounts_plan})
    		return render_to_response(self.template_name, context)

class CatalogTCDelete(DeleteView):
    model = AccountingAccount
    success_url = reverse_lazy('evaluation:catalog_tc_all')
    template_name = 'performance_evaluat'

def aa_list_json(request):
	print('intro >>> aa_list_json')
	accounting_account_list = AccountsPlan.objects.none()
	if request.method == 'GET':
		accounts_plan_id = int(request.GET['accounts_plan'])
		accounts_plan = AccountsPlan.objects.get(pk=accounts_plan_id)
		accounting_account_list = accounts_plan.accountingaccount_set.all()
	tree = generate_tree(accounts_plan_id)
	return HttpResponse(json.dumps(tree), content_type='application/json')

def generate_tree(accounts_plan_id):
	print('intro >>> generate_tree')
	root = []
	list_account_parent = AccountingAccount.objects.filter(
    		Q(accounts_plan__pk=accounts_plan_id) & Q(level = 1)
		)
	for account in list_account_parent:
		node = {}	
		node['text'] = '%s - %s' % (account.code, account.name)
		node['href'] = '#%s' % (account.pk)
		check_child(account, node)
		root.append(node)
	return root

def check_child(account, node):
	print('intro >>> check_child')
	list_child = AccountingAccount.objects.filter(parent__pk=account.pk)
	list_aux = []
	if not(not list_child):
		for account_aux in list_child:
			node_aux = {}	
			node_aux['text'] = '%s - %s' % (account_aux.code, account_aux.name)
			node_aux['href'] = '#%s' % (account_aux.pk)
			check_child(account_aux, node_aux)
			list_aux.append(node_aux)
		node['nodes'] = list_aux

def accounting_account_tree(request):
	print('intro >>> accounting_account_tree')
	if request.method == 'GET':
		accounts_plan_id = request.GET['accounts_plan']
		accounts_plan = AccountsPlan.objects.get(pk=accounts_plan_id)
		return render(request, 'accounting_account_tree.html', {'accounts_plan': accounts_plan})

def aa_object_json(request):
	print('intro >>> aa_object_json')
	obj = None
	if request.method == 'GET':
		accounting_account_id = int(request.GET['accounting_account'])
		obj = AccountingAccount.objects.get(pk=accounting_account_id)
	print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
	form = AccountingAccountForm(data=request.POST)
	form.is_valid()
	print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
	for boundfield in form: print(boundfield)
	print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
	print(type(form.errors))
	for a in form.errors:
		print(type(a))
		print(a)
	print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
	print(dir(form))
	print('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')
	data = serializers.serialize('json', [obj], use_natural_foreign_keys=True)
	print(data)
	struct = json.loads(data)
	data = json.dumps(struct[0])
	return HttpResponse(data, content_type='application/json')





	



