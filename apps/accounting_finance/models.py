from django.db import models
from apps.institution.models import Institucion
from datetime import date, datetime

# Create your models here.

STATES_ACTIVE_INACTIVE_CHOICES = (
    ('ACTIVE', 'Activo'),
    ('INACTIVE', 'Inactivo'),
)

STATES_YES_NOT_CHOICES = (
    ('YES', 'Sí'),
    ('NOT', 'No'),
)


class AccountingPeriod(models.Model):  # Periodo contable
    code = models.CharField(verbose_name='Código', max_length=100, unique=True)  # Codigo del periodo contable
    year = models.CharField(verbose_name='Año (Nombre)', max_length=4)  # Anio o nombre del periodo contable
    start_date = models.DateField(verbose_name='Fecha de inicio',
                                  default=date.today)  # Fecha de inicio del periodo contable
    end_date = models.DateField(verbose_name='Fecha de fin', default=date.today)  # Fecha de fin del perido contable
    state = models.CharField(verbose_name='Estado', max_length=10,
                             choices=STATES_ACTIVE_INACTIVE_CHOICES,
                             default='ACTIVE')  # Estado de la período contable
    observation = models.TextField(verbose_name='Observación')  # Observacion referente al periodo contable
    institution = models.ForeignKey(Institucion, on_delete=models.CASCADE,
                                    verbose_name='Institución')  # Referencia a la institución a la que pertenece

    class Meta:
        verbose_name = "Período contable"
        verbose_name_plural = "Períodos contables"

    def __str__(self):
        return self.code


class AccountsPlan(models.Model):  # Plan de cuentas
    code = models.CharField(verbose_name='Código', max_length=100, unique=True)  # Codigo del plan de cuentas
    name = models.CharField(verbose_name='Nombre', max_length=250)  # Nambre del plan de cuentas
    state = models.CharField(verbose_name='Estado', max_length=10,
                             choices=STATES_ACTIVE_INACTIVE_CHOICES,
                             default='ACTIVE')  # Estado del plan de cuentas
    observation = models.TextField(verbose_name='Observación')  # Observacion referente la plan de cuentas
    accounting_period = models.ForeignKey(AccountingPeriod, on_delete=models.CASCADE,
                                          verbose_name='Período contable')  # Referencia al periodo contable al que pertence el plan de cuentyas

    class Meta:
        verbose_name = "Plan de cuentas"
        verbose_name_plural = "Plan de cuentas"

    def __str__(self):
        return self.code


class AccountingAccount(models.Model):  # Cuenta contable
    code = models.CharField(verbose_name='Código', max_length=100, unique=True)  # Codigo de la cuenta contable
    name = models.CharField(verbose_name='Nombre', max_length=250)  # Nombre de la cuenta contable
    type = models.CharField(verbose_name='Tipo', max_length=50)  # Tipo de la cuenta contable
    level = models.IntegerField(verbose_name='Nivel', default=1)  # Nivel de la cuenta, indica el numero de padres
    state = models.CharField(verbose_name='Estado', max_length=10,
                             choices=STATES_ACTIVE_INACTIVE_CHOICES,
                             default='ACTIVE')  # Estado de la cuenta contable
    include_jornalization = models.CharField(verbose_name='Incluir en jornalización', max_length=3,
                                             choices=STATES_YES_NOT_CHOICES,
                                             default='NOT')  # Si esta incluida en la jornalizacion
    print = models.CharField(verbose_name='Imprimir', max_length=3,
                             choices=STATES_YES_NOT_CHOICES, default='NOT')  # Opcion si se imprime en la jornalizacion
    order_print = models.IntegerField(
        verbose_name='Orden de impresión')  # Orden de impresion dentro de la jornalizacion
    movement = models.CharField(verbose_name='Cuenta de movimiento', max_length=3,
                                choices=STATES_YES_NOT_CHOICES,
                                default='NOT')  # Induca si la cuenta es de movimiento o no
    name_parent = None
    accounts_plan = models.ForeignKey(AccountsPlan, on_delete=models.CASCADE,
                                      verbose_name='Plan de cuentas')  # Plan de cuentas al que pertenece
    parent = models.ForeignKey('self', models.SET_NULL, blank=True, null=True,
                               verbose_name='Cuenta contable padre')  # Cuenta contable padre

    class Meta:
        verbose_name = "Cuenta contable"
        verbose_name_plural = "Cuentas contables"
        unique_together = (('code', 'name'),)

    def __str__(self):
        return '%s - %s' % (self.code, self.name)

    def natural_key(self):
        # return (self.code, self.name)
        return ('%s - %s' % (self.code, self.name))


class BudgetClassifier(models.Model):  # Clasificador presupuestario
    code = models.CharField(verbose_name='Código', max_length=100, unique=True)  # Codigo del clasificador presuúestario
    name = models.CharField(verbose_name='Nombre', max_length=250)  # Nambre del clasificador presupuestario
    state = models.CharField(verbose_name='Estado', max_length=10,
                             choices=STATES_ACTIVE_INACTIVE_CHOICES,
                             default='ACTIVE')  # Estado del clasificador presupuestario
    observation = models.TextField(verbose_name='Observación')  # Observacion referente al clasificador presupuestario
    accounting_period = models.ForeignKey(AccountingPeriod, on_delete=models.CASCADE,
                                          verbose_name='Período contable')  # Referencia al periodo contable al que pertence el plan de cuentas

    class Meta:
        verbose_name = "Clasificador presupuestario"
        verbose_name_plural = "Clasificador presupuestario"

    def __str__(self):
        pass


class BudgetItem(models.Model):  # Partida presupuestaria
    code = models.CharField(verbose_name='Código', max_length=100, unique=True)  # Codigo de la partida presupuestaria
    name = models.CharField(verbose_name='Nombre', max_length=250)  # Nombre de la partida presupuestaria
    type = models.CharField(verbose_name='Tipo', max_length=50)  # Tipo de partida presupuestaria
    state = models.CharField(verbose_name='Estado', max_length=10,
                             choices=STATES_ACTIVE_INACTIVE_CHOICES,
                             default='ACTIVE')  # Estado de la partida presupuestaria
    print = models.CharField(verbose_name='Imprimir', max_length=3,
                             choices=STATES_YES_NOT_CHOICES, default='YES')  # Opcion si se imprime
    order_print = models.IntegerField(
        verbose_name='Orden de impresión')  # Orden de impresion de la partida presuúestaria
    budget_classifier = models.ForeignKey(BudgetClassifier, on_delete=models.CASCADE,
                                          verbose_name='Clasificador preupuestario')  # Referencia al clasificador presupuestario al que pertenece

    class Meta:
        verbose_name = "Partida presupuestaria",
        verbose_name_plural = "Partidas presupestarias"

    def __str__(self):
        pass
