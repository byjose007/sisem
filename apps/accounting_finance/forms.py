from django import forms
from django.forms import ModelForm
from .models import (AccountingPeriod, AccountsPlan, AccountingAccount)

class AccountinPeriodForm(ModelForm):
    start_date = forms.DateField(label='Birth Date', required=True,  
        widget=forms.DateTimeInput(format='%d/%m/%Y', attrs={
            'placeholder':'Select a date'
        })
    )
    # end_date = forms.DateField(widget=forms.DateInput(format = '%d/%m/%Y', attrs={
    #         'placeholder':'Select a date'
    #     }), input_formats=('%d/%m/%Y'),
    #     required=False)

    class Meta:
       model = AccountingPeriod
       fields = '__all__'
       exclude=[]

    def __init__(self, *args, **kwargs):
        super(AccountingPeriod, self).__init__(*args, **kwargs)
        #self.fields['end_date'].widget = forms.DateInput(format=('%d/%m/%Y'), attrs={'placeholder':'Select a date'})

class AccountsPlanForm(ModelForm):
    class Meta:
       model = AccountsPlan
       fields = '__all__'
       exclude=['accounting_period']

class AccountingAccountForm(ModelForm):
    #parent = forms.CharField(label='Cuenta padre')
    field_order = ['parent', 'code', 'name', 'type', 'level', 'state', 'include_jornalizacion', 'print', 'order_print', 'movement']
    class Meta:
        model = AccountingAccount
        fields = '__all__'
        exclude=['accounts_plan']
        
