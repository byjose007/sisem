from django.http import QueryDict
 
class HttpPostTunnelingMiddleware(object):
    def process_request(self, request):
        print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
        print(request.META)
        print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>')
        if 'HTTP_X_HTTP_METHOD_OVERRIDE' in request.META:
            print('intro HTTP_X_HTTP_METHOD_OVERRIDE')
            http_method = request.META['HTTP_X_HTTP_METHOD_OVERRIDE']
            print(http_method)
            if http_method.lower() == 'put':
                request.method = 'PUT'
                request.META['REQUEST_METHOD'] = 'PUT'
                request.PUT = QueryDict(request.body)
            if http_method.lower() == 'delete':
                print('intro delete')
                request.method = 'DELETE'
                request.META['REQUEST_METHOD'] = 'DELETE'
                request.DELETE = QueryDict(request.body)
        return None