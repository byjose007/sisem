from django.contrib import admin
from apps.accounting_finance.models import (AccountingPeriod)
# Register your models here.

admin.site.register(AccountingPeriod)