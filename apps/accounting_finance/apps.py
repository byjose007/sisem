from django.apps import AppConfig


class AccountingFinanceConfig(AppConfig):
    name = 'apps.accounting_finance'
