# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from apps.institution.models import Directivo
from apps.employee.models import Employee


# Create your models here.
class HistoryDirectorInstitutionalUnit(models.Model):
    institutional_unit = models.ForeignKey(Directivo, verbose_name='Direccion', on_delete=models.PROTECT)
    employee = models.ForeignKey(Employee, verbose_name='Empleado', on_delete=models.PROTECT)
    observation = models.TextField(verbose_name="Observación", null=True, blank=True)
    date_start = models.DateField(max_length=100, verbose_name='Desde')
    date_end = models.DateField(max_length=100, verbose_name='Hasta', blank=True, null=True)
    status = models.BooleanField(verbose_name="Estado", default=True)

    class Meta:
        ordering = ['institutional_unit', 'date_start', 'employee']
        verbose_name = 'Historial director de Unidad Institucional'
        verbose_name_plural = 'Historial de Directores de Unidad Institucional'

    def __str__(self):
        return u'{0}-{1}'.format(self.institutional_unit, self.employee)

    def __unicode__(self):
        return u'{0}-{1}-{2}'.format(self.institutional_unit, self.employee, self.status)
