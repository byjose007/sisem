from django.contrib import admin

# Register your models here.
from django.contrib import admin
from apps.directorinstitutionalunit.models import HistoryDirectorInstitutionalUnit





# Register your models here.

class HistoryDirectorInstitutionalUnitAdmin(admin.ModelAdmin):
	list_display = ('institutional_unit','employee','date_start','status')
	search_fields = ('institutional_unit','employee','date_start','status')




admin.site.register(HistoryDirectorInstitutionalUnit, HistoryDirectorInstitutionalUnitAdmin)

