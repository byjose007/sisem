from django.apps import AppConfig


class DirectorinstitutionalunitConfig(AppConfig):
    name = 'apps.directorinstitutionalunit'
