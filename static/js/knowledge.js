$("#knowledge").click(function(ev) { // for each edit knowledge url
        console.log('intro knowledge');
        ev.preventDefault(); // prevent navigation
        var url = $(this).data("form"); // get the knowledge form url
        console.log('url >>> ' + url);
        $("#knowledgeModal").load(url, function() { // load the url into the modal
            $(this).modal('show'); // display the modal on url load
        });
        return false; // prevent the click propagation
    });

$('#knowledge-form').on('submit', function() {
    console.log('intro submit');
    $.ajax({ 
        type: $(this).attr('method'), 
        url: this.action, 
        data: $(this).serialize(),
        context: this,
        success: function(data, status) {
            $('#knowledgeModal').html(data);
        }
    });
    return false;
});

$("a[id^=knowledge-]").click(function(ev) { // for each edit knowledge url
    console.log('intro del knowledge');
    ev.preventDefault(); // prevent navigation
    var k_id = $(this).attr('id').split('-')[1];
    var url = $(this).data("form");
    console.log('>>> ' + k_id + ' >>> ' + personal_eval_id + ' >>> ' + url);
    BootstrapDialog.show({
        type: BootstrapDialog.TYPE_SUCCESS, 
        title: 'Confirmar',
        message: 'Está seguro que desea eliminar el registro',
        closable: false,
        buttons: [ {
            label: 'Aceptar',
            action: function(dialog){
                $.ajax({ 
                    type: "DELETE", // http method
                    url: url, // get the knowledge form url 
                    data : {k_id: k_id}, // data sent with the delete request
                    context: this,
                    success: function(data, status) {
                        console.log("deletion successful");
                        console.log('>>>');
                        console.log(data);
                        dialog.close();
                        window.location="/evaluation/personal_evaluation/evaluate/" + personal_eval_id + "/";
                    },
                    //error: function(XMLHttpRequest, textStatus, errorThrown) {
                    error: function(data, status) {
                        console.log("deletion error");
                        console.log(data.status);
                        console.log('>>>');
                        console.log(data.responseText);
                      }
                });
                
                
            }
        }, {
            label: 'Cerrar',
            action: function(dialog){
                dialog.close();
            }
        }]
    });
    return false; // prevent the click propagation
});
