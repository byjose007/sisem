(function() {
    'use strict';
     angular
    .module('app')
    .controller('EstudiantesController', EstudiantesController);
 
    function EstudiantesController($scope) { 

    	var s = $scope;

    	 // Variables
	    s.estudiante =  {
		    nombre: "Oscar",
		    apellido: "Diáz",
		    asignaturas : [
		    	{nombre : 'Física', calificacion : 7.0},
		      	{nombre : 'Biología', calificacion : 8.0},
		      	{nombre : 'Matemáticas', calificacion : 2.0}
		    ],    
		    nombreCompleto: function() {
		      var estudianteObj;
		      estudianteObj = s.estudiante;
		      return estudianteObj.nombre + " " + estudianteObj.apellido;
		    }
		};

		s.nombre = "Javier";

	  	 // Funcionesestudiante

		s.reset = function(){
  			s.nombre = "";
  		};

	  	function nombre_ompleto() {
	        /* ... */
	    };
    }
})();
