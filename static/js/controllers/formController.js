(function() {
    'use strict';
     angular
    .module('app')
    .controller('FormController', FormController);
 
    function FormController($scope) { 

    	var s = $scope;

    	 // Variables
		
    	s.nombre = "Javier";

	  	 // Funciones

	  	s.reset = function(){
		 	s.nombre = "";
		}
    };

})();
