var app = angular.module('app.survey',['ngRoute', 'ngCookies'], function($interpolateProvider){
    // Contorna prroblema de interpolação da renderização de template do django
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
  })
  .run( function run($http, $cookies ){
    // Evita problemas relacionados ao CSRF
    $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
  })
  .config(function($routeProvider){
  $routeProvider.
  when('/add',{
    templateUrl: 'add.htm', controller: 'addController'
  }).
  when('/create',{
    templateUrl: 'create.htm', controller: 'createController'
  }).
  when('/update',{
    templateUrl: 'update.htm', controller: 'updateController'
  }).
  when('/acerca', {
    templateUrl: '/static/pages/acerca.html', // this location
    controller  : 'aboutController'
  }).
  otherwise({
    redirectTo : '/add'
  })
});

app.controller('addController',function($scope){
  $scope.name = "Add View";
})

app.controller('createController',function($scope){
  $scope.name = "Create View";
})

app.controller('updateController',function($scope){
  $scope.name = "Update View";
})

app.controller('aboutController',function($scope){
  $scope.message = "Acerca View";
})