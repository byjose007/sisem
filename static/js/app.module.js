
var app = angular.module('app', ['ui.bootstrap','ngCookies', 'ngRoute']);
/**
* configuración de nuestra aplicación
* @param $routeProvider
*/
app.config(['$routeProvider', function($routeProvider)
{
    $routeProvider.when("institution/", {
        templateUrl: "templates/home.html",
        controller: "homeController"
    })
    .otherwise({ redirectTo : "/"});
}]);

/**
* @description Controlador home
* @param $scope
* @param $modal
*/
app.controller('homeController', ['$scope','$modal',function($scope,$modal)
{	
	/**
	* abre la modal
	*/
	$scope.openModal = function (size) 
	{
	    var modalInstance = $modal.open({
		    templateUrl: 'myModal.html',
		    controller: 'myModalController',
		    size: size,
		    resolve: {
		    	Items: function() //scope del modal
		        {
		          	return "Hola que asé";
		        }
		    }
	    });
	}
}]);

/**
* @description Controlador para la modal
* @param $scope
* @param $modalInstance
* @param Items
*/
app.controller('myModalController', ['$scope','$modalInstance','Items', function($scope, $modalInstance,Items)
{
    $scope.items = Items;

	$scope.save = function (param) 
	{
		console.log(param)
	};

    $scope.cancel = function () 
    {
    	$modalInstance.dismiss('cancel');
    };
}]);